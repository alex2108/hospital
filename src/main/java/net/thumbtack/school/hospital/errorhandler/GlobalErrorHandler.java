package net.thumbtack.school.hospital.errorhandler;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.validator.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestCookieException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.*;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ControllerAdvice
public class GlobalErrorHandler {

    private final Map<String, ValidationErrorCode> validationFieldErrors = Stream.of(new Object[][]{
            {NameValidation.class.getSimpleName(), ValidationErrorCode.NAME_INVALID},
            {PatronymicValidation.class.getSimpleName(), ValidationErrorCode.PATRONYMIC_INVALID},
            {LoginValidation.class.getSimpleName(), ValidationErrorCode.LOGIN_INVALID},
            {PasswordValidation.class.getSimpleName(), ValidationErrorCode.PASSWORD_INVALID},
            {NotNull.class.getSimpleName(), ValidationErrorCode.EMPTY_NULL},
            {NotEmpty.class.getSimpleName(), ValidationErrorCode.EMPTY_NULL},
            {NotBlank.class.getSimpleName(), ValidationErrorCode.EMPTY_NULL},
            {Email.class.getSimpleName(), ValidationErrorCode.EMAIL_INVALID},
            {PhoneValidation.class.getSimpleName(), ValidationErrorCode.PHONE_INVALID},
            {Positive.class.getSimpleName(), ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED},
            {DateValidation.class.getSimpleName(), ValidationErrorCode.DATE_INVALID},
            {TimeValidation.class.getSimpleName(), ValidationErrorCode.TIME_FORMAT_INVALID},
            {WeekDayValidation.class.getSimpleName(), ValidationErrorCode.WEEK_DAY_FORMAT_INVALID},
            {Pattern.class.getSimpleName(), ValidationErrorCode.FORMAT_INVALID}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (ValidationErrorCode) data[1]));

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseError handleValidation(MethodArgumentNotValidException ex) {
        final ResponseError error = new ResponseError();
        ex.getBindingResult().getFieldErrors().forEach(
                field -> {
                    ValidationErrorCode errorCode = validationFieldErrors.get(field.getCode());
                    if (errorCode == null)
                        errorCode = ValidationErrorCode.GENERAL_VALIDATION_ERROR;
                    error.add(errorCode.name(), field.getField(), errorCode.getErrorCode());
                }
        );
        ex.getBindingResult().getGlobalErrors().forEach(
                global -> {
                    String code = global.getCode();
                    ValidationErrorCode errorCode = ValidationErrorCode.GENERAL_VALIDATION_ERROR;
                    String field = "validation";
                    if (code.equals(ScheduleValidation.class.getSimpleName())) {
                        errorCode = ValidationErrorCode.valueOf(global.getDefaultMessage());
                        field = "schedule";
                    }
                    if (code.equals(AppointmentValidation.class.getSimpleName())) {
                        errorCode = ValidationErrorCode.valueOf(global.getDefaultMessage());
                        field = "doctor";
                    }
                    error.add(errorCode.name(), field, errorCode.getErrorCode());
                }
        );
        return error;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseError handlePathParamValidation(ConstraintViolationException ex) {
        final ResponseError error = new ResponseError();
        ex.getConstraintViolations().forEach(constraint -> {
            String propertyPath = constraint.getPropertyPath().toString();
            String field = propertyPath.substring(propertyPath.lastIndexOf(".") + 1);
            String validationClassName = constraint.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName();
            ValidationErrorCode errorCode = validationFieldErrors.get(validationClassName);
            if (errorCode == null)
                errorCode = ValidationErrorCode.GENERAL_VALIDATION_ERROR;
            error.add(errorCode.name(), field, errorCode.getErrorCode());
        });
        return error;
    }

    @ExceptionHandler(MissingRequestCookieException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseError handleCookie(MissingRequestCookieException ex) {
        final ResponseError error = new ResponseError();
        error.add(ValidationErrorCode.COOKIE_MISSING.name(), ex.getCookieName(), ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        return error;
    }

    @ExceptionHandler(DataBaseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseError handleDataBaseException(DataBaseException ex) {
        final ResponseError error = new ResponseError();
        error.add(ex.getErrorCodeName(), ex.getField(), ex.getMessage());
        return error;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseError requestHandlingNoHandlerFound(NoHandlerFoundException ex) {
        final ResponseError error = new ResponseError();
        error.add(ValidationErrorCode.URL_INVALID.name(), "URL",
                ValidationErrorCode.URL_INVALID.getErrorCode() + ex.getHttpMethod() + " " + ex.getRequestURL());
        return error;
    }

}

