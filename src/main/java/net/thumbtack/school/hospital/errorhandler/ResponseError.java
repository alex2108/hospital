package net.thumbtack.school.hospital.errorhandler;

import java.util.ArrayList;
import java.util.List;

public class ResponseError {
    private List<ErrorEntity> errors = new ArrayList<>();

    public void add(String errorCode, String field, String message) {
        errors.add(new ErrorEntity(errorCode, field, message));
    }

    public List<ErrorEntity> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorEntity> errors) {
        this.errors = errors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResponseError error = (ResponseError) o;

        return (errors.containsAll(error.errors) && error.errors.containsAll(errors));
    }

    @Override
    public int hashCode() {
        return errors.hashCode();
    }
}

class ErrorEntity {
    private String errorCode;
    private String field;
    private String message;

    public ErrorEntity() {
    }

    public ErrorEntity(String errorCode, String field, String message) {
        this.errorCode = errorCode;
        this.field = field;
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorEntity that = (ErrorEntity) o;

        if (!errorCode.equals(that.errorCode)) return false;
        if (!field.equals(that.field)) return false;
        return message.equals(that.message);

    }

    @Override
    public int hashCode() {
        int result = errorCode.hashCode();
        result = 31 * result + field.hashCode();
        result = 31 * result + message.hashCode();
        return result;
    }
}