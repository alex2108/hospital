package net.thumbtack.school.hospital.model;

import java.time.LocalDate;
import java.util.List;

public class Schedule {

    private int id;
    private Doctor doctor;
    private LocalDate date;
    private List<SlotSchedule> slotSchedules;

    public Schedule() {
    }

    public Schedule(int id, Doctor doctor, LocalDate date, List<SlotSchedule> slotSchedules) {
        this.id = id;
        this.doctor = doctor;
        this.date = date;
        this.slotSchedules = slotSchedules;
    }

    public Schedule(Doctor doctor, LocalDate date, List<SlotSchedule> slotSchedules) {
        this(0, doctor, date, slotSchedules);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<SlotSchedule> getSlotSchedules() {
        return slotSchedules;
    }

    public void setSlotSchedules(List<SlotSchedule> slotSchedules) {
        this.slotSchedules = slotSchedules;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "doctor=" + doctor +
                ", date=" + date +
                ", timeSlots=" + slotSchedules +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Schedule)) return false;

        Schedule schedule = (Schedule) o;

        if (getId() != schedule.getId()) return false;
        if (!getDoctor().equals(schedule.getDoctor())) return false;
        if (!getDate().equals(schedule.getDate())) return false;
        return getSlotSchedules().equals(schedule.getSlotSchedules());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getDoctor().hashCode();
        result = 31 * result + getDate().hashCode();
        result = 31 * result + getSlotSchedules().hashCode();
        return result;
    }
}
