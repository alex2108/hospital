package net.thumbtack.school.hospital.model;

public class Patient extends User {

    private String email;
    private String address;
    private String phone;

    public Patient() {
    }

    public Patient(String firstName, String lastName, String patronymic,
                   String login, String password,
                   String email, String address, String phone) {
        this(0, firstName, lastName, patronymic, login, password, email, address, phone);
    }

    public Patient(int id,
                   String firstName, String lastName, String patronymic,
                   String login, String password,
                   String email, String address, String phone) {
        super(id, firstName, lastName, patronymic, login, password);
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", patronymic='" + getPatronymic() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Patient patient = (Patient) o;

        if (!email.equals(patient.email)) return false;
        if (!address.equals(patient.address)) return false;
        return phone.equals(patient.phone);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + phone.hashCode();
        return result;
    }
}
