package net.thumbtack.school.hospital.model;

import java.sql.Time;
import java.time.LocalTime;

public class SlotSchedule {

    private int id;
    private Ticket ticket;
    private LocalTime time;
    private int duration;
    private SlotScheduleState state;

    public SlotSchedule() {
    }

    public SlotSchedule(int id, Ticket ticket, LocalTime time, int duration, SlotScheduleState state) {
        this.id = id;
        this.ticket = ticket;
        this.time = time;
        this.duration = duration;
        this.state = state;
    }

    public SlotSchedule(LocalTime time, int duration, SlotScheduleState state) {
        this(0, null, time, duration, state);
    }

    public SlotSchedule(int id, Time time, int duration, String stateString) {
        this(id, null, time.toLocalTime(), duration, SlotScheduleState.valueOf(stateString));
    }

    public SlotSchedule(int id, LocalTime time, int duration) {
        this(id, null, time, duration, SlotScheduleState.VACANT);
    }

    public SlotSchedule(Ticket ticket, LocalTime time, int duration) {
        this(0, ticket, time, duration, SlotScheduleState.VACANT);
    }

    public SlotSchedule(LocalTime time, int duration) {
        this(null, time, duration);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public SlotScheduleState getState() {
        return state;
    }

    public void setState(SlotScheduleState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "TimeSlot{" +
                "time=" + time +
                ", duration=" + duration +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SlotSchedule)) return false;

        SlotSchedule that = (SlotSchedule) o;

        if (getId() != that.getId()) return false;
        if (getDuration() != that.getDuration()) return false;
        if (getTicket() != null ? !getTicket().equals(that.getTicket()) : that.getTicket() != null) return false;
        if (!getTime().equals(that.getTime())) return false;
        return getState() == that.getState();

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getTicket() != null ? getTicket().hashCode() : 0);
        result = 31 * result + getTime().hashCode();
        result = 31 * result + getDuration();
        result = 31 * result + getState().hashCode();
        return result;
    }
}
