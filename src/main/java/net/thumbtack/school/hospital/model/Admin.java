package net.thumbtack.school.hospital.model;

public class Admin extends User {

    private String position;

    public Admin() {
    }

    public Admin(String firstName,
                 String lastName,
                 String patronymic,
                 String login,
                 String password,
                 String position) {
        this(0, firstName, lastName, patronymic, login, password, position);
    }

    public Admin(int id,
                 String firstName,
                 String lastName,
                 String patronymic,
                 String login,
                 String password,
                 String position) {
        super(id, firstName, lastName, patronymic, login, password);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", patronymic='" + getPatronymic() + '\'' +
                ", position='" + position + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Admin admin = (Admin) o;

        return position.equals(admin.position);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + position.hashCode();
        return result;
    }
}
