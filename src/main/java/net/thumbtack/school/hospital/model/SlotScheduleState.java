package net.thumbtack.school.hospital.model;

public enum SlotScheduleState {
    VACANT, BUSY;
}
