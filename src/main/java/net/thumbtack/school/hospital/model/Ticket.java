package net.thumbtack.school.hospital.model;

public class Ticket {

    private int id;
    private Patient patient;
    private String number;

    public Ticket() {
    }

    public Ticket(int id, Patient patient, String number) {
        this.id = id;
        this.patient = patient;
        this.number = number;
    }

    public Ticket(Patient patient, String number) {
        this(0, patient, number);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "patient=" + patient +
                ", number='" + number + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ticket ticket = (Ticket) o;

        if (id != ticket.id) return false;
        if (!patient.equals(ticket.patient)) return false;
        return number.equals(ticket.number);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + patient.hashCode();
        result = 31 * result + number.hashCode();
        return result;
    }
}
