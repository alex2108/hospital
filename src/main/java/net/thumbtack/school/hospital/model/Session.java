package net.thumbtack.school.hospital.model;

public class Session {

    private int id;
    private User user;
    private String javaSessionId;

    public Session() {
    }

    public Session(int id, User user, String javaSessionId) {
        this.id = id;
        this.user = user;
        this.javaSessionId = javaSessionId;
    }

    public Session(User user, String javaSessionId) {
        this(0, user, javaSessionId);
    }

    public Session(User user) {
        this(0, user, null);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getJavaSessionId() {
        return javaSessionId;
    }

    public void setJavaSessionId(String javaSessionId) {
        this.javaSessionId = javaSessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;

        Session session = (Session) o;

        if (getId() != session.getId()) return false;
        if (!getUser().equals(session.getUser())) return false;
        return getJavaSessionId() != null ? getJavaSessionId().equals(session.getJavaSessionId()) : session.getJavaSessionId() == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getUser().hashCode();
        result = 31 * result + (getJavaSessionId() != null ? getJavaSessionId().hashCode() : 0);
        return result;
    }
}
