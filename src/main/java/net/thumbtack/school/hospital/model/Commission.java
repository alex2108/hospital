package net.thumbtack.school.hospital.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Commission {

    private int id;
    private List<Doctor> doctors;
    private Ticket ticket;
    private LocalDate date;
    private LocalTime time;
    private String room;
    private int duration;

    public Commission() {
    }

    public Commission(int id,
                      List<Doctor> doctors,
                      Ticket ticket,
                      LocalDate date,
                      LocalTime time,
                      String room,
                      int duration) {
        this.id = id;
        this.doctors = doctors;
        this.ticket = ticket;
        this.date = date;
        this.time = time;
        this.room = room;
        this.duration = duration;
    }

    public Commission(List<Doctor> doctors,
                      Ticket ticket,
                      LocalDate date,
                      LocalTime time,
                      String room,
                      int duration) {
        this(0, doctors, ticket, date, time, room, duration);
    }

    public Commission(Ticket ticket,
                      LocalDate date,
                      LocalTime time,
                      String room,
                      int duration) {
        this(new ArrayList<>(), ticket, date, time, room, duration);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Commission{" +
                "doctors=" + doctors +
                ", date=" + date +
                ", time=" + time +
                ", room='" + room + '\'' +
                ", duration=" + duration +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Commission)) return false;

        Commission commission = (Commission) o;

        if (getId() != commission.getId()) return false;
        if (getDuration() != commission.getDuration()) return false;
        if (!getDoctors().equals(commission.getDoctors())) return false;
        if (!getTicket().equals(commission.getTicket())) return false;
        if (!getDate().equals(commission.getDate())) return false;
        if (!getTime().equals(commission.getTime())) return false;
        return getRoom().equals(commission.getRoom());

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getDoctors().hashCode();
        result = 31 * result + getTicket().hashCode();
        result = 31 * result + getDate().hashCode();
        result = 31 * result + getTime().hashCode();
        result = 31 * result + getRoom().hashCode();
        result = 31 * result + getDuration();
        return result;
    }
}
