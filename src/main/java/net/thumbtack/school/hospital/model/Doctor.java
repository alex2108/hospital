package net.thumbtack.school.hospital.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Doctor extends User {

    private String speciality;
    private String room;
    private List<Schedule> schedules;
    private LocalDate firedDate;

    public Doctor() {
        schedules = new ArrayList<>();
    }

    public Doctor(String firstName, String lastName, String patronymic,
                  String login, String password,
                  String speciality, String room) {
        this(firstName, lastName, patronymic, login, password, speciality, room, new ArrayList<>());
    }

    public Doctor(String firstName, String lastName, String patronymic,
                  String login, String password,
                  String speciality, String room, List<Schedule> schedules) {
        this(0, firstName, lastName, patronymic, login, password, speciality, room, schedules);
    }

    public Doctor(int id,
                  String firstName, String lastName, String patronymic,
                  String login, String password,
                  String speciality, String room) {
        this(id, firstName, lastName, patronymic, login, password, speciality, room, new ArrayList<>());
    }

    public Doctor(int id,
                  String firstName, String lastName, String patronymic,
                  String login, String password,
                  String speciality, String room, List<Schedule> schedules) {
        super(id, firstName, lastName, patronymic, login, password);
        this.speciality = speciality;
        this.room = room;
        this.schedules = schedules;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public LocalDate getFiredDate() {
        return firedDate;
    }

    public void setFiredDate(LocalDate firedDate) {
        this.firedDate = firedDate;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", patronymic='" + getPatronymic() + '\'' +
                ", speciality='" + speciality + '\'' +
                ", room='" + room + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        if (!super.equals(o)) return false;

        Doctor doctor = (Doctor) o;

        if (!getSpeciality().equals(doctor.getSpeciality())) return false;
        if (!getRoom().equals(doctor.getRoom())) return false;
        if (!getSchedules().equals(doctor.getSchedules())) return false;
        return getFiredDate() != null ? getFiredDate().equals(doctor.getFiredDate()) : doctor.getFiredDate() == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getSpeciality().hashCode();
        result = 31 * result + getRoom().hashCode();
        result = 31 * result + getSchedules().hashCode();
        result = 31 * result + (getFiredDate() != null ? getFiredDate().hashCode() : 0);
        return result;
    }
}
