package net.thumbtack.school.hospital;

import net.thumbtack.school.hospital.utils.MyBatisUtils;
import net.thumbtack.school.hospital.utils.PropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(HospitalServer.class);

    public static void main(String args[]) {
        LOGGER.info("Start application");
        if (!MyBatisUtils.initSqlSessionFactory()) {
            LOGGER.error("Can't create connection to database. Application is stopped");
            throw new RuntimeException("Can't create connection to database, stop");
        }
        if (!PropertiesUtils.initProperties()) {
            LOGGER.error("Can't read configuration file. Application is stopped");
            throw new RuntimeException("Can't read configuration file, stop");
        }
        SpringApplication.run(HospitalServer.class);
        LOGGER.info("Application is running");
    }
}
