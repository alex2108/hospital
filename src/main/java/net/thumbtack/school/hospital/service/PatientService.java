package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.dto.RegPatientDtoRequest;
import net.thumbtack.school.hospital.dto.PatientDtoResponse;
import net.thumbtack.school.hospital.dto.UpdatePatientDtoRequest;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PatientService {
    private PatientDao patientDao;
    private SessionDao sessionDao;

    @Autowired
    public PatientService(PatientDao patientDao, SessionDao sessionDao) {
        this.patientDao = patientDao;
        this.sessionDao = sessionDao;
    }

    public PatientDtoResponse register(RegPatientDtoRequest regPatientReq) throws DataBaseException {
        regPatientReq.setPhone(regPatientReq.getPhone().replaceAll("-", ""));
        Patient patientDB = patientDao.insert(DtoMapper.INSTANCE.DtoReqToPatient(regPatientReq));
        Session session = new Session(patientDB, UUID.randomUUID().toString());
        sessionDao.insert(session);
        PatientDtoResponse response = DtoMapper.INSTANCE.patientToDtoResp(patientDB);
        response.setJavaSessionId(session.getJavaSessionId());
        return response;
    }

    public PatientDtoResponse update(UpdatePatientDtoRequest updateReq, String javaSessionId) throws DataBaseException {
        updateReq.setPhone(updateReq.getPhone().replaceAll("-", ""));
        Patient patient = (Patient) sessionDao.getByCookie(javaSessionId).getUser();
        if (patientDao.get(patient.getLogin(), updateReq.getOldPassword()) == null)
            throw new DataBaseException(DataBaseErrorCode.PASSWORD_WRONG);

        Patient newPatient = DtoMapper.INSTANCE.DtoUpdateToPatient(updateReq);
        newPatient.setId(patient.getId());
        return DtoMapper.INSTANCE.patientToDtoResp(patientDao.update(newPatient));
    }

    public PatientDtoResponse getPatient(int patientId) throws DataBaseException {
        Patient patient = patientDao.getById(patientId);
        if (patient == null)
            throw new DataBaseException(DataBaseErrorCode.PATIENT_ID_NOT_EXIST);
        return DtoMapper.INSTANCE.patientToDtoResp(patient);
    }
}
