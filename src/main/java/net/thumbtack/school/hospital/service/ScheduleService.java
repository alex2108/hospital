package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.CommissionDao;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.dao.ScheduleDao;
import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.dto.AppointDtoRequest;
import net.thumbtack.school.hospital.dto.AppointDtoResponse;
import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

import static net.thumbtack.school.hospital.service.CommonService.*;

@Service
public class ScheduleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleService.class);
    private DoctorDao doctorDao;
    private ScheduleDao scheduleDao;
    private CommissionDao commissionDao;
    private SessionDao sessionDao;

    @Autowired
    public ScheduleService(DoctorDao doctorDao, ScheduleDao scheduleDao, CommissionDao commissionDao, SessionDao sessionDao) {
        this.scheduleDao = scheduleDao;
        this.doctorDao = doctorDao;
        this.commissionDao = commissionDao;
        this.sessionDao = sessionDao;
    }

    public AppointDtoResponse insertAppointment(AppointDtoRequest appointReq, String javaSessionId) throws DataBaseException {
        String speciality = appointReq.getSpeciality();
        if (speciality != null && !doctorDao.isSpecialityExist(speciality))
            throw new DataBaseException(DataBaseErrorCode.SPECIALITY_INVALID);

        Patient patient = (Patient) sessionDao.getByCookie(javaSessionId).getUser();
        Integer doctorId = appointReq.getDoctorId();
        LocalDateTime dateTimeAppoint =
                LocalDateTime.parse(appointReq.getDate() + " " + appointReq.getTime(), formatterDateTime);
        Doctor doctorDB;
        if (doctorId == null) {
            doctorDB = chooseRandomDoctor(speciality, dateTimeAppoint);
        } else {
            doctorDB = doctorDao.getById(doctorId);
            if (doctorDB == null)
                throw new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST);
        }

        SlotSchedule slotAppoint = getSlotAppoint(doctorDB, dateTimeAppoint);
        checkAnotherAppointOnDate(patient, doctorDB, dateTimeAppoint, slotAppoint.getDuration());

        String ticketNumber = "D" + doctorDB.getId() + dateTimeAppoint.format(formatterTicket);
        Ticket ticket = new Ticket(patient, ticketNumber);
        slotAppoint.setTicket(ticket);
        slotAppoint.setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotAppoint);
        LOGGER.info("Appointment made successfully. {}, {}, {}", ticket, doctorDB, dateTimeAppoint.format(formatterDateTime));

        AppointDtoResponse appointResp = DtoMapper.INSTANCE.doctorToDtoAppointResp(doctorDB);
        appointResp.setTicket(slotAppoint.getTicket().getNumber());
        appointResp.setDate(dateTimeAppoint.toLocalDate().format(formatterDate));
        appointResp.setTime(dateTimeAppoint.toLocalTime().format(formatterTime));
        return appointResp;
    }

    private void checkAnotherAppointOnDate(Patient patient, Doctor doctor,
                                             LocalDateTime dateTimeAppoint, int durationAppoint) throws DataBaseException {
        List<SlotSchedule> slots = scheduleDao.getByPatient(patient, dateTimeAppoint.toLocalDate());
        for (SlotSchedule slot : slots) {
            if (isAppointmentsIntersect(dateTimeAppoint.toLocalTime(), durationAppoint, slot.getTime(), slot.getDuration()))
                throw new DataBaseException(DataBaseErrorCode.PATIENT_TIME_BUSY);
        }
        List<Commission> commissions = commissionDao.getByPatient(patient, dateTimeAppoint.toLocalDate());
        for (Commission commission : commissions) {
            if (isAppointmentsIntersect(dateTimeAppoint.toLocalTime(), durationAppoint, commission.getTime(), commission.getDuration()))
                throw new DataBaseException(DataBaseErrorCode.PATIENT_TIME_BUSY);

        }
        for (Schedule schedule : doctor.getSchedules()) {
            if (schedule.getDate().equals(dateTimeAppoint.toLocalDate())) {
                for (SlotSchedule slot : schedule.getSlotSchedules()) {
                    Ticket ticket = slot.getTicket();
                    if (ticket != null && ticket.getPatient().equals(patient)) {
                        throw new DataBaseException(DataBaseErrorCode.PATIENT_APPOINT_EXIST);
                    }
                }
            }
        }
    }

    private SlotSchedule getSlotAppoint(Doctor doctor, LocalDateTime dateTimeAppoint) throws DataBaseException {
        for (Schedule schedule : doctor.getSchedules()) {
            if (schedule.getDate().equals(dateTimeAppoint.toLocalDate())) {
                for (SlotSchedule slot : schedule.getSlotSchedules()) {
                    if (slot.getTime().equals(dateTimeAppoint.toLocalTime())) {
                        return slot;
                    }
                }
            }
        }
        throw new DataBaseException(DataBaseErrorCode.SLOT_TIME_FOR_DOCTOR_NOT_EXIST);
    }

    private Doctor chooseRandomDoctor(String speciality, LocalDateTime dateTime) throws DataBaseException {
        List<Doctor> doctors = doctorDao.getVacantBySpeciality(speciality, dateTime);
        if (doctors.isEmpty())
            throw new DataBaseException(DataBaseErrorCode.AVAILABLE_DOCTORS_NOT_EXIST);
        Random rand = new Random();
        return doctors.get(rand.nextInt(doctors.size()));
    }
}
