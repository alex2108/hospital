package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.CommissionDao;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.dao.ScheduleDao;
import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.dto.StatisticsDoctorDtoResponse;
import net.thumbtack.school.hospital.dto.StatisticsPatientDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static net.thumbtack.school.hospital.service.CommonService.formatterTime;

@Service
public class StatisticsService {
    private DoctorDao doctorDao;
    private PatientDao patientDao;
    private ScheduleDao scheduleDao;
    private CommissionDao commissionDao;

    @Autowired
    public StatisticsService(DoctorDao doctorDao, PatientDao patientDao, ScheduleDao scheduleDao, CommissionDao commissionDao) {
        this.doctorDao = doctorDao;
        this.patientDao = patientDao;
        this.scheduleDao = scheduleDao;
        this.commissionDao = commissionDao;
    }

    public StatisticsPatientDtoResponse getPatientStatistics(int patientId, String startDateStr, String endDateStr,
                                                             boolean isDetails, boolean isByDays) throws DataBaseException {
        Patient patient = patientDao.getById(patientId);
        if (patient == null)
            throw new DataBaseException(DataBaseErrorCode.PATIENT_ID_NOT_EXIST);

        StatisticsPatientDtoResponse response = DtoMapper.INSTANCE.patientToDtoStatistics(patient);
        LocalDate startDate = LocalDate.now();
        if (startDateStr != null)
            startDate = LocalDate.parse(startDateStr, formatterDate);
        LocalDate endDate = LocalDate.now().plusMonths(2);
        if (endDateStr != null)
            endDate = LocalDate.parse(endDateStr, formatterDate);
        List<Schedule> schedules = scheduleDao.getByPatient(patient, startDate, endDate);
        List<Commission> commissions = commissionDao.getByPatient(patient, startDate, endDate);
        if (schedules.isEmpty() && commissions.isEmpty())
            return response;

        if (isByDays) {
            List<StatisticsPatientDtoResponse.StatisticsOnDay> statByDays = new ArrayList<>();
            for (LocalDate date = startDate; date.isBefore(endDate.plusDays(1)); date = date.plusDays(1)) {
                StatisticsPatientDtoResponse.StatisticsOnDay statOnDay = getPatientStatisticsOnDay(schedules, isDetails, commissions, date);
                if (statOnDay != null) {
                    statByDays.add(statOnDay);
                }
            }
            response.setByDays(statByDays);
        } else {
            response.setStartDate(startDate.format(formatterDate));
            response.setEndDate(endDate.format(formatterDate));
            int durationAppoints = schedules.stream()
                    .mapToInt(schedule -> schedule.getSlotSchedules().get(0).getDuration())
                    .reduce(0, Integer::sum);
            int durationCommissions = commissions.stream()
                    .mapToInt(Commission::getDuration)
                    .reduce(0, Integer::sum);
            if (isDetails) {
                if (durationAppoints != 0) {
                    StatisticsPatientDtoResponse.Statistics stat = new StatisticsPatientDtoResponse.Statistics();
                    stat.setCount(schedules.size());
                    stat.setDuration(LocalTime.of(durationAppoints / 60, durationAppoints % 60).format(formatterTime));
                    response.addPropStatistics("appointment", stat);
                }
                if (durationCommissions != 0) {
                    StatisticsPatientDtoResponse.Statistics stat = new StatisticsPatientDtoResponse.Statistics();
                    stat.setCount(commissions.size());
                    stat.setDuration(LocalTime.of(durationCommissions / 60, durationCommissions % 60).format(formatterTime));
                    response.addPropStatistics("commission", stat);
                }
            } else {
                StatisticsPatientDtoResponse.Statistics stat = new StatisticsPatientDtoResponse.Statistics();
                stat.setCount(schedules.size() + commissions.size());
                int durationTotal = durationAppoints + durationCommissions;
                stat.setDuration(LocalTime.of(durationTotal / 60, durationTotal % 60).format(formatterTime));
                response.addPropStatistics("total", stat);
            }
        }
        return response;
    }

    private static StatisticsPatientDtoResponse.StatisticsOnDay getPatientStatisticsOnDay(List<Schedule> schedules,
                                                                                          boolean isDetails,
                                                                                          List<Commission> commissions,
                                                                                          LocalDate date) {
        StatisticsPatientDtoResponse.StatisticsOnDay statOnDay = new StatisticsPatientDtoResponse.StatisticsOnDay();
        int durationAppoints = 0;
        int durationCommissions = 0;
        List<Schedule> schedulesOnDay = schedules.stream()
                .filter(schedule -> schedule.getDate().equals(date))
                .collect(Collectors.toList());
        if (!schedulesOnDay.isEmpty()) {
            durationAppoints = schedulesOnDay.stream()
                    .mapToInt(schedule -> schedule.getSlotSchedules().get(0).getDuration())
                    .reduce(0, Integer::sum);
        }

        List<Commission> commissionOnDay = commissions.stream()
                .filter(commission -> commission.getDate().equals(date))
                .collect(Collectors.toList());
        if (!commissionOnDay.isEmpty()) {
            durationCommissions = commissionOnDay.stream()
                    .mapToInt(Commission::getDuration)
                    .reduce(0, Integer::sum);
        }
        if (commissionOnDay.isEmpty() && schedulesOnDay.isEmpty()) {
            return null;
        } else {
            statOnDay.setDate(date.format(formatterDate));
        }
        if (isDetails) {
            if (!schedulesOnDay.isEmpty()) {
                StatisticsPatientDtoResponse.Statistics stat = new StatisticsPatientDtoResponse.Statistics();
                stat.setCount(schedulesOnDay.size());
                stat.setDuration(LocalTime.of(durationAppoints / 60, durationAppoints % 60).format(formatterTime));
                statOnDay.addPropStatistics("appointment", stat);
            }
            if (!commissionOnDay.isEmpty()) {
                StatisticsPatientDtoResponse.Statistics stat = new StatisticsPatientDtoResponse.Statistics();
                stat.setCount(commissionOnDay.size());
                stat.setDuration(LocalTime.of(durationCommissions / 60, durationCommissions % 60).format(formatterTime));
                statOnDay.addPropStatistics("commission", stat);
            }
        } else {
            StatisticsPatientDtoResponse.Statistics stat = new StatisticsPatientDtoResponse.Statistics();
            stat.setCount(schedulesOnDay.size() + commissionOnDay.size());
            int durationTotal = durationAppoints + durationCommissions;
            stat.setDuration(LocalTime.of(durationTotal / 60, durationTotal % 60).format(formatterTime));
            statOnDay.addPropStatistics("total", stat);
        }
        return statOnDay;
    }

    public StatisticsDoctorDtoResponse getDoctorStatistics(int doctorId, String startDateStr, String endDateStr,
                                                           boolean isDetails, boolean isByDays) throws DataBaseException {
        Doctor doctor = doctorDao.getById(doctorId);
        if (doctor == null)
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST);

        StatisticsDoctorDtoResponse response = DtoMapper.INSTANCE.doctorToDtoStatistics(doctor);
        LocalDate startDate;
        if (startDateStr == null) {
            startDate = LocalDate.now();
        } else {
            startDate = LocalDate.parse(startDateStr, formatterDate);
        }
        LocalDate endDate;
        if (endDateStr == null) {
            endDate = LocalDate.now().plusMonths(2);
        } else {
            endDate = LocalDate.parse(endDateStr, formatterDate);
        }
        List<Schedule> schedules = doctor.getSchedules().stream()
                .filter(schedule -> (!schedule.getDate().isBefore(startDate) && !schedule.getDate().isAfter(endDate)))
                .collect(Collectors.toList());
        List<Commission> commissions = commissionDao.getByDoctor(doctor, startDate, endDate);

        return getDoctorResponse(response, schedules, commissions, startDate, endDate, isDetails, isByDays);
    }

    public StatisticsDoctorDtoResponse getAllDoctorsStatistics(String startDateStr, String endDateStr,
                                                               boolean isDetails, boolean isByDays) throws DataBaseException {
        StatisticsDoctorDtoResponse response = new StatisticsDoctorDtoResponse();
        LocalDate startDate;
        if (startDateStr == null) {
            startDate = LocalDate.now();
        } else {
            startDate = LocalDate.parse(startDateStr, formatterDate);
        }
        LocalDate endDate;
        if (endDateStr == null) {
            endDate = LocalDate.now().plusMonths(2);
        } else {
            endDate = LocalDate.parse(endDateStr, formatterDate);
        }
        List<Schedule> schedules = scheduleDao.getByInterval(startDate, endDate);
        List<Commission> commissions = commissionDao.getByInterval(startDate, endDate);

        return getDoctorResponse(response, schedules, commissions, startDate, endDate, isDetails, isByDays);
    }

    private static StatisticsDoctorDtoResponse getDoctorResponse(StatisticsDoctorDtoResponse response,
                                                                 List<Schedule> schedules,
                                                                 List<Commission> commissions,
                                                                 LocalDate startDate,
                                                                 LocalDate endDate,
                                                                 boolean isDetails,
                                                                 boolean isByDays) {
        if (schedules.isEmpty() && commissions.isEmpty())
            return response;

        if (isByDays) {
            List<StatisticsDoctorDtoResponse.StatisticsOnDay> statByDays = new ArrayList<>();
            for (LocalDate date = startDate; date.isBefore(endDate.plusDays(1)); date = date.plusDays(1)) {
                StatisticsDoctorDtoResponse.StatisticsOnDay statOnDay = getDoctorStatisticsOnDay(schedules, isDetails, commissions, date);
                if (statOnDay != null) {
                    statByDays.add(statOnDay);
                }
            }
            response.setByDays(statByDays);
        } else {
            response.setStartDate(startDate.format(formatterDate));
            response.setEndDate(endDate.format(formatterDate));
            int countAppointment = 0;
            int timeBySchedule = 0;
            int timeAppoint = 0;
            for (Schedule schedule : schedules) {
                for (SlotSchedule slot : schedule.getSlotSchedules()) {
                    if (slot.getTicket() != null) {
                        countAppointment++;
                        timeAppoint += slot.getDuration();
                    }
                    timeBySchedule += slot.getDuration();
                }
            }
            int timeCommission = commissions.stream().mapToInt(Commission::getDuration).sum();
            int countCommission = commissions.size();
            if (isDetails) {
                if (timeBySchedule != 0) {
                    response.addPropStatistics("appointment",
                            new StatisticsDoctorDtoResponse.Statistics(countAppointment,
                                    LocalTime.of(timeBySchedule / 60, timeBySchedule % 60).format(formatterTime),
                                    LocalTime.of(timeAppoint / 60, timeAppoint % 60).format(formatterTime)));
                }
                if (countCommission != 0) {
                    response.addPropStatistics("commission",
                            new StatisticsDoctorDtoResponse.Statistics(countCommission,
                                    LocalTime.of(timeCommission / 60, timeCommission % 60).format(formatterTime)));
                }
            } else {
                response.addPropStatistics("total",
                        new StatisticsDoctorDtoResponse.Statistics(countAppointment + countCommission,
                                LocalTime.of(timeBySchedule / 60, timeBySchedule % 60).format(formatterTime),
                                LocalTime.of((timeAppoint + timeCommission) / 60, (timeAppoint + timeCommission) % 60).format(formatterTime)));
            }
        }
        return response;
    }

    private static StatisticsDoctorDtoResponse.StatisticsOnDay getDoctorStatisticsOnDay(List<Schedule> schedules,
                                                                                        boolean isDetails,
                                                                                        List<Commission> commissions,
                                                                                        LocalDate date) {
        StatisticsDoctorDtoResponse.StatisticsOnDay statOnDay = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        int countAppointment = 0;
        int timeBySchedule = 0;
        int timeAppoint = 0;
        for (Schedule schedule : schedules) {
            if (schedule.getDate().equals(date)) {
                for (SlotSchedule slot : schedule.getSlotSchedules()) {
                    if (slot.getTicket() != null) {
                        countAppointment++;
                        timeAppoint += slot.getDuration();
                    }
                    timeBySchedule += slot.getDuration();
                }
            }
        }
        int countCommission = (int) commissions.stream()
                .filter(commission -> commission.getDate().equals(date))
                .count();
        int timeCommission = commissions.stream()
                .filter(commission -> commission.getDate().equals(date))
                .mapToInt(Commission::getDuration).sum();

        if (timeBySchedule == 0 && countCommission == 0) {
            return null;
        } else {
            statOnDay.setDate(date.format(formatterDate));
        }

        if (isDetails) {
            if (timeBySchedule != 0) {
                statOnDay.addPropStatistics("appointment",
                        new StatisticsDoctorDtoResponse.Statistics(countAppointment,
                                LocalTime.of(timeBySchedule / 60, timeBySchedule % 60).format(formatterTime),
                                LocalTime.of(timeAppoint / 60, timeAppoint % 60).format(formatterTime)));
            }
            if (countCommission != 0) {
                statOnDay.addPropStatistics("commission",
                        new StatisticsDoctorDtoResponse.Statistics(countCommission,
                                LocalTime.of(timeCommission / 60, timeCommission % 60).format(formatterTime)));
            }
        } else {
            statOnDay.addPropStatistics("total",
                    new StatisticsDoctorDtoResponse.Statistics(countAppointment + countCommission,
                            LocalTime.of(timeBySchedule / 60, timeBySchedule % 60).format(formatterTime),
                            LocalTime.of((timeAppoint + timeCommission) / 60, (timeAppoint + timeCommission) % 60).format(formatterTime)));
        }
        return statOnDay;
    }

}
