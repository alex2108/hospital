package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.*;
import net.thumbtack.school.hospital.dto.TicketDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static net.thumbtack.school.hospital.service.CommonService.getSlotsOnCommission;

@Service
public class TicketService {
    private ScheduleDao scheduleDao;
    private CommissionDao commissionDao;
    private TicketDao ticketDao;
    private SessionDao sessionDao;

    @Autowired
    public TicketService(ScheduleDao scheduleDao,
                         CommissionDao commissionDao,
                         TicketDao ticketDao,
                         SessionDao sessionDao) {
        this.scheduleDao = scheduleDao;
        this.commissionDao = commissionDao;
        this.ticketDao = ticketDao;
        this.sessionDao = sessionDao;
    }

    public List<TicketDtoResponse> getTickets(String javaSessionId) throws DataBaseException {
        Patient patient = (Patient) sessionDao.getByCookie(javaSessionId).getUser();
        return CommonService.ScheduleCommissionsToDtoTicket(
                scheduleDao.getByPatient(patient), commissionDao.getByPatient(patient));
    }

    public void deleteTicketAppointment(String ticketNumber, String javaSessionId) throws DataBaseException {
        User user = sessionDao.getByCookie(javaSessionId).getUser();

        if (user instanceof Doctor) {
            Doctor doctor = (Doctor) user;
            if (doctor.getSchedules().stream()
                    .map(Schedule::getSlotSchedules)
                    .noneMatch(slots -> slots.stream()
                            .anyMatch(slot -> slot.getTicket().getNumber().equals(ticketNumber)))) {
                throw new DataBaseException(DataBaseErrorCode.DOCTOR_HAS_NOT_TICKET);
            }
        }

        Ticket ticket = ticketDao.getAppointmentTicketByNumber(ticketNumber);
        if (user instanceof Patient) {
            if (ticket == null || !ticket.getPatient().equals(user))
                throw new DataBaseException(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET);
        }

        if (ticket == null)
            throw new DataBaseException(DataBaseErrorCode.TICKET_APPOINT_NUMBER_NOT_EXIST);
        ticketDao.deleteAppointment(ticket);
    }

    public void deleteTicketCommission(String ticketNumber, String javaSessionId) throws DataBaseException {
        Patient patient = (Patient) sessionDao.getByCookie(javaSessionId).getUser();
        Ticket ticket = ticketDao.getCommissionTicketByNumber(ticketNumber);
        if (ticket == null || !ticket.getPatient().equals(patient)) {
            throw new DataBaseException(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET);
        }
        Commission commission = commissionDao.getByTicket(ticket);
        List<SlotSchedule> slotsToVacant = getSlotsOnCommission(commission.getDoctors(),
                LocalDateTime.of(commission.getDate(), commission.getTime()), commission.getDuration());
        if (slotsToVacant.isEmpty()) {
            ticketDao.delete(ticket);
        } else {
            ticketDao.deleteCommission(ticket, slotsToVacant);
        }
    }
}
