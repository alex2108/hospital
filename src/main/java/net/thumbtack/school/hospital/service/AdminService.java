package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.AdminDao;
import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.dto.AdminDtoResponse;
import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.dto.RegAdminDtoRequest;
import net.thumbtack.school.hospital.dto.UpdateAdminDtoRequest;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    private AdminDao adminDao;
    private SessionDao sessionDao;

    @Autowired
    public AdminService(AdminDao adminDao, SessionDao sessionDao) {
        this.adminDao = adminDao;
        this.sessionDao = sessionDao;
    }

    public AdminDtoResponse register(RegAdminDtoRequest regAdminReq) throws DataBaseException {
        return DtoMapper.INSTANCE.adminToDtoResp(
                adminDao.insert(DtoMapper.INSTANCE.DtoReqToAdmin(regAdminReq)));
    }

    public AdminDtoResponse update(UpdateAdminDtoRequest updateReq, String javaSessionId) throws DataBaseException {
        Admin admin = (Admin) sessionDao.getByCookie(javaSessionId).getUser();
        if (adminDao.get(admin.getLogin(), updateReq.getOldPassword()) == null)
            throw new DataBaseException(DataBaseErrorCode.PASSWORD_WRONG);

        Admin adminNew = DtoMapper.INSTANCE.DtoUpdateToAdmin(updateReq);
        adminNew.setId(admin.getId());
        return DtoMapper.INSTANCE.adminToDtoResp(adminDao.update(adminNew));
    }


}
