package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.dto.TicketDtoResponse;
import net.thumbtack.school.hospital.dto.doctor.ScheduleDto;
import net.thumbtack.school.hospital.dto.doctor.SlotScheduleDto;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommonService {

    public static DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public static DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");
    public static DateTimeFormatter formatterTicket = DateTimeFormatter.ofPattern("ddMMyyyyHHmm");
    public static DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    public static List<ScheduleDto> doctorToScheduleDto(Doctor doctor) {
        return doctor.getSchedules().stream()
                .map(schedule ->
                        new ScheduleDto(formatterDate.format(schedule.getDate()),
                                schedule.getSlotSchedules().stream()
                                        .map(slot -> {
                                            Ticket ticket = slot.getTicket();
                                            if (ticket == null)
                                                return new SlotScheduleDto(formatterTime.format(slot.getTime()));
                                            else
                                                return new SlotScheduleDto(formatterTime.format(slot.getTime()),
                                                        DtoMapper.INSTANCE.patientToDtoPatient(ticket.getPatient()));
                                        }).collect(Collectors.toList())
                        )
                ).collect(Collectors.toList());
    }

    public static List<ScheduleDto> doctorToScheduleDto(Doctor doctor, Patient patient,
                                                        LocalDate startDate, LocalDate endDate) {
        return doctor.getSchedules().stream()
                .filter(schedule -> (!schedule.getDate().isBefore(startDate) && !schedule.getDate().isAfter(endDate)))
                .map(schedule ->
                        new ScheduleDto(formatterDate.format(schedule.getDate()),
                                schedule.getSlotSchedules().stream()
                                        .map(slot -> {
                                            Ticket ticket = slot.getTicket();
                                            if (ticket != null && (patient == null || ticket.getPatient().equals(patient))) {
                                                return new SlotScheduleDto(formatterTime.format(slot.getTime()),
                                                        DtoMapper.INSTANCE.patientToDtoPatient(ticket.getPatient()));
                                            } else {
                                                return new SlotScheduleDto(formatterTime.format(slot.getTime()));
                                            }
                                        }).collect(Collectors.toList())
                        )
                ).collect(Collectors.toList());
    }

    public static List<ScheduleDto> doctorToScheduleDto(Doctor doctor, LocalDate startDate, LocalDate endDate) {
        return doctorToScheduleDto(doctor, null, startDate, endDate);
    }

    public static boolean isAppointmentsIntersect(LocalTime timeStart1, int duration1,
                                                  LocalTime timeStart2, int duration2) {
        LocalTime timeEnd1 = timeStart1.plusMinutes(duration1);
        LocalTime timeEnd2 = timeStart2.plusMinutes(duration2);
        return ((timeStart1.isBefore(timeEnd2) && (timeEnd1.isAfter(timeEnd2) || timeEnd1.equals(timeEnd2))) ||
                ((timeStart1.isBefore(timeStart2) || timeStart1.equals(timeStart2)) && timeEnd1.isAfter(timeStart2)));
    }

    public static List<TicketDtoResponse> ScheduleCommissionsToDtoTicket(List<Schedule> schedules, List<Commission> commissions)
    {
        List<TicketDtoResponse> ticketDtoResponse = new ArrayList<>();
        schedules.forEach(schedule -> {
                SlotSchedule slot = schedule.getSlotSchedules().get(0);
                Doctor doctor = schedule.getDoctor();
                TicketDtoResponse response = new TicketDtoResponse();
                response.setTicket(slot.getTicket().getNumber());
                response.setRoom(schedule.getDoctor().getRoom());
                response.setDate(schedule.getDate().format(formatterDate));
                response.setTime(slot.getTime().format(formatterTime));
                response.setDoctorId(schedule.getDoctor().getId());
                response.setFirstName(doctor.getFirstName());
                response.setLastName(doctor.getLastName());
                response.setPatronymic(doctor.getPatronymic());
                response.setSpeciality(doctor.getSpeciality());
                //response.setDocAppoint(DtoMapper.INSTANCE.doctorToDtoTicketDoctor(schedule.getDoctor()));
                ticketDtoResponse.add(response);
        });
        commissions.forEach(commission -> {
            TicketDtoResponse response = new TicketDtoResponse();
            response.setTicket(commission.getTicket().getNumber());
            response.setRoom(commission.getRoom());
            response.setDate(commission.getDate().format(formatterDate));
            response.setTime(commission.getTime().format(formatterTime));
            response.setDoctorsCommission(DtoMapper.INSTANCE.doctorsToDtoTicketDoctors(commission.getDoctors()));
            ticketDtoResponse.add(response);
        });
        return ticketDtoResponse;
    }

    public static List<SlotSchedule> getSlotsOnCommission(List<Doctor> doctors,
                                                  LocalDateTime dateTimeCommission,
                                                  int durationCommission) throws DataBaseException {
        List<SlotSchedule> slotsCommission = new ArrayList<>();
        doctors.forEach(doctor -> {
            doctor.getSchedules().forEach(schedule -> {
                if (schedule.getDate().equals(dateTimeCommission.toLocalDate())) {
                    schedule.getSlotSchedules().forEach(slot -> {
                        if (isAppointmentsIntersect(slot.getTime(), slot.getDuration(),
                                dateTimeCommission.toLocalTime(), durationCommission)) {
                            slotsCommission.add(slot);
                        }
                    });
                }
            });
        });
        return slotsCommission;
    }

}
