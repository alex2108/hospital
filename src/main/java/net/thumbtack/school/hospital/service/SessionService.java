package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.*;
import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.dto.LoginDtoRequest;
import net.thumbtack.school.hospital.dto.LoginDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class SessionService {
    private static final String cookieName = "JAVASESSIONID";
    private SessionDao sessionDao;
    private AdminDao adminDao;
    private DoctorDao doctorDao;
    private PatientDao patientDao;

    @Autowired
    public SessionService(SessionDao sessionDao, AdminDao adminDao, DoctorDao doctorDao, PatientDao patientDao) {
        this.sessionDao = sessionDao;
        this.adminDao = adminDao;
        this.doctorDao = doctorDao;
        this.patientDao = patientDao;
    }

    public static String getCookieName() {
        return cookieName;
    }

    public LoginDtoResponse insert(LoginDtoRequest loginReq) throws DataBaseException {
        String login = loginReq.getLogin();
        String password = loginReq.getPassword();
        Admin admin = adminDao.get(login, password);
        Doctor doctor = doctorDao.get(login, password);
        Patient patient = patientDao.get(login, password);

        if (admin != null) {
            Session session = sessionDao.insert(new Session(admin, UUID.randomUUID().toString()));
            LoginDtoResponse loginResp = DtoMapper.INSTANCE.adminToDtoLoginResp(admin);
            loginResp.setJavaSessionId(session.getJavaSessionId());
            return loginResp;
        }
        if (doctor != null) {
            Session session = sessionDao.insert(new Session(doctor, UUID.randomUUID().toString()));
            LoginDtoResponse loginResp = DtoMapper.INSTANCE.doctorToDtoLoginResp(doctor);
            loginResp.setSchedule(CommonService.doctorToScheduleDto(doctor));
            loginResp.setJavaSessionId(session.getJavaSessionId());
            return loginResp;
        }
        if (patient != null) {
            Session session = sessionDao.insert(new Session(patient, UUID.randomUUID().toString()));
            LoginDtoResponse loginResp = DtoMapper.INSTANCE.patientToDtoLoginResp(patient);
            loginResp.setJavaSessionId(session.getJavaSessionId());
            return loginResp;
        }
        throw new DataBaseException(DataBaseErrorCode.LOGIN_PASSWORD_NOT_EXIST);
    }

    public void checkAuthorization(String javaSessionId) throws DataBaseException {
        if (sessionDao.getByCookie(javaSessionId) == null)
            throw new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST);
    }

    public void checkAdminAccess(String javaSessionId) throws DataBaseException {
        checkAccess(javaSessionId, Arrays.asList(Admin.class));
    }

    public void checkDoctorAccess(String javaSessionId) throws DataBaseException {
        checkAccess(javaSessionId, Arrays.asList(Doctor.class));
    }

    public void checkPatientAccess(String javaSessionId) throws DataBaseException {
        checkAccess(javaSessionId, Arrays.asList(Patient.class));
    }

    public void checkDoctorAdminAccess(String javaSessionId) throws DataBaseException {
        checkAccess(javaSessionId, Arrays.asList(Doctor.class, Admin.class));
    }

    private void checkAccess(String javaSessionId, List<Class<? extends User>> userClass) throws DataBaseException {
        Session session = sessionDao.getByCookie(javaSessionId);
        if (!userClass.contains(session.getUser().getClass()))
            throw new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED);
    }

    public LoginDtoResponse getUser(String javaSessionId) throws DataBaseException {
        Session session = sessionDao.getByCookie(javaSessionId);
        if (session == null)
            throw new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST);

        User user = session.getUser();
        if (session.getUser() instanceof Admin)
            return DtoMapper.INSTANCE.adminToDtoLoginResp((Admin) user);
        else if (session.getUser() instanceof Doctor) {
            Doctor doctor = (Doctor) user;
            LoginDtoResponse response = DtoMapper.INSTANCE.doctorToDtoLoginResp(doctor);
            response.setSchedule(CommonService.doctorToScheduleDto(doctor));
            return response;
        }
        else
            return DtoMapper.INSTANCE.patientToDtoLoginResp((Patient) user);
    }

    public void delete(String javaSessionId) throws DataBaseException {
        Session session = sessionDao.getByCookie(javaSessionId);
        if (session == null)
            throw new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST);
        sessionDao.delete(session);
    }
}
