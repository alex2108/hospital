package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.*;
import net.thumbtack.school.hospital.dto.DtoMapper;
import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private DoctorDao doctorDao;
    private ScheduleDao scheduleDao;
    private CommissionDao commissionDao;
    private TicketDao ticketDao;
    private SessionDao sessionDao;

    @Autowired
    public DoctorService(DoctorDao doctorDao, ScheduleDao scheduleDao, CommissionDao commissionDao,
                         TicketDao ticketDao, SessionDao sessionDao) {
        this.doctorDao = doctorDao;
        this.scheduleDao = scheduleDao;
        this.commissionDao = commissionDao;
        this.ticketDao = ticketDao;
        this.sessionDao = sessionDao;
    }

    public DoctorDtoResponse register(RegDoctorDtoRequest regDoctorReq) throws DataBaseException {
        if (!doctorDao.isRoomExist(regDoctorReq.getRoom()))
            throw new DataBaseException(DataBaseErrorCode.ROOM_INVALID);
        if (!doctorDao.isSpecialityExist(regDoctorReq.getSpeciality()))
            throw new DataBaseException(DataBaseErrorCode.SPECIALITY_INVALID);

        Doctor doctor = DtoMapper.INSTANCE.DtoReqToDoctor(regDoctorReq);
        doctor.setSchedules(getSchedules(regDoctorReq, doctor));
        doctorDao.insert(doctor);
        DoctorDtoResponse doctorDtoResp = DtoMapper.INSTANCE.doctorToDtoResp(doctor);
        doctorDtoResp.setSchedule(CommonService.doctorToScheduleDto(doctor));
        return doctorDtoResp;
    }

    public DoctorDtoResponse update(UpdateDoctorDtoRequest updateReq, int doctorId) throws DataBaseException {
        Doctor doctor = doctorDao.getById(doctorId);
        if (doctor == null)
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST);
        if (doctor.getFiredDate() != null &&
                !LocalDate.parse(updateReq.getDateEnd(), formatterDate).isBefore(doctor.getFiredDate()))
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_FIRED);

        List<Schedule> schedulesNew = getSchedules(updateReq, doctor);
        List<Schedule> schedulesUpdate = new ArrayList<>();
        for (Schedule oldSchedule : doctor.getSchedules()) {
            for (Schedule newSchedule : schedulesNew) {
                if (oldSchedule.getDate().equals(newSchedule.getDate())) {
                    oldSchedule.setSlotSchedules(newSchedule.getSlotSchedules());
                    schedulesUpdate.add(oldSchedule);
                    schedulesNew.remove(newSchedule);
                    break;
                }
            }
        }
        if (!schedulesUpdate.isEmpty()) {
            scheduleDao.update(schedulesUpdate);
        }
        scheduleDao.insert(schedulesNew);
        DoctorDtoResponse doctorDtoResp = DtoMapper.INSTANCE.doctorToDtoResp(doctor);
        doctorDtoResp.setSchedule(CommonService.doctorToScheduleDto(doctorDao.getById(doctorId)));
        return doctorDtoResp;
    }

    public DoctorDtoResponse getDoctor(int doctorId, String javaSessionId, boolean isSchedule,
                                       String startDateStr, String endDateStr) throws DataBaseException {
        Doctor doctor = doctorDao.getById(doctorId);
        if (doctor == null)
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST);
        return getDoctorInfoResponse(doctor, javaSessionId, isSchedule, startDateStr, endDateStr);
    }

    public List<DoctorDtoResponse> getDoctors(String speciality, String javaSessionId, boolean isSchedule,
                                              String startDateStr, String endDateStr) throws DataBaseException {
        List<Doctor> doctors;
        if (speciality == null) {
            doctors = doctorDao.getAll();
            if (doctors.isEmpty())
                throw new DataBaseException(DataBaseErrorCode.DOCTORS_NOT_EXIST);
        } else {
            if (!doctorDao.isSpecialityExist(speciality))
                throw new DataBaseException(DataBaseErrorCode.SPECIALITY_INVALID);
            doctors = doctorDao.getBySpeciality(speciality);
            if (doctors.isEmpty())
                throw new DataBaseException(DataBaseErrorCode.DOCTOR_SPECIALITY_NOT_EXIST);
        }
        List<DoctorDtoResponse> response = new ArrayList<>();
        for (Doctor doctor : doctors) {
            response.add(getDoctorInfoResponse(doctor, javaSessionId, isSchedule, startDateStr, endDateStr));
        }
        return response;
    }

    public void delete(DeleteDoctorDtoRequest deleteReq, int doctorId) throws DataBaseException {
        Doctor doctor = doctorDao.getById(doctorId);
        if (doctor == null)
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST);
        if (doctor.getFiredDate() != null)
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_ALREADY_FIRED);

        LocalDate date = LocalDate.parse(deleteReq.getDate(), formatterDate);
        doctor.setFiredDate(date);
        List<Ticket> ticketsDeleted = doctorDao.delete(doctor);
        for (Ticket ticket: ticketsDeleted) {
            LOGGER.info("Appointment is canceled. {}, {}", ticket, doctor);
        }
    }

    public static void checkIsAnyDoctorFired(Collection<Doctor> doctors, LocalDate date) throws DataBaseException {
        if (doctors.stream().anyMatch(doctor -> (doctor.getFiredDate() != null && !date.isBefore(doctor.getFiredDate()))))
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_FIRED);
    }

    private DoctorDtoResponse getDoctorInfoResponse(Doctor doctor, String javaSessionId, boolean isSchedule,
                                                    String startDateStr, String endDateStr) throws DataBaseException {
        DoctorDtoResponse doctorResp = DtoMapper.INSTANCE.doctorToDtoResp(doctor);
        if (!isSchedule) {
            return doctorResp;
        }

        LocalDate startDate = LocalDate.now();
        if (startDateStr != null)
            startDate = LocalDate.parse(startDateStr, formatterDate);
        LocalDate endDate = LocalDate.now().plusMonths(2);
        if (endDateStr != null)
            endDate = LocalDate.parse(endDateStr, formatterDate);

        User user = sessionDao.getByCookie(javaSessionId).getUser();
        if (user instanceof Patient) {
            doctorResp.setSchedule(CommonService.doctorToScheduleDto(doctor, (Patient) user, startDate, endDate));
        } else {
            doctorResp.setSchedule(CommonService.doctorToScheduleDto(doctor, startDate, endDate));
        }
        return doctorResp;
    }


    private List<Schedule> getSchedules(UpdateDoctorDtoRequest doctorReq, Doctor doctor) {
        List<Schedule> daysSchedule = new ArrayList<>();
        LocalDate dateStart = LocalDate.parse(doctorReq.getDateStart(), formatterDate);
        LocalDate dateEnd = LocalDate.parse(doctorReq.getDateEnd(), formatterDate);

        Map<String, List<SlotSchedule>> weekDaysSlots =
                generateWeekDaysSlots(doctorReq.getWeekSchedule(),
                        doctorReq.getWeekDaysSchedule(),
                        doctorReq.getDuration());
        for (LocalDate date = dateStart; date.isBefore(dateEnd.plusDays(1)); date = date.plusDays(1)) {
            List<SlotSchedule> slots =
                    weekDaysSlots.get(date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.ROOT));
            if (slots != null) {
                daysSchedule.add(new Schedule(doctor, date, slots));
            }
        }
        return daysSchedule;
    }

    private Map<String, List<SlotSchedule>> generateWeekDaysSlots(WeekSchedule weekSchedule,
                                                                  List<DaySchedule> weekDaysSchedule,
                                                                  int duration) {
        Map<String, List<SlotSchedule>> weekDaySlotsSchedule = new HashMap<>();
        DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");

        if (weekSchedule != null) {
            List<SlotSchedule> slotsSchedule = generateSlots(
                    LocalTime.parse(weekSchedule.getTimeStart(), formatterTime),
                    LocalTime.parse(weekSchedule.getTimeEnd(), formatterTime),
                    duration);
            if (weekSchedule.getWeekDays() == null || weekSchedule.getWeekDays().isEmpty()) {
                Arrays.asList(DayOfWeek.MONDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                        DayOfWeek.TUESDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                        DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                        DayOfWeek.THURSDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                        DayOfWeek.FRIDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT))
                        .forEach((entity) -> weekDaySlotsSchedule.put(entity, slotsSchedule));
            } else {
                weekSchedule.getWeekDays().forEach(
                        (entity) -> weekDaySlotsSchedule.put(entity, slotsSchedule));
            }
        }
        if (weekDaysSchedule != null) {
            weekDaysSchedule.forEach(
                    (entity) -> weekDaySlotsSchedule.put(
                            entity.getWeekDay(),
                            generateSlots(LocalTime.parse(entity.getTimeStart(), formatterTime),
                                    LocalTime.parse(entity.getTimeEnd(), formatterTime),
                                    duration)
                    )
            );
        }
        return weekDaySlotsSchedule;
    }

    private List<SlotSchedule> generateSlots(LocalTime timeStart, LocalTime timeEnd, int duration) {
        List<SlotSchedule> slotsSchedule = new ArrayList<>();
        for (LocalTime time = timeStart; time.isBefore(timeEnd); time = time.plusMinutes(duration))
            slotsSchedule.add(new SlotSchedule(time, duration));
        return slotsSchedule;
    }
}
