package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.dto.SettingsDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.User;
import net.thumbtack.school.hospital.utils.PropertiesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SettingsService {
    private SessionDao sessionDao;
    private final List<String> defaultSettings = Arrays.asList("max_name_length", "min_password_length");
    private final List<String> adminSettings = Arrays.asList("max_name_length", "min_password_length");
    private final List<String> doctorSettings = Arrays.asList("max_name_length", "min_password_length");
    private final List<String> patientSettings = Arrays.asList("max_name_length", "min_password_length");

    @Autowired
    public SettingsService(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public SettingsDtoResponse getSettings(String javaSessionId) throws DataBaseException {
        Properties props = PropertiesUtils.getProperties();
        if (javaSessionId != null) {
            User user = sessionDao.getByCookie(javaSessionId).getUser();
            if (user instanceof Admin) {
                return new SettingsDtoResponse(
                        Integer.valueOf(props.getProperty(adminSettings.get(0))),
                        Integer.valueOf(props.getProperty(adminSettings.get(1))));
            }
            if (user instanceof Doctor) {
                return new SettingsDtoResponse(
                        Integer.valueOf(props.getProperty(doctorSettings.get(0))),
                        Integer.valueOf(props.getProperty(doctorSettings.get(1))));
            }
            if (user instanceof Patient) {
                return new SettingsDtoResponse(
                        Integer.valueOf(props.getProperty(patientSettings.get(0))),
                        Integer.valueOf(props.getProperty(patientSettings.get(1))));
            }
        }
        return new SettingsDtoResponse(
                Integer.valueOf(props.getProperty(defaultSettings.get(0))),
                Integer.valueOf(props.getProperty(defaultSettings.get(1))));
    }
}
