package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.CommissionDao;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static net.thumbtack.school.hospital.service.CommonService.*;
import static net.thumbtack.school.hospital.service.DoctorService.*;

@Service
public class CommissionService {
    private DoctorDao doctorDao;
    private PatientDao patientDao;
    private CommissionDao commissionDao;
    private SessionDao sessionDao;

    @Autowired
    public CommissionService(DoctorDao doctorDao, PatientDao patientDao, CommissionDao commissionDao, SessionDao sessionDao) {
        this.doctorDao = doctorDao;
        this.patientDao = patientDao;
        this.commissionDao = commissionDao;
        this.sessionDao = sessionDao;
    }

    public CommissionDtoResponse insert(CommissionDtoRequest commissionReq, String javaSessionId) throws DataBaseException {
        Patient patient = patientDao.getById(commissionReq.getPatientId());
        if (patient == null)
            throw new DataBaseException(DataBaseErrorCode.PATIENT_ID_NOT_EXIST);

        if (!doctorDao.isRoomExist(commissionReq.getRoom()))
            throw new DataBaseException(DataBaseErrorCode.ROOM_INVALID);

        List<Integer> doctorIds = commissionReq.getDoctorIds().stream().distinct().collect(Collectors.toList());
        List<Doctor> doctors = doctorDao.getByIds(doctorIds);
        if (doctors.size() != doctorIds.size())
            throw new DataBaseException(DataBaseErrorCode.DOCTOR_IDS_NOT_EXIST);

        Doctor doctor = (Doctor) sessionDao.getByCookie(javaSessionId).getUser();
        if (!doctorIds.contains(doctor.getId())) {
            doctors.add(doctor);
            doctorIds.add(doctor.getId());
        }
        if (doctorIds.size() == 1)
            throw new DataBaseException(DataBaseErrorCode.SIZE_COMMISSION_INVALID);

        String room = commissionReq.getRoom();
        if (doctors.stream().noneMatch(doc -> doc.getRoom().equals(room)))
            throw new DataBaseException(DataBaseErrorCode.ROOM_NOT_BELONG);

        LocalDateTime dateTimeCommission =
                LocalDateTime.parse(commissionReq.getDate() + " " + commissionReq.getTime(), formatterDateTime);
        int duration = commissionReq.getDuration();
        checkIsAnyDoctorFired(doctors, dateTimeCommission.toLocalDate());
        checkOtherCommissions(patient, doctors, dateTimeCommission, duration);
        List<SlotSchedule> slotsCommission = getSlotsOnCommission(doctors, dateTimeCommission, duration);

        doctorIds.sort(Comparator.naturalOrder());
        String ticketNumber = "CD" +
                String.join("D", doctorIds.stream().map(id -> id.toString()).collect(Collectors.toList())) +
                dateTimeCommission.format(formatterTicket);
        Ticket ticket = new Ticket(patient, ticketNumber);
        Commission commission = new Commission(doctors, ticket,
                dateTimeCommission.toLocalDate(), dateTimeCommission.toLocalTime(),
                room, duration);
        if (slotsCommission.isEmpty()) {
            commissionDao.insert(commission);
        } else {
            commissionDao.insert(commission, slotsCommission);
        }

        CommissionDtoResponse commissionResp = new CommissionDtoResponse();
        commissionResp.setTicket(ticket.getNumber());
        commissionResp.setPatientId(patient.getId());
        commissionResp.setDoctorIds(doctorIds);
        commissionResp.setDate(commissionReq.getDate());
        commissionResp.setTime(commissionReq.getTime());
        commissionResp.setRoom(room);
        commissionResp.setDuration(duration);
        return commissionResp;
    }

    private void checkOtherCommissions(Patient patient, List<Doctor> doctors,
                                       LocalDateTime dateTimeCommission, int durationCommission) throws DataBaseException {
        List<Commission> commissionsOther = commissionDao.getByPatient(patient, dateTimeCommission.toLocalDate());
        commissionsOther.addAll(commissionDao.getByDoctors(doctors, dateTimeCommission.toLocalDate()));
        for (Commission commission : commissionsOther) {
            if (isAppointmentsIntersect(dateTimeCommission.toLocalTime(), durationCommission,
                    commission.getTime(), commission.getDuration()))
                throw new DataBaseException(DataBaseErrorCode.COMMISSION_EXIST);
        }
    }

}
