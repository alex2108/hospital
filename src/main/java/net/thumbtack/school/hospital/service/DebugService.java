package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.*;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import org.springframework.stereotype.Component;

@Component
public class DebugService {
    private AdminDao adminDao;
    private DoctorDao doctorDao;
    private PatientDao patientDao;
    private SessionDao sessionDao;

    public DebugService(AdminDao adminDao,
                        DoctorDao doctorDao,
                        PatientDao patientDao,
                        SessionDao sessionDao) {
        this.adminDao = adminDao;
        this.doctorDao = doctorDao;
        this.patientDao = patientDao;
        this.sessionDao = sessionDao;
    }

    public void clearAll() throws DataBaseException {
        adminDao.deleteAll();
        doctorDao.deleteAll();
        patientDao.deleteAll();
        sessionDao.deleteAll();
    }
}
