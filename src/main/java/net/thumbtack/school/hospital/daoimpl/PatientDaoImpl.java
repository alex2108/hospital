package net.thumbtack.school.hospital.daoimpl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PatientDaoImpl extends DaoImplBase implements PatientDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);

    @Override
    public Patient insert(Patient patient) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).insert(patient);
                getPatientMapper(sqlSession).insert(patient);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                if (ex.getCause() instanceof MySQLIntegrityConstraintViolationException)
                    throw new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS);
                else {
                    LOGGER.info("Can't insert Doctor {} {}", patient, ex);
                    throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
                }
            }
            sqlSession.commit();
        }
        return patient;
    }

    @Override
    public Patient get(String login, String password) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getPatientMapper(sqlSession).get(login, password);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Patient getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getPatientMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Patient update(Patient patient) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).update(patient);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Patient {} {}", patient, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return patient;
    }

    @Override
    public void deleteAll() throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }
}
