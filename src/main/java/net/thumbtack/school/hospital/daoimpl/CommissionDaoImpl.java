package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.dao.CommissionDao;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class CommissionDaoImpl extends DaoImplBase implements CommissionDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminDaoImpl.class);

    @Override
    public Commission insert(Commission commission, List<SlotSchedule> slots) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).insert(commission.getTicket());
                if (getSlotScheduleMapper(sqlSession).setSlotsBusy(slots) != slots.size()) {
                    sqlSession.rollback();
                    throw new DataBaseException(DataBaseErrorCode.SLOT_TIME_BUSY);
                }
                getComissionMapper(sqlSession).insert(commission);
                getDoctorComissionMapper(sqlSession).insert(commission.getDoctors(), commission);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Commission {}, update slots {}, {}", commission, slots, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return commission;
    }

    @Override
    public Commission insert(Commission commission) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).insert(commission.getTicket());
                getComissionMapper(sqlSession).insert(commission);
                getDoctorComissionMapper(sqlSession).insert(commission.getDoctors(), commission);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Commission {} {}", commission, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return commission;
    }

    @Override
    public Commission getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Commission getByTicket(Ticket ticket) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByTicket(ticket);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Commission> getByPatient(Patient patient) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByPatient(patient);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Commission> getByPatient(Patient patient, LocalDate date) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByPatientDate(patient, date);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Commission> getByPatient(Patient patient, LocalDate startDate, LocalDate endDate) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByPatientInterval(patient, startDate, endDate);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Commission> getByDoctors(List<Doctor> doctors, LocalDate date) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByDoctorsDate(doctors, date);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Commission> getByDoctor(Doctor doctor, LocalDate startDate, LocalDate endDate) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByDoctorInterval(doctor, startDate, endDate);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Commission> getByInterval(LocalDate startDate, LocalDate endDate) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getComissionMapper(sqlSession).getByInterval(startDate, endDate);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }
}
