package net.thumbtack.school.hospital.daoimpl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.hospital.model.Admin;
import net.thumbtack.school.hospital.dao.AdminDao;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AdminDaoImpl extends DaoImplBase implements AdminDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminDaoImpl.class);

    @Override
    public Admin insert(Admin admin) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).insert(admin);
                getAdminMapper(sqlSession).insert(admin);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                if (ex.getCause() instanceof MySQLIntegrityConstraintViolationException)
                    throw new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS);
                else {
                    LOGGER.info("Can't insert Admin {} {}", admin, ex);
                    throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
                }
            }
            sqlSession.commit();
        }

        return admin;
    }

    @Override
    public Admin get(String login, String password) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getAdminMapper(sqlSession).get(login, password);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Admin getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getAdminMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }


    @Override
    public Admin update(Admin admin) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).update(admin);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update Admin {} {}", admin, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return admin;
    }

    @Override
    public void deleteAll() {
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
