package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.model.SlotSchedule;
import net.thumbtack.school.hospital.model.Ticket;
import net.thumbtack.school.hospital.dao.TicketDao;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TicketDaoImpl extends DaoImplBase implements TicketDao {

    @Override
    public Ticket getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getTicketMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Ticket getAppointmentTicketByNumber(String number) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getTicketMapper(sqlSession).getAppointmentTicketByNumber(number);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Ticket getCommissionTicketByNumber(String number) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getTicketMapper(sqlSession).getCommissionTicketByNumber(number);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public void deleteAppointment(Ticket ticket) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSlotScheduleMapper(sqlSession).setSlotVacant(ticket);
                getTicketMapper(sqlSession).delete(ticket);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteCommission(Ticket ticket, List<SlotSchedule> slots) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSlotScheduleMapper(sqlSession).setSlotsVacant(slots);
                getTicketMapper(sqlSession).delete(ticket);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(Ticket ticket) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).delete(ticket);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }
}
