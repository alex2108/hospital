package net.thumbtack.school.hospital.daoimpl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Schedule;
import net.thumbtack.school.hospital.model.Ticket;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class DoctorDaoImpl extends DaoImplBase implements DoctorDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);

    @Override
    public Doctor insert(Doctor doctor) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getUserMapper(sqlSession).insert(doctor);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                if (ex.getCause() instanceof MySQLIntegrityConstraintViolationException)
                    throw new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS);
                else {
                    LOGGER.info("Can't insert Doctor {} {}", doctor, ex);
                    throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
                }
            }
            try {
                getDoctorMapper(sqlSession).insert(doctor);
                for (Schedule schedule : doctor.getSchedules()) {
                    getScheduleMapper(sqlSession).insert(schedule);
                    getSlotScheduleMapper(sqlSession).batchInsert(schedule.getSlotSchedules(), schedule.getId());
                }
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                if (ex.getCause() instanceof MySQLIntegrityConstraintViolationException)
                    throw new DataBaseException(DataBaseErrorCode.ROOM_BUSY);
                else {
                    LOGGER.info("Can't insert Doctor {} {}", doctor, ex);
                    throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
                }
            }
            sqlSession.commit();
        }
        return doctor;
    }

    @Override
    public Doctor get(String login, String password) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).get(login, password);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Doctor getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Doctor> getByIds(List<Integer> ids) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getByIds(ids);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Doctor> getBySpeciality(String speciality) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getBySpeciality(speciality);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Doctor> getVacantBySpeciality(String speciality, LocalDateTime dateTime) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getVacantBySpeciality(speciality, dateTime.toLocalDate(), dateTime.toLocalTime());
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Doctor> getAll() throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getDoctorMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Ticket> delete(Doctor doctor) throws DataBaseException {
        List<Ticket> tickets = new ArrayList<>();
        try (SqlSession sqlSession = getSession()) {
            try {
                tickets.addAll(getTicketMapper(sqlSession).getAppointTickets(doctor, doctor.getFiredDate()));
                tickets.addAll(getTicketMapper(sqlSession).getCommissionTickets(doctor, doctor.getFiredDate()));
                getTicketMapper(sqlSession).deleteAppointTickets(doctor);
                getTicketMapper(sqlSession).deleteCommissionTickets(doctor);
                getScheduleMapper(sqlSession).delete(doctor);
                getDoctorMapper(sqlSession).setFiredDate(doctor);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return tickets;
    }

    @Override
    public void deleteAll() throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getDoctorMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public boolean isSpecialityExist(String speciality) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return (getDoctorMapper(sqlSession).getSpecialityId(speciality) != null);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public boolean isRoomExist(String room) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return (getDoctorMapper(sqlSession).getRoomId(room) != null);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }
}
