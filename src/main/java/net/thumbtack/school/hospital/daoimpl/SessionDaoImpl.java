package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.model.Session;
import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SessionDaoImpl extends DaoImplBase implements SessionDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);

    @Override
    public Session insert(Session session) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSessionMapper(sqlSession).insert(session);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Session {} {}", session, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }

        return session;
    }

    @Override
    public Session getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            Session session = getSessionMapper(sqlSession).getByIdWithAdmin(id);
            if (session == null)
                return null;
            if (session.getUser() != null)
                return session;

            session = getSessionMapper(sqlSession).getByIdWithDoctor(id);
            if (session.getUser() != null)
                return session;

            return getSessionMapper(sqlSession).getByIdWithPatient(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public Session getByCookie(String javaSessionId) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            Session session = getSessionMapper(sqlSession).getByCookieWithAdmin(javaSessionId);
            if (session == null)
                return null;
            if (session.getUser() != null)
                return session;

            session = getSessionMapper(sqlSession).getByCookieWithDoctor(javaSessionId);
            if (session.getUser() != null)
                return session;

            return getSessionMapper(sqlSession).getByCookieWithPatient(javaSessionId);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public void delete(Session session) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSessionMapper(sqlSession).delete(session);
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getSessionMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

}
