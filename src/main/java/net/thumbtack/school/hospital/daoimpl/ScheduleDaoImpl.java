package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.dao.*;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class ScheduleDaoImpl extends DaoImplBase implements ScheduleDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);

    @Override
    public List<Schedule> insert(List<Schedule> schedules) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                for (Schedule schedule : schedules) {
                    getScheduleMapper(sqlSession).insert(schedule);
                    getSlotScheduleMapper(sqlSession).batchInsert(schedule.getSlotSchedules(), schedule.getId());
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Schedules {} {}", schedules, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return schedules;
    }

    @Override
    public void addTicket(SlotSchedule slot) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getTicketMapper(sqlSession).insert(slot.getTicket());
                if (getSlotScheduleMapper(sqlSession).addTicket(slot) == 0)
                    throw new DataBaseException(DataBaseErrorCode.SLOT_TIME_BUSY);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't add slot with ticket {} {}", slot, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

    @Override
    public Schedule getById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getScheduleMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Schedule> getByPatient(Patient patient) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getScheduleMapper(sqlSession).getByPatient(patient);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Schedule> getByPatient(Patient patient, LocalDate startDate, LocalDate endDate) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getScheduleMapper(sqlSession).getByPatientInterval(patient, startDate, endDate);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Schedule> getByInterval(LocalDate startDate, LocalDate endDate) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getScheduleMapper(sqlSession).getByInterval(startDate, endDate);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public SlotSchedule getSlotById(int id) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getSlotScheduleMapper(sqlSession).getSlotById(id);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<SlotSchedule> getByPatient(Patient patient, LocalDate date) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            return getSlotScheduleMapper(sqlSession).getByPatientDate(patient, date);
        } catch (RuntimeException ex) {
            throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
        }
    }

    @Override
    public List<Schedule> update(List<Schedule> schedules) throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                int sizeAllSlots = schedules.stream()
                        .mapToInt(schedule -> schedule.getSlotSchedules().size())
                        .sum();
                if (getSlotScheduleMapper(sqlSession).deleteByScheduleIds(schedules) != sizeAllSlots) {
                    sqlSession.rollback();
                    throw new DataBaseException(DataBaseErrorCode.SLOT_TIME_BUSY);
                }
                for (Schedule schedule : schedules) {
                    getSlotScheduleMapper(sqlSession).batchInsert(schedule.getSlotSchedules(), schedule.getId());
                }
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update schedules {} {}", schedules, ex);
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
        return schedules;
    }

    @Override
    public void deleteAll() throws DataBaseException {
        try (SqlSession sqlSession = getSession()) {
            try {
                getScheduleMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw new DataBaseException(DataBaseErrorCode.OTHER_ERROR);
            }
            sqlSession.commit();
        }
    }

}
