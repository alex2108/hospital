package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Validated
public class TicketEndpoint {
    private final TicketService ticketService;
    private final SessionService sessionService;

    @Autowired
    public TicketEndpoint(TicketService ticketService, SessionService sessionService) {
        this.ticketService = ticketService;
        this.sessionService = sessionService;
    }

    @DeleteMapping(value = {"/tickets", "/tickets/{number}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteAppointment(@PathVariable("number") Optional<@NotNull String> ticketNumber,
                                            @CookieValue("JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        ticketService.deleteTicketAppointment(ticketNumber.get(), javaSessionId);
        return ResponseEntity.ok().body("{}");
    }
}
