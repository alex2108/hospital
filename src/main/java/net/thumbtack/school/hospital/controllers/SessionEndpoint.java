package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class SessionEndpoint {
    private final SessionService sessionService;

    @Autowired
    public SessionEndpoint(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @PostMapping(value = "/sessions",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public LoginDtoResponse login(@Valid @RequestBody LoginDtoRequest loginReq, HttpServletResponse response) throws DataBaseException {
    	LoginDtoResponse loginResp = sessionService.insert(loginReq);
        response.addCookie(new Cookie(SessionService.getCookieName(), loginResp.getJavaSessionId()));
        return loginResp;
    }

    @GetMapping(value = "/account", produces = MediaType.APPLICATION_JSON_VALUE)
    public LoginDtoResponse getUser(@CookieValue("JAVASESSIONID") String javaSessionId) throws DataBaseException {
        return sessionService.getUser(javaSessionId);
    }

    @DeleteMapping(value = "/sessions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity logout(@CookieValue("JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.delete(javaSessionId);
        return ResponseEntity.ok().body("{}");
    }
}
