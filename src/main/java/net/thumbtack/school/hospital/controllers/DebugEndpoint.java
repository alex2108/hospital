package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.DebugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DebugEndpoint {
    private DebugService debugService;

    @Autowired
    public DebugEndpoint(DebugService debugService)
    {
        this.debugService = debugService;
    }

    @PostMapping(value = "/debug/clear", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity clearAll() throws DataBaseException {
        debugService.clearAll();
        return ResponseEntity.ok().body("{}");
    }
}
