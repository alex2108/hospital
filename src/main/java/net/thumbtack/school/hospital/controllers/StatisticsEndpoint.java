package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.StatisticsDoctorDtoResponse;
import net.thumbtack.school.hospital.dto.StatisticsPatientDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.service.StatisticsService;
import net.thumbtack.school.hospital.validator.DateValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.util.Optional;

@RestController
@RequestMapping("/api/statistics")
@Validated
public class StatisticsEndpoint {
    private StatisticsService statisticsService;
    private final SessionService sessionService;

    @Autowired
    public StatisticsEndpoint(StatisticsService statisticsService, SessionService sessionService) {
        this.statisticsService = statisticsService;
        this.sessionService = sessionService;
    }

    @GetMapping(value = {"/patients", "/patients/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public StatisticsPatientDtoResponse getPatientStatistics(@PathVariable("id") Optional<@NotNull @Positive Integer> patientId,
                                                             @RequestParam(value = "details", required = false, defaultValue = "no")
                                                             @Pattern(regexp = "yes|no") String details,
                                                             @RequestParam(value = "byDays", required = false, defaultValue = "no")
                                                             @Pattern(regexp = "yes|no") String byDays,
                                                             @RequestParam(value = "startDate", required = false)
                                                             @DateValidation String startDate,
                                                             @RequestParam(value = "endDate", required = false)
                                                             @DateValidation String endDate,
                                                             @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        return statisticsService.getPatientStatistics(patientId.get(), startDate, endDate,
                details.equals("yes"), byDays.equals("yes"));
    }

    @GetMapping(value = "/doctors/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StatisticsDoctorDtoResponse getDoctorStatistics(@PathVariable("id") @Positive Integer id,
                                                           @RequestParam(value = "details", required = false, defaultValue = "no")
                                                           @Pattern(regexp = "yes|no") String details,
                                                           @RequestParam(value = "byDays", required = false, defaultValue = "no")
                                                           @Pattern(regexp = "yes|no") String byDays,
                                                           @RequestParam(value = "startDate", required = false)
                                                           @DateValidation String startDate,
                                                           @RequestParam(value = "endDate", required = false)
                                                           @DateValidation String endDate,
                                                           @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        return statisticsService.getDoctorStatistics(id, startDate, endDate,
                details.equals("yes"), byDays.equals("yes"));
    }

    @GetMapping(value = "/doctors", produces = MediaType.APPLICATION_JSON_VALUE)
    public StatisticsDoctorDtoResponse getAllDoctorsStatistics(@RequestParam(value = "details", required = false, defaultValue = "no")
                                                               @Pattern(regexp = "yes|no") String details,
                                                               @RequestParam(value = "byDays", required = false, defaultValue = "no")
                                                               @Pattern(regexp = "yes|no") String byDays,
                                                               @RequestParam(value = "startDate", required = false)
                                                               @DateValidation String startDate,
                                                               @RequestParam(value = "endDate", required = false)
                                                               @DateValidation String endDate,
                                                               @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        return statisticsService.getAllDoctorsStatistics(startDate, endDate, details.equals("yes"), byDays.equals("yes"));
    }
}
