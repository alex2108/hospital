package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.SettingsDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.service.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class SettingsEndpoint {
    private final SettingsService settingsService;
    private final SessionService sessionService;

    @Autowired
    public SettingsEndpoint(SettingsService settingsService, SessionService sessionService)
    {
        this.settingsService = settingsService;
        this.sessionService = sessionService;
    }

    @GetMapping(value = "/settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public SettingsDtoResponse getSettings(@CookieValue(name = "JAVASESSIONID", required = false) String javaSessionId) throws DataBaseException {
        if (javaSessionId != null) {
            sessionService.checkAuthorization(javaSessionId);
        }
        return settingsService.getSettings(javaSessionId);
    }
}
