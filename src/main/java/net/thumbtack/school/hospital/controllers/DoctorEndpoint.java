package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.CommissionDtoRequest;
import net.thumbtack.school.hospital.dto.CommissionDtoResponse;
import net.thumbtack.school.hospital.dto.doctor.DoctorDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.CommissionService;
import net.thumbtack.school.hospital.service.DoctorService;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.DateValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@RestController
@RequestMapping("/api")
@Validated
public class DoctorEndpoint {
    private final CommissionService commissionService;
    private final SessionService sessionService;
    private final DoctorService doctorService;

    @Autowired
    public DoctorEndpoint(CommissionService commissionService,
                          SessionService sessionService,
                          DoctorService doctorService) {
        this.commissionService = commissionService;
        this.sessionService = sessionService;
        this.doctorService = doctorService;
    }

    @PostMapping(value = "/commissions",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public CommissionDtoResponse insertCommission(@Valid @RequestBody CommissionDtoRequest commissionReq,
                                                  @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkDoctorAccess(javaSessionId);
        return commissionService.insert(commissionReq, javaSessionId);
    }

    @GetMapping(value = "/doctors/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DoctorDtoResponse getDoctor(@PathVariable("id") @Positive Integer id,
                                       @RequestParam(value = "schedule", required = false, defaultValue = "no")
                                       @Pattern(regexp = "yes|no") String schedule,
                                       @RequestParam(value = "startDate", required = false)
                                       @DateValidation String startDate,
                                       @RequestParam(value = "endDate", required = false)
                                       @DateValidation String endDate,
                                       @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        return doctorService.getDoctor(id, javaSessionId, schedule.equals("yes"), startDate, endDate);
    }

    @GetMapping(value = "/doctors", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DoctorDtoResponse> getDoctors(@RequestParam(value = "schedule", required = false, defaultValue = "no")
                                              @Pattern(regexp = "yes|no") String schedule,
                                              @RequestParam(value = "speciality", required = false)
                                              String speciality,
                                              @RequestParam(value = "startDate", required = false)
                                              @DateValidation String startDate,
                                              @RequestParam(value = "endDate", required = false)
                                              @DateValidation String endDate,
                                              @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        return doctorService.getDoctors(speciality, javaSessionId, schedule.equals("yes"), startDate, endDate);
    }

}
