package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.RegAdminDtoRequest;
import net.thumbtack.school.hospital.dto.UpdateAdminDtoRequest;
import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.dto.AdminDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.service.AdminService;
import net.thumbtack.school.hospital.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Validated
public class AdminEndpoint {
    private final AdminService adminService;
    private final DoctorService doctorService;
    private final SessionService sessionService;

    @Autowired
    public AdminEndpoint(AdminService adminService,
                         DoctorService doctorService,
                         SessionService sessionService) {
        this.adminService = adminService;
        this.doctorService = doctorService;
        this.sessionService = sessionService;
    }

    @PostMapping(value = "/admins",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AdminDtoResponse registerAdmin(@Valid @RequestBody RegAdminDtoRequest regAdminReg,
                                          @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkAdminAccess(javaSessionId);
        return adminService.register(regAdminReg);
    }

    @PostMapping(value = "/doctors",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public DoctorDtoResponse registerDoctor(@Valid @RequestBody RegDoctorDtoRequest regDoctorReq,
                                            @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkAdminAccess(javaSessionId);
        return doctorService.register(regDoctorReq);
    }

    @PutMapping(value = "/admins",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AdminDtoResponse updateAdmin(@Valid @RequestBody UpdateAdminDtoRequest updateReq,
                                        @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkAdminAccess(javaSessionId);
        return adminService.update(updateReq, javaSessionId);
    }

    @PutMapping(value = {"/doctors", "/doctors/{id}"},
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public DoctorDtoResponse updateDoctor(@Valid @RequestBody UpdateDoctorDtoRequest updateReg,
                                          @PathVariable("id") Optional<@NotNull @Positive Integer> doctorId,
                                          @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkAdminAccess(javaSessionId);
        return doctorService.update(updateReg, doctorId.get());
    }

    @DeleteMapping(value = {"/doctors", "/doctors/{id}"},
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteDoctor(@Valid @RequestBody DeleteDoctorDtoRequest deleteReq,
                                       @PathVariable("id") Optional<@NotNull @Positive Integer> doctorId,
                                       @CookieValue("JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkAdminAccess(javaSessionId);
        doctorService.delete(deleteReq, doctorId.get());
        return ResponseEntity.ok().body("{}");
    }

}
