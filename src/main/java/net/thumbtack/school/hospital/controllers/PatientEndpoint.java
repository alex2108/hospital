package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.ScheduleService;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.service.PatientService;
import net.thumbtack.school.hospital.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Validated
public class PatientEndpoint {
    private final PatientService patientService;
    private final SessionService sessionService;
    private final ScheduleService scheduleService;
    private final TicketService ticketService;

    @Autowired
    public PatientEndpoint(PatientService patientService,
                           SessionService sessionService,
                           ScheduleService scheduleService,
                           TicketService ticketService) {
        this.patientService = patientService;
        this.sessionService = sessionService;
        this.scheduleService = scheduleService;
        this.ticketService = ticketService;
    }

    @PostMapping(value = "/patients",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PatientDtoResponse register(@Valid @RequestBody RegPatientDtoRequest regPatientReq, HttpServletResponse response) throws DataBaseException {
        PatientDtoResponse regResponse = patientService.register(regPatientReq);
        response.addCookie(new Cookie(SessionService.getCookieName(), regResponse.getJavaSessionId()));
        return regResponse;
    }

    @PostMapping(value = "/tickets",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AppointDtoResponse insertAppointment(@Valid @RequestBody AppointDtoRequest appointReq,
                                                @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkPatientAccess(javaSessionId);
        return scheduleService.insertAppointment(appointReq, javaSessionId);
    }

    @PutMapping(value = "/patients",
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PatientDtoResponse updatePatient(@Valid @RequestBody UpdatePatientDtoRequest updateReq,
                                            @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkPatientAccess(javaSessionId);
        return patientService.update(updateReq, javaSessionId);
    }

    @GetMapping(value = {"/patients/{id}", "/patients"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public PatientDtoResponse getPatient(@PathVariable("id") Optional<@NotNull @Positive Integer> patientId,
                                         @CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkDoctorAdminAccess(javaSessionId);
        return patientService.getPatient(patientId.get());
    }

    @GetMapping(value = "/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TicketDtoResponse> getTickets(@CookieValue(name = "JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkPatientAccess(javaSessionId);
        return ticketService.getTickets(javaSessionId);
    }

    @DeleteMapping(value = {"/commissions", "/commissions/{number}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteCommission(@PathVariable("number") Optional<@NotNull String> ticketNumber,
                                           @CookieValue("JAVASESSIONID") String javaSessionId) throws DataBaseException {
        sessionService.checkAuthorization(javaSessionId);
        sessionService.checkPatientAccess(javaSessionId);
        ticketService.deleteTicketCommission(ticketNumber.get(), javaSessionId);
        return ResponseEntity.ok().body("{}");
    }

}
