package net.thumbtack.school.hospital.utils;

import org.apache.ibatis.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

public class PropertiesUtils {
    private static Properties props;
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesUtils.class);

    public static boolean initProperties()
    {
        try (Reader reader = Resources.getResourceAsReader("application.properties")) {
            props = new Properties();
            props.load(reader);
            return true;
        } catch (IOException ex) {
            LOGGER.error("Error loading application.properties", ex);
            return false;
        }
    }

    public static Properties getProperties()
    {
        return props;
    }
}
