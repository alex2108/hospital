package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Ticket;

import java.time.LocalDateTime;
import java.util.List;

public interface DoctorDao extends UserDao<Doctor> {

    boolean isSpecialityExist(String speciality) throws DataBaseException;

    boolean isRoomExist(String room) throws DataBaseException;

    List<Doctor> getVacantBySpeciality(String speciality, LocalDateTime dateTime) throws DataBaseException;

    List<Doctor> getByIds(List<Integer> ids) throws DataBaseException;

    List<Doctor> getBySpeciality(String speciality) throws DataBaseException;

    List<Doctor> getAll() throws DataBaseException;

    List<Ticket> delete(Doctor doctor) throws DataBaseException;
}
