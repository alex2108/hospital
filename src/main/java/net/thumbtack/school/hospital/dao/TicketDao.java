package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.SlotSchedule;
import net.thumbtack.school.hospital.model.Ticket;

import java.util.List;

public interface TicketDao {

    Ticket getById(int id) throws DataBaseException;

    Ticket getAppointmentTicketByNumber(String number) throws DataBaseException;

    Ticket getCommissionTicketByNumber(String number) throws DataBaseException;

    void deleteAppointment(Ticket ticket) throws DataBaseException;

    void deleteCommission(Ticket ticket, List<SlotSchedule> slots) throws DataBaseException;

    void delete(Ticket ticket) throws DataBaseException;

    void deleteAll() throws DataBaseException;
}
