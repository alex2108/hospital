package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;

public interface AdminDao extends UserDao<Admin> {

    Admin update(Admin user) throws DataBaseException;

}
