package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Session;

public interface SessionDao {
    Session insert(Session session) throws DataBaseException;

    Session getById(int id) throws DataBaseException;

    Session getByCookie(String javaSessionId) throws DataBaseException;

    void delete(Session session) throws DataBaseException;

    void deleteAll() throws DataBaseException;
}
