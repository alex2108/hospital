package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Patient;

public interface PatientDao extends UserDao<Patient> {

    Patient update(Patient patient) throws DataBaseException;

}
