package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.User;

public interface UserDao<T extends User> {

    T insert(T user) throws DataBaseException;

    T get(String login, String password) throws DataBaseException;

    T getById(int id) throws DataBaseException;

    void deleteAll() throws DataBaseException;

}
