package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;

import java.time.LocalDate;
import java.util.List;

public interface CommissionDao {

    Commission insert(Commission commission, List<SlotSchedule> slots) throws DataBaseException;

    Commission insert(Commission commission) throws DataBaseException;

    Commission getById(int id) throws DataBaseException;

    Commission getByTicket(Ticket ticket) throws DataBaseException;

    List<Commission> getByPatient(Patient patient) throws DataBaseException;

    List<Commission> getByPatient(Patient patient, LocalDate date) throws DataBaseException;

    List<Commission> getByPatient(Patient patient, LocalDate startDate, LocalDate endDate) throws DataBaseException;

    List<Commission> getByDoctors(List<Doctor> doctors, LocalDate date) throws DataBaseException;

    List<Commission> getByDoctor(Doctor doctor, LocalDate startDate, LocalDate endDate) throws DataBaseException;

    List<Commission> getByInterval(LocalDate startDate, LocalDate endDate) throws DataBaseException;
}
