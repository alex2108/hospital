package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleDao {

    List<Schedule> insert(List<Schedule> schedules) throws DataBaseException;

    Schedule getById(int id) throws DataBaseException;

    List<Schedule> getByPatient(Patient patient) throws DataBaseException;

    List<Schedule> getByPatient(Patient patient, LocalDate startDate, LocalDate endDate) throws DataBaseException;

    List<Schedule> getByInterval(LocalDate startDate, LocalDate endDate) throws DataBaseException;

    void addTicket(SlotSchedule slot) throws DataBaseException;

    SlotSchedule getSlotById(int id) throws DataBaseException;

    List<SlotSchedule> getByPatient(Patient patient, LocalDate date) throws DataBaseException;

    List<Schedule> update(List<Schedule> schedules) throws DataBaseException;

    void deleteAll() throws DataBaseException;

}
