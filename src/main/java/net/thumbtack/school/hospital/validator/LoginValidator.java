package net.thumbtack.school.hospital.validator;

import net.thumbtack.school.hospital.utils.PropertiesUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Properties;

public class LoginValidator implements ConstraintValidator<LoginValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        if (field == null)
            return false;

        Properties props = PropertiesUtils.getProperties();
        int maxNameLength = Integer.valueOf(props.getProperty("max_name_length"));
        return (field.matches("[0-9a-zA-Zа-яА-Я]+") && field.length() <= maxNameLength);
    }
}
