package net.thumbtack.school.hospital.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AppointmentValidator implements ConstraintValidator<AppointmentValidation, Object> {

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        Integer doctorId = (Integer) new BeanWrapperImpl(obj)
                .getPropertyValue("doctorId");
        String speciality = (String) new BeanWrapperImpl(obj)
                .getPropertyValue("speciality");

        if (doctorId == null && (speciality == null || speciality.replace(" ","").isEmpty())) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(ValidationErrorCode.APPOINTMENT_DOCTOR_ABSENT.name())
                    .addConstraintViolation();
            return false;
        }
        if (doctorId != null && speciality != null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(ValidationErrorCode.APPOINTMENT_DOUBLE_DOCTOR.name())
                    .addConstraintViolation();
            return false;
        }
        return true;
    }
}
