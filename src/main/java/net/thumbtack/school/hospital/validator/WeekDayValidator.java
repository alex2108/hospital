package net.thumbtack.school.hospital.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class WeekDayValidator implements ConstraintValidator<WeekDayValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        List<String> weekDays = Arrays.asList(DayOfWeek.MONDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.TUESDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.THURSDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.FRIDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT));
        return weekDays.contains(field);
    }
}
