package net.thumbtack.school.hospital.validator;

import net.thumbtack.school.hospital.dto.doctor.DaySchedule;
import net.thumbtack.school.hospital.dto.doctor.WeekSchedule;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.List;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static net.thumbtack.school.hospital.service.CommonService.formatterTime;

public class ScheduleValidator implements ConstraintValidator<ScheduleValidation, Object> {

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        String dateStartStr = (String) new BeanWrapperImpl(obj)
                .getPropertyValue("dateStart");
        String dateEndStr = (String) new BeanWrapperImpl(obj)
                .getPropertyValue("dateEnd");
        WeekSchedule weekSchedule = (WeekSchedule) new BeanWrapperImpl(obj)
                .getPropertyValue("weekSchedule");
        List<DaySchedule> weekDaysSchedule = (List<DaySchedule>) new BeanWrapperImpl(obj)
                .getPropertyValue("weekDaysSchedule");

        if (weekSchedule == null && weekDaysSchedule == null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(ValidationErrorCode.SCHEDULE_ABSENT.name())
                    .addConstraintViolation();
            return false;
        }

        if (weekSchedule != null && weekDaysSchedule != null) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(ValidationErrorCode.DOUBLE_SCHEDULE.name())
                    .addConstraintViolation();
            return false;
        }

        boolean result = true;
        if (dateStartStr != null && dateEndStr != null) {
            try {
                LocalDate dateStart = LocalDate.parse(dateStartStr, formatterDate);
                LocalDate dateEnd = LocalDate.parse(dateEndStr, formatterDate);
                if (dateEnd.isBefore(dateStart)) {
                    context.disableDefaultConstraintViolation();
                    context.buildConstraintViolationWithTemplate(ValidationErrorCode.DATE_CHRONOLOGY_INVALID.name())
                            .addConstraintViolation();
                    result = false;
                }
            } catch (DateTimeParseException ex) {
            }
        }

        if (weekSchedule != null) {
            if (isTimeBefore(weekSchedule.getTimeEnd(), weekSchedule.getTimeStart())) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(ValidationErrorCode.TIME_CHRONOLOGY_INVALID.name())
                        .addConstraintViolation();
                result = false;
            }
        }

        if (weekDaysSchedule != null) {
            for (DaySchedule daySchedule : weekDaysSchedule) {
                if (isTimeBefore(daySchedule.getTimeEnd(), daySchedule.getTimeStart())) {
                    context.disableDefaultConstraintViolation();
                    context.buildConstraintViolationWithTemplate(ValidationErrorCode.TIME_CHRONOLOGY_INVALID.name())
                            .addConstraintViolation();
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    private boolean isTimeBefore(String time1, String time2) {
        if (time1 == null || time2 == null)
            return false;
        try {
            return LocalTime.parse(time1, formatterTime).isBefore(LocalTime.parse(time2, formatterTime));
        } catch (DateTimeParseException ex) {
            return false;
        }
    }
}
