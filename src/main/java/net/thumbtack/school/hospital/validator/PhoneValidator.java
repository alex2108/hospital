package net.thumbtack.school.hospital.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<PhoneValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        if (field == null)
            return false;
        return (field.matches("\\+7-?9\\d{2}-?\\d{3}-?\\d{2}-?\\d{2}") ||
                        field.matches("8-?9\\d{2}-?\\d{3}-?\\d{2}-?\\d{2}"));
    }
}
