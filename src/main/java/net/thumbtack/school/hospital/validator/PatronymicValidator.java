package net.thumbtack.school.hospital.validator;

import net.thumbtack.school.hospital.utils.PropertiesUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Properties;

public class PatronymicValidator implements ConstraintValidator<PatronymicValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        if (field == null)
            return true;
        Properties props = PropertiesUtils.getProperties();
        int maxNameLength = Integer.valueOf(props.getProperty("max_name_length"));
        return (field.replace(" ", "").matches("[а-яА-Я-]+") && field.length() <= maxNameLength);
    }
}
