package net.thumbtack.school.hospital.validator;

import net.thumbtack.school.hospital.utils.PropertiesUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Properties;

public class PasswordValidator implements ConstraintValidator<PasswordValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        if (field == null)
            return false;

        Properties props = PropertiesUtils.getProperties();
        int maxNameLength = Integer.valueOf(props.getProperty("max_name_length"));
        int minPassLength = Integer.valueOf(props.getProperty("min_password_length"));
        return (!field.isEmpty() && !field.replace(" ", "").isEmpty() &&
                field.length() <= maxNameLength && field.length() >= minPassLength);
    }
}
