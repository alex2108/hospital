package net.thumbtack.school.hospital.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class WeekDaysValidator implements ConstraintValidator<WeekDayValidation, List<String>> {

    @Override
    public boolean isValid(List<String> field, ConstraintValidatorContext context) {
        if (field == null || field.isEmpty())
            return true;
        List<String> weekDays = Arrays.asList(DayOfWeek.MONDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.TUESDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.THURSDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT),
                DayOfWeek.FRIDAY.getDisplayName(TextStyle.SHORT, Locale.ROOT));
        return weekDays.containsAll(field);
    }
}
