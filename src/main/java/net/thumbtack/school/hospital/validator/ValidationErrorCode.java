package net.thumbtack.school.hospital.validator;

import net.thumbtack.school.hospital.service.SessionService;

public enum ValidationErrorCode {

    EMPTY_NULL("Field should not be null or empty"),
    NAME_INVALID("First name, last name should be non-empty and contain only Russian letters, space, and dash. " +
            "Length should not exceed 50 letters"),
    PATRONYMIC_INVALID("Patronymic should contain only Russian letters, space, and dash. " +
            "Length should not exceed 50 letters"),
    LOGIN_INVALID("Login should be non-empty and contain only English and Russian letters, and numbers. " +
            "Length should not exceed 50 letters"),
    PASSWORD_INVALID("Password should be non-empty. Length of password should be at least 10 and no more than 50 letters"),
    EMAIL_INVALID("Invalid e-mail format"),
    PHONE_INVALID("Invalid phone format"),
    NON_POSITIVE_NOT_ALLOWED("Value should be greater than 0"),
    DATE_INVALID("Invalid date. For format use: dd-MM-yyyy. Date should be from now until date not older than 2 months"),
    TIME_FORMAT_INVALID("Invalid time format. Use: hh:mm"),
    WEEK_DAY_FORMAT_INVALID("Invalid day of week format. Use: \"Mon\", \"Tue\", \"Wed\", \"Thu\", \"Fri\""),
    SCHEDULE_ABSENT("Either \"weekSchedule\" or \"weekDaysSchedule\" should be present"),
    DOUBLE_SCHEDULE("Fields \"weekSchedule\" and \"weekDaysSchedule\" could not be present simultaneously"),
    DATE_CHRONOLOGY_INVALID("Field \"dateStart\" should be before \"dateEnd\""),
    TIME_CHRONOLOGY_INVALID("Field \"timeStart\" should be before \"timeEnd\""),
    APPOINTMENT_DOCTOR_ABSENT("Either \"doctorId\" or \"speciality\" should be present"),
    APPOINTMENT_DOUBLE_DOCTOR("Fields \"doctorId\" and \"speciality\" could not be present simultaneously"),
    FORMAT_INVALID("Format of parameter value is invalid"),
    COOKIE_MISSING("Missing cookie " + SessionService.getCookieName()),
    URL_INVALID("The following request is invalid: "),
    GENERAL_VALIDATION_ERROR("General validation error");

    private final String errorCode;

    private ValidationErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
