package net.thumbtack.school.hospital.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateValidator implements ConstraintValidator<DateValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        if (field != null && !field.isEmpty() && !field.replace(" ", "").isEmpty()) {
            DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            try {
                LocalDate date = LocalDate.parse(field, formatterDate);
                if (date.isBefore(LocalDate.now()) || date.isAfter(LocalDate.now().plusMonths(2)))
                    return false;
            } catch (DateTimeParseException ex) {
                return false;
            }
        }
        return true;
    }
}
