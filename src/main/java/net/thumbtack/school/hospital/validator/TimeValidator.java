package net.thumbtack.school.hospital.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class TimeValidator implements ConstraintValidator<TimeValidation, String> {

    @Override
    public boolean isValid(String field, ConstraintValidatorContext context) {
        if (field != null && !field.isEmpty() && !field.replace(" ", "").isEmpty()) {
            DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");
            try {
                LocalTime.parse(field, formatterTime);
            } catch (DateTimeParseException ex) {
                return false;
            }
        }
        return true;
    }
}
