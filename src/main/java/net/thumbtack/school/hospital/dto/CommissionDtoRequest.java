package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.validator.DateValidation;
import net.thumbtack.school.hospital.validator.TimeValidation;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

public class CommissionDtoRequest {
    @NotNull
    @Positive
    private Integer patientId;

    @NotEmpty
    private List<@Positive Integer> doctorIds;

    @NotBlank
    private String room;

    @NotBlank
    @DateValidation
    private String date;

    @NotBlank
    @TimeValidation
    private String time;

    @NotNull
    @Positive
    private Integer duration;

    public CommissionDtoRequest() {
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public List<Integer> getDoctorIds() {
        return doctorIds;
    }

    public void setDoctorIds(List<Integer> doctorIds) {
        this.doctorIds = doctorIds;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommissionDtoRequest that = (CommissionDtoRequest) o;

        if (!patientId.equals(that.patientId)) return false;
        if (!doctorIds.equals(that.doctorIds)) return false;
        if (!room.equals(that.room)) return false;
        if (!date.equals(that.date)) return false;
        if (!time.equals(that.time)) return false;
        return duration.equals(that.duration);

    }

    @Override
    public int hashCode() {
        int result = patientId.hashCode();
        result = 31 * result + doctorIds.hashCode();
        result = 31 * result + room.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }
}
