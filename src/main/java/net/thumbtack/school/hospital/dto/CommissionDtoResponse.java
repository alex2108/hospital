package net.thumbtack.school.hospital.dto;

import java.util.List;

public class CommissionDtoResponse {
    private String ticket;
    private Integer patientId;
    private List<Integer> doctorIds;
    private String room;
    private String date;
    private String time;
    private Integer duration;

    public CommissionDtoResponse() {
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public List<Integer> getDoctorIds() {
        return doctorIds;
    }

    public void setDoctorIds(List<Integer> doctorIds) {
        this.doctorIds = doctorIds;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommissionDtoResponse that = (CommissionDtoResponse) o;

        if (!ticket.equals(that.ticket)) return false;
        if (!patientId.equals(that.patientId)) return false;
        if (!doctorIds.equals(that.doctorIds)) return false;
        if (!room.equals(that.room)) return false;
        if (!date.equals(that.date)) return false;
        if (!time.equals(that.time)) return false;
        return duration.equals(that.duration);

    }

    @Override
    public int hashCode() {
        int result = ticket.hashCode();
        result = 31 * result + patientId.hashCode();
        result = 31 * result + doctorIds.hashCode();
        result = 31 * result + room.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }
}
