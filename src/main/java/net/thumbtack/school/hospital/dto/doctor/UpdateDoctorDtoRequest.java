package net.thumbtack.school.hospital.dto.doctor;

import net.thumbtack.school.hospital.validator.DateValidation;
import net.thumbtack.school.hospital.validator.ScheduleValidation;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@ScheduleValidation
public class UpdateDoctorDtoRequest {

    @NotBlank
    @DateValidation
    private String dateStart;

    @NotBlank
    @DateValidation
    private String dateEnd;

    @Valid
    private WeekSchedule weekSchedule;

    private List<@Valid DaySchedule> weekDaysSchedule;

    @NotNull
    @Positive
    private Integer duration;

    public UpdateDoctorDtoRequest() {
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public WeekSchedule getWeekSchedule() {
        return weekSchedule;
    }

    public void setWeekSchedule(WeekSchedule weekSchedule) {
        this.weekSchedule = weekSchedule;
    }

    public List<DaySchedule> getWeekDaysSchedule() {
        return weekDaysSchedule;
    }

    public void setWeekDaysSchedule(List<DaySchedule> weekDaysSchedule) {
        this.weekDaysSchedule = weekDaysSchedule;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateDoctorDtoRequest that = (UpdateDoctorDtoRequest) o;

        if (!dateStart.equals(that.dateStart)) return false;
        if (!dateEnd.equals(that.dateEnd)) return false;
        if (weekSchedule != null ? !weekSchedule.equals(that.weekSchedule) : that.weekSchedule != null) return false;
        if (weekDaysSchedule != null ? !weekDaysSchedule.equals(that.weekDaysSchedule) : that.weekDaysSchedule != null)
            return false;
        return duration.equals(that.duration);

    }

    @Override
    public int hashCode() {
        int result = dateStart.hashCode();
        result = 31 * result + dateEnd.hashCode();
        result = 31 * result + (weekSchedule != null ? weekSchedule.hashCode() : 0);
        result = 31 * result + (weekDaysSchedule != null ? weekDaysSchedule.hashCode() : 0);
        result = 31 * result + duration.hashCode();
        return result;
    }
}
