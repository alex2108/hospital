package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.validator.LoginValidation;
import net.thumbtack.school.hospital.validator.PasswordValidation;

public class LoginDtoRequest {

    @LoginValidation
    private String login;

    @PasswordValidation
    private String password;

    public LoginDtoRequest()
    {}

    public LoginDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginDtoRequest that = (LoginDtoRequest) o;

        if (!login.equals(that.login)) return false;
        return password.equals(that.password);

    }

    @Override
    public int hashCode() {
        int result = login.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }
}
