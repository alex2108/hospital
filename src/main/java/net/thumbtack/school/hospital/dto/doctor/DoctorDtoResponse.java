package net.thumbtack.school.hospital.dto.doctor;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class DoctorDtoResponse {
    private int id;
    private String firstName;
    private String lastName;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String patronymic;
    private String speciality;
    private String room;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ScheduleDto> schedule;

    public DoctorDtoResponse()
    {}

    public DoctorDtoResponse(int id, String firstName, String lastName, String patronymic, String speciality, String room, List<ScheduleDto> schedule) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.speciality = speciality;
        this.room = room;
        this.schedule = schedule;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public List<ScheduleDto> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ScheduleDto> schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DoctorDtoResponse that = (DoctorDtoResponse) o;

        if (id != that.id) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (patronymic != null ? !patronymic.equals(that.patronymic) : that.patronymic != null) return false;
        if (!speciality.equals(that.speciality)) return false;
        if (!room.equals(that.room)) return false;
        return schedule != null ? schedule.equals(that.schedule) : that.schedule == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + speciality.hashCode();
        result = 31 * result + room.hashCode();
        result = 31 * result + (schedule != null ? schedule.hashCode() : 0);
        return result;
    }
}

