package net.thumbtack.school.hospital.dto;

public class SettingsDtoResponse {
    private int maxNameLength;
    private int minPasswordLength;

    public SettingsDtoResponse() {
    }

    public SettingsDtoResponse(int maxNameLength, int minPasswordLength) {
        this.maxNameLength = maxNameLength;
        this.minPasswordLength = minPasswordLength;
    }

    public int getMaxNameLength() {
        return maxNameLength;
    }

    public void setMaxNameLength(int maxNameLength) {
        this.maxNameLength = maxNameLength;
    }

    public int getMinPasswordLength() {
        return minPasswordLength;
    }

    public void setMinPasswordLength(int minPasswordLength) {
        this.minPasswordLength = minPasswordLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SettingsDtoResponse response = (SettingsDtoResponse) o;

        if (maxNameLength != response.maxNameLength) return false;
        return minPasswordLength == response.minPasswordLength;

    }

    @Override
    public int hashCode() {
        int result = maxNameLength;
        result = 31 * result + minPasswordLength;
        return result;
    }
}
