package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.validator.AppointmentValidation;
import net.thumbtack.school.hospital.validator.DateValidation;
import net.thumbtack.school.hospital.validator.TimeValidation;

import javax.validation.constraints.NotBlank;

@AppointmentValidation
public class AppointDtoRequest {
    private Integer doctorId;
    private String speciality;

    @NotBlank
    @DateValidation
    private String date;

    @NotBlank
    @TimeValidation
    private String time;

    public AppointDtoRequest() {
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppointDtoRequest that = (AppointDtoRequest) o;

        if (doctorId != null ? !doctorId.equals(that.doctorId) : that.doctorId != null) return false;
        if (speciality != null ? !speciality.equals(that.speciality) : that.speciality != null) return false;
        if (!date.equals(that.date)) return false;
        return time.equals(that.time);

    }

    @Override
    public int hashCode() {
        int result = doctorId != null ? doctorId.hashCode() : 0;
        result = 31 * result + (speciality != null ? speciality.hashCode() : 0);
        result = 31 * result + date.hashCode();
        result = 31 * result + time.hashCode();
        return result;
    }
}
