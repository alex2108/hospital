package net.thumbtack.school.hospital.dto.doctor;

import net.thumbtack.school.hospital.validator.DateValidation;

import javax.validation.constraints.NotBlank;

public class DeleteDoctorDtoRequest {
    @NotBlank
    @DateValidation
    private String date;

    public DeleteDoctorDtoRequest() {
    }

    public DeleteDoctorDtoRequest(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeleteDoctorDtoRequest that = (DeleteDoctorDtoRequest) o;

        return date.equals(that.date);

    }

    @Override
    public int hashCode() {
        return date.hashCode();
    }
}
