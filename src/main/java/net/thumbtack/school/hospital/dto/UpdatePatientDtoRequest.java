package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.validator.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdatePatientDtoRequest {

    @NameValidation
    private String firstName;

    @NameValidation
    private String lastName;

    @PatronymicValidation
    private String patronymic;

    @PasswordValidation
    private String oldPassword;

    @PasswordValidation
    private String newPassword;

    @NotNull
    @Email(regexp = "\\w+[\\w\\.-]*@\\w+\\.\\w{2,4}")
    private String email;

    @NotBlank
    private String address;

    @PhoneValidation
    private String phone;

    public UpdatePatientDtoRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdatePatientDtoRequest that = (UpdatePatientDtoRequest) o;

        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (patronymic != null ? !patronymic.equals(that.patronymic) : that.patronymic != null) return false;
        if (!oldPassword.equals(that.oldPassword)) return false;
        if (!newPassword.equals(that.newPassword)) return false;
        if (!email.equals(that.email)) return false;
        if (!address.equals(that.address)) return false;
        return phone.equals(that.phone);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + oldPassword.hashCode();
        result = 31 * result + newPassword.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + phone.hashCode();
        return result;
    }
}
