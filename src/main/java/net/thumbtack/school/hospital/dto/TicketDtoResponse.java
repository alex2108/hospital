package net.thumbtack.school.hospital.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketDtoResponse {
    private String ticket;
    private String room;
    private String date;
    private String time;
    private Integer doctorId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String speciality;

    private List<DoctorDto> doctorsCommission;

    public static class DoctorDto {
        private Integer doctorId;
        private String firstName;
        private String lastName;
        private String patronymic;
        private String speciality;

        public DoctorDto() {
        }

        public DoctorDto(int doctorId, String firstName, String lastName, String patronymic, String speciality) {
            this.doctorId = doctorId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.patronymic = patronymic;
            this.speciality = speciality;
        }

        public int getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(int doctorId) {
            this.doctorId = doctorId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPatronymic() {
            return patronymic;
        }

        public void setPatronymic(String patronymic) {
            this.patronymic = patronymic;
        }

        public String getSpeciality() {
            return speciality;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DoctorDto doctorDto = (DoctorDto) o;

            if (doctorId != null ? !doctorId.equals(doctorDto.doctorId) : doctorDto.doctorId != null) return false;
            if (firstName != null ? !firstName.equals(doctorDto.firstName) : doctorDto.firstName != null) return false;
            if (lastName != null ? !lastName.equals(doctorDto.lastName) : doctorDto.lastName != null) return false;
            if (patronymic != null ? !patronymic.equals(doctorDto.patronymic) : doctorDto.patronymic != null)
                return false;
            return speciality != null ? speciality.equals(doctorDto.speciality) : doctorDto.speciality == null;

        }

        @Override
        public int hashCode() {
            int result = doctorId != null ? doctorId.hashCode() : 0;
            result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
            result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
            result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
            result = 31 * result + (speciality != null ? speciality.hashCode() : 0);
            return result;
        }
    }

    public TicketDtoResponse() {
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public List<DoctorDto> getDoctorsCommission() {
        return doctorsCommission;
    }

    public void setDoctorsCommission(List<DoctorDto> doctorsCommission) {
        this.doctorsCommission = doctorsCommission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketDtoResponse response = (TicketDtoResponse) o;

        if (!ticket.equals(response.ticket)) return false;
        if (!room.equals(response.room)) return false;
        if (!date.equals(response.date)) return false;
        if (!time.equals(response.time)) return false;
        if (doctorId != null ? !doctorId.equals(response.doctorId) : response.doctorId != null) return false;
        if (firstName != null ? !firstName.equals(response.firstName) : response.firstName != null) return false;
        if (lastName != null ? !lastName.equals(response.lastName) : response.lastName != null) return false;
        if (patronymic != null ? !patronymic.equals(response.patronymic) : response.patronymic != null) return false;
        if (speciality != null ? !speciality.equals(response.speciality) : response.speciality != null) return false;
        return doctorsCommission != null ? doctorsCommission.equals(response.doctorsCommission) : response.doctorsCommission == null;

    }

    @Override
    public int hashCode() {
        int result = ticket.hashCode();
        result = 31 * result + room.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + (doctorId != null ? doctorId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (speciality != null ? speciality.hashCode() : 0);
        result = 31 * result + (doctorsCommission != null ? doctorsCommission.hashCode() : 0);
        return result;
    }
}
