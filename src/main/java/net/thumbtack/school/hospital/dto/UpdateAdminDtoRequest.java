package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.validator.NameValidation;
import net.thumbtack.school.hospital.validator.PasswordValidation;
import net.thumbtack.school.hospital.validator.PatronymicValidation;

import javax.validation.constraints.NotBlank;

public class UpdateAdminDtoRequest {

    @NameValidation
    private String firstName;

    @NameValidation
    private String lastName;

    @PatronymicValidation
    private String patronymic;

    @NotBlank
    private String position;

    @PasswordValidation
    private String oldPassword;

    @PasswordValidation
    private String newPassword;

    public UpdateAdminDtoRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateAdminDtoRequest that = (UpdateAdminDtoRequest) o;

        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (patronymic != null ? !patronymic.equals(that.patronymic) : that.patronymic != null) return false;
        if (!position.equals(that.position)) return false;
        if (!oldPassword.equals(that.oldPassword)) return false;
        return newPassword.equals(that.newPassword);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + position.hashCode();
        result = 31 * result + oldPassword.hashCode();
        result = 31 * result + newPassword.hashCode();
        return result;
    }
}
