package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.validator.*;

import javax.validation.constraints.NotBlank;

public class RegAdminDtoRequest {

    @NameValidation
    private String firstName;

    @NameValidation
    private String lastName;

    @PatronymicValidation
    private String patronymic;

    @LoginValidation
    private String login;

    @PasswordValidation
    private String password;

    @NotBlank
    private String position;

    public RegAdminDtoRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegAdminDtoRequest)) return false;

        RegAdminDtoRequest that = (RegAdminDtoRequest) o;

        if (!getFirstName().equals(that.getFirstName())) return false;
        if (!getLastName().equals(that.getLastName())) return false;
        if (getPatronymic() != null ? !getPatronymic().equals(that.getPatronymic()) : that.getPatronymic() != null)
            return false;
        if (!getLogin().equals(that.getLogin())) return false;
        if (!getPassword().equals(that.getPassword())) return false;
        return getPosition().equals(that.getPosition());

    }

    @Override
    public int hashCode() {
        int result = getFirstName().hashCode();
        result = 31 * result + getLastName().hashCode();
        result = 31 * result + (getPatronymic() != null ? getPatronymic().hashCode() : 0);
        result = 31 * result + getLogin().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getPosition().hashCode();
        return result;
    }
}
