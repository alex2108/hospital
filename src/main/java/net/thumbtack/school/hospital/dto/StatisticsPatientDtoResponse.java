package net.thumbtack.school.hospital.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
{
	"patientId": id,
	"firstName": "firstName",
	"lastName": "lastName",
	"patronymic": "patronymic",
	"email": "email",
	"address": "address",
	"phone" : "phone",
	//byDays=no
	"startDate" : "dd-MM-yyyy",
	"endDate" : "dd-MM-yyyy",
	//details=no
	"totall" :
	{
		"count" : num,
		"duration" : "HH:mm"
	},
	//details=yes
	"appointment" :
	{
		"count" : num,
		"duration" : "HH:mm"
	},
	//details=yes
	"commission" :
	{
		"count" : num,
		"duration" : "HH:mm"
	}
	//byDays
	"statistics":
	[
		{
			"date": "dd-MM-yyyy",
			//details=no
			"totall" :
			{
				"count" : num,
				"duration" : "HH:mm"
			},
			//details=yes
			"appointment" :
			{
				"count" : num,
				"duration" : "HH:mm"
			},
			//details=yes
			"commission" :
			{
				"count" : num,
				"duration" : "HH:mm"
			}
		}
	...
	]
}
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatisticsPatientDtoResponse {
    private int patientId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;
    private String address;
    private String phone;
    private String startDate;
    private String endDate;
    private Map<String, Statistics> statistics;
    private List<StatisticsOnDay> byDays;

    public StatisticsPatientDtoResponse() {
        this.statistics = new HashMap<>();
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @JsonAnyGetter
    public Map<String, Statistics> getStatistics() {
        return statistics;
    }

    @JsonAnySetter
    public void addPropStatistics(String key, Statistics value) {
        statistics.put(key, value);
    }

    public List<StatisticsOnDay> getByDays() {
        return byDays;
    }

    public void setByDays(List<StatisticsOnDay> byDays) {
        this.byDays = byDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticsPatientDtoResponse response = (StatisticsPatientDtoResponse) o;

        if (patientId != response.patientId) return false;
        if (!firstName.equals(response.firstName)) return false;
        if (!lastName.equals(response.lastName)) return false;
        if (patronymic != null ? !patronymic.equals(response.patronymic) : response.patronymic != null) return false;
        if (!email.equals(response.email)) return false;
        if (!address.equals(response.address)) return false;
        if (!phone.equals(response.phone)) return false;
        if (startDate != null ? !startDate.equals(response.startDate) : response.startDate != null) return false;
        if (endDate != null ? !endDate.equals(response.endDate) : response.endDate != null) return false;
        if (statistics != null ? !statistics.equals(response.statistics) : response.statistics != null) return false;
        return byDays != null ? byDays.equals(response.byDays) : response.byDays == null;

    }

    @Override
    public int hashCode() {
        int result = patientId;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + email.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (statistics != null ? statistics.hashCode() : 0);
        result = 31 * result + (byDays != null ? byDays.hashCode() : 0);
        return result;
    }


    public static class Statistics {
        private int count;
        private String duration;

        public Statistics() {
        }

        public Statistics(int count, String duration) {
            this.count = count;
            this.duration = duration;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Statistics that = (Statistics) o;

            if (count != that.count) return false;
            return duration.equals(that.duration);

        }

        @Override
        public int hashCode() {
            int result = count;
            result = 31 * result + duration.hashCode();
            return result;
        }
    }

    public static class StatisticsOnDay {
        private String date;
        private Map<String, Statistics> statistics;

        public StatisticsOnDay() {
            statistics = new HashMap<>();
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @JsonAnyGetter
        public Map<String, Statistics> getStatistics() {
            return statistics;
        }

        @JsonAnySetter
        public void addPropStatistics(String key, Statistics value) {
            statistics.put(key, value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            StatisticsOnDay that = (StatisticsOnDay) o;

            if (!date.equals(that.date)) return false;
            return statistics.equals(that.statistics);

        }

        @Override
        public int hashCode() {
            int result = date.hashCode();
            result = 31 * result + statistics.hashCode();
            return result;
        }
    }

}
