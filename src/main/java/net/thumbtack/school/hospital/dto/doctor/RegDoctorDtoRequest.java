package net.thumbtack.school.hospital.dto.doctor;

import net.thumbtack.school.hospital.validator.*;

import javax.validation.constraints.NotBlank;

public class RegDoctorDtoRequest extends UpdateDoctorDtoRequest {
    @NameValidation
    private String firstName;

    @NameValidation
    private String lastName;

    @PatronymicValidation
    private String patronymic;

    @LoginValidation
    private String login;

    @PasswordValidation
    private String password;

    @NotBlank
    private String speciality;

    @NotBlank
    private String room;

    public RegDoctorDtoRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RegDoctorDtoRequest that = (RegDoctorDtoRequest) o;

        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (patronymic != null ? !patronymic.equals(that.patronymic) : that.patronymic != null) return false;
        if (!login.equals(that.login)) return false;
        if (!password.equals(that.password)) return false;
        if (!speciality.equals(that.speciality)) return false;
        return room.equals(that.room);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + speciality.hashCode();
        result = 31 * result + room.hashCode();
        return result;
    }
}

