package net.thumbtack.school.hospital.dto.doctor;

import net.thumbtack.school.hospital.validator.TimeValidation;
import net.thumbtack.school.hospital.validator.WeekDayValidation;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

public class WeekSchedule {
    @NotBlank
    @TimeValidation
    private String timeStart;

    @NotBlank
    @TimeValidation
    private String timeEnd;

    @WeekDayValidation
    private List<@Valid String> weekDays;

    public WeekSchedule() {
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public List<String> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(List<String> weekDays) {
        this.weekDays = weekDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeekSchedule that = (WeekSchedule) o;

        if (!timeStart.equals(that.timeStart)) return false;
        if (!timeEnd.equals(that.timeEnd)) return false;
        return weekDays != null ? weekDays.equals(that.weekDays) : that.weekDays == null;

    }

    @Override
    public int hashCode() {
        int result = timeStart.hashCode();
        result = 31 * result + timeEnd.hashCode();
        result = 31 * result + (weekDays != null ? weekDays.hashCode() : 0);
        return result;
    }
}
