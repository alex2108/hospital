package net.thumbtack.school.hospital.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
//fields "doctorId" ... "speciality" are included for statistics of doctor,
//for statistics of all doctors these fields are null
{
  "doctorId" : id,
  "firstName" : "firstName",
  "lastName" : "lastName",
  "patronymic" : "patronymic",
  "speciality" : "speciality",
  //byDays=no
  "startDate" : "startDate",
  "endDate" : "endDate",
  //details=no
  "totall" : "totall",
	{
		"count" : num,
		"timeBySchedule" : "HH:mm",
		"timeBusy" : "HH:mm"
	},
	//details=yes
	"appointment" :
	{
		"count" : num,
		"timeBySchedule" : "HH:mm",
		"timeBusy" : "HH:mm"
	},
	//details=yes
	"commission" :
	{
	    "count" : num,
		"timeBusy" : "HH:mm"
	}
	//byDays=yes
	"byDays":
	[
		{
			"date": "dd-MM-yyyy",
			//details=no
			"totall" :
			{
				"count" : num,
				"timeBySchedule" : "HH:mm",
				"timeBusy" : "HH:mm"
			},
			//details=yes
			"appointment" :
			{
				"count" : num,
				"timeBySchedule" : "HH:mm",
				"timeBusy" : "HH:mm"
			},
			//details=yes
			"commission" :
			{
				"count" : num,
				"timeBusy" : "HH:mm"
			}
		}
	...
	]
}
 */


@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatisticsDoctorDtoResponse {
    private Integer doctorId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String speciality;
    private String room;
    private String startDate;
    private String endDate;
    private Map<String, Statistics> statistics;
    private List<StatisticsOnDay> byDays;

    public StatisticsDoctorDtoResponse() {
        this.statistics = new HashMap<>();
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<StatisticsOnDay> getByDays() {
        return byDays;
    }

    public void setByDays(List<StatisticsOnDay> byDays) {
        this.byDays = byDays;
    }

    @JsonAnyGetter
    public Map<String, Statistics> getStatistics() {
        return statistics;
    }

    @JsonAnySetter
    public void addPropStatistics(String key, Statistics value) {
        statistics.put(key, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticsDoctorDtoResponse that = (StatisticsDoctorDtoResponse) o;

        if (doctorId != null ? !doctorId.equals(that.doctorId) : that.doctorId != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (patronymic != null ? !patronymic.equals(that.patronymic) : that.patronymic != null) return false;
        if (speciality != null ? !speciality.equals(that.speciality) : that.speciality != null) return false;
        if (room != null ? !room.equals(that.room) : that.room != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (statistics != null ? !statistics.equals(that.statistics) : that.statistics != null) return false;
        return byDays != null ? byDays.equals(that.byDays) : that.byDays == null;

    }

    @Override
    public int hashCode() {
        int result = doctorId != null ? doctorId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (speciality != null ? speciality.hashCode() : 0);
        result = 31 * result + (room != null ? room.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (statistics != null ? statistics.hashCode() : 0);
        result = 31 * result + (byDays != null ? byDays.hashCode() : 0);
        return result;
    }

    public static class Statistics {
        private int count;
        private String timeBySchedule;
        private String timeBusy;

        public Statistics() {
        }

        public Statistics(int count, String timeBySchedule, String timeBusy) {
            this.count = count;
            this.timeBySchedule = timeBySchedule;
            this.timeBusy = timeBusy;
        }

        public Statistics(int count, String timeBusy) {
            this(count, null, timeBusy);
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public String getTimeBySchedule() {
            return timeBySchedule;
        }

        public void setTimeBySchedule(String timeBySchedule) {
            this.timeBySchedule = timeBySchedule;
        }

        public String getTimeBusy() {
            return timeBusy;
        }

        public void setTimeBusy(String timeBusy) {
            this.timeBusy = timeBusy;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Statistics that = (Statistics) o;

            if (count != that.count) return false;
            if (timeBySchedule != null ? !timeBySchedule.equals(that.timeBySchedule) : that.timeBySchedule != null)
                return false;
            return timeBusy.equals(that.timeBusy);

        }

        @Override
        public int hashCode() {
            int result = count;
            result = 31 * result + (timeBySchedule != null ? timeBySchedule.hashCode() : 0);
            result = 31 * result + timeBusy.hashCode();
            return result;
        }
    }

    public static class StatisticsOnDay {
        private String date;
        private Map<String, Statistics> statistics;

        public StatisticsOnDay() {
            statistics = new HashMap<>();
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @JsonAnyGetter
        public Map<String, Statistics> getStatistics() {
            return statistics;
        }

        @JsonAnySetter
        public void addPropStatistics(String key, Statistics value) {
            statistics.put(key, value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            StatisticsOnDay that = (StatisticsOnDay) o;

            if (!date.equals(that.date)) return false;
            return statistics.equals(that.statistics);

        }

        @Override
        public int hashCode() {
            int result = date.hashCode();
            result = 31 * result + statistics.hashCode();
            return result;
        }
    }
}
