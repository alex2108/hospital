package net.thumbtack.school.hospital.dto.doctor;

import net.thumbtack.school.hospital.validator.TimeValidation;
import net.thumbtack.school.hospital.validator.WeekDayValidation;

import javax.validation.constraints.NotBlank;

public class DaySchedule {
    @WeekDayValidation
    private String weekDay;

    @NotBlank
    @TimeValidation
    private String timeStart;

    @NotBlank
    @TimeValidation
    private String timeEnd;

    public DaySchedule() {
    }

    public DaySchedule(String weekDay, String timeStart, String timeEnd) {
        this.weekDay = weekDay;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DaySchedule that = (DaySchedule) o;

        if (!weekDay.equals(that.weekDay)) return false;
        if (!timeStart.equals(that.timeStart)) return false;
        return timeEnd.equals(that.timeEnd);

    }

    @Override
    public int hashCode() {
        int result = weekDay.hashCode();
        result = 31 * result + timeStart.hashCode();
        result = 31 * result + timeEnd.hashCode();
        return result;
    }
}
