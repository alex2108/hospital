package net.thumbtack.school.hospital.dto.doctor;

import java.util.List;

public class ScheduleDto {
    private String date;
    private List<SlotScheduleDto> daySchedule;

    public ScheduleDto() {
    }

    public ScheduleDto(String date) {
        this.date = date;
    }

    public ScheduleDto(String date, List<SlotScheduleDto> daySchedule) {
        this.date = date;
        this.daySchedule = daySchedule;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<SlotScheduleDto> getDaySchedule() {
        return daySchedule;
    }

    public void setDaySchedule(List<SlotScheduleDto> daySchedule) {
        this.daySchedule = daySchedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScheduleDto that = (ScheduleDto) o;

        if (!date.equals(that.date)) return false;
        return daySchedule.equals(that.daySchedule);

    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + daySchedule.hashCode();
        return result;
    }
}
