package net.thumbtack.school.hospital.dto;

import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.model.*;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DtoMapper {

    DtoMapper INSTANCE = Mappers.getMapper(DtoMapper.class);

    //admin
    RegAdminDtoRequest adminToDtoReq(Admin admin);

    AdminDtoResponse adminToDtoResp(Admin admin);

    Admin DtoReqToAdmin(RegAdminDtoRequest regAdminReq);

    @Mapping(source = "newPassword", target = "password")
    Admin DtoUpdateToAdmin(UpdateAdminDtoRequest updateReq);


    //doctor
    RegDoctorDtoRequest doctorToDtoReq(Doctor doctor);

    DoctorDtoResponse doctorToDtoResp(Doctor doctor);

    Doctor DtoReqToDoctor(RegDoctorDtoRequest regDoctorReq);

    @Mapping(source = "id", target = "doctorId")
    AppointDtoResponse doctorToDtoAppointResp(Doctor doctor);

    @Mapping(source = "id", target = "doctorId")

    TicketDtoResponse.DoctorDto doctorToDtoTicketDoctor(Doctor doctor);

    @IterableMapping(elementTargetType = TicketDtoResponse.DoctorDto.class)
    List<TicketDtoResponse.DoctorDto> doctorsToDtoTicketDoctors(List<Doctor> doctors);

    @Mapping(source = "id", target = "doctorId")
    StatisticsDoctorDtoResponse doctorToDtoStatistics(Doctor doctor);


    //patient
    RegPatientDtoRequest patientToDtoReq(Patient patient);

    PatientDtoResponse patientToDtoResp(Patient patient);

    Patient DtoReqToPatient(RegPatientDtoRequest regPatientReq);

    @Mapping(source = "id", target = "patientId")
    PatientDto patientToDtoPatient(Patient patient);

    @Mapping(source = "newPassword", target = "password")
    Patient DtoUpdateToPatient(UpdatePatientDtoRequest updateReq);

    @Mapping(source = "id", target = "patientId")
    StatisticsPatientDtoResponse patientToDtoStatistics(Patient patient);


    //login
    LoginDtoResponse adminToDtoLoginResp(Admin admin);

    LoginDtoResponse doctorToDtoLoginResp(Doctor doctor);

    LoginDtoResponse patientToDtoLoginResp(Patient patient);
}
