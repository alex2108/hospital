package net.thumbtack.school.hospital.dto.doctor;

import com.fasterxml.jackson.annotation.JsonInclude;

public class SlotScheduleDto {
    private String time;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private PatientDto patient;

    public SlotScheduleDto() {
    }

    public SlotScheduleDto(String time) {
        this(time, null);
    }

    public SlotScheduleDto(String time, PatientDto patient) {
        this.time = time;
        this.patient = patient;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public PatientDto getPatient() {
        return patient;
    }

    public void setPatient(PatientDto patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SlotScheduleDto that = (SlotScheduleDto) o;

        if (!time.equals(that.time)) return false;
        return patient != null ? patient.equals(that.patient) : that.patient == null;

    }

    @Override
    public int hashCode() {
        int result = time.hashCode();
        result = 31 * result + (patient != null ? patient.hashCode() : 0);
        return result;
    }
}

