package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.*;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;

public interface SlotScheduleMapper {

    @Insert({"<script>",
            "INSERT INTO slot_schedule (schedule_id, time, duration) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "( #{idSchedule}, #{item.time}, #{item.duration} )",
            "</foreach>",
            "</script>"})
    @Options(useGeneratedKeys = true)
    Integer batchInsert(@Param("list") List<SlotSchedule> slotSchedules, @Param("idSchedule") int idSchedule);

    @Select("SELECT * FROM slot_schedule " +
            "WHERE id=#{id}")
    SlotSchedule getSlotById(int id);

    @Select("SELECT * FROM slot_schedule " +
            "LEFT JOIN ticket ON slot_schedule.ticket_id=ticket.id " +
            "WHERE ticket.id=#{ticket.id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY))
    })
    SlotSchedule getByTicket(@Param("ticket") Ticket ticket);

    @Select("SELECT * FROM slot_schedule " +
            "WHERE schedule_id=#{id} " +
            "ORDER BY time")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY))
    })
    List<SlotSchedule> getByScheduleId(int id);

    @Select("SELECT * FROM slot_schedule " +
            "LEFT JOIN schedule ON slot_schedule.schedule_id=schedule.id " +
            "LEFT JOIN ticket ON slot_schedule.ticket_id=ticket.id " +
            "WHERE ticket.patient_id=#{patient.id} AND schedule.date=#{date}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY))
    })
    List<SlotSchedule> getByPatientDate(@Param("patient") Patient patient, @Param("date") LocalDate date);

    @Update("UPDATE slot_schedule " +
            "SET ticket_id=#{slot.ticket.id}, state=#{slot.state} " +
            "WHERE slot_schedule.id=#{slot.id} AND state=\"VACANT\"")
    Integer addTicket(@Param("slot") SlotSchedule slot);

    @Update({"<script>",
            "UPDATE slot_schedule ",
            "SET state=\"BUSY\" ",
            "WHERE state=\"VACANT\" AND slot_schedule.id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item.id}",
            "</foreach>",
            ")",
            "</script>"})
    Integer setSlotsBusy(@Param("list") List<SlotSchedule> slots);

    @Update({"<script>",
            "UPDATE slot_schedule ",
            "SET state=\"VACANT\" ",
            "WHERE slot_schedule.id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item.id}",
            "</foreach>",
            ")",
            "</script>"})
    void setSlotsVacant(@Param("list") List<SlotSchedule> slots);

    @Update("UPDATE slot_schedule " +
            "SET state=\"VACANT\" " +
            "WHERE ticket_id=#{id}")
    void setSlotVacant(Ticket ticket);

    @Delete({"<script>",
            "DELETE FROM slot_schedule WHERE state=\"VACANT\" AND schedule_id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item.id}",
            "</foreach>",
            ")",
            "</script>"})
    Integer deleteByScheduleIds(@Param("list") List<Schedule> schedules);

}
