package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Commission;
import net.thumbtack.school.hospital.model.Doctor;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface DoctorMapper {

    @Insert("INSERT INTO doctor (user_id, speciality_id, room_id)" +
            "SELECT #{id}, speciality.id, room.id " +
            "FROM speciality, room " +
            "WHERE name=#{speciality} AND number=#{room}")
    @Options(useGeneratedKeys = true)
    Integer insert(Doctor doctor);

    @Select("SELECT user.id, firstname, lastname, patronymic, password, login, " +
            "speciality.name as speciality, room.number as room, fireddate " +
            "FROM user " +
            "LEFT JOIN doctor ON id=user_id " +
            "LEFT JOIN speciality ON speciality.id=speciality_id " +
            "LEFT JOIN room ON room.id=room_id " +
            "WHERE login=#{login} AND password=#{password} COLLATE utf8_bin")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    Doctor get(@Param("login") String login, @Param("password") String password);

    @Select("SELECT user.id, firstname, lastname, patronymic, password, login, " +
            "speciality.name as speciality, room.number as room, fireddate " +
            "FROM user " +
            "LEFT JOIN doctor ON id=user_id " +
            "LEFT JOIN speciality ON speciality.id=speciality_id " +
            "LEFT JOIN room ON room.id=room_id " +
            "WHERE user_id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    Doctor getById(int id);

    @Select({"<script>",
            "SELECT user.id, firstname, lastname, patronymic, password, login,",
            "speciality.name as speciality, room.number as room, fireddate",
            "FROM user ",
            "LEFT JOIN doctor ON id=user_id ",
            "LEFT JOIN speciality ON speciality.id=speciality_id ",
            "LEFT JOIN room ON room.id=room_id ",
            "WHERE user_id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item}",
            "</foreach>",
            ")",
            "</script>"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    List<Doctor> getByIds(@Param("list") List<Integer> ids);

    @Select("SELECT user.id, firstname, lastname, patronymic, password, login, " +
            "speciality.name as speciality, room.number as room, fireddate " +
            "FROM user " +
            "LEFT JOIN doctor ON user.id=user_id " +
            "LEFT JOIN speciality ON speciality.id=speciality_id " +
            "LEFT JOIN room ON room.id=room_id " +
            "WHERE user.id IN " +
            "(SELECT doctor_id FROM doctor_commission WHERE commission_id=#{commission.id})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    List<Doctor> getByCommission(@Param("commission") Commission commission);

    @Select("SELECT user.id, firstname, lastname, patronymic, password, login, " +
            "speciality.name as speciality, room.number as room, fireddate " +
            "FROM user " +
            "LEFT JOIN doctor ON user.id=user_id " +
            "LEFT JOIN speciality ON speciality.id=speciality_id " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN schedule ON schedule.doctor_id=user_id " +
            "LEFT JOIN slot_schedule ON slot_schedule.schedule_id=schedule.id " +
            "WHERE speciality.name=#{speciality}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    List<Doctor> getBySpeciality(String speciality);


    @Select("SELECT user.id, firstname, lastname, patronymic, password, login, " +
            "speciality.name as speciality, room.number as room, fireddate " +
            "FROM user " +
            "LEFT JOIN doctor ON user.id=user_id " +
            "LEFT JOIN speciality ON speciality.id=speciality_id " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN schedule ON schedule.doctor_id=user_id " +
            "LEFT JOIN slot_schedule ON slot_schedule.schedule_id=schedule.id " +
            "WHERE speciality.name=#{speciality} AND schedule.date=#{date} AND slot_schedule.time=#{time} " +
            "AND state=\"VACANT\"")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    List<Doctor> getVacantBySpeciality(@Param("speciality") String speciality,
                                       @Param("date") LocalDate date, @Param("time") LocalTime time);

    @Select("SELECT user.id, firstname, lastname, patronymic, password, login, " +
            "speciality.name as speciality, room.number as room, fireddate " +
            "FROM doctor " +
            "LEFT JOIN user ON user.id=user_id " +
            "LEFT JOIN speciality ON speciality.id=speciality_id " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN schedule ON schedule.doctor_id=user_id " +
            "LEFT JOIN slot_schedule ON slot_schedule.schedule_id=schedule.id")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "schedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.ScheduleMapper.getByDoctor"))
    })
    List<Doctor> getAll();

    @Update("UPDATE user " +
            "LEFT JOIN doctor ON id=user_id " +
            "SET firstname=#{firstName}, lastname=#{lastName}, patronymic=#{patronymic}, " +
            "password=#{password}, " +
            "speciality_id=(SELECT id FROM speciality WHERE name=#{speciality}), " +
            "room_id=(SELECT id FROM room WHERE number=#{room}) " +
            "WHERE id=#{id}")
    void update(Doctor doctor);

    @Update("UPDATE doctor " +
            "SET fireddate=#{firedDate} " +
            "WHERE user_id=#{id}")
    void setFiredDate(Doctor doctor);

    @Delete("DELETE FROM user WHERE id IN (SELECT user_id FROM doctor)")
    void deleteAll();

    @Select("SELECT id FROM speciality WHERE name=#{speciality}")
    Integer getSpecialityId(String speciality);

    @Select("SELECT id FROM room WHERE number=#{room}")
    Integer getRoomId(String room);
}
