package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Admin;
import org.apache.ibatis.annotations.*;

public interface AdminMapper {

    @Insert("INSERT INTO admin (user_id, position) VALUES (#{id}, #{position})")
    Integer insert(Admin admin);

    @Select("SELECT id, firstname, lastname, patronymic, password, login, position " +
            "FROM user " +
            "LEFT JOIN admin ON id=user_id " +
            "WHERE login=#{login} AND password=#{password} COLLATE utf8_bin")
    Admin get(@Param("login") String login, @Param("password") String password);

    @Select("SELECT id, firstname, lastname, patronymic, password, login, position " +
            "FROM user " +
            "LEFT JOIN admin ON id=user_id WHERE user_id=#{id}")
    Admin getById(int id);

    @Update("UPDATE user LEFT JOIN admin ON id=user_id SET firstname=#{firstName}, lastname=#{lastName}, patronymic=#{patronymic}, " +
            "password=#{password}, position=#{position} WHERE id=#{id}")
    Integer update(Admin admin);

    @Delete("DELETE FROM user WHERE id IN (SELECT user_id FROM admin) AND id<>1")
    void deleteAll();
}
