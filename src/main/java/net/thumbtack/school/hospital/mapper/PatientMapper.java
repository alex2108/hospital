package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Patient;
import org.apache.ibatis.annotations.*;

public interface PatientMapper {

    @Insert("INSERT INTO patient (user_id, email, address, phone) VALUES (#{id}, #{email}, #{address}, #{phone})")
    Integer insert(Patient patient);

    @Select("SELECT id, firstname, lastname, patronymic, password, login, email, address, phone " +
            "FROM user " +
            "LEFT JOIN patient ON id=user_id WHERE login=#{login} AND password=#{password} COLLATE utf8_bin")
    Patient get(@Param("login") String login, @Param("password") String password);

    @Select("SELECT id, firstname, lastname, patronymic, password, login, email, address, phone " +
            "FROM user " +
            "LEFT JOIN patient ON id=user_id WHERE user_id=#{id}")
    Patient getById(int id);

    @Update("UPDATE user LEFT JOIN patient ON id=user_id SET firstname=#{firstName}, lastname=#{lastName}, patronymic=#{patronymic}, " +
            "password=#{password}, email=#{email}, address=#{address}, phone=#{phone} WHERE id=#{id}")
    void update(Patient patient);


    @Delete("DELETE FROM user WHERE id IN (SELECT user_id FROM patient)")
    void deleteAll();

}
