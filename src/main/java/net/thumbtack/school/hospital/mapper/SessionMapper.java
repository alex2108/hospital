package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Admin;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.Session;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

public interface SessionMapper {

    @Insert("INSERT INTO session (user_id, javasessionid) VALUES (#{user.id}, #{javaSessionId}) " +
            "ON DUPLICATE KEY UPDATE user_id=user_id, javasessionid=#{javaSessionId};")
    @Options(useGeneratedKeys = true)
    Integer insert(Session session);

    @Select("SELECT * FROM session " +
            "WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "user_id", javaType = Admin.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.AdminMapper.getById", fetchType = FetchType.LAZY))
    })
    Session getByIdWithAdmin(int id);

    @Select("SELECT * FROM session " +
            "WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "user_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY))
    })
    Session getByIdWithDoctor(int id);

    @Select("SELECT * FROM session " +
            "WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "user_id", javaType = Patient.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById", fetchType = FetchType.LAZY))
    })
    Session getByIdWithPatient(int id);

    @Select("SELECT * FROM session " +
            "WHERE javasessionid=#{javaSessionId} COLLATE utf8_bin")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "user_id", javaType = Admin.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.AdminMapper.getById", fetchType = FetchType.LAZY))
    })
    Session getByCookieWithAdmin(String javaSessionId);

    @Select("SELECT * FROM session " +
            "WHERE javasessionid=#{javaSessionId} COLLATE utf8_bin")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "user_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY))
    })
    Session getByCookieWithDoctor(String javaSessionId);

    @Select("SELECT * FROM session " +
            "WHERE javasessionid=#{javaSessionId} COLLATE utf8_bin")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user", column = "user_id", javaType = Patient.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById", fetchType = FetchType.LAZY))
    })
    Session getByCookieWithPatient(String javaSessionId);

    @Delete("DELETE FROM session WHERE id=#{id}")
    void delete(Session session);

    @Delete("DELETE FROM session")
    void deleteAll();
}
