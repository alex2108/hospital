package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Commission;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.Ticket;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;

public interface CommissionMapper {

    @Insert("INSERT INTO commission (room_id, ticket_id, date, time, duration) " +
            "SELECT room.id, #{ticket.id}, #{date}, #{time}, #{duration} " +
            "FROM room " +
            "WHERE number=#{room}")
    @Options(useGeneratedKeys = true)
    Integer insert(Commission commission);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration " +
            "FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "WHERE commission.id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    Commission getById(int id);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN ticket ON commission.ticket_id=ticket.id " +
            "WHERE ticket.id=#{id} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    Commission getByTicket(Ticket ticket);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN ticket ON commission.ticket_id=ticket.id " +
            "WHERE ticket.patient_id=#{id} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    List<Commission> getByPatient(Patient patient);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN ticket ON commission.ticket_id=ticket.id " +
            "WHERE ticket.patient_id=#{patient.id} AND commission.date=#{date}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    List<Commission> getByPatientDate(@Param("patient") Patient patient, @Param("date") LocalDate date);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN ticket ON commission.ticket_id=ticket.id " +
            "WHERE ticket.patient_id=#{patient.id} AND date >= #{startDate} AND date <= #{endDate} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    List<Commission> getByPatientInterval(@Param("patient") Patient patient,
                                          @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Select({"<script>",
            "SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission ",
            "LEFT JOIN room ON room.id=room_id ",
            "LEFT JOIN doctor_commission ON doctor_commission.commission_id=commission.id ",
            "WHERE commission.date=#{date} AND doctor_commission.doctor_id IN (",
            "<foreach item='item' collection='list' separator=','>",
            "#{item.id}",
            "</foreach>",
            ")",
            "</script>"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    List<Commission> getByDoctorsDate(@Param("list") List<Doctor> doctors, @Param("date") LocalDate date);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "LEFT JOIN doctor_commission ON doctor_commission.commission_id=commission.id " +
            "WHERE doctor_commission.doctor_id=#{doctor.id} AND date >= #{startDate} AND date <= #{endDate} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    List<Commission> getByDoctorInterval(@Param("doctor") Doctor doctor,
                                         @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Select("SELECT commission.id, ticket_id, date, time, room.number as room, duration FROM commission " +
            "LEFT JOIN room ON room.id=room_id " +
            "WHERE date >= #{startDate} AND date <= #{endDate} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "ticket", column = "ticket_id", javaType = Ticket.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.TicketMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "doctors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getByCommission", fetchType = FetchType.LAZY))
    })
    List<Commission> getByInterval(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);
}
