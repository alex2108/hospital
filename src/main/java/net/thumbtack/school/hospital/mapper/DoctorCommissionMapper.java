package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Commission;
import net.thumbtack.school.hospital.model.Doctor;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DoctorCommissionMapper {

    @Insert({"<script>",
            "INSERT INTO doctor_commission (doctor_id, commission_id) VALUES",
            "<foreach item='item' collection='list' separator=','>",
            "( #{item.id}, #{commission.id} )",
            "</foreach>",
            "</script>"})
    void insert(@Param("list") List<Doctor> doctors, @Param("commission") Commission commission);
}
