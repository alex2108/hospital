package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Ticket;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.util.List;

public interface TicketMapper {

    @Insert("INSERT INTO ticket (patient_id, number) " +
            "VALUES (#{patient.id}, #{number})")
    @Options(useGeneratedKeys = true)
    Integer insert(Ticket ticket);

    @Select("SELECT * FROM ticket " +
            "WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "patient", column = "patient_id",
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById"))
    })
    Ticket getById(int id);

    @Select("SELECT * FROM ticket " +
            "LEFT JOIN slot_schedule ON slot_schedule.ticket_id=ticket.id " +
            "LEFT JOIN schedule ON slot_schedule.schedule_id=schedule.id " +
            "WHERE doctor_id=#{doctor.id} AND date>=#{date}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "patient", column = "patient_id",
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById"))
    })
    List<Ticket> getAppointTickets(@Param("doctor") Doctor doctor, @Param("date") LocalDate date);

    @Select("SELECT * FROM ticket " +
            "LEFT JOIN commission ON commission.ticket_id=ticket.id " +
            "LEFT JOIN doctor_commission ON commission.id=doctor_commission.commission_id " +
            "WHERE doctor_id=#{doctor.id} AND date>=#{date}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "patient", column = "patient_id",
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById"))
    })
    List<Ticket> getCommissionTickets(@Param("doctor") Doctor doctor, @Param("date") LocalDate date);

    @Select("SELECT * FROM ticket " +
            "JOIN slot_schedule ON slot_schedule.ticket_id=ticket.id " +
            "JOIN schedule ON slot_schedule.schedule_id=schedule.id " +
            "WHERE number=#{number}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "patient", column = "patient_id",
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById"))
    })
    Ticket getAppointmentTicketByNumber(String number);

    @Select("SELECT * FROM ticket " +
            "JOIN commission ON commission.ticket_id=ticket.id " +
            "WHERE number=#{number}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "patient", column = "patient_id",
                    one = @One(select = "net.thumbtack.school.hospital.mapper.PatientMapper.getById"))
    })
    Ticket getCommissionTicketByNumber(String number);

    @Delete("DELETE FROM ticket WHERE id IN " +
            "(SELECT ticket_id FROM commission " +
            "LEFT JOIN doctor_commission ON commission.id=doctor_commission.commission_id " +
            "WHERE doctor_id=#{id} AND date>=#{firedDate})")
    void deleteCommissionTickets(Doctor doctor);

    @Delete("DELETE FROM ticket WHERE id IN " +
            "(SELECT ticket_id FROM slot_schedule " +
            "LEFT JOIN schedule ON slot_schedule.schedule_id=schedule.id " +
            "WHERE doctor_id=#{id} AND date>=#{firedDate})")
    void deleteAppointTickets(Doctor doctor);

    @Delete("DELETE FROM ticket WHERE id=#{id}")
    void delete(Ticket ticket);

    @Delete("DELETE FROM ticket")
    void deleteAll();

}
