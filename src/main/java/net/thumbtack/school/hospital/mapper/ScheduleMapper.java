package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.Schedule;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleMapper {

    @Insert("INSERT INTO schedule (doctor_id, date) " +
            "VALUES (#{doctor.id}, #{date})")
    @Options(useGeneratedKeys = true)
    Integer insert(Schedule schedule);

    @Select("SELECT * FROM schedule " +
            "WHERE id=#{id} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctor_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "slotSchedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.SlotScheduleMapper.getByScheduleId", fetchType = FetchType.LAZY))
    })
    Schedule getById(int id);

    @Select("SELECT * FROM schedule " +
            "WHERE doctor_id=#{doctor.id} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctor_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "slotSchedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.SlotScheduleMapper.getByScheduleId", fetchType = FetchType.LAZY))
    })
    List<Schedule> getByDoctor(Doctor doctor);

    @Select("SELECT * FROM schedule " +
            "LEFT JOIN slot_schedule ON slot_schedule.schedule_id=schedule.id " +
            "LEFT JOIN ticket ON slot_schedule.ticket_id=ticket.id " +
            "WHERE ticket.patient_id=#{id} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctor_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "slotSchedules", column = "ticket_id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.SlotScheduleMapper.getByTicket", fetchType = FetchType.LAZY))
    })
    List<Schedule> getByPatient(Patient patient);

    @Select("SELECT * FROM schedule " +
            "LEFT JOIN slot_schedule ON slot_schedule.schedule_id=schedule.id " +
            "LEFT JOIN ticket ON slot_schedule.ticket_id=ticket.id " +
            "WHERE ticket.patient_id=#{patient.id} AND date >= #{startDate} AND date <= #{endDate}" +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctor_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "slotSchedules", column = "ticket_id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.SlotScheduleMapper.getByTicket", fetchType = FetchType.LAZY))
    })
    List<Schedule> getByPatientInterval(@Param("patient") Patient patient,
                                @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Select("SELECT * FROM schedule " +
            "WHERE date >= #{startDate} AND date <= #{endDate} " +
            "ORDER BY date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "doctor", column = "doctor_id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mapper.DoctorMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "slotSchedules", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mapper.SlotScheduleMapper.getByScheduleId", fetchType = FetchType.LAZY))

    })
    List<Schedule> getByInterval(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);


    @Delete("DELETE FROM schedule WHERE date >= #{firedDate} AND doctor_id=#{id}")
    void delete(Doctor doctor);

    @Delete("DELETE FROM schedule")
    void deleteAll();

}
