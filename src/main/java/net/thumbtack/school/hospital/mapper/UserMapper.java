package net.thumbtack.school.hospital.mapper;

import net.thumbtack.school.hospital.model.User;
import org.apache.ibatis.annotations.*;

public interface UserMapper {

    @Insert("INSERT INTO user (firstname, lastname, patronymic, login, password) " +
            "VALUES (#{firstName}, #{lastName}, #{patronymic}, #{login}, #{password})")
    @Options(useGeneratedKeys = true)
    Integer insert(User user);

    @Select("SELECT * FROM user " +
            "WHERE id=#{id}")
    User getById(int id);
}
