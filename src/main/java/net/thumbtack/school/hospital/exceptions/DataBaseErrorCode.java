package net.thumbtack.school.hospital.exceptions;

public enum DataBaseErrorCode {

    LOGIN_EXISTS("User with this login exists", "login"),
    LOGIN_PASSWORD_NOT_EXIST("User with these login and password does not exist", "authentication"),
    PASSWORD_WRONG("Password is wrong", "oldPassword"),
    SPECIALITY_INVALID("This speciality does not exist", "speciality"),
    ROOM_INVALID("This room does not exist", "room"),
    ROOM_BUSY("This room is busy by another doctor", "room"),
    ROOM_NOT_BELONG("This room does not belong to any doctor of commission", "room"),
    AVAILABLE_DOCTORS_NOT_EXIST("There is no available doctor on this speciality", "appointment"),
    DOCTOR_ID_NOT_EXIST("Doctor with id does not exist", "doctorId"),
    PATIENT_ID_NOT_EXIST("Patient with this id does not exist", "patientId"),
    DOCTOR_IDS_NOT_EXIST("Doctors with some doctorIds do not exist", "doctorIds"),
    DOCTOR_SPECIALITY_NOT_EXIST("There is not any doctor on this speciality", "speciality"),
    DOCTORS_NOT_EXIST("There is not any doctor in database", "doctor"),
    DOCTOR_FIRED("Date/date interval is inappropriate. This doctor will be fired", "doctor"),
    DOCTOR_ALREADY_FIRED("This doctor has been already fired", "doctor"),
    SLOT_TIME_BUSY("This/these date/dates and time are busy", "schedule"),
    SLOT_TIME_FOR_DOCTOR_NOT_EXIST("The doctor has not appointment at this date and time", "appointment"),
    PATIENT_TIME_BUSY("Patient has appointment on this date and time", "appointment"),
    PATIENT_APPOINT_EXIST("Patient has appointment to this doctor on this date", "appointment"),
    COMMISSION_EXIST("Doctors and/or patient have commission on this date, time", "commission"),
    SIZE_COMMISSION_INVALID("Commission should contains more than 1 doctor", "commission"),
    TICKET_APPOINT_NUMBER_NOT_EXIST("Ticket of appointment with this number does not exist", "ticketNumber"),
    DOCTOR_HAS_NOT_TICKET("Doctor has not ticket with this number", "ticketNumber"),
    PATIENT_HAS_NOT_TICKET("Patient has not ticket with this number", "ticketNumber"),
    JAVASESSIONID_NOT_EXIST("Wrong JAVASESIONID", "cookie"),
    OPERATION_NOT_ALLOWED("You do not have rights for this operation", "privacy"),
    OTHER_ERROR("Internal error", "undefined error");

    private String errorCode;
    private String field;

    private DataBaseErrorCode(String errorCode, String field) {
        this.errorCode = errorCode;
        this.field = field;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getField() {
        return field;
    }
}
