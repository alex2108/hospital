package net.thumbtack.school.hospital.exceptions;

public class DataBaseException extends Exception {
    private DataBaseErrorCode errorCode;

    public DataBaseException(DataBaseErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public DataBaseErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(DataBaseErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return errorCode.getErrorCode();
    }

    public String getErrorCodeName() {
        return errorCode.name();
    }

    public String getField() {
        return errorCode.getField();
    }
}
