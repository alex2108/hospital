package net.thumbtack.school.hospital;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.utils.MyBatisUtils;
import net.thumbtack.school.hospital.utils.PropertiesUtils;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static net.thumbtack.school.hospital.service.CommonService.formatterTime;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestAcceptance {
    private RestTemplate template = new RestTemplate();

    @Autowired
    private ObjectMapper mapper;

    private static boolean setUpIsDone = false;
    private static String port;

    @BeforeAll()
    public static void setUp() {
        if (!setUpIsDone) {
            if (!MyBatisUtils.initSqlSessionFactory()) {
                throw new RuntimeException("Can't create connection, stop");
            }
            if (!PropertiesUtils.initProperties()) {
                throw new RuntimeException("Can't read configuration file, stop");
            }
            port = PropertiesUtils.getProperties().getProperty("server.port");
            setUpIsDone = true;
        }
    }

    @BeforeEach
    public void clearDataBase() {
        template.postForLocation("http://localhost:" + port + "/api/debug/clear", null);
    }

    @Test
    public void testRegisterAdmin() {
        String cookie = loginDefaultAdmin();
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("Иванов");
        regAdmin.setPatronymic("Иванович");
        regAdmin.setLogin("ivanИв123");
        regAdmin.setPassword("ivan3454646");
        regAdmin.setPosition("Главный администратор");

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity<RegAdminDtoRequest> requestEntity = new HttpEntity(regAdmin, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/admins", HttpMethod.POST,
                requestEntity, AdminDtoResponse.class);
        AdminDtoResponse admin = (AdminDtoResponse) response.getBody();
        assertNotEquals(0, admin.getId());
        checkAdminResponse(regAdmin, admin);
    }

    @Test
    public void testRegisterDoctor() {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivanИв123");
        regDoctorReq.setPassword("ivan3454646");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(6).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        DoctorDtoResponse doctor = registerDoctor("ivanИв123", "ivan3454646", "110");
        assertAll(
                () -> assertNotEquals(0, doctor.getId()),
                () -> assertEquals(3, doctor.getSchedule().size())
        );
        checkDoctorResponse(regDoctorReq, doctor);
    }

    @Test
    public void testRegisterAdminError() throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("");
        regAdmin.setLogin("ivan***");
        regAdmin.setPassword("ivan");
        regAdmin.setPosition("Главный администратор");

        String cookie = loginDefaultAdmin();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity<RegAdminDtoRequest> requestEntity = new HttpEntity(regAdmin, requestHeaders);
        try {
            template.exchange("http://localhost:" + port + "/api/admins", HttpMethod.POST,
                    requestEntity, AdminDtoResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "lastName", ValidationErrorCode.NAME_INVALID.getErrorCode());
            errorsExp.add(ValidationErrorCode.LOGIN_INVALID.name(), "login", ValidationErrorCode.LOGIN_INVALID.getErrorCode());
            errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "password", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(400, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
    }

    @Test
    public void testRegisterPatient() {
        RegPatientDtoRequest regRequest = new RegPatientDtoRequest();
        regRequest.setFirstName("Иван");
        regRequest.setLastName("Иванов");
        regRequest.setPatronymic("Иванович");
        regRequest.setLogin("ivan");
        regRequest.setPassword("ivan123563");
        regRequest.setEmail("ivan@mail.ru");
        regRequest.setAddress("ул. Строителей, д. 3, кв. 5");
        regRequest.setPhone("+79041234567");

        HttpEntity<RegPatientDtoRequest> httpEntity = new HttpEntity(regRequest, new HttpHeaders());
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/patients", HttpMethod.POST,
                httpEntity, PatientDtoResponse.class);
        HttpHeaders headers = response.getHeaders();
        String cookie = headers.getFirst(headers.SET_COOKIE);

        PatientDtoResponse patient = (PatientDtoResponse) response.getBody();
        assertAll(
                () -> assertNotEquals(0, patient.getId()),
                () -> assertTrue(cookie.contains(SessionService.getCookieName())),
                () -> assertTrue(cookie.length() > SessionService.getCookieName().length() + 1)
        );
        checkPatientResponse(regRequest, patient);
    }

    @Test
    public void testLoginLogout() {
        String cookie = registerPatient("ivan", "ivan123563");
        LoginDtoResponse patient1 = getUserInfo(cookie);
        logout(cookie);
        String cookieNew1 = login("ivan", "ivan123563");
        LoginDtoResponse patient2 = getUserInfo(cookieNew1);
        String cookieNew2 = login("ivan", "ivan123563");
        LoginDtoResponse patient3 = getUserInfo(cookieNew2);
        assertAll(
                () -> assertNotEquals(cookie, cookieNew1),
                () -> assertNotEquals(cookie, cookieNew2),
                () -> assertNotEquals(cookieNew1, cookieNew2),
                () -> assertEquals(patient1.getId(), patient2.getId()),
                () -> assertEquals(patient1.getId(), patient3.getId())
        );
    }

    @Test
    public void addTicket() {
        DoctorDtoResponse doctor = registerDoctor("ivanИв123", "ivan123456", "110");
        String cookiePatient = registerPatient("ivan", "ivan123563");

        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctor.getId());
        appointReq.setDate(doctor.getSchedule().get(0).getDate());
        appointReq.setTime("10:00");
        AppointDtoResponse ticket = makeAppointment(appointReq, cookiePatient);
        assertAll(
                () -> assertTrue(ticket.getTicket().matches("D" + doctor.getId() + "\\d{8}1000")),
                () -> assertEquals(doctor.getId(), ticket.getDoctorId())
        );
    }

    @Test
    public void addTicketFailOneDoctorOneDay() throws Exception {
        DoctorDtoResponse doctor = registerDoctor("ivanИв123", "ivan1234567", "110");
        String cookiePatient = registerPatient("ivan", "ivan123563");

        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctor.getId());
        appointReq.setDate(doctor.getSchedule().get(0).getDate());
        appointReq.setTime("10:00");
        makeAppointment(appointReq, cookiePatient);
        try {
            AppointDtoRequest appointReq2 = new AppointDtoRequest();
            appointReq2.setDoctorId(doctor.getId());
            appointReq2.setDate(doctor.getSchedule().get(0).getDate());
            appointReq2.setTime("09:00");
            makeAppointment(appointReq2, cookiePatient);
            fail();
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(DataBaseErrorCode.PATIENT_APPOINT_EXIST.name(),
                    DataBaseErrorCode.PATIENT_APPOINT_EXIST.getField(), DataBaseErrorCode.PATIENT_APPOINT_EXIST.getErrorCode());
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(400, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
    }

    @Test
    public void addCommission() {
        DoctorDtoResponse doctor1 = registerDoctor("ivan", "ivan3454646", "110");
        DoctorDtoResponse doctor2 = registerDoctor("petr", "ivan3454646", "111");
        String cookiePatient = registerPatient("semen", "semen123563");
        LoginDtoResponse patient = getUserInfo(cookiePatient);
        String cookieDoc1 = login("ivan", "ivan3454646");

        CommissionDtoRequest commissionReq = new CommissionDtoRequest();
        commissionReq.setPatientId(patient.getId());
        commissionReq.setDoctorIds(Arrays.asList(doctor1.getId(), doctor2.getId()));
        commissionReq.setRoom("110");
        commissionReq.setDate(LocalDate.now().plusDays(10).format(formatterDate));
        commissionReq.setTime("10:30");
        commissionReq.setDuration(20);
        CommissionDtoResponse commission = makeCommission(commissionReq, cookieDoc1);
        assertTrue(commission.getTicket()
                .matches("CD" + doctor1.getId() + "D" + doctor2.getId() + "\\d{8}1030"));
        checkCommissionResponse(commissionReq, commission);
    }

    @Test
    public void addCommissionFailBusy() throws Exception {
        DoctorDtoResponse doctor1 = registerDoctor("ivan", "ivan3454646", "110");
        DoctorDtoResponse doctor2 = registerDoctor("petr", "ivan3454646", "111");
        String cookiePatient1 = registerPatient("semen", "semen123563");

        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctor1.getId());
        appointReq.setDate(doctor1.getSchedule().get(0).getDate());
        appointReq.setTime("10:00");
        makeAppointment(appointReq, cookiePatient1);

        String cookiePatient2 = registerPatient("svetlana", "svetlana123563");
        LoginDtoResponse infoPatient2 = getUserInfo(cookiePatient2);
        String cookieDoc1 = login("ivan", "ivan3454646");
        CommissionDtoRequest commissionReq = new CommissionDtoRequest();
        commissionReq.setPatientId(infoPatient2.getId());
        commissionReq.setDoctorIds(Arrays.asList(doctor1.getId(), doctor2.getId()));
        commissionReq.setRoom("110");
        commissionReq.setDate(doctor1.getSchedule().get(0).getDate());
        commissionReq.setTime("10:29");
        commissionReq.setDuration(20);

        try {
            makeCommission(commissionReq, cookieDoc1);
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(DataBaseErrorCode.SLOT_TIME_BUSY.name(),
                    DataBaseErrorCode.SLOT_TIME_BUSY.getField(), DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode());
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(400, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
    }

    @Test
    public void complexUpdateTicketCommission() throws Exception {
        DoctorDtoResponse doctor1 = registerDoctor("ivanИв123", "ivan123456", "110");
        DoctorDtoResponse doctor2 = registerDoctor("maria", "maria123456", "111");
        String cookiePatient1 = registerPatient("ivan", "ivan123563");
        String cookiePatient2 = registerPatient("petr", "ivan123563");

        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctor1.getId());
        appointReq.setDate(doctor1.getSchedule().get(0).getDate());
        appointReq.setTime("10:00");
        AppointDtoResponse ticket1 = makeAppointment(appointReq, cookiePatient1);

        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusDays(6).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("12:00");
        weekSchedule.setTimeEnd("15:00");
        weekSchedule.setWeekDays(Arrays.asList("Tue", "Thu"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(20);
        DoctorDtoResponse doctorUpd = updateSchedule(updateReq, doctor1.getId());

        AppointDtoRequest appointReq2 = new AppointDtoRequest();
        appointReq2.setDoctorId(doctor1.getId());
        appointReq2.setDate(doctorUpd.getSchedule().get(1).getDate());
        appointReq2.setTime(doctorUpd.getSchedule().get(1).getDaySchedule().get(0).getTime());
        AppointDtoResponse ticket2 = makeAppointment(appointReq2, cookiePatient2);
        assertTrue(ticket2.getTicket().matches("D" + doctor1.getId() + "\\d{12}"));

        HttpStatus status = deleteAppointment(cookiePatient2, ticket2.getTicket());
        assertEquals(status, HttpStatus.OK);

        AppointDtoResponse ticket3 = makeAppointment(appointReq2, cookiePatient2);
        assertEquals(ticket2, ticket3);

        LoginDtoResponse infoPatient2 = getUserInfo(cookiePatient2);
        String cookieDoc2 = login("maria", "maria123456");
        CommissionDtoRequest commissionReq = new CommissionDtoRequest();
        commissionReq.setPatientId(infoPatient2.getId());
        commissionReq.setDoctorIds(Arrays.asList(doctor1.getId(), doctor2.getId()));
        commissionReq.setRoom("110");
        commissionReq.setDate(doctorUpd.getSchedule().get(2).getDate());
        String timeCommission = LocalTime.parse(doctorUpd.getSchedule().get(2).getDaySchedule().get(1).getTime())
                .minusMinutes(1).format(formatterTime);
        commissionReq.setTime(timeCommission);
        commissionReq.setDuration(20);
        CommissionDtoResponse commission = makeCommission(commissionReq, cookieDoc2);
        assertTrue(commission.getTicket()
                .matches("CD" + doctor1.getId() + "D" + doctor2.getId() + "\\d{12}"));
        checkCommissionResponse(commissionReq, commission);

        assertEquals(HttpStatus.OK, deleteAppointment(cookiePatient1, ticket1.getTicket()));
        try {
            AppointDtoRequest appointReq3 = new AppointDtoRequest();
            appointReq3.setDoctorId(doctor1.getId());
            appointReq3.setDate(doctorUpd.getSchedule().get(2).getDate());
            appointReq3.setTime(doctorUpd.getSchedule().get(2).getDaySchedule().get(0).getTime());
            makeAppointment(appointReq3, cookiePatient1);
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(DataBaseErrorCode.SLOT_TIME_BUSY.name(),
                    DataBaseErrorCode.SLOT_TIME_BUSY.getField(), DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode());
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(400, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
    }

    @Test
    public void updateScheduleFailBusy() throws Exception {
        DoctorDtoResponse doctor = registerDoctor("ivanИв123", "ivan123456", "110");
        String cookiePatient = registerPatient("ivan", "ivan123563");

        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctor.getId());
        appointReq.setDate(doctor.getSchedule().get(0).getDate());
        appointReq.setTime("10:00");
        makeAppointment(appointReq, cookiePatient);

        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusDays(6).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("12:00");
        weekSchedule.setTimeEnd("15:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(20);
        try {
            updateSchedule(updateReq, doctor.getId());
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(DataBaseErrorCode.SLOT_TIME_BUSY.name(),
                    DataBaseErrorCode.SLOT_TIME_BUSY.getField(), DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode());
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(400, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
    }

    @Test
    public void deleteCommission() throws Exception {
        DoctorDtoResponse doctor1 = registerDoctor("ivan", "ivan3454646", "110");
        DoctorDtoResponse doctor2 = registerDoctor("petr", "ivan3454646", "111");
        String cookiePatient = registerPatient("semen", "semen123563");
        LoginDtoResponse patient = getUserInfo(cookiePatient);
        String cookieDoc1 = login("ivan", "ivan3454646");

        CommissionDtoRequest commissionReq = new CommissionDtoRequest();
        commissionReq.setPatientId(patient.getId());
        commissionReq.setDoctorIds(Arrays.asList(doctor1.getId(), doctor2.getId()));
        commissionReq.setRoom("110");
        commissionReq.setDate(LocalDate.now().plusDays(10).format(formatterDate));
        commissionReq.setTime("10:30");
        commissionReq.setDuration(20);
        CommissionDtoResponse commission = makeCommission(commissionReq, cookieDoc1);
        try {
            deleteCommission(cookieDoc1, commission.getTicket());
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(), DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                    DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(400, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
        assertEquals(HttpStatus.OK, deleteCommission(cookiePatient, commission.getTicket()));
    }

    @Test
    public void deleteDoctor() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan123456");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(14).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);
        String cookieAdmin = loginDefaultAdmin();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookieAdmin);
        HttpEntity<RegDoctorDtoRequest> requestEntity = new HttpEntity(regDoctorReq, requestHeaders);
        ResponseEntity response1 = template.exchange("http://localhost:" + port + "/api/doctors", HttpMethod.POST,
                requestEntity, DoctorDtoResponse.class);
        DoctorDtoResponse doctor1 = (DoctorDtoResponse) response1.getBody();
        DoctorDtoResponse doctor2 = registerDoctor("semen", "semen123456", "111");

        String cookiePatient = registerPatient("petr", "petr123563");
        LoginDtoResponse patient = getUserInfo(cookiePatient);

        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctor1.getId());
        appointReq.setDate(doctor1.getSchedule().get(4).getDate());
        appointReq.setTime("10:00");
        AppointDtoResponse appointment = makeAppointment(appointReq, cookiePatient);

        String cookieDoctor1 = login("ivan", "ivan123456");
        CommissionDtoRequest commissionReq = new CommissionDtoRequest();
        commissionReq.setPatientId(patient.getId());
        commissionReq.setDoctorIds(Arrays.asList(doctor1.getId(), doctor2.getId()));
        commissionReq.setRoom("110");
        commissionReq.setDate(LocalDate.now().plusDays(10).format(formatterDate));
        commissionReq.setTime("10:30");
        commissionReq.setDuration(20);
        CommissionDtoResponse commission = makeCommission(commissionReq, cookieDoctor1);

        ResponseEntity respTickets = getTickets(cookiePatient);
        TicketDtoResponse[] tickets = (TicketDtoResponse []) respTickets.getBody();
        List<String> ticketsNumber = Arrays.asList(tickets[0].getTicket(), tickets[1].getTicket());

        DeleteDoctorDtoRequest deleteReq = new DeleteDoctorDtoRequest(doctor1.getSchedule().get(4).getDate());
        HttpStatus statusDelete = deleteDoctor(doctor1.getId(), deleteReq);

        ResponseEntity respTicketsNo = getTickets(cookiePatient);
        TicketDtoResponse[] ticketsNo = (TicketDtoResponse[]) respTicketsNo.getBody();
        assertAll(
                () -> assertEquals(2, tickets.length),
                () -> assertTrue(ticketsNumber.contains(appointment.getTicket())),
                () -> assertTrue(ticketsNumber.contains(commission.getTicket())),
                () -> assertEquals(HttpStatus.OK, statusDelete),
                () -> assertEquals(0, ticketsNo.length)
        );
    }

    @Test
    public void testError404() throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("Иванoв");
        regAdmin.setLogin("ivan");
        regAdmin.setPassword("ivan123456");
        regAdmin.setPosition("Главный администратор");

        String cookie = loginDefaultAdmin();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity<RegAdminDtoRequest> requestEntity = new HttpEntity(regAdmin, requestHeaders);
        try {
            template.exchange("http://localhost:" + port + "/api/admns", HttpMethod.POST,
                    requestEntity, AdminDtoResponse.class);
            fail();
        } catch (HttpClientErrorException exc) {
            ResponseError errorsExp = new ResponseError();
            errorsExp.add(ValidationErrorCode.URL_INVALID.name(), "URL", ValidationErrorCode.URL_INVALID.getErrorCode()+"POST /api/admns");
            ResponseError errorsAct = mapper.readValue(exc.getResponseBodyAsString(), ResponseError.class);
            assertAll(
                    () -> assertEquals(404, exc.getStatusCode().value()),
                    () -> assertEquals(errorsExp, errorsAct)
            );
        }
    }

    private DoctorDtoResponse registerDoctor(String login, String password, String room) {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin(login);
        regDoctorReq.setPassword(password);
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom(room);
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(6).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        String cookie = loginDefaultAdmin();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity<RegDoctorDtoRequest> requestEntity = new HttpEntity(regDoctorReq, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/doctors", HttpMethod.POST,
                requestEntity, DoctorDtoResponse.class);
        return (DoctorDtoResponse) response.getBody();
    }

    private DoctorDtoResponse updateSchedule(UpdateDoctorDtoRequest request, int doctorId) {
        String cookieAdmin = loginDefaultAdmin();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookieAdmin);
        HttpEntity<UpdateDoctorDtoRequest> requestEntity = new HttpEntity(request, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/doctors/" + doctorId, HttpMethod.PUT,
                requestEntity, DoctorDtoResponse.class);
        return (DoctorDtoResponse) response.getBody();
    }

    private HttpStatus deleteDoctor(int doctorId, DeleteDoctorDtoRequest request) {
        String cookieAdmin = loginDefaultAdmin();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookieAdmin);
        HttpEntity requestEntity = new HttpEntity(request, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/doctors/" + doctorId, HttpMethod.DELETE,
                requestEntity, Object.class);
        return response.getStatusCode();
    }

    private String registerPatient(String login, String password) {
        RegPatientDtoRequest regRequest = new RegPatientDtoRequest();
        regRequest.setFirstName("Иван");
        regRequest.setLastName("Иванов");
        regRequest.setPatronymic("Иванович");
        regRequest.setLogin(login);
        regRequest.setPassword(password);
        regRequest.setEmail("ivan@mail.ru");
        regRequest.setAddress("ул. Строителей, д. 3, кв. 5");
        regRequest.setPhone("+79041234567");

        HttpEntity<RegPatientDtoRequest> httpEntity = new HttpEntity(regRequest, new HttpHeaders());
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/patients", HttpMethod.POST,
                httpEntity, PatientDtoResponse.class);
        HttpHeaders headers = response.getHeaders();
        return headers.getFirst(headers.SET_COOKIE);
    }

    private LoginDtoResponse getUserInfo(String cookie) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/account", HttpMethod.GET,
                requestEntity, LoginDtoResponse.class);
        return (LoginDtoResponse) response.getBody();
    }


    private String loginDefaultAdmin() {
        HttpEntity<LoginDtoRequest> loginEntity = new HttpEntity(new LoginDtoRequest("administrator", "administrator"), new HttpHeaders());
        ResponseEntity respLogin = template.exchange("http://localhost:" + port + "/api/sessions", HttpMethod.POST,
                loginEntity, LoginDtoResponse.class);
        HttpHeaders headers = respLogin.getHeaders();
        return headers.getFirst(headers.SET_COOKIE);
    }

    private String login(String login, String password) {
        HttpEntity<LoginDtoRequest> loginEntity = new HttpEntity(new LoginDtoRequest(login, password), new HttpHeaders());
        ResponseEntity respLogin = template.exchange("http://localhost:" + port + "/api/sessions", HttpMethod.POST,
                loginEntity, LoginDtoResponse.class);
        HttpHeaders headers = respLogin.getHeaders();
        return headers.getFirst(headers.SET_COOKIE);
    }

    private void logout(String cookie) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookie);
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        template.exchange("http://localhost:" + port + "/api/sessions", HttpMethod.DELETE,
                requestEntity, LoginDtoResponse.class);
    }

    private AppointDtoResponse makeAppointment(AppointDtoRequest request, String cookiePatient) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookiePatient);
        HttpEntity<AppointDtoRequest> requestEntity = new HttpEntity(request, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/tickets", HttpMethod.POST,
                requestEntity, AppointDtoResponse.class);
        return (AppointDtoResponse) response.getBody();
    }

    private HttpStatus deleteAppointment(String cookiePatient, String ticketNumber) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookiePatient);
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/tickets/" + ticketNumber, HttpMethod.DELETE,
                requestEntity, Object.class);
        return response.getStatusCode();
    }

    private CommissionDtoResponse makeCommission(CommissionDtoRequest request, String cookieDoctor) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookieDoctor);
        HttpEntity<CommissionDtoRequest> requestEntity = new HttpEntity(request, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/commissions", HttpMethod.POST,
                requestEntity, CommissionDtoResponse.class);
        return  (CommissionDtoResponse) response.getBody();
    }

    private HttpStatus deleteCommission(String cookiePatient, String ticketNumber) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", cookiePatient);
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity response = template.exchange("http://localhost:" + port + "/api/commissions/" + ticketNumber, HttpMethod.DELETE,
                requestEntity, Object.class);
        return response.getStatusCode();
    }

    private ResponseEntity getTickets(String cookiePatient) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", cookiePatient);
        HttpEntity reqGetTicketsEntity = new HttpEntity(null, httpHeaders);
        return template.exchange("http://localhost:" + port + "/api/tickets", HttpMethod.GET,
                reqGetTicketsEntity, TicketDtoResponse[].class);
    }


    private void checkAdminResponse(RegAdminDtoRequest request, AdminDtoResponse response) {
        assertAll(
                () -> assertEquals(request.getFirstName(), response.getFirstName()),
                () -> assertEquals(request.getLastName(), response.getLastName()),
                () -> assertEquals(request.getPatronymic(), response.getPatronymic()),
                () -> assertEquals(request.getPosition(), response.getPosition())
        );
    }

    private void checkDoctorResponse(RegDoctorDtoRequest request, DoctorDtoResponse response) {
        assertAll(
                () -> assertEquals(request.getFirstName(), response.getFirstName()),
                () -> assertEquals(request.getLastName(), response.getLastName()),
                () -> assertEquals(request.getPatronymic(), response.getPatronymic()),
                () -> assertEquals(request.getRoom(), response.getRoom()),
                () -> assertEquals(request.getSpeciality(), response.getSpeciality())
        );
    }

    private void checkPatientResponse(RegPatientDtoRequest request, PatientDtoResponse response) {
        assertAll(
                () -> assertEquals(request.getFirstName(), response.getFirstName()),
                () -> assertEquals(request.getLastName(), response.getLastName()),
                () -> assertEquals(request.getPatronymic(), response.getPatronymic()),
                () -> assertEquals(request.getAddress(), response.getAddress()),
                () -> assertEquals(request.getEmail(), response.getEmail()),
                () -> assertEquals(request.getPhone(), response.getPhone())
        );
    }

    private void checkCommissionResponse(CommissionDtoRequest request, CommissionDtoResponse response) {
        assertAll(
                () -> assertEquals(request.getPatientId(), response.getPatientId()),
                () -> assertEquals(request.getDoctorIds(), response.getDoctorIds()),
                () -> assertEquals(request.getRoom(), response.getRoom()),
                () -> assertEquals(request.getDate(), response.getDate()),
                () -> assertEquals(request.getTime(), response.getTime()),
                () -> assertEquals(request.getDuration(), response.getDuration())
        );
    }

}
