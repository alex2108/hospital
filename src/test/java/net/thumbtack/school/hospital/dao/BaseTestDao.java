package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.daoimpl.*;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.utils.MyBatisUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BaseTestDao {

    protected AdminDao adminDao = new AdminDaoImpl();
    protected DoctorDao doctorDao = new DoctorDaoImpl();
    protected PatientDao patientDao = new PatientDaoImpl();
    protected ScheduleDao scheduleDao = new ScheduleDaoImpl();
    protected TicketDao ticketDao = new TicketDaoImpl();
    protected SessionDao sessionDao = new SessionDaoImpl();
    protected CommissionDao commissionDao = new CommissionDaoImpl();

    protected static boolean setUpIsDone = false;
    protected static Comparator<Doctor> doctorComparatorByName =
            (doc1, doc2)-> doc1.getFirstName().compareTo(doc2.getFirstName());
    protected static Comparator<Doctor> doctorComparatorById =
            (doc1, doc2) -> doc1.getId() - doc2.getId();
    protected static Comparator<SlotSchedule> slotComparator =
            (slot1, slot2) -> slot1.getTime().compareTo(slot2.getTime());
    protected static Comparator<Schedule> scheduleComparator =
            (schedule1, schedule2) -> schedule1.getDate().compareTo(schedule2.getDate());

    @BeforeAll()
    public static void setUp() {
        if (!setUpIsDone) {
            boolean initSqlSessionFactory = MyBatisUtils.initSqlSessionFactory();
            if (!initSqlSessionFactory) {
                throw new RuntimeException("Can't create connection, stop");
            }
            setUpIsDone = true;
        }
    }

    @BeforeEach
    public void clearAll() throws DataBaseException {
        adminDao.deleteAll();
        doctorDao.deleteAll();
        patientDao.deleteAll();
        scheduleDao.deleteAll();
        ticketDao.deleteAll();
        sessionDao.deleteAll();
    }

    @Test
    public void testInsertComplex1() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Doctor doctor = new Doctor("Петр", "Петров", "Петрович",
                "ivan", "ivan345", "отоларинголог", "110");
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "ivan", "petr123", "petr@mail.ru", "ул. Южная, д. 3, кв. 5", "+79042345678");

        adminDao.insert(admin);
        Throwable throwDoc = assertThrows(DataBaseException.class,
                () -> doctorDao.insert(doctor));
        Throwable throwPat = assertThrows(DataBaseException.class,
                () -> patientDao.insert(patient));
        assertAll(
                () -> assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwDoc.getMessage()),
                () -> assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwPat.getMessage())
        );
    }

    @Test
    public void testInsertComplex2() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Doctor doctor = new Doctor("Петр", "Петров", "Петрович",
                "ivan", "ivan345", "отоларинголог", "110");
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "ivan", "petr123", "petr@mail.ru", "ул. Южная, д. 3, кв. 5", "+79042345678");

        doctorDao.insert(doctor);
        Throwable throwAdmin = assertThrows(DataBaseException.class,
                () -> adminDao.insert(admin));
        Throwable throwPat = assertThrows(DataBaseException.class,
                () -> patientDao.insert(patient));
        assertAll(
                () -> assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwAdmin.getMessage()),
                () -> assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwPat.getMessage())
        );
    }

    @Test
    public void testInsertComplex3() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Doctor doctor = new Doctor("Петр", "Петров", "Петрович",
                "ivan", "ivan345", "отоларинголог", "110");
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "ivan", "petr123", "petr@mail.ru", "ул. Южная, д. 3, кв. 5", "+79042345678");

        patientDao.insert(patient);
        Throwable throwAdmin = assertThrows(DataBaseException.class,
                () -> adminDao.insert(admin));
        Throwable throwDoc = assertThrows(DataBaseException.class,
                () -> doctorDao.insert(doctor));
        assertAll(
                () -> assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwAdmin.getMessage()),
                () -> assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwDoc.getMessage())
        );
    }

    public static void checkDoctorFields(Doctor doctor1, Doctor doctor2) {
        assertAll(
                () -> assertEquals(doctor1.getId(), doctor2.getId()),
                () -> assertEquals(doctor1.getFirstName(), doctor2.getFirstName()),
                () -> assertEquals(doctor1.getLastName(), doctor2.getLastName()),
                () -> assertEquals(doctor1.getPatronymic(), doctor2.getPatronymic()),
                () -> assertEquals(doctor1.getLogin(), doctor2.getLogin()),
                () -> assertEquals(doctor1.getPassword(), doctor2.getPassword()),
                () -> assertEquals(doctor1.getRoom(), doctor2.getRoom()),
                () -> assertEquals(doctor1.getSpeciality(), doctor2.getSpeciality())
        );
        for (int i = 0; i < doctor1.getSchedules().size(); i++) {
            assertEquals(doctor1.getSchedules().get(i).getId(), doctor2.getSchedules().get(i).getId());
            assertEquals(doctor1.getSchedules().get(i).getDate(), doctor2.getSchedules().get(i).getDate());
            assertEquals(doctor1.getSchedules().get(i).getSlotSchedules(), doctor2.getSchedules().get(i).getSlotSchedules());
        }
    }

    public static void checkScheduleFields(Schedule schedule1, Schedule schedule2) {
        assertAll(
                () -> assertEquals(schedule1.getId(), schedule2.getId()),
                () -> assertEquals(schedule1.getDate(), schedule2.getDate()),
                () -> assertEquals(schedule1.getSlotSchedules(), schedule2.getSlotSchedules()),
                () -> assertEquals(schedule1.getDoctor().getId(), schedule2.getDoctor().getId()),
                () -> assertEquals(schedule1.getDoctor().getFirstName(), schedule2.getDoctor().getFirstName()),
                () -> assertEquals(schedule1.getDoctor().getLastName(), schedule2.getDoctor().getLastName()),
                () -> assertEquals(schedule1.getDoctor().getPatronymic(), schedule2.getDoctor().getPatronymic()),
                () -> assertEquals(schedule1.getDoctor().getLogin(), schedule2.getDoctor().getLogin()),
                () -> assertEquals(schedule1.getDoctor().getPassword(), schedule2.getDoctor().getPassword()),
                () -> assertEquals(schedule1.getDoctor().getRoom(), schedule2.getDoctor().getRoom()),
                () -> assertEquals(schedule1.getDoctor().getSpeciality(), schedule2.getDoctor().getSpeciality())
        );
    }

    public static void checkCommissionFields(Commission commission1, Commission commission2) {
        assertAll(
                () -> assertEquals(commission1.getId(), commission1.getId()),
                () -> assertEquals(commission1.getTicket(), commission2.getTicket()),
                () -> assertEquals(commission1.getRoom(), commission2.getRoom()),
                () -> assertEquals(commission1.getDate(), commission2.getDate()),
                () -> assertEquals(commission1.getTime(), commission2.getTime()),
                () -> assertEquals(commission1.getDuration(), commission2.getDuration())
        );
        List<Doctor> doctors1 = commission1.getDoctors().stream().sorted(doctorComparatorById)
                .collect(Collectors.toList());
        List<Doctor> doctors2 = commission1.getDoctors().stream().sorted(doctorComparatorById)
                .collect(Collectors.toList());
        for (int i = 0; i < doctors1.size(); i++) {
            checkDoctorFields(doctors1.get(i), doctors2.get(i));
        }

    }

}
