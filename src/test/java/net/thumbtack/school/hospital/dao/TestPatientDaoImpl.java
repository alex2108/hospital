package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Patient;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestPatientDaoImpl extends BaseTestDao {

    @Test
    public void testInsertPatient() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);
        assertAll(
                () -> assertNotEquals(0, patient.getId()),
                () -> assertEquals(patient, patientDao.getById(patient.getId()))
        );
    }

    @Test
    public void testDoubleInsertPatient() throws DataBaseException {
        Patient patient1 = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Patient patient2 = new Patient("Петр", "Петров", "Петрович",
                "ivan", "petr123", "petr@mail.ru", "ул. Южная, д. 3, кв. 5", "+79042345678");
        patientDao.insert(patient1);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> patientDao.insert(patient2));
        assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetPatientByLogin() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "iВan", "ivAN123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        assertAll(
                () -> assertEquals(patient, patientDao.get("iВan", "ivAN123")),
                () -> assertEquals(patient, patientDao.get("iвan", "ivAN123")),
                () -> assertEquals(patient, patientDao.get("Iвan", "ivAN123")),
                () -> assertNull(patientDao.get("petr", "ivAN123")),
                () -> assertNull(patientDao.get("ivan", "ivan345")),
                () -> assertNull(patientDao.get("ivan", "ivan123"))
        );
    }

    @Test
    public void testUpdatePatient() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);
        patient.setFirstName("Петр");
        patient.setLastName("Петров");
        patient.setPatronymic("Петрович");
        patient.setPassword("123");
        patient.setEmail("petr@mail.ru");
        patient.setAddress("ул. Главная, д. 56, кв. 4");
        patient.setPhone("+79312546780");

        patientDao.update(patient);
        assertAll(
                () -> assertEquals(patient, patientDao.getById(patient.getId())),
                () -> assertNull(patientDao.get("ivan", "ivan123"))
        );
    }

    @Test
    public void testUpdatePatientNullPatronymic() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);
        patient.setFirstName("Петр");
        patient.setLastName("Петров");
        patient.setPassword("123");
        patient.setEmail("petr@mail.ru");
        patient.setAddress("ул. Главная, д. 56, кв. 4");
        patient.setPhone("+79312546780");

        patientDao.update(patient);
        assertAll(
                () -> assertEquals(patient, patientDao.getById(patient.getId())),
                () -> assertNull(patientDao.get("ivan", "ivan123"))
        );
    }
}
