package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestTicketDaoImpl extends BaseTestDao {

    @Test
    public void testGetTicketByNumber() throws DataBaseException
    {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 3, 25),
                Arrays.asList(
                        new SlotSchedule(LocalTime.of(9, 0), 30),
                        new SlotSchedule(LocalTime.of(9, 30), 30)));
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);

        Doctor doctor2 = new Doctor("Семен", "Семенов", "Иванович",
                "semen", "semen123", "хирург", "111");
        Schedule schedule2 = new Schedule(doctor2,
                LocalDate.of(2019, 3, 27),
                Arrays.asList(
                        new SlotSchedule(LocalTime.of(10, 0), 20)));
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);

        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Ticket ticket1 = new Ticket(patient, "D123");
        schedule1.getSlotSchedules().get(1).setTicket(ticket1);
        schedule1.getSlotSchedules().get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(schedule1.getSlotSchedules().get(1));

        Ticket ticket2 = new Ticket(patient, "CD456");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2), ticket2,
                LocalDate.of(2019, 3, 30), LocalTime.of(9, 35), "110", 30);
        commissionDao.insert(commission);

        assertAll(
                () -> assertEquals(ticket1, ticketDao.getAppointmentTicketByNumber("D123")),
                () -> assertEquals(ticket2, ticketDao.getCommissionTicketByNumber("CD456")),
                () -> assertNull(ticketDao.getCommissionTicketByNumber("D123")),
                () -> assertNull(ticketDao.getAppointmentTicketByNumber("CD456"))
        );
    }

    @Test
    public void testDeleteAppointmentTicket() throws DataBaseException
    {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Schedule schedule = new Schedule(doctor, LocalDate.of(2019, 3, 25),
                Arrays.asList(
                        new SlotSchedule(LocalTime.of(9, 0), 30),
                        new SlotSchedule(LocalTime.of(9, 30), 30)));
        doctor.setSchedules(Arrays.asList(schedule));
        doctorDao.insert(doctor);

        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);
        Ticket ticket = new Ticket(patient, "D123");
        schedule.getSlotSchedules().get(1).setTicket(ticket);
        schedule.getSlotSchedules().get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(schedule.getSlotSchedules().get(1));

        ticketDao.deleteAppointment(ticket);
        assertAll(
                () -> assertNull(ticketDao.getById(ticket.getId())),
                () -> assertEquals(SlotScheduleState.VACANT,
                        scheduleDao.getSlotById(schedule.getSlotSchedules().get(1).getId()).getState())
        );
    }

    @Test
    public void testDeleteCommissionTicket() throws DataBaseException {
        LocalDate dateCommission = LocalDate.of(2019, 5, 22);
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 15),
                new SlotSchedule(LocalTime.of(9, 15), 15),
                new SlotSchedule(LocalTime.of(9, 30), 15),
                new SlotSchedule(LocalTime.of(9, 45), 15),
                new SlotSchedule(LocalTime.of(10, 0), 15));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20),
                new SlotSchedule(LocalTime.of(10, 20), 20),
                new SlotSchedule(LocalTime.of(10, 40), 20));
        Schedule schedule3 = new Schedule(doctor3, dateCommission, slots3);
        doctor3.setSchedules(Arrays.asList(schedule3));
        doctorDao.insert(doctor3);

        Patient patient = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient);

        Ticket ticket = new Ticket(patient, "CD123");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2, doctor3), ticket,
                dateCommission, LocalTime.of(9, 35), "110", 30);
        List<SlotSchedule> slotsComission = Arrays.asList(
                new SlotSchedule(slots1.get(1).getId(), LocalTime.of(9, 30), 30),
                new SlotSchedule(slots2.get(2).getId(), LocalTime.of(9, 30), 15),
                new SlotSchedule(slots2.get(3).getId(), LocalTime.of(9, 45), 15),
                new SlotSchedule(slots2.get(4).getId(), LocalTime.of(10, 0), 15),
                new SlotSchedule(slots3.get(0).getId(), LocalTime.of(10, 0), 20));
        commissionDao.insert(commission, slotsComission);

        ticketDao.deleteCommission(ticket, slotsComission);
        assertAll(
                () -> assertNull(commissionDao.getById(commission.getId())),
                () -> assertNull(ticketDao.getById(ticket.getId())),
                () -> assertEquals(SlotScheduleState.VACANT, scheduleDao.getSlotById(slots1.get(1).getId()).getState()),
                () -> assertEquals(SlotScheduleState.VACANT, scheduleDao.getSlotById(slots2.get(2).getId()).getState()),
                () -> assertEquals(SlotScheduleState.VACANT, scheduleDao.getSlotById(slots2.get(3).getId()).getState()),
                () -> assertEquals(SlotScheduleState.VACANT, scheduleDao.getSlotById(slots2.get(4).getId()).getState()),
                () -> assertEquals(SlotScheduleState.VACANT, scheduleDao.getSlotById(slots3.get(0).getId()).getState())
        );
    }

    @Test
    public void testDeleteCommissionTicketNotIntersectSlots() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        doctor1.setSchedules(Arrays.asList(new Schedule(doctor1, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor1);
        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        doctor2.setSchedules(Arrays.asList(new Schedule(doctor2, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor2);

        Patient patient = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient);
        Ticket ticket = new Ticket(patient, "CD123");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2), ticket,
                LocalDate.of(2019, 6, 5), LocalTime.of(9, 35), "110", 30);
        commissionDao.insert(commission);

        ticketDao.delete(ticket);
        assertAll(
                () -> assertNull(commissionDao.getById(commission.getId())),
                () -> assertNull(ticketDao.getById(ticket.getId()))
        );
    }


}
