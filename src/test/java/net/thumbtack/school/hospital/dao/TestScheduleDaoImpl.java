package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class TestScheduleDaoImpl extends BaseTestDao {

    @Test
    public void testInsertSchedules() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30));
        Schedule schedule = new Schedule(doctor, LocalDate.of(2019, 5, 22), slots);
        doctor.setSchedules(Arrays.asList(schedule));
        doctorDao.insert(doctor);

        List<SlotSchedule> newSlots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 30), 30),
                new SlotSchedule(LocalTime.of(12, 0), 30));
        List<SlotSchedule> newSlots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30));
        List<Schedule> newSchedules = Arrays.asList(
                new Schedule(doctor, LocalDate.of(2019, 5, 23), newSlots1),
                new Schedule(doctor, LocalDate.of(2019, 5, 25), newSlots2));
        scheduleDao.insert(newSchedules);

        List<Schedule> schedulesDB = doctorDao.getById(doctor.getId()).getSchedules();
        newSchedules.forEach(sched -> sched.getSlotSchedules().sort(slotComparator));
        checkScheduleFields(schedule, schedulesDB.get(0));
        checkScheduleFields(newSchedules.get(0), schedulesDB.get(1));
        checkScheduleFields(newSchedules.get(1), schedulesDB.get(2));
    }

    @Test
    public void testGetScheduleById() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30)
        );
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2019, 5, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30)
        );
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2019, 5, 22),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));
        doctorDao.insert(doctor);
        Schedule scheduleDB = scheduleDao.getById(schedule2.getId());
        Doctor doctorDB = scheduleDB.getDoctor();
        assertAll(
                () -> assertNotEquals(0, scheduleDB.getId()),
                () -> assertEquals(schedule2.getId(), scheduleDB.getId()),
                () -> assertEquals(schedule2.getDate(), scheduleDB.getDate()),
                () -> assertEquals(schedule2.getSlotSchedules(), scheduleDB.getSlotSchedules()),
                () -> assertNotEquals(0, scheduleDB.getSlotSchedules().get(0).getId()),
                () -> assertNotEquals(0, scheduleDB.getSlotSchedules().get(1).getId()),
                () -> assertNotEquals(0, doctorDB.getId())
        );
        checkDoctorFields(doctor, doctorDB);
    }

    @Test
    public void testUpdateSlotsOk() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2019, 5, 22), slots1);
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 20), 20),
                new SlotSchedule(LocalTime.of(11, 40), 20));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2019, 5, 23), slots2);
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 20),
                new SlotSchedule(LocalTime.of(9, 20), 20));
        Schedule schedule3 = new Schedule(doctor,
                LocalDate.of(2019, 5, 27), slots3);
        List<SlotSchedule> slots4 = Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 20),
                new SlotSchedule(LocalTime.of(15, 20), 20));
        Schedule schedule4 = new Schedule(doctor,
                LocalDate.of(2019, 5, 30), slots4);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2, schedule3, schedule4));
        doctorDao.insert(doctor);

        schedule1.setSlotSchedules(Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 20),
                new SlotSchedule(LocalTime.of(9, 20), 20)));
        schedule2.setSlotSchedules(Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 20),
                new SlotSchedule(LocalTime.of(12, 20), 20)));
        schedule3.setSlotSchedules(Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 30),
                new SlotSchedule(LocalTime.of(15, 30), 30)));
        scheduleDao.update(Arrays.asList(schedule1, schedule2, schedule3));

        checkScheduleFields(schedule1, scheduleDao.getById(schedule1.getId()));
        checkScheduleFields(schedule2, scheduleDao.getById(schedule2.getId()));
        checkScheduleFields(schedule3, scheduleDao.getById(schedule3.getId()));
        checkScheduleFields(schedule4, scheduleDao.getById(schedule4.getId()));
    }


    @Test
    public void testUpdateSlotsBusy() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2019, 5, 22), slots1);
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 20), 20),
                new SlotSchedule(LocalTime.of(11, 40), 20));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2019, 5, 23), slots2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));
        doctorDao.insert(doctor);

        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);
        Ticket ticket = new Ticket(patient, "123");
        slots2.get(0).setTicket(ticket);
        slots2.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots2.get(0));

        schedule1.setSlotSchedules(Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 20),
                new SlotSchedule(LocalTime.of(9, 20), 20)));
        schedule2.setSlotSchedules(Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 20),
                new SlotSchedule(LocalTime.of(12, 20), 20)));
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleDao.update(Arrays.asList(schedule1, schedule2)));
        Schedule scheduleDB1 = scheduleDao.getById(schedule1.getId());
        Schedule scheduleDB2 = scheduleDao.getById(schedule2.getId());
        assertAll(
                () -> assertEquals(DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode(), throwable.getMessage()),
                () -> assertEquals(slots1, scheduleDB1.getSlotSchedules()),
                () -> assertEquals(slots2, scheduleDB2.getSlotSchedules()),
                () -> assertEquals(schedule1.getDate(), scheduleDB1.getDate()),
                () -> assertEquals(schedule2.getDate(), scheduleDB2.getDate())
        );
    }

    @Test
    public void testUpdateSlotBusyByCommission() throws DataBaseException {
        LocalDate dateCommission = LocalDate.of(2019, 5, 22);
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);

        Patient patient = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient);

        Ticket ticket = new Ticket(patient, "123");
        Commission commission = new Commission(Arrays.asList(doctor1), ticket,
                dateCommission, LocalTime.of(9, 35), "110", 30);
        List<SlotSchedule> slotsComission = Arrays.asList(
                new SlotSchedule(slots1.get(1).getId(), LocalTime.of(9, 30), 30));
        commissionDao.insert(commission, slotsComission);

        schedule1.setSlotSchedules(Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 20),
                new SlotSchedule(LocalTime.of(9, 20), 20)));
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleDao.update(Arrays.asList(schedule1)));
        Schedule scheduleDB1 = scheduleDao.getById(schedule1.getId());
        slots1.get(1).setState(SlotScheduleState.BUSY);
        assertAll(
                () -> assertEquals(DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode(), throwable.getMessage()),
                () -> assertEquals(slots1, scheduleDB1.getSlotSchedules())
        );
    }

    @Test
    public void testAddTicket() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30));
        Schedule schedule = new Schedule(doctor,
                LocalDate.of(2019, 5, 25),
                slotSchedules);
        doctor.setSchedules(Arrays.asList(schedule));
        doctorDao.insert(doctor);
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Ticket ticket = new Ticket(patient, "D1250520190930");
        slotSchedules.get(1).setTicket(ticket);
        slotSchedules.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules.get(1));

        List<SlotSchedule> slotsDB = scheduleDao.getById(schedule.getId()).getSlotSchedules();
        assertAll(
                () -> assertEquals(slotSchedules.get(0), slotsDB.get(0)),
                () -> assertEquals(slotSchedules.get(1).getId(), slotsDB.get(1).getId()),
                () -> assertEquals(ticket, slotsDB.get(1).getTicket()),
                () -> assertEquals(slotSchedules.get(1).getTime(), slotsDB.get(1).getTime()),
                () -> assertEquals(slotSchedules.get(1).getDuration(), slotsDB.get(1).getDuration()),
                () -> assertEquals(SlotScheduleState.BUSY, slotsDB.get(1).getState()),
                () -> assertEquals(slotSchedules.get(2), slotsDB.get(2))
        );
    }

    @Test
    public void testAddTicketTimeBusy() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30));
        Schedule schedule = new Schedule(doctor,
                LocalDate.of(2019, 5, 25),
                slotSchedules);
        doctor.setSchedules(Arrays.asList(schedule));
        doctorDao.insert(doctor);
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Ticket ticket1 = new Ticket(patient, "D1250520190930");
        slotSchedules.get(1).setTicket(ticket1);
        slotSchedules.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules.get(1));

        Patient patient2 = new Patient("Степан", "Петров", "Петрович",
                "stephan", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient2);
        Ticket ticket2 = new Ticket(patient2, "D1250520190930");
        slotSchedules.get(1).setTicket(ticket2);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleDao.addTicket(slotSchedules.get(1)));
        assertEquals(DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetByPatientDate() throws DataBaseException {
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Doctor doctor1 = new Doctor("А", "А", "А", "А", "А", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 25),
                slotSchedules1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);
        Ticket ticket1 = new Ticket(patient, "123");
        slotSchedules1.get(0).setTicket(ticket1);
        slotSchedules1.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules1.get(0));

        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "терапевт", "111");
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30),
                new SlotSchedule(LocalTime.of(11, 30), 30));
        Schedule schedule2 = new Schedule(doctor2,
                LocalDate.of(2019, 5, 25),
                slotSchedules2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);
        Ticket ticket2 = new Ticket(patient, "123");
        slotSchedules2.get(1).setTicket(ticket2);
        slotSchedules2.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules2.get(1));

        Doctor doctor3 = new Doctor("В", "В", "В", "В", "В", "терапевт", "111а");
        List<SlotSchedule> slotSchedules3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule3 = new Schedule(doctor3,
                LocalDate.of(2019, 5, 20),
                slotSchedules3);
        doctor3.setSchedules(Arrays.asList(schedule3));
        doctorDao.insert(doctor3);
        Ticket ticket3 = new Ticket(patient, "123");
        slotSchedules3.get(0).setTicket(ticket3);
        slotSchedules3.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules3.get(0));

        List<SlotSchedule> slotsDB = scheduleDao.getByPatient(patient, LocalDate.of(2019, 5, 25));
        assertAll(
                () -> assertEquals(Arrays.asList(slotSchedules1.get(0), slotSchedules2.get(1)), slotsDB),
                () -> assertTrue(scheduleDao.getByPatient(patient,
                        LocalDate.of(2019, 5, 10)).isEmpty())
        );
    }

    @Test
    public void testGetByPatient() throws DataBaseException {
        Patient patient1 = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient1);
        Patient patient2 = new Patient("Семен", "Иванов", "Петрович",
                "semen", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient2);
        Patient patient3 = new Patient("Мария", "Иванова", "Петрович",
                "maria", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient3);

        Doctor doctor1 = new Doctor("А", "А", "А", "А", "А", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 25), slotSchedules1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);
        slotSchedules1.get(0).setTicket(new Ticket(patient1, "123"));
        slotSchedules1.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules1.get(0));

        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "терапевт", "111");
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30),
                new SlotSchedule(LocalTime.of(11, 30), 30));
        Schedule schedule2 = new Schedule(doctor2,
                LocalDate.of(2019, 5, 25), slotSchedules2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);
        slotSchedules2.get(0).setTicket(new Ticket(patient1, "456"));
        slotSchedules2.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules2.get(0));
        slotSchedules2.get(1).setTicket(new Ticket(patient2, "789"));
        slotSchedules2.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules2.get(1));

        Doctor doctor3 = new Doctor("В", "В", "В", "В", "В", "терапевт", "111а");
        List<SlotSchedule> slotSchedules3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule3 = new Schedule(doctor2,
                LocalDate.of(2019, 5, 20), slotSchedules3);
        doctor3.setSchedules(Arrays.asList(schedule3));
        doctorDao.insert(doctor3);
        slotSchedules3.get(0).setTicket(new Ticket(patient1, "321"));
        slotSchedules3.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules3.get(0));

        List<Schedule> schedules1 = scheduleDao.getByPatient(patient1);
        List<Schedule> schedules2 = scheduleDao.getByPatient(patient2);
        List<SlotSchedule> slots25 = Arrays.asList(slotSchedules2.get(0), slotSchedules1.get(0));
        slots25.sort(slotComparator);
        List<SlotSchedule> slots25ByPatient = Arrays.asList(schedules1.get(1).getSlotSchedules().get(0),
                schedules1.get(2).getSlotSchedules().get(0));
        slots25ByPatient.sort(slotComparator);
        assertAll(
                () -> assertEquals(3, schedules1.size()),
                () -> assertEquals(1, schedules2.size()),
                () -> assertEquals(1, schedules1.get(0).getSlotSchedules().size()),
                () -> assertEquals(1, schedules1.get(1).getSlotSchedules().size()),
                () -> assertEquals(1, schedules1.get(2).getSlotSchedules().size()),
                () -> assertEquals(1, schedules2.get(0).getSlotSchedules().size()),
                () -> assertEquals(slotSchedules3.get(0), schedules1.get(0).getSlotSchedules().get(0)),
                () -> assertEquals(slots25, slots25ByPatient),
                () -> assertEquals(slotSchedules2.get(1), schedules2.get(0).getSlotSchedules().get(0)),
                () -> assertTrue(scheduleDao.getByPatient(patient3).isEmpty())
        );
    }

    @Test
    public void testGetByPatientInterval() throws DataBaseException {
        Patient patient = new Patient("Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Doctor doctor1 = new Doctor("А", "А", "А", "А", "А", "терапевт", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 25),
                slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);
        slots1.get(0).setTicket(new Ticket(patient, "D123"));
        slots1.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots1.get(0));

        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "терапевт", "111");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30),
                new SlotSchedule(LocalTime.of(11, 30), 30));
        Schedule schedule2 = new Schedule(doctor2,
                LocalDate.of(2019, 5, 25),
                slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);
        slots2.get(1).setTicket(new Ticket(patient, "D789"));
        slots2.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots2.get(1));

        Doctor doctor3 = new Doctor("В", "В", "В", "В", "В", "терапевт", "111а");
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule3 = new Schedule(doctor3,
                LocalDate.of(2019, 5, 20),
                slots3);
        List<SlotSchedule> slots4 = Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 20),
                new SlotSchedule(LocalTime.of(15, 20), 20));
        Schedule schedule4 = new Schedule(doctor3,
                LocalDate.of(2019, 5, 25),
                slots4);
        doctor3.setSchedules(Arrays.asList(schedule3, schedule4));
        doctorDao.insert(doctor3);
        slots3.get(0).setTicket(new Ticket(patient, "D321"));
        slots3.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots3.get(0));
        slots4.get(1).setTicket(new Ticket(patient, "D987"));
        slots4.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots4.get(1));

        List<Schedule> schedulesByPatient1 = scheduleDao.getByPatient(patient, LocalDate.of(2019,5,20),
                LocalDate.of(2019, 5,25));
        List<Schedule> schedulesByPatient2 = scheduleDao.getByPatient(patient, LocalDate.of(2019,5,20),
                LocalDate.of(2019, 5,24));
        List<Schedule> schedulesByPatient3 = scheduleDao.getByPatient(patient, LocalDate.of(2019,5,10),
                LocalDate.of(2019, 5,19));
        List<Schedule> schedulesInterval1 = scheduleDao.getByInterval(LocalDate.of(2019,5,20),
                LocalDate.of(2019, 5,25));
        List<Schedule> schedulesInterval2 = scheduleDao.getByInterval(LocalDate.of(2019,5,25),
                LocalDate.of(2019, 5,25));
        List<Schedule> schedulesInterval3 = scheduleDao.getByInterval(LocalDate.of(2019,5,21),
                LocalDate.of(2019, 5,24));
        List<SlotSchedule> slotsPat = new ArrayList<>();
        for (Schedule schedule : schedulesByPatient1)
            slotsPat.add(schedule.getSlotSchedules().get(0));
        assertAll(
                () -> assertEquals(4, schedulesByPatient1.size()),
                () -> assertEquals(1, schedulesByPatient2.size()),
                () -> assertTrue(schedulesByPatient3.isEmpty()),
                () -> assertTrue(slotsPat.containsAll(Arrays.asList(slots1.get(0), slots2.get(1),
                        slots3.get(0), slots4.get(1)))),
                () -> assertEquals(slots3.get(0), schedulesByPatient2.get(0).getSlotSchedules().get(0)),
                () -> assertEquals(4, schedulesInterval1.size()),
                () -> assertEquals(3, schedulesInterval2.size()),
                () -> assertTrue(schedulesInterval3.isEmpty()),
                () -> assertTrue(schedulesInterval1.stream().map(Schedule::getId).collect(Collectors.toList())
                        .containsAll(Arrays.asList(schedule1.getId(), schedule2.getId(), schedule3.getId(), schedule4.getId()))),
                () -> assertTrue(schedulesInterval2.stream().map(Schedule::getId).collect(Collectors.toList())
                        .containsAll(Arrays.asList(schedule1.getId(), schedule2.getId(), schedule4.getId())))
        );
    }

}
