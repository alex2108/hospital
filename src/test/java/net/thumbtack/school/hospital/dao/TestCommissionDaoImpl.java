package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestCommissionDaoImpl extends BaseTestDao {

    @Test
    public void testInsertCommission() throws DataBaseException {
        LocalDate dateCommission = LocalDate.of(2019, 5, 22);
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 15),
                new SlotSchedule(LocalTime.of(9, 15), 15),
                new SlotSchedule(LocalTime.of(9, 30), 15),
                new SlotSchedule(LocalTime.of(9, 45), 15),
                new SlotSchedule(LocalTime.of(10, 0), 15));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20),
                new SlotSchedule(LocalTime.of(10, 20), 20),
                new SlotSchedule(LocalTime.of(10, 40), 20));
        Schedule schedule3 = new Schedule(doctor3, dateCommission, slots3);
        doctor3.setSchedules(Arrays.asList(schedule3));
        doctorDao.insert(doctor3);

        Patient patient = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient);

        Ticket ticket = new Ticket(patient, "CD123");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2, doctor3), ticket,
                dateCommission, LocalTime.of(9, 35), "110", 30);
        List<SlotSchedule> slotsComission = Arrays.asList(
                new SlotSchedule(slots1.get(1).getId(), LocalTime.of(9, 30), 30),
                new SlotSchedule(slots2.get(2).getId(), LocalTime.of(9, 30), 15),
                new SlotSchedule(slots2.get(3).getId(), LocalTime.of(9, 45), 15),
                new SlotSchedule(slots2.get(4).getId(), LocalTime.of(10, 0), 15),
                new SlotSchedule(slots3.get(0).getId(), LocalTime.of(10, 0), 20));

        commissionDao.insert(commission, slotsComission);
        assertAll(
                () -> assertNotEquals(0, commission.getId()),
                () -> assertEquals(ticket, ticketDao.getById(commission.getTicket().getId())),
                () -> assertEquals(SlotScheduleState.BUSY, scheduleDao.getSlotById(slots1.get(1).getId()).getState()),
                () -> assertEquals(SlotScheduleState.BUSY, scheduleDao.getSlotById(slots2.get(2).getId()).getState()),
                () -> assertEquals(SlotScheduleState.BUSY, scheduleDao.getSlotById(slots2.get(3).getId()).getState()),
                () -> assertEquals(SlotScheduleState.BUSY, scheduleDao.getSlotById(slots2.get(4).getId()).getState()),
                () -> assertEquals(SlotScheduleState.BUSY, scheduleDao.getSlotById(slots3.get(0).getId()).getState()),
                () -> assertTrue(commissionDao.getByPatient(patient,
                        LocalDate.of(2019, 5, 10)).isEmpty()),
                () -> assertTrue(commissionDao.getByDoctors(Arrays.asList(doctor1, doctor2),
                        LocalDate.of(2019, 5, 15)).isEmpty())

        );
        checkCommissionFields(commission, commissionDao.getById(commission.getId()));
        checkCommissionFields(commission,
                commissionDao.getByPatient(patient, dateCommission).get(0));
        checkCommissionFields(commission,
                commissionDao.getByDoctors(Arrays.asList(doctor1, doctor2, doctor3), dateCommission).get(0));
        checkCommissionFields(commission,
                commissionDao.getByDoctors(Arrays.asList(doctor1, doctor2), dateCommission).get(0));
        checkCommissionFields(commission, commissionDao.getByTicket(ticket));
    }

    @Test
    public void testInsertCommissionSlotBusy() throws DataBaseException {
        LocalDate dateCommission = LocalDate.of(2019, 5, 22);
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);
        Patient patient1 = new Patient("Б", "Б", "Б", "Б", "Б", "Б", "Б", "Б");
        patientDao.insert(patient1);
        slots1.get(1).setTicket(new Ticket(patient1, "CD123"));
        slots1.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots1.get(1));

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 15),
                new SlotSchedule(LocalTime.of(9, 15), 15),
                new SlotSchedule(LocalTime.of(9, 30), 15),
                new SlotSchedule(LocalTime.of(9, 45), 15),
                new SlotSchedule(LocalTime.of(10, 0), 15));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20),
                new SlotSchedule(LocalTime.of(10, 20), 20),
                new SlotSchedule(LocalTime.of(10, 40), 20));
        Schedule schedule3 = new Schedule(doctor3, dateCommission, slots3);
        doctor3.setSchedules(Arrays.asList(schedule3));
        doctorDao.insert(doctor3);

        Patient patient2 = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient2);

        Ticket ticket2 = new Ticket(patient2, "CD456");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2, doctor3), ticket2,
                dateCommission, LocalTime.of(9, 35), "110", 30);
        List<SlotSchedule> slotsComission = Arrays.asList(
                new SlotSchedule(slots1.get(1).getId(), LocalTime.of(9, 30), 30),
                new SlotSchedule(slots2.get(2).getId(), LocalTime.of(9, 30), 15),
                new SlotSchedule(slots2.get(3).getId(), LocalTime.of(9, 45), 15),
                new SlotSchedule(slots2.get(4).getId(), LocalTime.of(10, 0), 15),
                new SlotSchedule(slots3.get(0).getId(), LocalTime.of(10, 0), 20));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionDao.insert(commission, slotsComission));
        assertEquals(DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertCommissionNotIntersectSlots() throws DataBaseException {
        LocalDate dateCommission1 = LocalDate.of(2019, 5, 27);
        LocalDate dateCommission2 = LocalDate.of(2019, 6, 5);
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        doctor1.setSchedules(Arrays.asList(new Schedule(doctor1, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor1);

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        doctor2.setSchedules(Arrays.asList(new Schedule(doctor2, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor2);

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        doctor3.setSchedules(Arrays.asList(new Schedule(doctor3, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor3);

        Patient patient1 = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient1);
        Patient patient2 = new Patient("Б", "Б", "A", "Б", "A", "A", "A", "A");
        patientDao.insert(patient2);

        Ticket ticket1 = new Ticket(patient1, "CD123");
        Commission commission1 = new Commission(Arrays.asList(doctor1, doctor2), ticket1,
                dateCommission1, LocalTime.of(9, 35), "110", 30);
        commissionDao.insert(commission1);
        Ticket ticket2 = new Ticket(patient1, "CD456");
        Commission commission2 = new Commission(Arrays.asList(doctor2, doctor3), ticket2,
                dateCommission2, LocalTime.of(9, 35), "111а", 30);
        commissionDao.insert(commission2);

        List<Commission> commissions = commissionDao.getByPatient(patient1);
        List<Commission> commissionsInterval1 = commissionDao.getByPatient(patient1, dateCommission1, dateCommission2);
        List<Commission> commissionsInterval2 = commissionDao.getByPatient(patient1, LocalDate.of(2019, 5, 30), dateCommission2);
        List<Commission> commissionsInterval3 = commissionDao.getByPatient(patient1, LocalDate.of(2019, 3, 30),
                dateCommission1.minusDays(1));

        assertAll(
                () -> assertTrue(commissionDao.getByPatient(patient2).isEmpty()),
                () -> assertEquals(2, commissionsInterval1.size()),
                () -> assertEquals(1, commissionsInterval2.size()),
                () -> assertTrue(commissionsInterval3.isEmpty())
        );
        checkCommissionFields(commission1, commissionDao.getById(commission1.getId()));
        checkCommissionFields(commission2, commissionDao.getById(commission2.getId()));
        checkCommissionFields(commission1, commissions.get(0));
        checkCommissionFields(commission2, commissions.get(1));
        checkCommissionFields(commission1, commissionDao.getByTicket(ticket1));
        checkCommissionFields(commission2, commissionDao.getByTicket(ticket2));
        checkCommissionFields(commission1, commissionsInterval1.get(0));
        checkCommissionFields(commission2, commissionsInterval1.get(1));
        checkCommissionFields(commission2, commissionsInterval2.get(0));
    }

    @Test
    public void testGetCommissionsByDoctorFromInterval() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        doctor1.setSchedules(Arrays.asList(new Schedule(doctor1, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor1);
        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        doctor2.setSchedules(Arrays.asList(new Schedule(doctor2, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor2);
        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        doctor3.setSchedules(Arrays.asList(new Schedule(doctor3, LocalDate.of(2019, 5, 20),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30)))));
        doctorDao.insert(doctor3);

        Patient patient = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient);

        LocalDate dateCommission1 = LocalDate.of(2019, 5, 27);
        LocalDate dateCommission2 = LocalDate.of(2019, 6, 5);
        LocalDate dateCommission3 = LocalDate.of(2019, 6, 7);
        LocalDate dateCommission4 = LocalDate.of(2019, 6, 12);
        Commission commission1 = new Commission(Arrays.asList(doctor1, doctor2), new Ticket(patient, "CD123"),
                dateCommission1, LocalTime.of(9, 35), "110", 30);
        commissionDao.insert(commission1);
        Commission commission2 = new Commission(Arrays.asList(doctor2, doctor3), new Ticket(patient, "CD456"),
                dateCommission2, LocalTime.of(9, 35), "111а", 30);
        commissionDao.insert(commission2);
        Commission commission3 = new Commission(Arrays.asList(doctor1, doctor3), new Ticket(patient, "CD789"),
                dateCommission3, LocalTime.of(9, 35), "110", 30);
        commissionDao.insert(commission3);
        Commission commission4 = new Commission(Arrays.asList(doctor1, doctor2, doctor3), new Ticket(patient, "CD321"),
                dateCommission4, LocalTime.of(9, 35), "111а", 30);
        commissionDao.insert(commission4);

        List<Commission> commissionsInterval1 = commissionDao.getByInterval(dateCommission1, dateCommission4);
        List<Commission> commissionsInterval2 = commissionDao.getByInterval(dateCommission1.plusDays(1), dateCommission2);
        List<Commission> commissionsInterval3 = commissionDao.getByInterval(dateCommission2.plusDays(1), dateCommission3.minusDays(1));
        List<Commission> commissionsByDoctor1 = commissionDao.getByDoctor(doctor1, dateCommission1, dateCommission4);
        List<Commission> commissionsByDoctor2 = commissionDao.getByDoctor(doctor2, dateCommission2.plusDays(1), dateCommission4);
        List<Commission> commissionsByDoctor3 = commissionDao.getByDoctor(doctor2, dateCommission2.plusDays(1), dateCommission4.minusDays(1));
        assertAll(
                () -> assertEquals(4, commissionsInterval1.size()),
                () -> assertEquals(1, commissionsInterval2.size()),
                () -> assertTrue(commissionsInterval3.isEmpty()),
                () -> assertEquals(3, commissionsByDoctor1.size()),
                () -> assertEquals(1, commissionsByDoctor2.size()),
                () -> assertTrue(commissionsByDoctor3.isEmpty())
        );
        checkCommissionFields(commission1, commissionsInterval1.get(0));
        checkCommissionFields(commission2, commissionsInterval1.get(1));
        checkCommissionFields(commission3, commissionsInterval1.get(2));
        checkCommissionFields(commission4, commissionsInterval1.get(3));
        checkCommissionFields(commission2, commissionsInterval2.get(0));
        checkCommissionFields(commission1, commissionsByDoctor1.get(0));
        checkCommissionFields(commission3, commissionsByDoctor1.get(1));
        checkCommissionFields(commission4, commissionsByDoctor1.get(2));
        checkCommissionFields(commission4, commissionsByDoctor2.get(0));
    }
}
