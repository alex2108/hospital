package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestAdminDaoImpl extends BaseTestDao {

    @Test
    public void testInsertAdmin() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);
        assertAll(
                () -> assertNotEquals(0, admin.getId()),
                () -> assertNotEquals(1, admin.getId()),
                () -> assertEquals(admin, adminDao.getById(admin.getId()))
        );
    }

    @Test
    public void testInsertDefaultAdmin() {
        Admin adminDefault = new Admin("Иван", "Иванов", "Иванович",
                "administrator", "ivan123", "Главный администратор");

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> adminDao.insert(adminDefault));
        assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testDoubleInsertAdmin() throws DataBaseException {
        Admin admin1 = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Admin admin2 = new Admin("Петр", "Петров", "Петрович",
                "ivan", "ivan345", "Старший администратор");
        adminDao.insert(admin1);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> adminDao.insert(admin2));
        assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetAdminByLogin() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "Иvan", "ivaN123", "Главный администратор");
        adminDao.insert(admin);

        assertAll(
                () -> assertEquals(admin, adminDao.get("Иvan", "ivaN123")),
                () -> assertEquals(admin, adminDao.get("иvan", "ivaN123")),
                () -> assertEquals(admin, adminDao.get("иvaN", "ivaN123")),
                () -> assertNull(adminDao.get("petr", "ivaN123")),
                () -> assertNull(adminDao.get("иvan", "ivan345")),
                () -> assertNull(adminDao.get("иvan", "ivan123"))
        );
    }

    @Test
    public void testUpdateAdmin() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);
        admin.setFirstName("Петр");
        admin.setLastName("Петров");
        admin.setPatronymic("Петрович");
        admin.setPosition("Старший администратор");
        admin.setPassword("ivan345");

        adminDao.update(admin);
        assertAll(
                () -> assertEquals(admin, adminDao.getById(admin.getId())),
                () -> assertNull(adminDao.get("ivan", "ivan123"))
        );
    }

    @Test
    public void testUpdateAdminNullPatronymic() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);
        admin.setFirstName("Петр");
        admin.setLastName("Петров");
        admin.setPosition("Старший администратор");
        admin.setPassword("ivan345");

        adminDao.update(admin);
        assertAll(
                () -> assertEquals(admin, adminDao.getById(admin.getId())),
                () -> assertNull(adminDao.get("ivan", "ivan123"))
        );
    }

}
