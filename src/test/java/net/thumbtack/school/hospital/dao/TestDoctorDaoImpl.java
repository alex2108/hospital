package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestDoctorDaoImpl extends BaseTestDao {

    @Test
    public void testInsertDoctor() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30),
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30)
        );
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2019, 5, 22),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 30), 30),
                new SlotSchedule(LocalTime.of(12, 0), 30)
        );
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2019, 5, 20),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));
        doctorDao.insert(doctor);
        Doctor doctorDB = doctorDao.getById(doctor.getId());
        doctor.getSchedules().sort(scheduleComparator);
        slotSchedules1.sort(slotComparator);
        slotSchedules2.sort(slotComparator);
        checkDoctorFields(doctor, doctorDB);
        assertAll(
                () -> assertNotEquals(0, doctorDB.getId()),
                () -> assertNotEquals(0, doctorDB.getSchedules().get(0).getId()),
                () -> assertNotEquals(0, doctorDB.getSchedules().get(1).getId())
        );
    }

    @Test
    public void testDoubleInsertDoctor() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Doctor doctor2 = new Doctor("Иван", "Петров", "Иванович",
                "ivan", "ivan345", "отоларинголог", "111а");
        doctorDao.insert(doctor1);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorDao.insert(doctor2));
        assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertDoctorRoomBusy() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "110");
        doctorDao.insert(doctor1);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorDao.insert(doctor2));
        assertAll(
                () -> assertEquals(DataBaseErrorCode.ROOM_BUSY.getErrorCode(), throwable.getMessage()),
                () -> assertNull(doctorDao.get("petr", "ivan345"))
        );
    }

    @Test
    public void testSpecialityRoomExist() throws DataBaseException {
        assertAll(
                () -> assertTrue(doctorDao.isRoomExist("110")),
                () -> assertFalse(doctorDao.isRoomExist("0000001")),
                () -> assertTrue(doctorDao.isSpecialityExist("терапевт")),
                () -> assertFalse(doctorDao.isSpecialityExist("чародей"))
        );
    }

    @Test
    public void testGetDoctorByLogin() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "Ivan", "ivaN123", "терапевт", "110");
        doctorDao.insert(doctor);

        assertAll(
                () -> assertEquals(doctor, doctorDao.get("Ivan", "ivaN123")),
                () -> assertEquals(doctor, doctorDao.get("ivan", "ivaN123")),
                () -> assertEquals(doctor, doctorDao.get("ivAn", "ivaN123")),
                () -> assertNull(doctorDao.get("petr", "ivaN123")),
                () -> assertNull(doctorDao.get("ivan", "ivan345")),
                () -> assertNull(doctorDao.get("ivan", "ivan123"))
        );
    }

    @Test
    public void testGetDoctorsBySpeciality() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "отоларинголог", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 22),
                slotSchedules1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctorDao.insert(doctor1);
        Patient patient1 = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient1);
        slotSchedules1.get(0).setTicket(new Ticket(patient1, "123"));
        slotSchedules1.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules1.get(0));

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor2,
                LocalDate.of(2019, 5, 22),
                slotSchedules2);
        doctor2.setSchedules(Arrays.asList(schedule2));
        doctorDao.insert(doctor2);
        Patient patient2 = new Patient("Б", "Б", "Б", "Б", "Б", "Б", "Б", "Б");
        patientDao.insert(patient2);
        slotSchedules2.get(1).setTicket(new Ticket(patient2, "123"));
        slotSchedules2.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules2.get(1));

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephen", "ivan345", "отоларинголог", "112");
        List<SlotSchedule> slotSchedules3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule3 = new Schedule(doctor3,
                LocalDate.of(2019, 5, 20),
                slotSchedules3);
        doctor3.setSchedules(Arrays.asList(schedule3));
        doctorDao.insert(doctor3);
        Patient patient3 = new Patient("В", "В", "В", "В", "В", "В", "В", "В");
        patientDao.insert(patient3);
        slotSchedules3.get(0).setTicket(new Ticket(patient3, "123"));
        slotSchedules3.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules3.get(0));

        Doctor doctor4 = new Doctor("Дмитрий", "Петров", "Иванович",
                "dmitriy", "ivan345", "терапевт", "201");
        List<SlotSchedule> slotSchedules4 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30)
        );
        Schedule schedule4 = new Schedule(doctor4,
                LocalDate.of(2019, 5, 22),
                slotSchedules4);
        doctor4.setSchedules(Arrays.asList(schedule4));
        doctorDao.insert(doctor4);
        Patient patient4 = new Patient("Г", "Г", "Г", "Г", "Г", "Г", "Г", "Г");
        patientDao.insert(patient4);
        slotSchedules4.get(0).setTicket(new Ticket(patient4, "123"));
        slotSchedules4.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slotSchedules4.get(0));

        List<Doctor> doctors = doctorDao.getVacantBySpeciality("отоларинголог",
                LocalDateTime.of(2019, 5, 22, 9, 0));
        assertEquals(1, doctors.size());
        checkDoctorFields(doctor2, doctors.get(0));
    }

    @Test
    public void testGetDoctorByListId() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30));
        doctor1.setSchedules(Arrays.asList(new Schedule(doctor1,
                LocalDate.of(2019, 5, 22), slotSchedules1)));
        doctorDao.insert(doctor1);

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30));
        doctor2.setSchedules(Arrays.asList(new Schedule(doctor2,
                LocalDate.of(2019, 5, 20), slotSchedules2)));
        doctorDao.insert(doctor2);

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        List<SlotSchedule> slotSchedules3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30));
        doctor3.setSchedules(Arrays.asList(new Schedule(doctor3,
                LocalDate.of(2019, 5, 22), slotSchedules3)));
        doctorDao.insert(doctor3);

        List<Doctor> doctors = doctorDao.getByIds(Arrays.asList(doctor1.getId(), doctor2.getId(), doctor3.getId()));
        assertEquals(3, doctors.size());
        doctors.sort(doctorComparatorByName);
        checkDoctorFields(doctor1, doctors.get(0));
        checkDoctorFields(doctor2, doctors.get(1));
        checkDoctorFields(doctor3, doctors.get(2));
    }

    @Test
    public void testGetDoctorsFiltered() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30));
        doctor1.setSchedules(Arrays.asList(new Schedule(doctor1,
                LocalDate.of(2019, 5, 22), slotSchedules1)));
        doctorDao.insert(doctor1);

        Doctor doctor2 = new Doctor("Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111а");
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30));
        doctor2.setSchedules(Arrays.asList(new Schedule(doctor2,
                LocalDate.of(2019, 5, 20), slotSchedules2)));
        doctorDao.insert(doctor2);

        Doctor doctor3 = new Doctor("Степан", "Петров", "Иванович",
                "stephan", "ivan345", "отоларинголог", "112");
        List<SlotSchedule> slotSchedules3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30));
        doctor3.setSchedules(Arrays.asList(new Schedule(doctor3,
                LocalDate.of(2019, 5, 22), slotSchedules3)));
        doctorDao.insert(doctor3);

        List<Doctor> doctorsLOR = doctorDao.getBySpeciality("отоларинголог");
        List<Doctor> doctorsAll = doctorDao.getAll();
        assertAll(
                () -> assertEquals(2, doctorsLOR.size()),
                () -> assertEquals(3, doctorsAll.size())
        );
        doctorsLOR.sort(doctorComparatorByName);
        doctorsAll.sort(doctorComparatorByName);
        checkDoctorFields(doctor2, doctorsLOR.get(0));
        checkDoctorFields(doctor3, doctorsLOR.get(1));
        checkDoctorFields(doctor1, doctorsAll.get(0));
        checkDoctorFields(doctor2, doctorsAll.get(1));
        checkDoctorFields(doctor3, doctorsAll.get(2));
    }

    @Test
    public void testDeleteDoctorNoTickets() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2019, 5, 19), Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20),
                new SlotSchedule(LocalTime.of(10, 20), 20)));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2019, 5, 20), Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30)));
        Schedule schedule3 = new Schedule(doctor,
                LocalDate.of(2019, 5, 22), Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30)));
        doctor.setSchedules(Arrays.asList(schedule1, schedule2, schedule3));
        doctorDao.insert(doctor);

        doctor.setFiredDate(LocalDate.of(2019, 5, 20));
        List<Ticket> ticketsDeleted = doctorDao.delete(doctor);
        Doctor doctorDB = doctorDao.getById(doctor.getId());
        assertAll(
                () -> assertTrue(ticketsDeleted.isEmpty()),
                () -> assertEquals(1, doctorDB.getSchedules().size()),
                () -> assertNull(scheduleDao.getById(schedule2.getId())),
                () -> assertNull(scheduleDao.getById(schedule3.getId())),
                () -> assertEquals(LocalDate.of(2019, 5, 20), doctorDB.getFiredDate())
        );
        checkScheduleFields(schedule1, doctorDB.getSchedules().get(0));
    }

    @Test
    public void testDeleteDoctorWithTickets() throws DataBaseException {
        Doctor doctor1 = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20),
                new SlotSchedule(LocalTime.of(10, 20), 20));
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 19), slots1);
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule2 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 20), slots2);
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        Schedule schedule3 = new Schedule(doctor1,
                LocalDate.of(2019, 5, 22), slots3);
        doctor1.setSchedules(Arrays.asList(schedule1, schedule2, schedule3));
        doctorDao.insert(doctor1);

        Patient patient1 = new Patient("A", "A", "A", "A", "A", "A", "A", "A");
        patientDao.insert(patient1);
        Ticket ticket1 = new Ticket(patient1, "D1905");
        slots1.get(0).setTicket(ticket1);
        slots1.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots1.get(0));
        Ticket ticket2 = new Ticket(patient1, "D2005");
        slots2.get(1).setTicket(ticket2);
        slots2.get(1).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots2.get(1));
        Ticket ticket3 = new Ticket(patient1, "D2005");
        slots3.get(0).setTicket(ticket3);
        slots3.get(0).setState(SlotScheduleState.BUSY);
        scheduleDao.addTicket(slots3.get(0));

        Ticket ticket4 = new Ticket(patient1, "CD2705");
        Doctor doctor2 = new Doctor("Петр", "Иванов", "Иванович",
                "petr", "ivan123", "терапевт", "111");
        doctor2.setSchedules(Arrays.asList(new Schedule(doctor1,
                LocalDate.of(2019, 5, 23), Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20)))));
        doctorDao.insert(doctor2);
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2), ticket4,
                LocalDate.of(2019, 5, 27), LocalTime.of(9, 35), "110", 30);
        commissionDao.insert(commission);

        doctor1.setFiredDate(LocalDate.of(2019, 5, 20));
        List<Ticket> ticketsDeleted = doctorDao.delete(doctor1);
        Doctor doctorDB = doctorDao.getById(doctor1.getId());
        assertAll(
                () -> assertEquals(3, ticketsDeleted.size()),
                () -> assertTrue(ticketsDeleted.containsAll(Arrays.asList(ticket2, ticket3, ticket4))),
                () -> assertEquals(1, doctorDB.getSchedules().size()),
                () -> assertNull(scheduleDao.getById(schedule2.getId())),
                () -> assertNull(scheduleDao.getById(schedule3.getId())),
                () -> assertEquals(ticket1, ticketDao.getById(ticket1.getId())),
                () -> assertNull(ticketDao.getById(ticket2.getId())),
                () -> assertNull(ticketDao.getById(ticket3.getId())),
                () -> assertNull(ticketDao.getById(ticket4.getId())),
                () -> assertNull(commissionDao.getById(commission.getId())),
                () -> assertEquals(LocalDate.of(2019, 5, 20), doctorDB.getFiredDate())
        );
        checkScheduleFields(schedule1, doctorDB.getSchedules().get(0));
    }


}
