package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.Session;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;

public class TestSessionDaoImpl extends BaseTestDao {

    @Test
    public void testInsertSessionAdmin() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);

        Session session = new Session(admin, "123abc");
        sessionDao.insert(session);
        assertAll(
                () -> assertNotEquals(0, session.getId()),
                () -> assertEquals(session, sessionDao.getById(session.getId())),
                () -> assertEquals(session, sessionDao.getByCookie("123abc")),
                () -> assertNull(sessionDao.getByCookie("123Abc"))
        );
    }

    @Test
    public void testDoubleInsertSessionAdmin() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);

        Session session = new Session(admin, "123");
        sessionDao.insert(session);
        Session sessionNew = new Session(admin, "345");
        sessionDao.insert(sessionNew);
        Session sessionDB = sessionDao.getById(session.getId());
        assertAll(
                () -> assertEquals(session.getId(), sessionDB.getId()),
                () -> assertEquals(session.getUser().getId(), sessionDB.getUser().getId()),
                () -> assertEquals(sessionNew.getJavaSessionId(), sessionDB.getJavaSessionId())
        );
    }

    @Test
    public void testInsertSessionDoctor() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        doctorDao.insert(doctor);

        Session session = new Session(doctor, "123abc");
        sessionDao.insert(session);
        assertAll(
                () -> assertNotEquals(0, session.getId()),
                () -> assertEquals(session, sessionDao.getById(session.getId())),
                () -> assertEquals(session, sessionDao.getByCookie("123abc")),
                () -> assertNull(sessionDao.getByCookie("123Abc"))
        );
    }

    @Test
    public void testDoubleInsertSessionDoctor() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        doctorDao.insert(doctor);

        Session session = new Session(doctor, "123");
        sessionDao.insert(session);
        Session sessionNew = new Session(doctor, "345");
        sessionDao.insert(sessionNew);
        Session sessionDB = sessionDao.getById(session.getId());
        assertAll(
                () -> assertEquals(session.getId(), sessionDB.getId()),
                () -> assertEquals(session.getUser().getId(), sessionDB.getUser().getId()),
                () -> assertEquals(sessionNew.getJavaSessionId(), sessionDB.getJavaSessionId())
        );
    }

    @Test
    public void testInsertSessionPatient() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Session session = new Session(patient, "123abc");
        sessionDao.insert(session);
        assertAll(
                () -> assertNotEquals(0, session.getId()),
                () -> assertEquals(session, sessionDao.getById(session.getId())),
                () -> assertEquals(session, sessionDao.getByCookie("123abc")),
                () -> assertNull(sessionDao.getByCookie("123Abc"))
        );
    }

    @Test
    public void testDoubleInsertSessionPatient() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        patientDao.insert(patient);

        Session session = new Session(patient, "123");
        sessionDao.insert(session);
        Session sessionNew = new Session(patient, "345");
        sessionDao.insert(sessionNew);
        Session sessionDB = sessionDao.getById(session.getId());
        assertAll(
                () -> assertEquals(session.getId(), sessionDB.getId()),
                () -> assertEquals(session.getUser().getId(), sessionDB.getUser().getId()),
                () -> assertEquals(sessionNew.getJavaSessionId(), sessionDB.getJavaSessionId())
        );
    }

    @Test
    public void testGetSessionWrongId() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);

        Session session = new Session(admin, "123");
        sessionDao.insert(session);
        assertNull(sessionDao.getById(session.getId() + 1));
    }

    @Test
    public void testDeleteSession() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        adminDao.insert(admin);
        Session session = new Session(admin, "123abc");
        sessionDao.insert(session);
        assertNotNull(sessionDao.getById(session.getId()));
        sessionDao.delete(session);
        assertAll(
                () -> assertNull(sessionDao.getById(session.getId())),
                () -> assertNull(sessionDao.getByCookie("123abc"))
        );
    }
}
