package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.SettingsDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.Session;
import net.thumbtack.school.hospital.service.SettingsService;
import net.thumbtack.school.hospital.utils.PropertiesUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class TestSettingsService extends BaseTestService {
    private SettingsService settingsService = new SettingsService(sessionDao);

    @BeforeAll()
    public static void setUpProperties() {
        boolean initProperties = PropertiesUtils.initProperties();
        if (!initProperties) {
            throw new RuntimeException("Can't read configuration file, stop");
        }
    }

    @Test
    public void testGetPropertiesDefault() throws DataBaseException {
        SettingsDtoResponse responseExp = new SettingsDtoResponse(50, 10);
        assertEquals(responseExp, settingsService.getSettings(null));
    }

    @Test
    public void testGetPropertiesAdmin() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Admin()));
        SettingsDtoResponse responseExp = new SettingsDtoResponse(50, 10);
        assertEquals(responseExp, settingsService.getSettings("abc-123"));
    }

    @Test
    public void testGetPropertiesDoctor() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Doctor()));
        SettingsDtoResponse responseExp = new SettingsDtoResponse(50, 10);
        assertEquals(responseExp, settingsService.getSettings("abc-123"));
    }

    @Test
    public void testGetPropertiesPatient() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Patient()));
        SettingsDtoResponse responseExp = new SettingsDtoResponse(50, 10);
        assertEquals(responseExp, settingsService.getSettings("abc-123"));
    }
}
