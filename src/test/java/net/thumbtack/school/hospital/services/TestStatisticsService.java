package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.StatisticsDoctorDtoResponse;
import net.thumbtack.school.hospital.dto.StatisticsPatientDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.service.StatisticsService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

public class TestStatisticsService extends BaseTestService {
    private StatisticsService statisticsService =
            new StatisticsService(doctorDao, patientDao, scheduleDao, commissionDao);

    @Test
    public void testPatientStatistics() throws DataBaseException {
        Patient patient = new Patient(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Mockito.when(patientDao.getById(1)).thenReturn(patient);

        List<Schedule> schedules = new ArrayList<>();
        schedules.add(new Schedule(new Doctor(), LocalDate.of(2019, 3, 25),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30))));
        schedules.add(new Schedule(new Doctor(), LocalDate.of(2019, 3, 30),
                Arrays.asList(new SlotSchedule(LocalTime.of(15, 0), 20))));
        schedules.add(new Schedule(new Doctor(), LocalDate.of(2019, 4, 2),
                Arrays.asList(new SlotSchedule(LocalTime.of(10, 0), 15))));
        schedules.add(new Schedule(new Doctor(), LocalDate.of(2019, 4, 2),
                Arrays.asList(new SlotSchedule(LocalTime.of(12, 0), 20))));
        schedules.add(new Schedule(new Doctor(), LocalDate.of(2019, 4, 5),
                Arrays.asList(new SlotSchedule(LocalTime.of(9, 0), 30))));
        Mockito.when(scheduleDao.getByPatient(patient, LocalDate.of(2019, 3, 25),
                LocalDate.of(2019, 4, 20))).thenReturn(schedules);
        Mockito.when(scheduleDao.getByPatient(patient, LocalDate.of(2019, 4, 10),
                LocalDate.of(2019, 4, 20))).thenReturn(new ArrayList<>());

        List<Commission> commissions = new ArrayList<>();
        commissions.add(new Commission(Arrays.asList(new Doctor()), new Ticket(),
                LocalDate.of(2019, 3, 30), LocalTime.of(9, 35), "110", 30));
        commissions.add(new Commission(Arrays.asList(new Doctor()), new Ticket(),
                LocalDate.of(2019, 4, 2), LocalTime.of(15, 0), "110", 30));
        Mockito.when(commissionDao.getByPatient(patient, LocalDate.of(2019, 3, 25),
                LocalDate.of(2019, 4, 20))).thenReturn(commissions);
        Mockito.when(commissionDao.getByPatient(patient, LocalDate.of(2019, 4, 10),
                LocalDate.of(2019, 4, 20))).thenReturn(new ArrayList<>());

        StatisticsPatientDtoResponse responseTotal = new StatisticsPatientDtoResponse();
        responseTotal.setPatientId(1);
        responseTotal.setFirstName("Иван");
        responseTotal.setLastName("Иванов");
        responseTotal.setPatronymic("Иванович");
        responseTotal.setAddress("ул. Строителей, д. 3, кв. 5");
        responseTotal.setEmail("ivan@mail.ru");
        responseTotal.setPhone("+79041234567");
        responseTotal.setStartDate("25-03-2019");
        responseTotal.setEndDate("20-04-2019");
        responseTotal.addPropStatistics("total", new StatisticsPatientDtoResponse.Statistics(7, "02:55"));

        StatisticsPatientDtoResponse responseDetails = new StatisticsPatientDtoResponse();
        responseDetails.setPatientId(1);
        responseDetails.setFirstName("Иван");
        responseDetails.setLastName("Иванов");
        responseDetails.setPatronymic("Иванович");
        responseDetails.setAddress("ул. Строителей, д. 3, кв. 5");
        responseDetails.setEmail("ivan@mail.ru");
        responseDetails.setPhone("+79041234567");
        responseDetails.setStartDate("25-03-2019");
        responseDetails.setEndDate("20-04-2019");
        responseDetails.addPropStatistics("appointment", new StatisticsPatientDtoResponse.Statistics(5, "01:55"));
        responseDetails.addPropStatistics("commission", new StatisticsPatientDtoResponse.Statistics(2, "01:00"));

        StatisticsPatientDtoResponse respTotalByDays = new StatisticsPatientDtoResponse();
        respTotalByDays.setPatientId(1);
        respTotalByDays.setFirstName("Иван");
        respTotalByDays.setLastName("Иванов");
        respTotalByDays.setPatronymic("Иванович");
        respTotalByDays.setAddress("ул. Строителей, д. 3, кв. 5");
        respTotalByDays.setEmail("ivan@mail.ru");
        respTotalByDays.setPhone("+79041234567");
        StatisticsPatientDtoResponse.StatisticsOnDay stat25 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        stat25.setDate("25-03-2019");
        stat25.addPropStatistics("total", new StatisticsPatientDtoResponse.Statistics(1, "00:30"));
        StatisticsPatientDtoResponse.StatisticsOnDay stat30 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        stat30.setDate("30-03-2019");
        stat30.addPropStatistics("total", new StatisticsPatientDtoResponse.Statistics(2, "00:50"));
        StatisticsPatientDtoResponse.StatisticsOnDay stat02 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        stat02.setDate("02-04-2019");
        stat02.addPropStatistics("total", new StatisticsPatientDtoResponse.Statistics(3, "01:05"));
        StatisticsPatientDtoResponse.StatisticsOnDay stat05 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        stat05.setDate("05-04-2019");
        stat05.addPropStatistics("total", new StatisticsPatientDtoResponse.Statistics(1, "00:30"));
        respTotalByDays.setByDays(Arrays.asList(stat25, stat30, stat02, stat05));

        StatisticsPatientDtoResponse respDetailsByDays = new StatisticsPatientDtoResponse();
        respDetailsByDays.setPatientId(1);
        respDetailsByDays.setFirstName("Иван");
        respDetailsByDays.setLastName("Иванов");
        respDetailsByDays.setPatronymic("Иванович");
        respDetailsByDays.setAddress("ул. Строителей, д. 3, кв. 5");
        respDetailsByDays.setEmail("ivan@mail.ru");
        respDetailsByDays.setPhone("+79041234567");
        StatisticsPatientDtoResponse.StatisticsOnDay statDetail25 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        statDetail25.setDate("25-03-2019");
        statDetail25.addPropStatistics("appointment", new StatisticsPatientDtoResponse.Statistics(1, "00:30"));
        StatisticsPatientDtoResponse.StatisticsOnDay statDetail30 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        statDetail30.setDate("30-03-2019");
        statDetail30.addPropStatistics("appointment", new StatisticsPatientDtoResponse.Statistics(1, "00:20"));
        statDetail30.addPropStatistics("commission", new StatisticsPatientDtoResponse.Statistics(1, "00:30"));
        StatisticsPatientDtoResponse.StatisticsOnDay statDetail02 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        statDetail02.setDate("02-04-2019");
        statDetail02.addPropStatistics("appointment", new StatisticsPatientDtoResponse.Statistics(2, "00:35"));
        statDetail02.addPropStatistics("commission", new StatisticsPatientDtoResponse.Statistics(1, "00:30"));
        StatisticsPatientDtoResponse.StatisticsOnDay statDetail05 = new StatisticsPatientDtoResponse.StatisticsOnDay();
        statDetail05.setDate("05-04-2019");
        statDetail05.addPropStatistics("appointment", new StatisticsPatientDtoResponse.Statistics(1, "00:30"));
        respDetailsByDays.setByDays(Arrays.asList(statDetail25, statDetail30, statDetail02, statDetail05));

        StatisticsPatientDtoResponse respNon = new StatisticsPatientDtoResponse();
        respNon.setPatientId(1);
        respNon.setFirstName("Иван");
        respNon.setLastName("Иванов");
        respNon.setPatronymic("Иванович");
        respNon.setAddress("ул. Строителей, д. 3, кв. 5");
        respNon.setEmail("ivan@mail.ru");
        respNon.setPhone("+79041234567");
        assertAll(
                () -> assertEquals(responseTotal, statisticsService.getPatientStatistics(1, "25-03-2019",
                        "20-04-2019", false, false)),
                () -> assertEquals(responseDetails, statisticsService.getPatientStatistics(1, "25-03-2019",
                        "20-04-2019", true, false)),
                () -> assertEquals(respTotalByDays, statisticsService.getPatientStatistics(1, "25-03-2019",
                        "20-04-2019", false, true)),
                () -> assertEquals(respDetailsByDays, statisticsService.getPatientStatistics(1, "25-03-2019",
                        "20-04-2019", true, true)),
                () -> assertEquals(respNon, statisticsService.getPatientStatistics(1, "10-04-2019",
                        "20-04-2019", true, true))
        );
    }

    @Test
    public void testDoctorStatistics() throws DataBaseException {
        Doctor doctor1 = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<Schedule> schedules = Arrays.asList(
                new Schedule(doctor1, LocalDate.of(2020, 1, 15), Arrays.asList(
                        new SlotSchedule(LocalTime.of(9, 0), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(9, 30), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(10, 0), 30))),
                new Schedule(doctor1, LocalDate.of(2020, 1, 17), Arrays.asList(
                        new SlotSchedule(LocalTime.of(12, 0), 30),
                        new SlotSchedule(LocalTime.of(12, 30), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(13, 0), 30),
                        new SlotSchedule(LocalTime.of(13, 30), 30))),
                new Schedule(doctor1, LocalDate.of(2020, 1, 20), Arrays.asList(
                        new SlotSchedule(LocalTime.of(12, 0), 30))),
                new Schedule(doctor1, LocalDate.of(2020, 1, 22), Arrays.asList(
                        new SlotSchedule(new Ticket(), LocalTime.of(10, 0), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(10, 30), 30))));
        doctor1.setSchedules(schedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor1);

        List<Commission> commissions = new ArrayList<>();
        commissions.add(new Commission(Arrays.asList(doctor1, new Doctor()), new Ticket(),
                LocalDate.of(2020, 1, 17), LocalTime.of(10, 0), "110", 20));
        commissions.add(new Commission(Arrays.asList(doctor1, new Doctor()), new Ticket(),
                LocalDate.of(2020, 1, 22), LocalTime.of(12, 0), "110", 30));
        commissions.add(new Commission(Arrays.asList(doctor1, new Doctor()), new Ticket(),
                LocalDate.of(2020, 1, 24), LocalTime.of(15, 0), "110", 30));
        Mockito.when(commissionDao.getByDoctor(doctor1, LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 1, 30))).thenReturn(commissions);
        Mockito.when(commissionDao.getByDoctor(doctor1, LocalDate.of(2020, 1, 17),
                LocalDate.of(2020, 1, 17))).thenReturn(Arrays.asList(new Commission(Arrays.asList(doctor1, new Doctor()), new Ticket(),
                LocalDate.of(2020, 1, 17), LocalTime.of(10, 0), "110", 20)));
        Mockito.when(commissionDao.getByDoctor(doctor1, LocalDate.of(2020, 1, 25),
                LocalDate.of(2020, 1, 30))).thenReturn(new ArrayList<>());

        StatisticsDoctorDtoResponse responseTotal = new StatisticsDoctorDtoResponse();
        responseTotal.setDoctorId(1);
        responseTotal.setFirstName("Иван");
        responseTotal.setLastName("Иванов");
        responseTotal.setPatronymic("Иванович");
        responseTotal.setSpeciality("терапевт");
        responseTotal.setRoom("110");
        responseTotal.setStartDate("15-01-2020");
        responseTotal.setEndDate("30-01-2020");
        responseTotal.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(8, "05:00", "03:50"));

        StatisticsDoctorDtoResponse responseDetails = new StatisticsDoctorDtoResponse();
        responseDetails.setDoctorId(1);
        responseDetails.setFirstName("Иван");
        responseDetails.setLastName("Иванов");
        responseDetails.setPatronymic("Иванович");
        responseDetails.setSpeciality("терапевт");
        responseDetails.setRoom("110");
        responseDetails.setStartDate("15-01-2020");
        responseDetails.setEndDate("30-01-2020");
        responseDetails.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(5, "05:00", "02:30"));
        responseDetails.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(3, "01:20"));

        StatisticsDoctorDtoResponse respTotalByDays = new StatisticsDoctorDtoResponse();
        respTotalByDays.setDoctorId(1);
        respTotalByDays.setFirstName("Иван");
        respTotalByDays.setLastName("Иванов");
        respTotalByDays.setPatronymic("Иванович");
        respTotalByDays.setSpeciality("терапевт");
        respTotalByDays.setRoom("110");
        StatisticsDoctorDtoResponse.StatisticsOnDay stat15 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat15.setDate("15-01-2020");
        stat15.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(2, "01:30", "01:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay stat17 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat17.setDate("17-01-2020");
        stat17.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(2, "02:00", "00:50"));
        StatisticsDoctorDtoResponse.StatisticsOnDay stat20 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat20.setDate("20-01-2020");
        stat20.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(0, "00:30", "00:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay stat22 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat22.setDate("22-01-2020");
        stat22.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(3, "01:00", "01:30"));
        StatisticsDoctorDtoResponse.StatisticsOnDay stat24 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat24.setDate("24-01-2020");
        stat24.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(1, "00:00", "00:30"));
        respTotalByDays.setByDays(Arrays.asList(stat15, stat17, stat20, stat22, stat24));

        StatisticsDoctorDtoResponse respDetailsByDays1 = new StatisticsDoctorDtoResponse();
        respDetailsByDays1.setDoctorId(1);
        respDetailsByDays1.setFirstName("Иван");
        respDetailsByDays1.setLastName("Иванов");
        respDetailsByDays1.setPatronymic("Иванович");
        respDetailsByDays1.setSpeciality("терапевт");
        respDetailsByDays1.setRoom("110");
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail15 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail15.setDate("15-01-2020");
        statDetail15.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(2, "01:30", "01:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail17 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail17.setDate("17-01-2020");
        statDetail17.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(1, "02:00", "00:30"));
        statDetail17.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1, "00:20"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail20 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail20.setDate("20-01-2020");
        statDetail20.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(0, "00:30", "00:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail22 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail22.setDate("22-01-2020");
        statDetail22.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(2, "01:00", "01:00"));
        statDetail22.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1, "00:30"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail24 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail24.setDate("24-01-2020");
        statDetail24.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1, "00:30"));
        respDetailsByDays1.setByDays(Arrays.asList(statDetail15, statDetail17, statDetail20, statDetail22, statDetail24));

        StatisticsDoctorDtoResponse respDetailsByDays2 = new StatisticsDoctorDtoResponse();
        respDetailsByDays2.setDoctorId(1);
        respDetailsByDays2.setFirstName("Иван");
        respDetailsByDays2.setLastName("Иванов");
        respDetailsByDays2.setPatronymic("Иванович");
        respDetailsByDays2.setSpeciality("терапевт");
        respDetailsByDays2.setRoom("110");
        respDetailsByDays2.setByDays(Arrays.asList(statDetail17));

        StatisticsDoctorDtoResponse respNon = new StatisticsDoctorDtoResponse();
        respNon.setDoctorId(1);
        respNon.setFirstName("Иван");
        respNon.setLastName("Иванов");
        respNon.setPatronymic("Иванович");
        respNon.setSpeciality("терапевт");
        respNon.setRoom("110");
        assertAll(
                () -> assertEquals(responseTotal, statisticsService.getDoctorStatistics(1, "15-01-2020",
                        "30-01-2020", false, false)),
                () -> assertEquals(responseDetails, statisticsService.getDoctorStatistics(1, "15-01-2020",
                        "30-01-2020", true, false)),
                () -> assertEquals(respTotalByDays, statisticsService.getDoctorStatistics(1, "15-01-2020",
                        "30-01-2020", false, true)),
                () -> assertEquals(respDetailsByDays1, statisticsService.getDoctorStatistics(1, "15-01-2020",
                        "30-01-2020", true, true)),
                () -> assertEquals(respDetailsByDays2, statisticsService.getDoctorStatistics(1, "17-01-2020",
                        "17-01-2020", true, true)),
                () -> assertEquals(respNon, statisticsService.getDoctorStatistics(1, "25-01-2020",
                        "30-01-2020", true, false))
        );
    }

    @Test
    public void testTwoDoctorsStatistics() throws DataBaseException {
        List<Schedule> schedules = Arrays.asList(
                new Schedule(new Doctor(), LocalDate.of(2020, 1, 15), Arrays.asList(
                        new SlotSchedule(LocalTime.of(9, 0), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(9, 30), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(10, 0), 30))),
                new Schedule(new Doctor(), LocalDate.of(2020, 1, 17), Arrays.asList(
                        new SlotSchedule(new Ticket(), LocalTime.of(12, 0), 30),
                        new SlotSchedule(LocalTime.of(12, 30), 30),
                        new SlotSchedule(new Ticket(), LocalTime.of(13, 0), 30),
                        new SlotSchedule(LocalTime.of(13, 30), 30))),
                new Schedule(new Doctor(), LocalDate.of(2020, 1, 17), Arrays.asList(
                        new SlotSchedule(new Ticket(), LocalTime.of(10, 0), 20),
                        new SlotSchedule(new Ticket(), LocalTime.of(10, 20), 20))),
                new Schedule(new Doctor(), LocalDate.of(2020, 1, 20), Arrays.asList(
                        new SlotSchedule(new Ticket(), LocalTime.of(13, 0), 20),
                        new SlotSchedule(LocalTime.of(13, 20), 20))));
        Mockito.when(scheduleDao.getByInterval(LocalDate.of(2020, 1, 15), LocalDate.of(2020, 1, 30)))
                .thenReturn(schedules);
        Mockito.when(scheduleDao.getByInterval(LocalDate.of(2020, 1, 25), LocalDate.of(2020, 1, 30)))
                .thenReturn(new ArrayList<>());

        Commission commission1 = new Commission(Arrays.asList(new Doctor()), new Ticket(),
                LocalDate.of(2020, 1, 17), LocalTime.of(10, 0), "110", 20);
        Commission commission2 = new Commission(Arrays.asList(new Doctor()), new Ticket(),
                LocalDate.of(2020, 1, 20), LocalTime.of(10, 0), "110", 35);
        Mockito.when(commissionDao.getByInterval(LocalDate.of(2020, 1, 15),
                LocalDate.of(2020, 1, 30))).thenReturn(Arrays.asList(commission1, commission2));
        Mockito.when(commissionDao.getByInterval(LocalDate.of(2020, 1, 25),
                LocalDate.of(2020, 1, 30))).thenReturn(new ArrayList<>());

        StatisticsDoctorDtoResponse responseTotal = new StatisticsDoctorDtoResponse();
        responseTotal.setStartDate("15-01-2020");
        responseTotal.setEndDate("30-01-2020");
        responseTotal.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(9, "04:50", "03:55"));

        StatisticsDoctorDtoResponse responseDetails = new StatisticsDoctorDtoResponse();
        responseDetails.setStartDate("15-01-2020");
        responseDetails.setEndDate("30-01-2020");
        responseDetails.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(7, "04:50", "03:00"));
        responseDetails.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(2, "00:55"));

        StatisticsDoctorDtoResponse respTotalByDays = new StatisticsDoctorDtoResponse();
        StatisticsDoctorDtoResponse.StatisticsOnDay stat15 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat15.setDate("15-01-2020");
        stat15.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(2, "01:30", "01:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay stat17 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat17.setDate("17-01-2020");
        stat17.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(5, "02:40", "02:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay stat20 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        stat20.setDate("20-01-2020");
        stat20.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(2, "00:40", "00:55"));
        respTotalByDays.setByDays(Arrays.asList(stat15, stat17, stat20));

        StatisticsDoctorDtoResponse respDetailsByDays = new StatisticsDoctorDtoResponse();
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail15 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail15.setDate("15-01-2020");
        statDetail15.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(2, "01:30", "01:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail17 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail17.setDate("17-01-2020");
        statDetail17.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(4, "02:40", "01:40"));
        statDetail17.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1, "00:20"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail20 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail20.setDate("20-01-2020");
        statDetail20.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(1, "00:40", "00:20"));
        statDetail20.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1,  "00:35"));
        respDetailsByDays.setByDays(Arrays.asList(statDetail15, statDetail17, statDetail20));

        assertAll(
                () -> assertEquals(responseTotal, statisticsService.getAllDoctorsStatistics( "15-01-2020",
                        "30-01-2020", false, false)),
                () -> assertEquals(responseDetails, statisticsService.getAllDoctorsStatistics( "15-01-2020",
                        "30-01-2020", true, false)),
                () -> assertEquals(respTotalByDays, statisticsService.getAllDoctorsStatistics( "15-01-2020",
                        "30-01-2020", false, true)),
                () -> assertEquals(respDetailsByDays, statisticsService.getAllDoctorsStatistics( "15-01-2020",
                        "30-01-2020", true, true)),
                () -> assertEquals(new StatisticsDoctorDtoResponse(), statisticsService.getAllDoctorsStatistics( "25-01-2020",
                        "30-01-2020", true, false))
        );
    }
}
