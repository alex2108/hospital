package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Session;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.service.PatientService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class TestPatientService extends BaseTestService {
    private PatientService patientService = new PatientService(patientDao, sessionDao);

    @Test
    public void testRegisterPatientOk() throws DataBaseException {
        RegPatientDtoRequest regPatientReq = new RegPatientDtoRequest();
        regPatientReq.setFirstName("Иван");
        regPatientReq.setLastName("Иванов");
        regPatientReq.setPatronymic("Иванович");
        regPatientReq.setLogin("ivan");
        regPatientReq.setPassword("ivan123");
        regPatientReq.setEmail("ivan@mail.ru");
        regPatientReq.setAddress("ул. Строителей, д. 3, кв. 5");
        regPatientReq.setPhone("+7-904-123-45-67");
        Patient patientNoDash = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Patient patientDB = new Patient(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Session session = new Session(patientDB);
        Mockito.when(patientDao.insert(patientNoDash)).thenReturn(patientDB);

        PatientDtoResponse responseAct = patientService.register(regPatientReq);
        ArgumentCaptor<Session> captorSession = ArgumentCaptor.forClass(Session.class);
        Mockito.verify(sessionDao).insert(captorSession.capture());
        assertAll(
                () -> assertEquals(1, responseAct.getId()),
                () -> assertEquals("Иван", responseAct.getFirstName()),
                () -> assertEquals("Иванов", responseAct.getLastName()),
                () -> assertEquals("Иванович", responseAct.getPatronymic()),
                () -> assertEquals("ivan@mail.ru", responseAct.getEmail()),
                () -> assertEquals("ул. Строителей, д. 3, кв. 5", responseAct.getAddress()),
                () -> assertEquals("+79041234567", responseAct.getPhone()),
                () -> assertNotNull(responseAct.getJavaSessionId()),
                () -> assertEquals(patientDB, captorSession.getValue().getUser())
        );
    }

    @Test
    public void testRegisterPatientLoginExist() throws DataBaseException {
        RegPatientDtoRequest regPatientReq = new RegPatientDtoRequest();
        regPatientReq.setFirstName("Иван");
        regPatientReq.setLastName("Иванов");
        regPatientReq.setPatronymic("Иванович");
        regPatientReq.setLogin("ivan");
        regPatientReq.setPassword("ivan123");
        regPatientReq.setEmail("ivan@mail.ru");
        regPatientReq.setAddress("ул. Строителей, д. 3, кв. 5");
        regPatientReq.setPhone("+7-904-123-45-67");
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Mockito.when(patientDao.insert(patient)).thenThrow(new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> patientService.register(regPatientReq));
        assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testUpdatePatient() throws DataBaseException {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPatronymic("Иванович");
        updateReq.setOldPassword("ivan123");
        updateReq.setNewPassword("ivan345");
        updateReq.setEmail("ivan@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 5, кв. 1");
        updateReq.setPhone("+7-901-123-45-67");

        Patient oldPatient = new Patient();
        oldPatient.setId(1);
        oldPatient.setLogin("petr");
        Patient newPatient = new Patient(1,"Иван", "Иванов", "Иванович",
                null, "ivan345", "ivan@mail.ru", "ул. Центральная, д. 5, кв. 1", "+79011234567");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(oldPatient));
        Mockito.when(patientDao.get("petr", "ivan123")).thenReturn(oldPatient);
        Mockito.when(patientDao.update(newPatient)).thenReturn(newPatient);

        PatientDtoResponse responseExp = new PatientDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName("Иван");
        responseExp.setLastName("Иванов");
        responseExp.setPatronymic("Иванович");
        responseExp.setEmail("ivan@mail.ru");
        responseExp.setAddress("ул. Центральная, д. 5, кв. 1");
        responseExp.setPhone("+79011234567");

        assertEquals(responseExp, patientService.update(updateReq, "abc-123"));
    }

    @Test
    public void testUpdateAdminWrongPass() throws DataBaseException {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPatronymic("Иванович");
        updateReq.setOldPassword("ivan_123");
        updateReq.setNewPassword("ivan345");
        updateReq.setEmail("ivan@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 5, кв. 1");
        updateReq.setPhone("+7-901-123-45-67");

        Patient patient = new Patient();
        patient.setId(1);
        patient.setLogin("petr");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));
        Mockito.when(patientDao.get("petr", "ivan_123")).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> patientService.update(updateReq, "abc-123"));
        assertEquals(DataBaseErrorCode.PASSWORD_WRONG.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetPatientNoId() throws DataBaseException {
        Mockito.when(patientDao.getById(1))
                .thenThrow(new DataBaseException(DataBaseErrorCode.PATIENT_ID_NOT_EXIST));
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> patientService.getPatient(1));
        assertEquals(DataBaseErrorCode.PATIENT_ID_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetPatientOk() throws DataBaseException {
        Patient patient = new Patient(1,"Иван", "Иванов", "Иванович",
                null, "ivan345", "ivan@mail.ru", "ул. Центральная, д. 5, кв. 1", "+79011234567");
        Mockito.when(patientDao.getById(1)).thenReturn(patient);

        PatientDtoResponse responseExp = new PatientDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName("Иван");
        responseExp.setLastName("Иванов");
        responseExp.setPatronymic("Иванович");
        responseExp.setEmail("ivan@mail.ru");
        responseExp.setAddress("ул. Центральная, д. 5, кв. 1");
        responseExp.setPhone("+79011234567");
        assertEquals(responseExp, patientService.getPatient(1));
    }
}
