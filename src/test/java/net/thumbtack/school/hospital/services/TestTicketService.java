package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.TicketDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.service.TicketService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestTicketService extends BaseTestService {
    private TicketService ticketService =
            new TicketService(scheduleDao, commissionDao, ticketDao, sessionDao);

    @Test
    public void testGetTickets() throws DataBaseException {
        Patient patient = new Patient();
        Mockito.when(sessionDao.getByCookie("abc-123"))
                .thenReturn(new Session(patient));

        Doctor doctor1 = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Doctor doctor2 = new Doctor(2, "Петр", "Петров", "Иванович",
                "petr", "ivan345", "отоларинголог", "111");
        Doctor doctor3 = new Doctor(3, "Семен", "Сергеев", "Иванович",
                "semen", "ivan345", "отоларинголог", "112");
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2019, 3, 25), Arrays.asList(
                        new SlotSchedule(new Ticket(patient, "D2503"), LocalTime.of(9, 30), 30)));
        Schedule schedule2 = new Schedule(doctor2,
                LocalDate.of(2019, 4, 10), Arrays.asList(
                new SlotSchedule(new Ticket(patient, "D1004"), LocalTime.of(12, 0), 20)));
        Mockito.when(scheduleDao.getByPatient(patient))
                .thenReturn(Arrays.asList(schedule1, schedule2));

        Commission commission1 = new Commission(Arrays.asList(doctor1, doctor2),
                new Ticket(patient, "CD2603"), LocalDate.of(2019, 3, 26),
                LocalTime.of(9, 35), "110", 30);
        Commission commission2 = new Commission(Arrays.asList(doctor1, doctor3),
                new Ticket(patient, "CD0104"), LocalDate.of(2019, 4, 1),
                LocalTime.of(10, 0), "112", 20);
        Mockito.when(commissionDao.getByPatient(patient))
                .thenReturn(Arrays.asList(commission1, commission2));

        TicketDtoResponse ticketResp1 = new TicketDtoResponse();
        ticketResp1.setTicket("D2503");
        ticketResp1.setRoom("110");
        ticketResp1.setDate("25-03-2019");
        ticketResp1.setTime("09:30");
        ticketResp1.setDoctorId(1);
        ticketResp1.setFirstName("Иван");
        ticketResp1.setLastName("Иванов");
        ticketResp1.setPatronymic("Иванович");
        ticketResp1.setSpeciality("терапевт");
        TicketDtoResponse ticketResp2 = new TicketDtoResponse();
        ticketResp2.setTicket("D1004");
        ticketResp2.setRoom("111");
        ticketResp2.setDate("10-04-2019");
        ticketResp2.setTime("12:00");
        ticketResp2.setDoctorId(2);
        ticketResp2.setFirstName("Петр");
        ticketResp2.setLastName("Петров");
        ticketResp2.setPatronymic("Иванович");
        ticketResp2.setSpeciality("отоларинголог");
        TicketDtoResponse ticketResp3 = new TicketDtoResponse();
        ticketResp3.setTicket("CD2603");
        ticketResp3.setRoom("110");
        ticketResp3.setDate("26-03-2019");
        ticketResp3.setTime("09:35");
        ticketResp3.setDoctorsCommission(Arrays.asList(
                new TicketDtoResponse.DoctorDto(1, "Иван", "Иванов",
                        "Иванович", "терапевт"),
                new TicketDtoResponse.DoctorDto(2, "Петр", "Петров",
                        "Иванович", "отоларинголог")));
        TicketDtoResponse ticketResp4 = new TicketDtoResponse();
        ticketResp4.setTicket("CD0104");
        ticketResp4.setRoom("112");
        ticketResp4.setDate("01-04-2019");
        ticketResp4.setTime("10:00");
        ticketResp4.setDoctorsCommission(Arrays.asList(
                new TicketDtoResponse.DoctorDto(1, "Иван", "Иванов",
                        "Иванович", "терапевт"),
                new TicketDtoResponse.DoctorDto(3, "Семен", "Сергеев",
                        "Иванович", "отоларинголог")));
        assertEquals(Arrays.asList(ticketResp1, ticketResp2, ticketResp3, ticketResp4),
                ticketService.getTickets("abc-123"));
    }

    @Test
    public void testDeleteTicketAppointmentByDoctor() throws DataBaseException {
        Doctor doctor = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Ticket ticket = new Ticket(new Patient(), "D2503");
        Schedule schedule = new Schedule(doctor,
                LocalDate.of(2019, 3, 25), Arrays.asList(
                new SlotSchedule(ticket, LocalTime.of(9, 30), 30)));
        doctor.setSchedules(Arrays.asList(schedule));

        Mockito.when(sessionDao.getByCookie("abc-123"))
                .thenReturn(new Session(doctor));
        Mockito.when(ticketDao.getAppointmentTicketByNumber("D2503")).thenReturn(ticket);

        ticketService.deleteTicketAppointment("D2503", "abc-123");
        Mockito.verify(ticketDao).deleteAppointment(ticket);
    }

    @Test
    public void testDeleteTicketAppointmentByDoctorNoTicket() throws DataBaseException {
        Doctor doctor = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Ticket ticket = new Ticket(new Patient(), "D2503");
        Schedule schedule = new Schedule(doctor,
                LocalDate.of(2019, 3, 25), Arrays.asList(
                new SlotSchedule(ticket, LocalTime.of(9, 30), 30)));
        doctor.setSchedules(Arrays.asList(schedule));
        Mockito.when(sessionDao.getByCookie("abc-123"))
                .thenReturn(new Session(doctor));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> ticketService.deleteTicketAppointment("D0001", "abc-123"));
        assertEquals(DataBaseErrorCode.DOCTOR_HAS_NOT_TICKET.getErrorCode(), throwable.getMessage());
        Mockito.verify(ticketDao, Mockito.never()).deleteAppointment(Mockito.any(Ticket.class));
    }

    @Test
    public void testDeleteTicketAppointmentByAdmin() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Ticket ticket = new Ticket(new Patient(), "D2503");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(admin));
        Mockito.when(ticketDao.getAppointmentTicketByNumber("D2503")).thenReturn(ticket);

        ticketService.deleteTicketAppointment("D2503", "abc-123");
        Mockito.verify(ticketDao).deleteAppointment(ticket);
    }

    @Test
    public void testDeleteTicketAppointmentByAdminNoTicket() throws DataBaseException {
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(admin));
        Mockito.when(ticketDao.getAppointmentTicketByNumber("D2503")).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> ticketService.deleteTicketAppointment("D2503", "abc-123"));
        assertEquals(DataBaseErrorCode.TICKET_APPOINT_NUMBER_NOT_EXIST.getErrorCode(), throwable.getMessage());
        Mockito.verify(ticketDao, Mockito.never()).deleteAppointment(Mockito.any(Ticket.class));
    }

    @Test
    public void testDeleteTicketAppointmentByPatient() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Ticket ticket = new Ticket(patient, "D2503");
        Mockito.when(sessionDao.getByCookie("abc-123"))
                .thenReturn(new Session(patient));
        Mockito.when(ticketDao.getAppointmentTicketByNumber("D2503")).thenReturn(ticket);

        ticketService.deleteTicketAppointment("D2503", "abc-123");
        Mockito.verify(ticketDao).deleteAppointment(ticket);
    }

    @Test
    public void testDeleteTicketAppointmentByPatientNoTicket() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));
        Mockito.when(ticketDao.getAppointmentTicketByNumber("D2503")).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> ticketService.deleteTicketAppointment("D2503", "abc-123"));
        assertEquals(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.getErrorCode(), throwable.getMessage());
        Mockito.verify(ticketDao, Mockito.never()).deleteAppointment(Mockito.any(Ticket.class));
    }

    @Test
    public void testDeleteTicketAppointmentByAnotherPatient() throws DataBaseException {
        Patient patient1 = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Patient patient2 = new Patient("Петр", "Петров", "Петрович",
                "petr", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Ticket ticket = new Ticket(patient2, "D2503");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient1));
        Mockito.when(ticketDao.getAppointmentTicketByNumber("D2503")).thenReturn(ticket);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> ticketService.deleteTicketAppointment("D2503", "abc-123"));
        assertEquals(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.getErrorCode(), throwable.getMessage());
        Mockito.verify(ticketDao, Mockito.never()).deleteAppointment(Mockito.any(Ticket.class));
    }

    @Test
    public void testDeleteCommission() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        LocalDate dateCommission = LocalDate.of(2020, 3, 20);
        Doctor doctor1 = new Doctor("A", "А", "A", "A", "A", "A", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30),
                new SlotSchedule(LocalTime.of(11, 30), 30),
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "Б", "Б");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 25), 20),
                new SlotSchedule(LocalTime.of(11, 45), 20),
                new SlotSchedule(LocalTime.of(12, 5), 20),
                new SlotSchedule(LocalTime.of(12, 25), 20));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));

        Ticket ticket = new Ticket(patient, "D2003");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2), ticket,
                dateCommission, LocalTime.of(12, 0), "110", 20);
        List<SlotSchedule> slotsCommission = new ArrayList<>();
        slotsCommission.add(slots1.get(2));
        slotsCommission.add(slots2.get(1));
        slotsCommission.add(slots2.get(2));
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));
        Mockito.when(ticketDao.getCommissionTicketByNumber("D2003")).thenReturn(ticket);
        Mockito.when(commissionDao.getByTicket(ticket)).thenReturn(commission);

        ticketService.deleteTicketCommission("D2003", "abc-123");
        Mockito.verify(ticketDao).deleteCommission(ticket, slotsCommission);
        Mockito.verify(ticketDao, Mockito.never()).delete(Mockito.any(Ticket.class));
    }

    @Test
    public void testDeleteCommissionNotIntersectSlots() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        LocalDate dateCommission = LocalDate.of(2020, 3, 20);
        Doctor doctor1 = new Doctor("A", "А", "A", "A", "A", "A", "110");
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30),
                new SlotSchedule(LocalTime.of(11, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "Б", "Б");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 20), 20),
                new SlotSchedule(LocalTime.of(12, 40), 20));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));

        Ticket ticket = new Ticket(patient, "D2003");
        Commission commission = new Commission(Arrays.asList(doctor1, doctor2), ticket,
                dateCommission, LocalTime.of(12, 0), "110", 20);
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));
        Mockito.when(ticketDao.getCommissionTicketByNumber("D2003")).thenReturn(ticket);
        Mockito.when(commissionDao.getByTicket(ticket)).thenReturn(commission);

        ticketService.deleteTicketCommission("D2003", "abc-123");
        Mockito.verify(ticketDao).delete(ticket);
        Mockito.verify(ticketDao, Mockito.never()).deleteCommission(Mockito.any(Ticket.class), Mockito.anyList());
    }

    @Test
    public void testDeleteCommissionAnotherPatient() throws DataBaseException {
        Patient patient1 = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Patient patient2 = new Patient("Петр", "Петров", "Петрович",
                "petr", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Ticket ticket = new Ticket(patient2, "D2003");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient1));
        Mockito.when(ticketDao.getCommissionTicketByNumber("D2003")).thenReturn(ticket);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> ticketService.deleteTicketCommission("D2003", "abc-123"));
        assertEquals(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.getErrorCode(), throwable.getMessage());
        Mockito.verify(ticketDao, Mockito.never()).deleteAppointment(Mockito.any(Ticket.class));
        Mockito.verify(ticketDao, Mockito.never()).deleteCommission(Mockito.any(Ticket.class), Mockito.anyList());
    }

    @Test
    public void testDeleteCommissionNoTicket() throws DataBaseException {
        Patient patient = new Patient("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));
        Mockito.when(ticketDao.getCommissionTicketByNumber("D2003")).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> ticketService.deleteTicketCommission("D2003", "abc-123"));
        assertEquals(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.getErrorCode(), throwable.getMessage());
        Mockito.verify(ticketDao, Mockito.never()).deleteAppointment(Mockito.any(Ticket.class));
        Mockito.verify(ticketDao, Mockito.never()).deleteCommission(Mockito.any(Ticket.class), Mockito.anyList());
    }

}
