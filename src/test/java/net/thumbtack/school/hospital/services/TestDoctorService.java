package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.service.DoctorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static net.thumbtack.school.hospital.dao.BaseTestDao.checkDoctorFields;
import static net.thumbtack.school.hospital.dao.BaseTestDao.checkScheduleFields;
import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.powermock.api.mockito.PowerMockito.mock;

public class TestDoctorService extends BaseTestService {
    private DoctorService doctorService =
            new DoctorService(doctorDao, scheduleDao, commissionDao, ticketDao, sessionDao);

    @Test
    public void testRegisterDoctorSpecialityInvalid() throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setSpeciality("чародей");
        Mockito.when(doctorDao.isSpecialityExist("чародей")).thenReturn(false);
        Mockito.when(doctorDao.isRoomExist(null)).thenReturn(true);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.register(regDoctorReq));
        assertEquals(DataBaseErrorCode.SPECIALITY_INVALID.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testRegisterDoctorRoomInvalid() throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setRoom("001");
        Mockito.when(doctorDao.isSpecialityExist(null)).thenReturn(true);
        Mockito.when(doctorDao.isRoomExist("001")).thenReturn(false);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.register(regDoctorReq),
                DataBaseErrorCode.ROOM_INVALID.getErrorCode());
        assertEquals(DataBaseErrorCode.ROOM_INVALID.getErrorCode(), throwable.getMessage());
    }

    public static Stream<Arguments> paramsDataBaseException() {
        return Stream.of(
                Arguments.arguments(DataBaseErrorCode.ROOM_BUSY),
                Arguments.arguments(DataBaseErrorCode.LOGIN_EXISTS)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsDataBaseException")
    public void testRegisterDoctorRoomBusy(DataBaseErrorCode errorCode) throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart("15-01-2020");
        regDoctorReq.setDateEnd("15-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("10:00");
        weekSchedule.setWeekDays(Arrays.asList("Wed"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        Mockito.when(doctorDao.isSpecialityExist(Mockito.anyString())).thenReturn(true);
        Mockito.when(doctorDao.isRoomExist(Mockito.anyString())).thenReturn(true);
        Mockito.when(doctorDao.insert(Mockito.any(Doctor.class)))
                .thenThrow(new DataBaseException(errorCode));
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.register(regDoctorReq));
        assertEquals(errorCode.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testRegisterDoctorWithSchedule1() throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan123");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart("15-01-2020");
        regDoctorReq.setDateEnd("23-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<Schedule> schedules = Arrays.asList(
                new Schedule(doctor, LocalDate.of(2020, 1, 15), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 17), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 20), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 22), slotSchedules));
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);

        DoctorDtoResponse doctorRespAct = doctorService.register(regDoctorReq);
        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setFirstName("Иван");
        doctorRespExp.setLastName("Иванов");
        doctorRespExp.setPatronymic("Иванович");
        doctorRespExp.setRoom("110");
        doctorRespExp.setSpeciality("терапевт");
        List<SlotScheduleDto> slotsDto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        doctorRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("15-01-2020", slotsDto),
                new ScheduleDto("17-01-2020", slotsDto),
                new ScheduleDto("20-01-2020", slotsDto),
                new ScheduleDto("22-01-2020", slotsDto)));
        assertEquals(doctorRespExp, doctorRespAct);
        ArgumentCaptor<Doctor> captorDoctor = ArgumentCaptor.forClass(Doctor.class);
        Mockito.verify(doctorDao).insert(captorDoctor.capture());
        checkDoctorFields(doctor, captorDoctor.getValue());
    }

    @Test
    public void testRegisterDoctorWithSchedule2() throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan123");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart("15-01-2020");
        regDoctorReq.setDateEnd("23-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(null);
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<Schedule> schedules = Arrays.asList(
                new Schedule(doctor, LocalDate.of(2020, 1, 15), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 16), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 17), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 20), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 21), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 22), slotSchedules),
                new Schedule(doctor, LocalDate.of(2020, 1, 23), slotSchedules));
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);

        DoctorDtoResponse doctorRespAct = doctorService.register(regDoctorReq);
        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setFirstName("Иван");
        doctorRespExp.setLastName("Иванов");
        doctorRespExp.setPatronymic("Иванович");
        doctorRespExp.setRoom("110");
        doctorRespExp.setSpeciality("терапевт");
        List<SlotScheduleDto> slotsDto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        doctorRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("15-01-2020", slotsDto),
                new ScheduleDto("16-01-2020", slotsDto),
                new ScheduleDto("17-01-2020", slotsDto),
                new ScheduleDto("20-01-2020", slotsDto),
                new ScheduleDto("21-01-2020", slotsDto),
                new ScheduleDto("22-01-2020", slotsDto),
                new ScheduleDto("23-01-2020", slotsDto)));
        assertEquals(doctorRespExp, doctorRespAct);
        ArgumentCaptor<Doctor> captorDoctor = ArgumentCaptor.forClass(Doctor.class);
        Mockito.verify(doctorDao).insert(captorDoctor.capture());
        checkDoctorFields(doctor, captorDoctor.getValue());
    }

    @Test
    public void testInsertSchedule3() throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan123");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart("15-02-2020");
        regDoctorReq.setDateEnd("01-03-2020");
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Tue", "15:00", "17:00"));
        weekDaysSchedule.add(new DaySchedule("Thu", "10:00", "12:00"));
        regDoctorReq.setWeekDaysSchedule(weekDaysSchedule);
        regDoctorReq.setDuration(20);

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedulesTue = new ArrayList<>();
        List<SlotSchedule> slotSchedulesThu = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            slotSchedulesTue.add(new SlotSchedule(LocalTime.of(15, 0).plusMinutes(i * 20), 20));
            slotSchedulesThu.add(new SlotSchedule(LocalTime.of(10, 0).plusMinutes(i * 20), 20));
        }
        Schedule schedule18 = new Schedule(doctor,
                LocalDate.of(2020, 2, 18), slotSchedulesTue);
        Schedule schedule20 = new Schedule(doctor,
                LocalDate.of(2020, 2, 20), slotSchedulesThu);
        Schedule schedule25 = new Schedule(doctor,
                LocalDate.of(2020, 2, 25), slotSchedulesTue);
        Schedule schedule27 = new Schedule(doctor,
                LocalDate.of(2020, 2, 27), slotSchedulesThu);
        List<Schedule> schedules = Arrays.asList(schedule18, schedule20, schedule25, schedule27);
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);

        DoctorDtoResponse doctorRespAct = doctorService.register(regDoctorReq);
        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setFirstName("Иван");
        doctorRespExp.setLastName("Иванов");
        doctorRespExp.setPatronymic("Иванович");
        doctorRespExp.setRoom("110");
        doctorRespExp.setSpeciality("терапевт");
        List<SlotScheduleDto> slotsTueDto = Arrays.asList(
                new SlotScheduleDto("15:00"),
                new SlotScheduleDto("15:20"),
                new SlotScheduleDto("15:40"),
                new SlotScheduleDto("16:00"),
                new SlotScheduleDto("16:20"),
                new SlotScheduleDto("16:40"));
        List<SlotScheduleDto> slotsThuDto = Arrays.asList(
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:20"),
                new SlotScheduleDto("10:40"),
                new SlotScheduleDto("11:00"),
                new SlotScheduleDto("11:20"),
                new SlotScheduleDto("11:40"));
        doctorRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("18-02-2020", slotsTueDto),
                new ScheduleDto("20-02-2020", slotsThuDto),
                new ScheduleDto("25-02-2020", slotsTueDto),
                new ScheduleDto("27-02-2020", slotsThuDto)));
        assertEquals(doctorRespExp, doctorRespAct);
        ArgumentCaptor<Doctor> captorDoctor = ArgumentCaptor.forClass(Doctor.class);
        Mockito.verify(doctorDao).insert(captorDoctor.capture());
        checkDoctorFields(doctor, captorDoctor.getValue());
    }

    @Test
    public void testInsertSchedule4() throws DataBaseException {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan123");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart("15-01-2020");
        regDoctorReq.setDateEnd("16-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<Schedule> schedules = Arrays.asList(new Schedule(doctor,
                LocalDate.of(2020, 1, 15), slotSchedules));
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);

        DoctorDtoResponse doctorRespAct = doctorService.register(regDoctorReq);
        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setFirstName("Иван");
        doctorRespExp.setLastName("Иванов");
        doctorRespExp.setPatronymic("Иванович");
        doctorRespExp.setRoom("110");
        doctorRespExp.setSpeciality("терапевт");
        List<SlotScheduleDto> slotsDto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        doctorRespExp.setSchedule(Arrays.asList(new ScheduleDto("15-01-2020", slotsDto)));
        assertEquals(doctorRespExp, doctorRespAct);
        ArgumentCaptor<Doctor> captorDoctor = ArgumentCaptor.forClass(Doctor.class);
        Mockito.verify(doctorDao).insert(captorDoctor.capture());
        checkDoctorFields(doctor, captorDoctor.getValue());
    }

    @Test
    public void testUpdateDoctorNoId() throws DataBaseException {
        Mockito.when(doctorDao.getById(1))
                .thenThrow(new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.update(new UpdateDoctorDtoRequest(), 1));
        assertEquals(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testUpdateDoctorFired() throws DataBaseException {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart("15-01-2020");
        updateReq.setDateEnd("23-01-2020");
        Doctor doctor = new Doctor();
        doctor.setFiredDate(LocalDate.of(2020, 1,23));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.update(updateReq, 1));
        assertEquals(DataBaseErrorCode.DOCTOR_FIRED.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testUpdateDoctorNewSchedule() throws DataBaseException {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart("15-01-2020");
        updateReq.setDateEnd("23-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        Doctor doctorOldSched = new Doctor("А", "А", "А", "А", "А", "А", "А");
        doctorOldSched.setId(1);
        List<SlotSchedule> oldSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 20),
                new SlotSchedule(LocalTime.of(15, 20), 20));
        doctorOldSched.setSchedules(Arrays.asList(new Schedule(doctorOldSched,
                LocalDate.of(2020, 1, 14), oldSlots)));

        Doctor doctorNewSched = new Doctor("А", "А", "А", "А", "А", "А", "А");
        doctorNewSched.setId(1);
        List<SlotSchedule> newSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<Schedule> newSchedules = Arrays.asList(
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 15), newSlots),
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 17), newSlots),
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 20), newSlots),
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 22), newSlots));
        doctorNewSched.getSchedules().add(new Schedule(doctorNewSched, LocalDate.of(2020, 1, 14), oldSlots));
        doctorNewSched.getSchedules().addAll(newSchedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctorOldSched).thenReturn(doctorNewSched);

        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setId(1);
        doctorRespExp.setFirstName("А");
        doctorRespExp.setLastName("А");
        doctorRespExp.setPatronymic("А");
        doctorRespExp.setSpeciality("А");
        doctorRespExp.setRoom("А");
        List<SlotScheduleDto> slotsDto1 = Arrays.asList(
                new SlotScheduleDto("15:00"),
                new SlotScheduleDto("15:20"));
        List<SlotScheduleDto> slotsDto2 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        doctorRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("14-01-2020", slotsDto1),
                new ScheduleDto("15-01-2020", slotsDto2),
                new ScheduleDto("17-01-2020", slotsDto2),
                new ScheduleDto("20-01-2020", slotsDto2),
                new ScheduleDto("22-01-2020", slotsDto2)));
        assertEquals(doctorRespExp, doctorService.update(updateReq, 1));

        Mockito.verify(scheduleDao, Mockito.never()).update(Mockito.anyList());
        ArgumentCaptor<List<Schedule>> captorShedules = ArgumentCaptor.forClass(List.class);
        Mockito.verify(scheduleDao).insert(captorShedules.capture());
        List<Schedule> schedulesInserted = captorShedules.getValue();
        checkScheduleFields(doctorNewSched.getSchedules().get(1), schedulesInserted.get(0));
        checkScheduleFields(doctorNewSched.getSchedules().get(2), schedulesInserted.get(1));
        checkScheduleFields(doctorNewSched.getSchedules().get(3), schedulesInserted.get(2));
    }

    @Test
    public void testUpdateDoctorUpdateSchedule1() throws DataBaseException {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart("20-01-2020");
        updateReq.setDateEnd("24-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("15:00");
        weekSchedule.setTimeEnd("16:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        Doctor doctorOldSched = new Doctor(1, "А", "А", "А", "А", "А", "А", "А");
        List<SlotSchedule> oldSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        doctorOldSched.setSchedules(Arrays.asList(
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 15), oldSlots),
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 17), oldSlots),
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 20), oldSlots),
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 22), oldSlots)));

        Doctor doctorNewSched = new Doctor(1, "А", "А", "А", "А", "А", "А", "А");
        List<SlotSchedule> newSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 30),
                new SlotSchedule(LocalTime.of(15, 30), 30));
        List<Schedule> updateSchedules = Arrays.asList(
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 20), newSlots),
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 22), newSlots));
        List<Schedule> newSchedules = Arrays.asList(
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 24), newSlots));
        doctorNewSched.getSchedules().add(new Schedule(doctorNewSched, LocalDate.of(2020, 1, 15), oldSlots));
        doctorNewSched.getSchedules().add(new Schedule(doctorNewSched, LocalDate.of(2020, 1, 17), oldSlots));
        doctorNewSched.getSchedules().addAll(updateSchedules);
        doctorNewSched.getSchedules().addAll(newSchedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctorOldSched).thenReturn(doctorNewSched);

        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setId(1);
        doctorRespExp.setFirstName("А");
        doctorRespExp.setLastName("А");
        doctorRespExp.setPatronymic("А");
        doctorRespExp.setSpeciality("А");
        doctorRespExp.setRoom("А");
        List<SlotScheduleDto> slotsDto1 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"));
        List<SlotScheduleDto> slotsDto2 = Arrays.asList(
                new SlotScheduleDto("15:00"),
                new SlotScheduleDto("15:30"));
        doctorRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("15-01-2020", slotsDto1),
                new ScheduleDto("17-01-2020", slotsDto1),
                new ScheduleDto("20-01-2020", slotsDto2),
                new ScheduleDto("22-01-2020", slotsDto2),
                new ScheduleDto("24-01-2020", slotsDto2)));
        assertEquals(doctorRespExp, doctorService.update(updateReq, 1));

        ArgumentCaptor<List<Schedule>> captorUpdateSchedules = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<Schedule>> captorNewSchedules = ArgumentCaptor.forClass(List.class);
        Mockito.verify(scheduleDao).update(captorUpdateSchedules.capture());
        Mockito.verify(scheduleDao).insert(captorNewSchedules.capture());
        List<Schedule> schedulesUpdated = captorUpdateSchedules.getValue();
        List<Schedule> schedulesInserted = captorNewSchedules.getValue();
        checkScheduleFields(doctorNewSched.getSchedules().get(2), schedulesUpdated.get(0));
        checkScheduleFields(doctorNewSched.getSchedules().get(3), schedulesUpdated.get(1));
        checkScheduleFields(doctorNewSched.getSchedules().get(4), schedulesInserted.get(0));
    }

    @Test
    public void testUpdateDoctorUpdateSchedule2() throws DataBaseException {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart("20-01-2020");
        updateReq.setDateEnd("24-01-2020");
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Tue", "15:00", "16:00"));
        weekDaysSchedule.add(new DaySchedule("Thu", "10:00", "11:00"));
        updateReq.setWeekDaysSchedule(weekDaysSchedule);
        updateReq.setDuration(30);

        Doctor doctorOldSched = new Doctor(1, "А", "А", "А", "А", "А", "А", "А");
        List<SlotSchedule> oldSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        doctorOldSched.setSchedules(Arrays.asList(
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 17), oldSlots),
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 21), oldSlots)));

        Doctor doctorNewSched = new Doctor(1, "А", "А", "А", "А", "А", "А", "А");
        List<Schedule> updateSchedules = Arrays.asList(
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 21), Arrays.asList(
                        new SlotSchedule(LocalTime.of(15, 0), 30),
                        new SlotSchedule(LocalTime.of(15, 30), 30))));
        List<Schedule> newSchedules = Arrays.asList(
                new Schedule(doctorNewSched, LocalDate.of(2020, 1, 23), Arrays.asList(
                        new SlotSchedule(LocalTime.of(10, 0), 30),
                        new SlotSchedule(LocalTime.of(10, 30), 30))));
        doctorNewSched.getSchedules().add(new Schedule(doctorNewSched, LocalDate.of(2020, 1, 17), oldSlots));
        doctorNewSched.getSchedules().addAll(updateSchedules);
        doctorNewSched.getSchedules().addAll(newSchedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctorOldSched).thenReturn(doctorNewSched);

        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setId(1);
        doctorRespExp.setFirstName("А");
        doctorRespExp.setLastName("А");
        doctorRespExp.setPatronymic("А");
        doctorRespExp.setSpeciality("А");
        doctorRespExp.setRoom("А");
        List<SlotScheduleDto> slotsDto1 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"));
        List<SlotScheduleDto> slotsDto2 = Arrays.asList(
                new SlotScheduleDto("15:00"),
                new SlotScheduleDto("15:30"));
        List<SlotScheduleDto> slotsDto3 = Arrays.asList(
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        doctorRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("17-01-2020", slotsDto1),
                new ScheduleDto("21-01-2020", slotsDto2),
                new ScheduleDto("23-01-2020", slotsDto3)));
        assertEquals(doctorRespExp, doctorService.update(updateReq, 1));

        ArgumentCaptor<List<Schedule>> captorUpdateSchedules = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<Schedule>> captorNewSchedules = ArgumentCaptor.forClass(List.class);
        Mockito.verify(scheduleDao).update(captorUpdateSchedules.capture());
        Mockito.verify(scheduleDao).insert(captorNewSchedules.capture());
        List<Schedule> schedulesUpdated = captorUpdateSchedules.getValue();
        List<Schedule> schedulesInserted = captorNewSchedules.getValue();
        checkScheduleFields(doctorNewSched.getSchedules().get(1), schedulesUpdated.get(0));
        checkScheduleFields(doctorNewSched.getSchedules().get(2), schedulesInserted.get(0));
    }

    @Test
    public void testUpdateDoctorBusy() throws DataBaseException {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart("20-01-2020");
        updateReq.setDateEnd("24-01-2020");
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("15:00");
        weekSchedule.setTimeEnd("16:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        Doctor doctorOldSched = new Doctor(1, "А", "А", "А", "А", "А", "А", "А");
        List<SlotSchedule> oldSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        doctorOldSched.setSchedules(Arrays.asList(
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 15), oldSlots),
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 17), oldSlots),
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 20), oldSlots)));

        List<SlotSchedule> newSlots = Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 30),
                new SlotSchedule(LocalTime.of(15, 30), 30));
        List<Schedule> updateSchedules = Arrays.asList(
                new Schedule(doctorOldSched, LocalDate.of(2020, 1, 20), newSlots));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctorOldSched);
        Mockito.when(scheduleDao.update(updateSchedules)).thenThrow(new DataBaseException(DataBaseErrorCode.SLOT_TIME_BUSY));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.update(updateReq, 1));
        assertEquals(DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetDoctorNoId() throws DataBaseException {
        Mockito.when(doctorDao.getById(1))
                .thenThrow(new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.getDoctor(1, "123", true,
                        "01-03-2020", "01-04-2020"));
        assertEquals(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetDoctorNoSchedule() throws DataBaseException {
        Doctor doctor = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        List<Schedule> schedules = new ArrayList<>();
        schedules.add(new Schedule(doctor, LocalDate.now().plusDays(10), slots));
        schedules.add(new Schedule(doctor, LocalDate.now().plusDays(20), slots));
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);

        DoctorDtoResponse doctorRespExp = new DoctorDtoResponse();
        doctorRespExp.setId(1);
        doctorRespExp.setFirstName("Иван");
        doctorRespExp.setLastName("Иванов");
        doctorRespExp.setPatronymic("Иванович");
        doctorRespExp.setSpeciality("терапевт");
        doctorRespExp.setRoom("110");

        assertEquals(doctorRespExp, doctorService.getDoctor(1, "123", false,
                LocalDate.now().plusDays(5).format(formatterDate), LocalDate.now().plusDays(30).format(formatterDate)));
    }

    @Test
    public void testGetDoctorWithSchedule() throws DataBaseException {
        Doctor doctor = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<String> datesStr = new ArrayList<>();
        List<Schedule> schedules = new ArrayList<>();
        for (int i = 0; i < 35; i++) {
            LocalDate date = LocalDate.now().plusDays(2 * i);
            datesStr.add(date.format(formatterDate));
            schedules.add(new Schedule(doctor, date, slots));
        }
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);
        Mockito.when(sessionDao.getByCookie("123"))
                .thenReturn(new Session(new Admin())).thenReturn(new Session(new Patient()));
        Mockito.when(sessionDao.getByCookie("321")).thenReturn(new Session(new Doctor()));

        DoctorDtoResponse responseExp1 = new DoctorDtoResponse();
        responseExp1.setId(1);
        responseExp1.setFirstName("Иван");
        responseExp1.setLastName("Иванов");
        responseExp1.setPatronymic("Иванович");
        responseExp1.setSpeciality("терапевт");
        responseExp1.setRoom("110");
        List<SlotScheduleDto> slotsDto1 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        List<ScheduleDto> schedulesDto1 = new ArrayList<>();
        for (int i = 10; i < 15; i++) {
            schedulesDto1.add(new ScheduleDto(datesStr.get(i), slotsDto1));
        }
        responseExp1.setSchedule(schedulesDto1);

        DoctorDtoResponse responseExp2 = new DoctorDtoResponse();
        responseExp2.setId(1);
        responseExp2.setFirstName("Иван");
        responseExp2.setLastName("Иванов");
        responseExp2.setPatronymic("Иванович");
        responseExp2.setSpeciality("терапевт");
        responseExp2.setRoom("110");
        List<SlotScheduleDto> slotsDto2 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        List<ScheduleDto> schedulesDto2 = new ArrayList<>();
        for (String dateStr : datesStr) {
            if (LocalDate.parse(dateStr, formatterDate).isAfter(LocalDate.now().plusMonths(2)))
                break;
            schedulesDto2.add(new ScheduleDto(dateStr, slotsDto2));
        }
        responseExp2.setSchedule(schedulesDto2);

        DoctorDtoResponse responseExp3 = new DoctorDtoResponse();
        responseExp3.setId(1);
        responseExp3.setFirstName("Иван");
        responseExp3.setLastName("Иванов");
        responseExp3.setPatronymic("Иванович");
        responseExp3.setSpeciality("терапевт");
        responseExp3.setRoom("110");
        List<SlotScheduleDto> slotsDto3 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        List<ScheduleDto> schedulesDto3 = new ArrayList<>();
        for (int i = 0; i < 22; i++) {
            schedulesDto3.add(new ScheduleDto(datesStr.get(i), slotsDto3));
        }
        responseExp3.setSchedule(schedulesDto3);

        DoctorDtoResponse responseExp4 = new DoctorDtoResponse();
        responseExp4.setId(1);
        responseExp4.setFirstName("Иван");
        responseExp4.setLastName("Иванов");
        responseExp4.setPatronymic("Иванович");
        responseExp4.setSpeciality("терапевт");
        responseExp4.setRoom("110");
        List<SlotScheduleDto> slotsDto4 = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        List<ScheduleDto> schedulesDto4 = new ArrayList<>();
        for (int i = 28; i < 35; i++) {
            if (LocalDate.parse(datesStr.get(i), formatterDate).isAfter(LocalDate.now().plusMonths(2)))
                break;
            schedulesDto4.add(new ScheduleDto(datesStr.get(i), slotsDto4));
        }
        responseExp4.setSchedule(schedulesDto4);

        assertAll(
                () -> assertEquals(responseExp1, doctorService.getDoctor(1, "123", true,
                        LocalDate.now().plusDays(20).format(formatterDate), LocalDate.now().plusDays(29).format(formatterDate))),
                () -> assertEquals(responseExp2, doctorService.getDoctor(1, "123", true,
                        null, null)),
                () -> assertEquals(responseExp3, doctorService.getDoctor(1, "321", true,
                        null, LocalDate.now().plusDays(43).format(formatterDate))),
                () -> assertEquals(responseExp4, doctorService.getDoctor(1, "321", true,
                        LocalDate.now().plusDays(56).format(formatterDate), null))
        );
    }

    @Test
    public void testGetDoctorWithScheduleAndPatients() throws DataBaseException {
        Doctor doctor = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Patient patient1 = new Patient(2, "Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Patient patient2 = new Patient(3, "Мария", "Петрова", "Сергеевна",
                "maria", "petrova123", "maria@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");

        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(new Ticket(patient1, "123"), LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(new Ticket(patient2, "456"), LocalTime.of(15, 0), 20),
                new SlotSchedule(LocalTime.of(15, 20), 20),
                new SlotSchedule(LocalTime.of(15, 40), 20));
        List<Schedule> schedules = new ArrayList<>();
        schedules.add(new Schedule(doctor, LocalDate.now().plusDays(10), slots1));
        schedules.add(new Schedule(doctor, LocalDate.now().plusDays(50), slots2));
        doctor.setSchedules(schedules);
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(patient1));
        Mockito.when(sessionDao.getByCookie("456")).thenReturn(new Session(patient2));
        Mockito.when(sessionDao.getByCookie("321")).thenReturn(new Session(new Admin()));
        Mockito.when(sessionDao.getByCookie("654")).thenReturn(new Session(new Doctor()));

        DoctorDtoResponse responseExp1 = new DoctorDtoResponse();
        responseExp1.setId(1);
        responseExp1.setFirstName("Иван");
        responseExp1.setLastName("Иванов");
        responseExp1.setPatronymic("Иванович");
        responseExp1.setSpeciality("терапевт");
        responseExp1.setRoom("110");
        responseExp1.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(
                                new SlotScheduleDto("09:00"),
                                new SlotScheduleDto("09:30"),
                                new SlotScheduleDto("10:00", new PatientDto(2, "Петр", "Петров", "Петрович",
                                        "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")),
                                new SlotScheduleDto("10:30"))),
                new ScheduleDto(LocalDate.now().plusDays(50).format(formatterDate),
                        Arrays.asList(
                                new SlotScheduleDto("15:00"),
                                new SlotScheduleDto("15:20"),
                                new SlotScheduleDto("15:40")))
        ));

        DoctorDtoResponse responseExp2 = new DoctorDtoResponse();
        responseExp2.setId(1);
        responseExp2.setFirstName("Иван");
        responseExp2.setLastName("Иванов");
        responseExp2.setPatronymic("Иванович");
        responseExp2.setSpeciality("терапевт");
        responseExp2.setRoom("110");
        responseExp2.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(
                                new SlotScheduleDto("09:00"),
                                new SlotScheduleDto("09:30"),
                                new SlotScheduleDto("10:00"),
                                new SlotScheduleDto("10:30"))),
                new ScheduleDto(LocalDate.now().plusDays(50).format(formatterDate),
                        Arrays.asList(
                                new SlotScheduleDto("15:00", new PatientDto(3, "Мария", "Петрова", "Сергеевна",
                                        "maria@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")),
                                new SlotScheduleDto("15:20"),
                                new SlotScheduleDto("15:40")))
        ));

        DoctorDtoResponse responseExp3 = new DoctorDtoResponse();
        responseExp3.setId(1);
        responseExp3.setFirstName("Иван");
        responseExp3.setLastName("Иванов");
        responseExp3.setPatronymic("Иванович");
        responseExp3.setSpeciality("терапевт");
        responseExp3.setRoom("110");
        responseExp3.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(
                                new SlotScheduleDto("09:00"),
                                new SlotScheduleDto("09:30"),
                                new SlotScheduleDto("10:00", new PatientDto(2, "Петр", "Петров", "Петрович",
                                        "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")),
                                new SlotScheduleDto("10:30"))),
                new ScheduleDto(LocalDate.now().plusDays(50).format(formatterDate),
                        Arrays.asList(
                                new SlotScheduleDto("15:00", new PatientDto(3, "Мария", "Петрова", "Сергеевна",
                                        "maria@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")),
                                new SlotScheduleDto("15:20"),
                                new SlotScheduleDto("15:40")))
        ));

        assertAll(
                () -> assertEquals(responseExp1, doctorService.getDoctor(1, "123", true,
                        null, null)),
                () -> assertEquals(responseExp2, doctorService.getDoctor(1, "456", true,
                        null, null)),
                () -> assertEquals(responseExp3, doctorService.getDoctor(1, "321", true,
                        null, null)),
                () -> assertEquals(responseExp3, doctorService.getDoctor(1, "654", true,
                        null, null))
        );
    }

    @Test
    public void testGetDoctorsNoDoctors() throws DataBaseException {
        Mockito.when(doctorDao.getAll()).thenReturn(Collections.emptyList());

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.getDoctors(null, null, true,
                        null, null));
        assertEquals(DataBaseErrorCode.DOCTORS_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetDoctorsInvalidSpeciality() throws DataBaseException {
        Mockito.when(doctorDao.isSpecialityExist("маг"))
                .thenThrow(new DataBaseException(DataBaseErrorCode.SPECIALITY_INVALID));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.getDoctors("маг", null, true,
                        null, null));
        assertEquals(DataBaseErrorCode.SPECIALITY_INVALID.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetDoctorsNoDocBySpeciality() throws DataBaseException {
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);
        Mockito.when(doctorDao.getBySpeciality("терапевт")).thenReturn(Collections.emptyList());

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.getDoctors("терапевт", null, true,
                        null, null));
        assertEquals(DataBaseErrorCode.DOCTOR_SPECIALITY_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testGetDoctorsNoSchedule() throws DataBaseException {
        Doctor doctor1 = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slots = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30));
        List<Schedule> schedules1 = new ArrayList<>();
        schedules1.add(new Schedule(doctor1, LocalDate.now().plusDays(10), slots));
        schedules1.add(new Schedule(doctor1, LocalDate.now().plusDays(20), slots));
        doctor1.setSchedules(schedules1);

        Doctor doctor2 = new Doctor(2, "Петр", "Иванов", "Иванович",
                "ivan", "ivan123", "хирург", "111");
        List<Schedule> schedules2 = new ArrayList<>();
        schedules2.add(new Schedule(doctor2, LocalDate.now().plusDays(5), slots));
        schedules2.add(new Schedule(doctor2, LocalDate.now().plusDays(20), slots));
        doctor2.setSchedules(schedules2);

        Doctor doctor3 = new Doctor(3, "Семен", "Иванов", "Иванович",
                "ivan", "ivan123", "хирург", "112");
        List<Schedule> schedules3 = new ArrayList<>();
        schedules3.add(new Schedule(doctor3, LocalDate.now().plusDays(20), slots));
        schedules3.add(new Schedule(doctor3, LocalDate.now().plusDays(25), slots));
        doctor3.setSchedules(schedules3);

        Mockito.when(doctorDao.getAll()).thenReturn(Arrays.asList(doctor1, doctor2, doctor3));
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);
        Mockito.when(doctorDao.isSpecialityExist("хирург")).thenReturn(true);
        Mockito.when(doctorDao.getBySpeciality("терапевт")).thenReturn(Arrays.asList(doctor1));
        Mockito.when(doctorDao.getBySpeciality("хирург")).thenReturn(Arrays.asList(doctor2, doctor3));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(new Admin()));
        Mockito.when(sessionDao.getByCookie("456")).thenReturn(new Session(new Patient()));
        Mockito.when(sessionDao.getByCookie("321")).thenReturn(new Session(new Doctor()));

        DoctorDtoResponse doctorRespExp1 = new DoctorDtoResponse();
        doctorRespExp1.setId(1);
        doctorRespExp1.setFirstName("Иван");
        doctorRespExp1.setLastName("Иванов");
        doctorRespExp1.setPatronymic("Иванович");
        doctorRespExp1.setSpeciality("терапевт");
        doctorRespExp1.setRoom("110");

        DoctorDtoResponse doctorRespExp2 = new DoctorDtoResponse();
        doctorRespExp2.setId(2);
        doctorRespExp2.setFirstName("Петр");
        doctorRespExp2.setLastName("Иванов");
        doctorRespExp2.setPatronymic("Иванович");
        doctorRespExp2.setSpeciality("хирург");
        doctorRespExp2.setRoom("111");

        DoctorDtoResponse doctorRespExp3 = new DoctorDtoResponse();
        doctorRespExp3.setId(3);
        doctorRespExp3.setFirstName("Семен");
        doctorRespExp3.setLastName("Иванов");
        doctorRespExp3.setPatronymic("Иванович");
        doctorRespExp3.setSpeciality("хирург");
        doctorRespExp3.setRoom("112");

        assertAll(
                () -> assertEquals(Arrays.asList(doctorRespExp1),
                        doctorService.getDoctors("терапевт", "123", false,
                                LocalDate.now().plusDays(5).format(formatterDate), LocalDate.now().plusDays(30).format(formatterDate))),
                () -> assertEquals(Arrays.asList(doctorRespExp2, doctorRespExp3),
                        doctorService.getDoctors("хирург", "456", false,
                                LocalDate.now().plusDays(5).format(formatterDate), null)),
                () -> assertEquals(Arrays.asList(doctorRespExp1, doctorRespExp2, doctorRespExp3),
                        doctorService.getDoctors(null, "321", false,
                                null, LocalDate.now().plusDays(30).format(formatterDate)))
        );
    }

    @Test
    public void testGetDoctorsWithSchedule() throws DataBaseException {
        Doctor doctor1 = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<Schedule> schedules1 = new ArrayList<>();
        schedules1.add(new Schedule(doctor1, LocalDate.now().plusDays(10), Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30))));
        schedules1.add(new Schedule(doctor1, LocalDate.now().plusDays(50), Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 30))));
        doctor1.setSchedules(schedules1);

        Doctor doctor2 = new Doctor(2, "Петр", "Иванов", "Иванович",
                "ivan", "ivan123", "хирург", "111");
        List<Schedule> schedules2 = new ArrayList<>();
        schedules2.add(new Schedule(doctor2, LocalDate.now().plusDays(5), Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 20), 20))));
        schedules2.add(new Schedule(doctor2, LocalDate.now().plusDays(10), Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 20), 20))));
        doctor2.setSchedules(schedules2);

        Doctor doctor3 = new Doctor(3, "Семен", "Иванов", "Иванович",
                "ivan", "ivan123", "хирург", "112");
        List<Schedule> schedules3 = new ArrayList<>();
        schedules3.add(new Schedule(doctor3, LocalDate.now().plusDays(20),Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 0), 20))));
        schedules3.add(new Schedule(doctor3, LocalDate.now().plusDays(55),Arrays.asList(
                new SlotSchedule(LocalTime.of(15, 0), 20))));
        doctor3.setSchedules(schedules3);

        Mockito.when(doctorDao.getAll()).thenReturn(Arrays.asList(doctor1, doctor2, doctor3));
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);
        Mockito.when(doctorDao.isSpecialityExist("хирург")).thenReturn(true);
        Mockito.when(doctorDao.getBySpeciality("терапевт")).thenReturn(Arrays.asList(doctor1));
        Mockito.when(doctorDao.getBySpeciality("хирург")).thenReturn(Arrays.asList(doctor2, doctor3));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(new Admin()));
        Mockito.when(sessionDao.getByCookie("456")).thenReturn(new Session(new Patient()));
        Mockito.when(sessionDao.getByCookie("321")).thenReturn(new Session(new Doctor()));

        DoctorDtoResponse doctor1RespExp1 = new DoctorDtoResponse();
        doctor1RespExp1.setId(1);
        doctor1RespExp1.setFirstName("Иван");
        doctor1RespExp1.setLastName("Иванов");
        doctor1RespExp1.setPatronymic("Иванович");
        doctor1RespExp1.setSpeciality("терапевт");
        doctor1RespExp1.setRoom("110");
        doctor1RespExp1.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("09:00")))));

        DoctorDtoResponse doctor2RespExp1 = new DoctorDtoResponse();
        doctor2RespExp1.setId(2);
        doctor2RespExp1.setFirstName("Петр");
        doctor2RespExp1.setLastName("Иванов");
        doctor2RespExp1.setPatronymic("Иванович");
        doctor2RespExp1.setSpeciality("хирург");
        doctor2RespExp1.setRoom("111");
        doctor2RespExp1.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(5).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("12:20"))),
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:20")))));

        DoctorDtoResponse doctor2RespExp2 = new DoctorDtoResponse();
        doctor2RespExp2.setId(2);
        doctor2RespExp2.setFirstName("Петр");
        doctor2RespExp2.setLastName("Иванов");
        doctor2RespExp2.setPatronymic("Иванович");
        doctor2RespExp2.setSpeciality("хирург");
        doctor2RespExp2.setRoom("111");
        doctor2RespExp2.setSchedule(new ArrayList<>());

        DoctorDtoResponse doctor3RespExp1 = new DoctorDtoResponse();
        doctor3RespExp1.setId(3);
        doctor3RespExp1.setFirstName("Семен");
        doctor3RespExp1.setLastName("Иванов");
        doctor3RespExp1.setPatronymic("Иванович");
        doctor3RespExp1.setSpeciality("хирург");
        doctor3RespExp1.setRoom("112");
        doctor3RespExp1.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(20).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:00")))));

        DoctorDtoResponse doctor3RespExp2 = new DoctorDtoResponse();
        doctor3RespExp2.setId(3);
        doctor3RespExp2.setFirstName("Семен");
        doctor3RespExp2.setLastName("Иванов");
        doctor3RespExp2.setPatronymic("Иванович");
        doctor3RespExp2.setSpeciality("хирург");
        doctor3RespExp2.setRoom("112");
        doctor3RespExp2.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(20).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:00"))),
                new ScheduleDto(LocalDate.now().plusDays(55).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("15:00")))));
        assertAll(
                () -> assertEquals(Arrays.asList(doctor1RespExp1),
                        doctorService.getDoctors("терапевт", "123", true,
                                LocalDate.now().plusDays(10).format(formatterDate), LocalDate.now().plusDays(20).format(formatterDate))),
                () -> assertEquals(Arrays.asList(doctor2RespExp1, doctor3RespExp1),
                        doctorService.getDoctors("хирург", "456", true,
                                LocalDate.now().plusDays(2).format(formatterDate), LocalDate.now().plusDays(25).format(formatterDate))),
                () -> assertEquals(Arrays.asList(doctor1RespExp1, doctor2RespExp1, doctor3RespExp1),
                        doctorService.getDoctors(null, "321", true,
                                null, LocalDate.now().plusDays(30).format(formatterDate))),
                () -> assertEquals(Arrays.asList(doctor2RespExp2, doctor3RespExp2),
                        doctorService.getDoctors("хирург", "321", true,
                                LocalDate.now().plusDays(15).format(formatterDate), null))
        );
    }

    @Test
    public void testGetDoctorsWithScheduleAndPatients() throws DataBaseException {
        Patient patient1 = new Patient(3, "Семен", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Patient patient2 = new Patient(4, "Мария", "Петрова", "Сергеевна",
                "maria", "petrova123", "maria@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");

        Doctor doctor1 = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<Schedule> schedules1 = new ArrayList<>();
        schedules1.add(new Schedule(doctor1, LocalDate.now().plusDays(10), Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30))));
        schedules1.add(new Schedule(doctor1, LocalDate.now().plusDays(50), Arrays.asList(
                new SlotSchedule(new Ticket(patient1, "123"), LocalTime.of(10, 0), 30))));
        doctor1.setSchedules(schedules1);

        Doctor doctor2 = new Doctor(2, "Петр", "Иванов", "Иванович",
                "ivan", "ivan123", "хирург", "111");
        List<Schedule> schedules2 = new ArrayList<>();
        schedules2.add(new Schedule(doctor2, LocalDate.now().plusDays(5), Arrays.asList(
                new SlotSchedule(new Ticket(patient2, "456"),LocalTime.of(12, 20), 20))));
        schedules2.add(new Schedule(doctor2, LocalDate.now().plusDays(10), Arrays.asList(
                new SlotSchedule(LocalTime.of(10, 20), 20))));
        doctor2.setSchedules(schedules2);

        Mockito.when(doctorDao.getAll()).thenReturn(Arrays.asList(doctor1, doctor2));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(new Doctor()));
        Mockito.when(sessionDao.getByCookie("456")).thenReturn(new Session(patient1));
        Mockito.when(sessionDao.getByCookie("321")).thenReturn(new Session(patient2));
        Mockito.when(sessionDao.getByCookie("654")).thenReturn(new Session(new Patient()));

        DoctorDtoResponse doctor1RespExp1 = new DoctorDtoResponse();
        doctor1RespExp1.setId(1);
        doctor1RespExp1.setFirstName("Иван");
        doctor1RespExp1.setLastName("Иванов");
        doctor1RespExp1.setPatronymic("Иванович");
        doctor1RespExp1.setSpeciality("терапевт");
        doctor1RespExp1.setRoom("110");
        doctor1RespExp1.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("09:00"))),
                new ScheduleDto(LocalDate.now().plusDays(50).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:00", new PatientDto(3, "Семен", "Петров", "Петрович",
                                "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567"))))));

        DoctorDtoResponse doctor1RespExp2 = new DoctorDtoResponse();
        doctor1RespExp2.setId(1);
        doctor1RespExp2.setFirstName("Иван");
        doctor1RespExp2.setLastName("Иванов");
        doctor1RespExp2.setPatronymic("Иванович");
        doctor1RespExp2.setSpeciality("терапевт");
        doctor1RespExp2.setRoom("110");
        doctor1RespExp2.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("09:00"))),
                new ScheduleDto(LocalDate.now().plusDays(50).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:00")))));

        DoctorDtoResponse doctor2RespExp1 = new DoctorDtoResponse();
        doctor2RespExp1.setId(2);
        doctor2RespExp1.setFirstName("Петр");
        doctor2RespExp1.setLastName("Иванов");
        doctor2RespExp1.setPatronymic("Иванович");
        doctor2RespExp1.setSpeciality("хирург");
        doctor2RespExp1.setRoom("111");
        doctor2RespExp1.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(5).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("12:20", new PatientDto(4, "Мария", "Петрова", "Сергеевна",
                                "maria@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")))),
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:20")))));


        DoctorDtoResponse doctor2RespExp2 = new DoctorDtoResponse();
        doctor2RespExp2.setId(2);
        doctor2RespExp2.setFirstName("Петр");
        doctor2RespExp2.setLastName("Иванов");
        doctor2RespExp2.setPatronymic("Иванович");
        doctor2RespExp2.setSpeciality("хирург");
        doctor2RespExp2.setRoom("111");
        doctor2RespExp2.setSchedule(Arrays.asList(
                new ScheduleDto(LocalDate.now().plusDays(5).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("12:20"))),
                new ScheduleDto(LocalDate.now().plusDays(10).format(formatterDate),
                        Arrays.asList(new SlotScheduleDto("10:20")))));

        assertAll(
                () -> assertEquals(Arrays.asList(doctor1RespExp1, doctor2RespExp1),
                        doctorService.getDoctors(null, "123", true, null, null)),
                () -> assertEquals(Arrays.asList(doctor1RespExp1, doctor2RespExp2),
                        doctorService.getDoctors(null, "456", true, null, null)),
                () -> assertEquals(Arrays.asList(doctor1RespExp2, doctor2RespExp1),
                        doctorService.getDoctors(null, "321", true, null, null)),
                () -> assertEquals(Arrays.asList(doctor1RespExp2, doctor2RespExp2),
                        doctorService.getDoctors(null, "654", true, null, null))
        );
    }

    @Test
    public void testDeleteDoctorOk() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);

        doctorService.delete(new DeleteDoctorDtoRequest("15-04-2019"), 1);
        ArgumentCaptor<Doctor> captorDoctor = ArgumentCaptor.forClass(Doctor.class);
        Mockito.verify(doctorDao).delete(captorDoctor.capture());
        assertEquals(LocalDate.of(2019,4,15), captorDoctor.getValue().getFiredDate());
    }

    @Test
    public void testDeleteDoctorIdNotExist() throws DataBaseException {
        Mockito.when(doctorDao.getById(1)).thenReturn(null);
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.delete(new DeleteDoctorDtoRequest("15-04-2019"), 1));
        assertEquals(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST.getErrorCode(), throwable.getMessage());
        Mockito.verify(doctorDao, Mockito.never()).delete(Mockito.any(Doctor.class));
    }

    @Test
    public void testDeleteDoctorAlreadyFired() throws DataBaseException {
        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        doctor.setFiredDate(LocalDate.of(2019,4,15));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> doctorService.delete(new DeleteDoctorDtoRequest("15-04-2019"), 1));
        assertEquals(DataBaseErrorCode.DOCTOR_ALREADY_FIRED.getErrorCode(), throwable.getMessage());
        Mockito.verify(doctorDao, Mockito.never()).delete(Mockito.any(Doctor.class));
    }


}
