package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.CommissionDtoRequest;
import net.thumbtack.school.hospital.dto.CommissionDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.service.CommissionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static net.thumbtack.school.hospital.dao.BaseTestDao.checkCommissionFields;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;

public class TestCommissionService extends BaseTestService {

    private CommissionService commissionService =
            new CommissionService(doctorDao, patientDao, commissionDao, sessionDao);

    @Test
    public void testCommissionInsertNoPatient() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);

        Mockito.when(patientDao.getById(1)).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.PATIENT_ID_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testCommissionInsertInvalidRoom() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setRoom("1a");

        Mockito.when(patientDao.getById(1)).thenReturn(new Patient());
        Mockito.when(doctorDao.isRoomExist("1a")).thenReturn(false);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.ROOM_INVALID.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testCommissionInsertNoDoctors() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2, 2, 4, 5));

        Mockito.when(patientDao.getById(1)).thenReturn(new Patient());
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2, 2, 4, 5)))
                .thenReturn(Arrays.asList(new Doctor(), new Doctor(), new Doctor()));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.DOCTOR_IDS_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testCommissionInsertOneDoctor() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(2);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1));
        Doctor doctor = new Doctor();
        doctor.setId(1);

        Mockito.when(patientDao.getById(2)).thenReturn(new Patient());
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1))).thenReturn(Arrays.asList(doctor));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.SIZE_COMMISSION_INVALID.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testCommissionOtherRoom() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setRoom("113");
        comReq.setDoctorIds(Arrays.asList(1, 2, 5));
        Doctor doctor1 = new Doctor();
        doctor1.setId(1);
        doctor1.setRoom("110");
        Doctor doctor2 = new Doctor();
        doctor2.setRoom("111");
        Doctor doctor3 = new Doctor();
        doctor3.setRoom("112");

        Mockito.when(patientDao.getById(1)).thenReturn(new Patient());
        Mockito.when(doctorDao.isRoomExist("113")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2, 5)))
                .thenReturn(new ArrayList<>(Arrays.asList(doctor1, doctor2, doctor3)));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.ROOM_NOT_BELONG.getErrorCode(), throwable.getMessage());
    }

    public static Stream<Arguments> paramsIntersectCommission1() {
        return Stream.of(
                Arguments.arguments(LocalTime.of(11, 40), 21),
                Arguments.arguments(LocalTime.of(12, 19), 20)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsIntersectCommission1")
    public void testCommissionOtherCommission1(LocalTime time, int duration) throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2, 5));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);
        Doctor doctor1 = new Doctor();
        doctor1.setId(1);
        doctor1.setRoom("110");
        Doctor doctor2 = new Doctor();
        doctor2.setRoom("111");
        Doctor doctor3 = new Doctor();
        doctor3.setRoom("112");
        Patient patient = new Patient();

        Mockito.when(patientDao.getById(1)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2, 5)))
                .thenReturn(new ArrayList<>(Arrays.asList(doctor1, doctor2, doctor3)));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));

        Commission commission = new Commission();
        commission.setTime(time);
        commission.setDuration(duration);
        List<Commission> comPatient = Arrays.asList(commission);
        Mockito.when(commissionDao.getByPatient(patient, LocalDate.of(2020, 3, 20)))
                .thenReturn(comPatient);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.COMMISSION_EXIST.getErrorCode(), throwable.getMessage());
    }

    @ParameterizedTest
    @MethodSource("paramsIntersectCommission1")
    public void testCommissionOtherCommission2(LocalTime time, int duration) throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2, 5));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);
        Doctor doctor1 = new Doctor();
        doctor1.setId(1);
        doctor1.setRoom("110");
        Doctor doctor2 = new Doctor();
        doctor2.setRoom("111");
        Doctor doctor3 = new Doctor();
        doctor3.setRoom("112");
        List<Doctor> doctors = new ArrayList<>(Arrays.asList(doctor1, doctor2, doctor3));
        Patient patient = new Patient();

        Mockito.when(patientDao.getById(1)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2, 5))).thenReturn(doctors);
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));

        Commission commission = new Commission();
        commission.setTime(time);
        commission.setDuration(duration);
        List<Commission> comDoctors = Arrays.asList(commission);
        Mockito.when(commissionDao.getByDoctors(doctors, LocalDate.of(2020, 3, 20)))
                .thenReturn(comDoctors);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.COMMISSION_EXIST.getErrorCode(), throwable.getMessage());
    }

    public static Stream<Arguments> paramsIntersectCommission2() {
        return Stream.of(
                Arguments.arguments(LocalTime.of(11, 40), 21, LocalTime.of(12, 31), 15),
                Arguments.arguments(LocalTime.of(11, 0), 20, LocalTime.of(12, 19), 15)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsIntersectCommission2")
    public void testCommissionOtherCommission3(LocalTime timePatient, int durationPatient,
                                               LocalTime timeDoctors, int durationDoctors) throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2, 5));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);
        Doctor doctor1 = new Doctor();
        doctor1.setId(1);
        doctor1.setRoom("110");
        Doctor doctor2 = new Doctor();
        doctor2.setRoom("111");
        Doctor doctor3 = new Doctor();
        doctor3.setRoom("112");
        List<Doctor> doctors = new ArrayList<>(Arrays.asList(doctor1, doctor2, doctor3));
        Patient patient = new Patient();

        Mockito.when(patientDao.getById(1)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2, 5))).thenReturn(doctors);
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));

        Commission commission1 = new Commission();
        commission1.setTime(timePatient);
        commission1.setDuration(durationPatient);
        List<Commission> comPatient = new ArrayList<>();
        comPatient.add(commission1);
        Commission commission2 = new Commission();
        commission2.setTime(timeDoctors);
        commission2.setDuration(durationDoctors);
        List<Commission> comDoctors = new ArrayList<>();
        comDoctors.add(commission2);
        Mockito.when(commissionDao.getByPatient(patient, LocalDate.of(2020, 3, 20)))
                .thenReturn(comPatient);
        Mockito.when(commissionDao.getByDoctors(doctors, LocalDate.of(2020, 3, 20)))
                .thenReturn(comDoctors);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.COMMISSION_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertCommission1() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(2, 2, 5));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);

        LocalDate dateCommission = LocalDate.of(2020, 3, 20);
        Doctor doctor1 = new Doctor("A", "А", "A", "A", "A", "A", "110");
        doctor1.setId(1);
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 30),
                new SlotSchedule(LocalTime.of(11, 30), 30),
                new SlotSchedule(1, LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, dateCommission, slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));

        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "Б", "Б");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 25), 20),
                new SlotSchedule(2, LocalTime.of(11, 45), 20),
                new SlotSchedule(3, LocalTime.of(12, 5), 20),
                new SlotSchedule(LocalTime.of(12, 25), 20));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));

        Doctor doctor3 = new Doctor("В", "В", "В", "В", "В", "В", "Б");
        List<SlotSchedule> slots3 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 20), 20),
                new SlotSchedule(LocalTime.of(11, 40), 20),
                new SlotSchedule(4, LocalTime.of(12, 0), 20),
                new SlotSchedule(LocalTime.of(12, 20), 20));
        Schedule schedule3 = new Schedule(doctor3, dateCommission, slots3);
        doctor3.setSchedules(Arrays.asList(schedule3));

        List<Doctor> doctors = new ArrayList<>(Arrays.asList(doctor2, doctor3, doctor1));
        Patient patient = new Patient();
        patient.setId(10);

        Mockito.when(patientDao.getById(10)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(2, 5)))
                .thenReturn(new ArrayList<>(Arrays.asList(doctor2, doctor3)));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));

        Commission commission1 = new Commission();
        commission1.setTime(LocalTime.of(11, 40));
        commission1.setDuration(20);
        Commission commission2 = new Commission();
        commission2.setTime(LocalTime.of(12, 20));
        commission2.setDuration(15);
        List<Commission> comPatient = new ArrayList<>(Arrays.asList(commission1, commission2));
        Commission commission3 = new Commission();
        commission3.setTime(LocalTime.of(11, 35));
        commission3.setDuration(25);
        Commission commission4 = new Commission();
        commission4.setTime(LocalTime.of(12, 20));
        commission4.setDuration(30);
        List<Commission> comDoctors = new ArrayList<>(Arrays.asList(commission3, commission4));
        Mockito.when(commissionDao.getByPatient(patient, dateCommission))
                .thenReturn(comPatient);
        Mockito.when(commissionDao.getByDoctors(doctors, dateCommission))
                .thenReturn(comDoctors);

        List<SlotSchedule> slotsCommission = new ArrayList<>();
        slotsCommission.add(slots2.get(1));
        slotsCommission.add(slots2.get(2));
        slotsCommission.add(slots3.get(2));
        slotsCommission.add(slots1.get(2));
        Commission commission = new Commission(doctors, new Ticket(patient, "CD1D2D5200320201200"),
                dateCommission, LocalTime.of(12, 0), "110", 20);
        Mockito.when(commissionDao.insert(commission, slotsCommission)).thenReturn(commission);

        CommissionDtoResponse commissionRespExp = new CommissionDtoResponse();
        commissionRespExp.setTicket("CD1D2D5200320201200");
        commissionRespExp.setPatientId(10);
        commissionRespExp.setDoctorIds(Arrays.asList(1,2,5));
        commissionRespExp.setDate("20-03-2020");
        commissionRespExp.setTime("12:00");
        commissionRespExp.setRoom("110");
        commissionRespExp.setDuration(20);

        CommissionDtoResponse respAct = commissionService.insert(comReq, "123");
        ArgumentCaptor<Commission> captorComission = ArgumentCaptor.forClass(Commission.class);
        ArgumentCaptor<List<SlotSchedule>> captorSlots = ArgumentCaptor.forClass(List.class);
        Mockito.verify(commissionDao).insert(captorComission.capture(), captorSlots.capture());
        assertAll(
                () -> assertEquals(commissionRespExp, respAct),
                () -> assertEquals(slotsCommission, captorSlots.getValue())
        );
        checkCommissionFields(commission, captorComission.getValue());
        Mockito.verify(commissionDao, Mockito.never()).insert(Mockito.any(Commission.class));
    }

    @Test
    public void testInsertCommission2() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);

        LocalDate dateCommission = LocalDate.of(2020, 3, 20);
        Doctor doctor1 = new Doctor("A", "А", "A", "A", "A", "A", "110");
        doctor1.setId(1);
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, LocalDate.of(2020, 3, 15), slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));

        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "Б", "Б");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 20),
                new SlotSchedule(LocalTime.of(11, 20), 20));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));

        List<Doctor> doctors = new ArrayList<>(Arrays.asList(doctor1, doctor2));
        Patient patient = new Patient();
        patient.setId(10);

        Mockito.when(patientDao.getById(10)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2))).thenReturn(doctors);
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));
        Mockito.when(commissionDao.getByPatient(patient, dateCommission))
                .thenReturn(Collections.emptyList());
        Mockito.when(commissionDao.getByDoctors(doctors, dateCommission))
                .thenReturn(Collections.emptyList());

        Commission commission = new Commission(doctors, new Ticket(patient, "CD1D2200320201200"),
                dateCommission, LocalTime.of(12, 0), "110", 20);
        Mockito.when(commissionDao.insert(commission)).thenReturn(commission);

        CommissionDtoResponse commissionRespExp = new CommissionDtoResponse();
        commissionRespExp.setTicket("CD1D2200320201200");
        commissionRespExp.setPatientId(10);
        commissionRespExp.setDoctorIds(Arrays.asList(1,2));
        commissionRespExp.setDate("20-03-2020");
        commissionRespExp.setTime("12:00");
        commissionRespExp.setRoom("110");
        commissionRespExp.setDuration(20);

        assertEquals(commissionRespExp, commissionService.insert(comReq, "123"));
        ArgumentCaptor<Commission> captorComission = ArgumentCaptor.forClass(Commission.class);
        Mockito.verify(commissionDao).insert(captorComission.capture());
        checkCommissionFields(commission, captorComission.getValue());
        Mockito.verify(commissionDao, Mockito.never()).insert(Mockito.any(Commission.class), Mockito.anyList());
    }

    @Test
    public void testInsertCommissionSlotBusy() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);

        LocalDate dateCommission = LocalDate.of(2020, 3, 20);
        Doctor doctor = new Doctor();
        doctor.setId(1);
        doctor.setRoom("110");
        List<Doctor> doctors = Arrays.asList(doctor, new Doctor());
        Schedule schedule = new Schedule(doctor, dateCommission, Arrays.asList(
                new SlotSchedule(1, LocalTime.of(12, 19), 30),
                new SlotSchedule(LocalTime.of(12, 49), 30)));
        doctor.setSchedules(Arrays.asList(schedule));
        Patient patient = new Patient();
        Mockito.when(patientDao.getById(10)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2))).thenReturn(doctors);
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor));

        Commission commission = new Commission(doctors, new Ticket(patient, "CD1D2200320201200"),
                dateCommission, LocalTime.of(12, 0), "110", 20);
        Mockito.when(commissionDao.insert(commission, Arrays.asList(schedule.getSlotSchedules().get(0))))
                .thenThrow(new DataBaseException(DataBaseErrorCode.SLOT_TIME_BUSY));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertCommissionDoctorFired() throws DataBaseException {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1, 2));
        comReq.setDate("20-03-2020");
        comReq.setTime("12:00");
        comReq.setDuration(20);

        LocalDate dateCommission = LocalDate.of(2020, 3, 20);
        Doctor doctor1 = new Doctor("A", "А", "A", "A", "A", "A", "110");
        doctor1.setId(1);
        List<SlotSchedule> slots1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor1, LocalDate.of(2020, 3, 15), slots1);
        doctor1.setSchedules(Arrays.asList(schedule1));
        doctor1.setFiredDate(LocalDate.of(2020, 3, 20));

        Doctor doctor2 = new Doctor("Б", "Б", "Б", "Б", "Б", "Б", "Б");
        List<SlotSchedule> slots2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(11, 0), 20),
                new SlotSchedule(LocalTime.of(11, 20), 20));
        Schedule schedule2 = new Schedule(doctor2, dateCommission, slots2);
        doctor2.setSchedules(Arrays.asList(schedule2));

        Patient patient = new Patient();
        patient.setId(10);
        Mockito.when(patientDao.getById(10)).thenReturn(patient);
        Mockito.when(doctorDao.isRoomExist("110")).thenReturn(true);
        Mockito.when(doctorDao.getByIds(Arrays.asList(1, 2)))
                .thenReturn(new ArrayList<>(Arrays.asList(doctor1, doctor2)));
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(doctor1));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> commissionService.insert(comReq, "123"));
        assertEquals(DataBaseErrorCode.DOCTOR_FIRED.getErrorCode(), throwable.getMessage());
        Mockito.verify(commissionDao, Mockito.never()).insert(Mockito.any(Commission.class));
        Mockito.verify(commissionDao, Mockito.never()).insert(Mockito.any(Commission.class), Mockito.anyList());
    }



}
