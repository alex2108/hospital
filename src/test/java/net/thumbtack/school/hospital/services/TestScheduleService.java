package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dao.CommissionDao;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.dao.ScheduleDao;
import net.thumbtack.school.hospital.dao.SessionDao;
import net.thumbtack.school.hospital.dto.AppointDtoRequest;
import net.thumbtack.school.hospital.dto.AppointDtoResponse;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.service.ScheduleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.powermock.api.mockito.PowerMockito.mock;

public class TestScheduleService extends BaseTestService {

    private DoctorDao doctorDao = mock(DoctorDao.class);
    private ScheduleDao scheduleDao = mock(ScheduleDao.class);
    private CommissionDao commissionDao = mock(CommissionDao.class);
    private SessionDao sessionDao = mock(SessionDao.class);
    private ScheduleService scheduleService = new ScheduleService(
            doctorDao, scheduleDao, commissionDao, sessionDao);

    @Test
    public void testInsertAppointmentInvalidSpeciality() throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setSpeciality("чародей");
        appointReq.setDate("20-01-2020");
        appointReq.setTime("09:00");

        Mockito.when(doctorDao.isSpecialityExist("чародей")).thenReturn(false);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleService.insertAppointment(appointReq, "123"));
        assertEquals(DataBaseErrorCode.SPECIALITY_INVALID.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertAppointmentNoAvailableDoctor() throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setSpeciality("терапевт");
        appointReq.setDate("20-01-2020");
        appointReq.setTime("09:00");

        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(new Patient()));
        Mockito.when(doctorDao.getVacantBySpeciality("терапевт",
                LocalDateTime.of(2020, 1, 20,9,0))).thenReturn(Collections.emptyList());

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleService.insertAppointment(appointReq, "123"));
        assertEquals(DataBaseErrorCode.AVAILABLE_DOCTORS_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertAppointmentNoTimeSlot() throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate("20-01-2020");
        appointReq.setTime("09:00");

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2020, 1, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2020, 1, 22),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));

        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(new Patient()));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleService.insertAppointment(appointReq, "123"));
        assertEquals(DataBaseErrorCode.SLOT_TIME_FOR_DOCTOR_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }


    public static Stream<Arguments> paramsSlotPatientInvalid() {
        return Stream.of(
                Arguments.arguments(LocalTime.of(9,50), 20),
                Arguments.arguments(LocalTime.of(9,50), 11),
                Arguments.arguments(LocalTime.of(10,20), 20),
                Arguments.arguments(LocalTime.of(10,29), 20)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsSlotPatientInvalid")
    public void testInsertAppointPatientAppoint(LocalTime time, int duration) throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate("22-01-2020");
        appointReq.setTime("10:00");

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2020, 1, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2020, 1, 22),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));

        Patient patient = new Patient("A","A","A","A","A","A","A","A");
        List<SlotSchedule> slotsPatient = Arrays.asList(
                new SlotSchedule(time, duration),
                new SlotSchedule(LocalTime.of(15,0), 30));

        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(patient));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);
        Mockito.when(scheduleDao.getByPatient(patient, LocalDate.of(2020, 1, 22)))
                .thenReturn(slotsPatient);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleService.insertAppointment(appointReq, "123"));
        assertEquals(DataBaseErrorCode.PATIENT_TIME_BUSY.getErrorCode(), throwable.getMessage());
    }

    @ParameterizedTest
    @MethodSource("paramsSlotPatientInvalid")
    public void testInsertAppointPatientCommission(LocalTime time, int duration) throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate("22-01-2020");
        appointReq.setTime("10:00");

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2020, 1, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2020, 1, 22),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));

        Patient patient = new Patient("A","A","A","A","A","A","A","A");
        List<Commission> commsPatient = Arrays.asList(
                new Commission(null, null, time, null, duration),
                new Commission(null, null, LocalTime.of(13,20), null, 30));

        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(patient));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);
        Mockito.when(commissionDao.getByPatient(patient, LocalDate.of(2020, 1, 22)))
                .thenReturn(commsPatient);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleService.insertAppointment(appointReq, "123"));
        assertEquals(DataBaseErrorCode.PATIENT_TIME_BUSY.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testInsertAppointDoctorOnDate() throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate("20-01-2020");
        appointReq.setTime("12:00");

        Patient patient = new Patient("A","A","A","A","A","A","A","A");

        Doctor doctor = new Doctor("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30),
                new SlotSchedule(new Ticket(patient, "123"), LocalTime.of(13, 0), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2020, 1, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2020, 1, 22),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));

        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(patient));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> scheduleService.insertAppointment(appointReq, "123"));
        assertEquals(DataBaseErrorCode.PATIENT_APPOINT_EXIST.getErrorCode(), throwable.getMessage());
    }

    public static Stream<Arguments> paramsSlotPatient() {
        return Stream.of(
                Arguments.arguments(LocalTime.of(9,30), 30),
                Arguments.arguments(LocalTime.of(10,30), 20)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsSlotPatient")
    public void testInsertAppointWithDoctorId(LocalTime time, int duration) throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate("22-01-2020");
        appointReq.setTime("10:00");

        Doctor doctor = new Doctor(123,"Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor,
                LocalDate.of(2020, 1, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(3,LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor,
                LocalDate.of(2020, 1, 22),
                slotSchedules2);
        doctor.setSchedules(Arrays.asList(schedule1, schedule2));

        Patient patient = new Patient("A","A","A","A","A","A","A","A");
        List<SlotSchedule> slotsPatient = Arrays.asList(
                new SlotSchedule(time, duration),
                new SlotSchedule(LocalTime.of(15,0), 30));

        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(patient));
        Mockito.when(doctorDao.getById(1)).thenReturn(doctor);
        Mockito.when(scheduleDao.getByPatient(patient, LocalDate.of(2020, 1, 22)))
                .thenReturn(slotsPatient);
        SlotSchedule slotAppoint = new SlotSchedule(3, new Ticket(patient, "D123220120201000"),
                LocalTime.of(10,0),30, SlotScheduleState.BUSY);

        AppointDtoResponse appointRespExp = new AppointDtoResponse();
        appointRespExp.setTicket("D123220120201000");
        appointRespExp.setDoctorId(123);
        appointRespExp.setFirstName("Иван");
        appointRespExp.setLastName("Иванов");
        appointRespExp.setPatronymic("Иванович");
        appointRespExp.setSpeciality("терапевт");
        appointRespExp.setRoom("110");
        appointRespExp.setDate("22-01-2020");
        appointRespExp.setTime("10:00");

        assertEquals(appointRespExp, scheduleService.insertAppointment(appointReq, "123"));
        Mockito.verify(scheduleDao).addTicket(slotAppoint);
    }

    @ParameterizedTest
    @MethodSource("paramsSlotPatient")
    public void testInsertAppointWithDoctorSpeciality(LocalTime time, int duration) throws DataBaseException
    {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setSpeciality("терапевт");
        appointReq.setDate("22-01-2020");
        appointReq.setTime("10:00");

        Doctor doctor1 = new Doctor(123,"Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(12, 0), 30),
                new SlotSchedule(LocalTime.of(12, 30), 30));
        Schedule schedule1 = new Schedule(doctor1,
                LocalDate.of(2020, 1, 20),
                slotSchedules1);
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(3,LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule2 = new Schedule(doctor1,
                LocalDate.of(2020, 1, 22),
                slotSchedules2);
        doctor1.setSchedules(Arrays.asList(schedule1, schedule2));

        Doctor doctor2 = new Doctor(456,"Петр", "Петров", "Петрович",
                "петр", "петр123", "терапевт", "111");
        List<SlotSchedule> slotSchedules3 = Arrays.asList(
                new SlotSchedule(4,LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        Schedule schedule3 = new Schedule(doctor2,
                LocalDate.of(2020, 1, 22),
                slotSchedules3);
        doctor2.setSchedules(Arrays.asList(schedule3));

        Patient patient = new Patient("A","A","A","A","A","A","A","A");
        List<SlotSchedule> slotsPatient = Arrays.asList(
                new SlotSchedule(time, duration),
                new SlotSchedule(LocalTime.of(15,0), 30));

        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(patient));
        Mockito.when(doctorDao.isSpecialityExist("терапевт")).thenReturn(true);
        Mockito.when(doctorDao.getVacantBySpeciality("терапевт",
                LocalDateTime.of(2020, 1, 22,10,0)))
                .thenReturn(Arrays.asList(doctor1, doctor2));
        Mockito.when(scheduleDao.getByPatient(patient, LocalDate.of(2020, 1, 22)))
                .thenReturn(slotsPatient);
        SlotSchedule slotAppoint1 = new SlotSchedule(3, new Ticket(patient, "D123220120201000"),
                LocalTime.of(10,0),30, SlotScheduleState.BUSY);
        SlotSchedule slotAppoint2 = new SlotSchedule(4, new Ticket(patient, "D456220120201000"),
                LocalTime.of(10,0),30, SlotScheduleState.BUSY);

        AppointDtoResponse appointRespExp1 = new AppointDtoResponse();
        appointRespExp1.setTicket("D123220120201000");
        appointRespExp1.setDoctorId(123);
        appointRespExp1.setFirstName("Иван");
        appointRespExp1.setLastName("Иванов");
        appointRespExp1.setPatronymic("Иванович");
        appointRespExp1.setSpeciality("терапевт");
        appointRespExp1.setRoom("110");
        appointRespExp1.setDate("22-01-2020");
        appointRespExp1.setTime("10:00");

        AppointDtoResponse appointRespExp2 = new AppointDtoResponse();
        appointRespExp2.setTicket("D456220120201000");
        appointRespExp2.setDoctorId(456);
        appointRespExp2.setFirstName("Петр");
        appointRespExp2.setLastName("Петров");
        appointRespExp2.setPatronymic("Петрович");
        appointRespExp2.setSpeciality("терапевт");
        appointRespExp2.setRoom("111");
        appointRespExp2.setDate("22-01-2020");
        appointRespExp2.setTime("10:00");

        AppointDtoResponse appointRespAct = scheduleService.insertAppointment(appointReq, "123");
        ArgumentCaptor<SlotSchedule> argumentCaptor = ArgumentCaptor.forClass(SlotSchedule.class);
        Mockito.verify(scheduleDao).addTicket(argumentCaptor.capture());
        assertAll(
                () -> assertTrue(appointRespAct.equals(appointRespExp1) || appointRespAct.equals(appointRespExp2)),
                () -> assertTrue(argumentCaptor.getValue().equals(slotAppoint1) || argumentCaptor.getValue().equals(slotAppoint2))
        );
    }
}
