package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dao.*;
import net.thumbtack.school.hospital.dto.LoginDtoRequest;
import net.thumbtack.school.hospital.dto.LoginDtoResponse;
import net.thumbtack.school.hospital.dto.doctor.PatientDto;
import net.thumbtack.school.hospital.dto.doctor.ScheduleDto;
import net.thumbtack.school.hospital.dto.doctor.SlotScheduleDto;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.service.SessionService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.powermock.api.mockito.PowerMockito.mock;

public class TestSessionService {

    private SessionDao sessionDao = mock(SessionDao.class);
    private AdminDao adminDao = mock(AdminDao.class);
    private DoctorDao doctorDao = mock(DoctorDao.class);
    private PatientDao patientDao = mock(PatientDao.class);
    private SessionService sessionService = new SessionService(sessionDao, adminDao, doctorDao, patientDao);

    @Test
    public void testLoginAdmin() throws DataBaseException {
        Admin adminDB = new Admin(2, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Session sessionDB = new Session(1, adminDB, "123");
        Mockito.when(adminDao.get("ivan", "ivan123")).thenReturn(adminDB);
        Mockito.when(sessionDao.insert(Mockito.any(Session.class))).thenReturn(sessionDB);

        LoginDtoResponse loginRespExp = new LoginDtoResponse();
        loginRespExp.setId(2);
        loginRespExp.setFirstName("Иван");
        loginRespExp.setLastName("Иванов");
        loginRespExp.setPatronymic("Иванович");
        loginRespExp.setPosition("Главный администратор");
        loginRespExp.setJavaSessionId("123");
        assertEquals(loginRespExp, sessionService.insert(new LoginDtoRequest("ivan", "ivan123")));
    }

    @Test
    public void testLoginDoctor() throws DataBaseException {
        Doctor doctorDB = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Patient patientDB = new Patient(2, "Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(new Ticket(patientDB, "D01200520190930"),
                        LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<Schedule> schedules = Arrays.asList(
                new Schedule(doctorDB, LocalDate.of(2019, 5, 20), slotSchedules1),
                new Schedule(doctorDB, LocalDate.of(2019, 5, 22), slotSchedules2));
        doctorDB.setSchedules(schedules);

        Session sessionDB = new Session(1, doctorDB, "123");
        Mockito.when(doctorDao.get("ivan", "ivan123")).thenReturn(doctorDB);
        Mockito.when(sessionDao.insert(Mockito.any(Session.class))).thenReturn(sessionDB);

        LoginDtoResponse loginRespExp = new LoginDtoResponse();
        loginRespExp.setId(1);
        loginRespExp.setFirstName("Иван");
        loginRespExp.setLastName("Иванов");
        loginRespExp.setPatronymic("Иванович");
        loginRespExp.setSpeciality("терапевт");
        loginRespExp.setRoom("110");
        List<SlotScheduleDto> slots1Dto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30", new PatientDto(2, "Петр", "Петров", "Петрович",
                        "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        List<SlotScheduleDto> slots2Dto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        loginRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("20-05-2019", slots1Dto),
                new ScheduleDto("22-05-2019", slots2Dto)));
        loginRespExp.setJavaSessionId("123");
        assertEquals(loginRespExp, sessionService.insert(new LoginDtoRequest("ivan", "ivan123")));
    }

    @Test
    public void testLoginPatient() throws DataBaseException {
        Patient patientDB = new Patient(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Session sessionDB = new Session(1, patientDB, "123");
        Mockito.when(patientDao.get("ivan", "ivan123")).thenReturn(patientDB);
        Mockito.when(sessionDao.insert(Mockito.any(Session.class))).thenReturn(sessionDB);

        LoginDtoResponse loginRespExp = new LoginDtoResponse();
        loginRespExp.setId(1);
        loginRespExp.setFirstName("Иван");
        loginRespExp.setLastName("Иванов");
        loginRespExp.setPatronymic("Иванович");
        loginRespExp.setEmail("ivan@mail.ru");
        loginRespExp.setAddress("ул. Строителей, д. 3, кв. 5");
        loginRespExp.setPhone("+79041234567");
        loginRespExp.setJavaSessionId("123");
        assertEquals(loginRespExp, sessionService.insert(new LoginDtoRequest("ivan", "ivan123")));
    }

    @Test
    public void testLoginUserNotExist() throws DataBaseException {
        LoginDtoRequest logReq = new LoginDtoRequest("ivan", "ivan123");
        Mockito.when(adminDao.get("ivan", "ivan123")).thenReturn(null);
        Mockito.when(doctorDao.get("ivan", "ivan123")).thenReturn(null);
        Mockito.when(patientDao.get("ivan", "ivan123")).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class, () -> sessionService.insert(logReq));
        assertEquals(DataBaseErrorCode.LOGIN_PASSWORD_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testCheckAuthorizationOk() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Admin()));
        assertDoesNotThrow(() -> sessionService.checkAuthorization("abc-123"));
        Mockito.verify(sessionDao).getByCookie("abc-123");
    }

    @Test
    public void testCheckAuthorizationThrow() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123"))
                .thenThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST));
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> sessionService.checkAuthorization("abc-123"));
        assertEquals(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testCheckAdminAccess() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Admin()));
        assertDoesNotThrow(() -> sessionService.checkAdminAccess("abc-123"));

        Throwable throwablePatient = assertThrows(DataBaseException.class,
                () -> sessionService.checkPatientAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwablePatient.getMessage());

        Throwable throwableDoctor = assertThrows(DataBaseException.class,
                () -> sessionService.checkDoctorAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwableDoctor.getMessage());
    }

    @Test
    public void testCheckDoctorAccess() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Doctor()));
        assertDoesNotThrow(() -> sessionService.checkDoctorAccess("abc-123"));

        Throwable throwableAdmin = assertThrows(DataBaseException.class,
                () -> sessionService.checkAdminAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwableAdmin.getMessage());

        Throwable throwablePatient = assertThrows(DataBaseException.class,
                () -> sessionService.checkPatientAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwablePatient.getMessage());
    }

    @Test
    public void testCheckPatientAccess() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Patient()));
        assertDoesNotThrow(() -> sessionService.checkPatientAccess("abc-123"));

        Throwable throwableAdmin = assertThrows(DataBaseException.class,
                () -> sessionService.checkAdminAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwableAdmin.getMessage());

        Throwable throwableDoctor = assertThrows(DataBaseException.class,
                () -> sessionService.checkDoctorAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwableDoctor.getMessage());
    }

    @Test
    public void testCheckAdminDoctorAccess() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Admin()));
        assertDoesNotThrow(() -> sessionService.checkDoctorAdminAccess("abc-123"));

        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(new Doctor()));
        assertDoesNotThrow(() -> sessionService.checkDoctorAdminAccess("abc-123"));

        Throwable throwablePatient = assertThrows(DataBaseException.class,
                () -> sessionService.checkPatientAccess("abc-123"));
        assertEquals(DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode(), throwablePatient.getMessage());
    }

    @Test
    public void testGetUserAdmin() throws DataBaseException {
        Admin admin = new Admin(2, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(admin));

        LoginDtoResponse responseExp = new LoginDtoResponse();
        responseExp.setId(2);
        responseExp.setFirstName("Иван");
        responseExp.setLastName("Иванов");
        responseExp.setPatronymic("Иванович");
        responseExp.setPosition("Главный администратор");
        assertEquals(responseExp, sessionService.getUser("abc-123"));
    }

    @Test
    public void testGetUserDoctor() throws DataBaseException {
        Doctor doctor = new Doctor(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "терапевт", "110");
        Patient patient = new Patient(2, "Петр", "Петров", "Петрович",
                "petr", "petr123", "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        List<SlotSchedule> slotSchedules1 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(new Ticket(patient, "D01200520190930"),
                        LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        List<SlotSchedule> slotSchedules2 = Arrays.asList(
                new SlotSchedule(LocalTime.of(9, 0), 30),
                new SlotSchedule(LocalTime.of(9, 30), 30),
                new SlotSchedule(LocalTime.of(10, 0), 30),
                new SlotSchedule(LocalTime.of(10, 30), 30));
        doctor.setSchedules(Arrays.asList(
                new Schedule(doctor, LocalDate.of(2019, 5, 20), slotSchedules1),
                new Schedule(doctor, LocalDate.of(2019, 5, 22), slotSchedules2)));
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(doctor));

        LoginDtoResponse responseExp = new LoginDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName("Иван");
        responseExp.setLastName("Иванов");
        responseExp.setPatronymic("Иванович");
        responseExp.setSpeciality("терапевт");
        responseExp.setRoom("110");
        List<SlotScheduleDto> slots1Dto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30", new PatientDto(2, "Петр", "Петров", "Петрович",
                        "petr@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567")),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        List<SlotScheduleDto> slots2Dto = Arrays.asList(
                new SlotScheduleDto("09:00"),
                new SlotScheduleDto("09:30"),
                new SlotScheduleDto("10:00"),
                new SlotScheduleDto("10:30"));
        responseExp.setSchedule(Arrays.asList(
                new ScheduleDto("20-05-2019", slots1Dto),
                new ScheduleDto("22-05-2019", slots2Dto)));
        LoginDtoResponse respAct = sessionService.getUser("abc-123");
        assertEquals(responseExp, sessionService.getUser("abc-123"));
    }

    @Test
    public void testGetUserPatient() throws DataBaseException {
        Patient patient = new Patient(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));

        LoginDtoResponse responseExp = new LoginDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName("Иван");
        responseExp.setLastName("Иванов");
        responseExp.setPatronymic("Иванович");
        responseExp.setEmail("ivan@mail.ru");
        responseExp.setAddress("ул. Строителей, д. 3, кв. 5");
        responseExp.setPhone("+79041234567");
        assertEquals(responseExp, sessionService.getUser("abc-123"));
    }

    @Test
    public void testGetUserCookieNotExist() throws DataBaseException {
        Mockito.when(sessionDao.getByCookie("abc-123"))
                .thenThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> sessionService.getUser("abc-123"));
        assertEquals(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode(), throwable.getMessage());

    }

    @Test
    public void testDeleteSession() throws DataBaseException {
        Patient patient = new Patient(1, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567");
        Session session = new Session(patient);
        Mockito.when(sessionDao.getByCookie("abc-123")).thenReturn(new Session(patient));

        sessionService.delete("abc-123");
        Mockito.verify(sessionDao).delete(session);
    }

    @Test
    public void testDeleteSessionCookieNotExist() throws DataBaseException {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionDao).getByCookie("abc-123");
        Throwable throwable = assertThrows(DataBaseException.class,
                () -> sessionService.delete("abc-123"));
        assertEquals(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode(), throwable.getMessage());
    }
}
