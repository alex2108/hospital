package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dto.RegAdminDtoRequest;
import net.thumbtack.school.hospital.dto.AdminDtoResponse;
import net.thumbtack.school.hospital.dto.UpdateAdminDtoRequest;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.model.Admin;
import net.thumbtack.school.hospital.model.Session;
import net.thumbtack.school.hospital.service.AdminService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestAdminService extends BaseTestService {
    private AdminService adminService = new AdminService(adminDao, sessionDao);

    @Test
    public void testRegisterAdmin() throws DataBaseException {
        RegAdminDtoRequest regAdminReq = new RegAdminDtoRequest();
        regAdminReq.setFirstName("Иван");
        regAdminReq.setLastName("Иванов");
        regAdminReq.setPatronymic("Иванович");
        regAdminReq.setLogin("ivan");
        regAdminReq.setPassword("ivan123");
        regAdminReq.setPosition("Главный администратор");
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Admin adminDB = new Admin(2, "Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Mockito.when(adminDao.insert(admin)).thenReturn(adminDB);

        AdminDtoResponse regAdminRespExp = new AdminDtoResponse();
        regAdminRespExp.setId(2);
        regAdminRespExp.setFirstName("Иван");
        regAdminRespExp.setLastName("Иванов");
        regAdminRespExp.setPatronymic("Иванович");
        regAdminRespExp.setPosition("Главный администратор");

        assertEquals(regAdminRespExp, adminService.register(regAdminReq));
    }

    @Test
    public void testRegisterAdminLoginExist() throws DataBaseException {
        RegAdminDtoRequest regAdminReq = new RegAdminDtoRequest();
        regAdminReq.setFirstName("Иван");
        regAdminReq.setLastName("Иванов");
        regAdminReq.setPatronymic("Иванович");
        regAdminReq.setLogin("ivan");
        regAdminReq.setPassword("ivan123");
        regAdminReq.setPosition("Главный администратор");
        Admin admin = new Admin("Иван", "Иванов", "Иванович",
                "ivan", "ivan123", "Главный администратор");
        Mockito.when(adminDao.insert(admin)).thenThrow(new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS));

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> adminService.register(regAdminReq));
        assertEquals(DataBaseErrorCode.LOGIN_EXISTS.getErrorCode(), throwable.getMessage());
    }

    @Test
    public void testUpdateAdmin() throws DataBaseException {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPatronymic("Иванович");
        updateReq.setPosition("Ведущий администратор");
        updateReq.setOldPassword("ivan123");
        updateReq.setNewPassword("ivan345");

        Admin adminOld = new Admin();
        adminOld.setId(1);
        adminOld.setLogin("petr");
        Admin adminNew = new Admin(1,"Иван", "Иванов", "Иванович",
                null, "ivan345", "Ведущий администратор");
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(adminOld));
        Mockito.when(adminDao.get("petr", "ivan123")).thenReturn(adminOld);
        Mockito.when(adminDao.update(adminNew)).thenReturn(adminNew);

        AdminDtoResponse regAdminRespExp = new AdminDtoResponse();
        regAdminRespExp.setId(1);
        regAdminRespExp.setFirstName("Иван");
        regAdminRespExp.setLastName("Иванов");
        regAdminRespExp.setPatronymic("Иванович");
        regAdminRespExp.setPosition("Ведущий администратор");

        assertEquals(regAdminRespExp, adminService.update(updateReq, "123"));
    }

    @Test
    public void testUpdateAdminWrongPass() throws DataBaseException {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPatronymic("Иванович");
        updateReq.setPosition("Главный администратор");
        updateReq.setOldPassword("ivan123");
        updateReq.setNewPassword("ivan345");

        Admin admin = new Admin();
        admin.setId(1);
        admin.setLogin("petr");
        Mockito.when(sessionDao.getByCookie("123")).thenReturn(new Session(admin));
        Mockito.when(adminDao.get("petr", "ivan_123")).thenReturn(null);

        Throwable throwable = assertThrows(DataBaseException.class,
                () -> adminService.update(updateReq, "123"));
        assertEquals(DataBaseErrorCode.PASSWORD_WRONG.getErrorCode(), throwable.getMessage());
    }

}
