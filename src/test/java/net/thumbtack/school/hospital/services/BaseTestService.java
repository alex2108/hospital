package net.thumbtack.school.hospital.services;

import net.thumbtack.school.hospital.dao.*;

import static org.powermock.api.mockito.PowerMockito.mock;

public class BaseTestService {

    protected AdminDao adminDao = mock(AdminDao.class);
    protected DoctorDao doctorDao = mock(DoctorDao.class);
    protected PatientDao patientDao = mock(PatientDao.class);
    protected ScheduleDao scheduleDao = mock(ScheduleDao.class);
    protected CommissionDao commissionDao = mock(CommissionDao.class);
    protected TicketDao ticketDao = mock(TicketDao.class);
    protected SessionDao sessionDao = mock(SessionDao.class);

}
