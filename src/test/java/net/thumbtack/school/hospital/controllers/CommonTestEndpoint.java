package net.thumbtack.school.hospital.controllers;


import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.servlet.http.Cookie;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {TicketEndpoint.class, PatientEndpoint.class, DoctorEndpoint.class,
        PatientEndpoint.class, AdminEndpoint.class, SessionEndpoint.class,
        StatisticsEndpoint.class})
public class CommonTestEndpoint extends BaseTestEndpoint {

    public static Stream<Arguments> requestCookieNotExist() {
        return Stream.of(
                Arguments.arguments(get("/api/doctors/1")),
                Arguments.arguments(get("/api/doctors")),
                Arguments.arguments(get("/api/patients/1")),
                Arguments.arguments(get("/api/tickets")),
                Arguments.arguments(get("/api/statistics/patients/1")),
                Arguments.arguments(get("/api/statistics/doctors/1")),
                Arguments.arguments(get("/api/statistics/doctors")),
                Arguments.arguments(delete("/api/tickets/1")),
                Arguments.arguments(delete("/api/commissions/1"))
        );
    }

    @ParameterizedTest
    @MethodSource("requestCookieNotExist")
    public void testCookieNotExist(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(requestBuilder
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> requestCookieMissing() {
        return Stream.of(
                Arguments.arguments(get("/api/account")),
                Arguments.arguments(get("/api/doctors/1")),
                Arguments.arguments(get("/api/doctors")),
                Arguments.arguments(get("/api/patients/1")),
                Arguments.arguments(get("/api/tickets")),
                Arguments.arguments(get("/api/statistics/patients/1")),
                Arguments.arguments(get("/api/statistics/doctors/1")),
                Arguments.arguments(get("/api/statistics/doctors")),
                Arguments.arguments(delete("/api/sessions")),
                Arguments.arguments(delete("/api/tickets/1")),
                Arguments.arguments(delete("/api/commissions/1"))
        );
    }

    @ParameterizedTest
    @MethodSource("requestCookieMissing")
    public void testCookieMissing(MockHttpServletRequestBuilder requestBuilder) throws Exception {
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(), SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramsInvalidURL() {
        return Stream.of(
                Arguments.arguments("/api/doctos/1"),
                Arguments.arguments("/api/doctores"),
                Arguments.arguments("/api/appointments"),
                Arguments.arguments("/api/sessions"),
                Arguments.arguments("/api/statistics/patients")
        );
    }

    @ParameterizedTest
    @MethodSource("paramsInvalidURL")
    public void testError404(String url) throws Exception {
        MvcResult result = mvc.perform(get(url)).andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.URL_INVALID.name(), "URL",
                ValidationErrorCode.URL_INVALID.getErrorCode() + "GET " + url);
        assertAll(
                () -> assertEquals(404, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

}
