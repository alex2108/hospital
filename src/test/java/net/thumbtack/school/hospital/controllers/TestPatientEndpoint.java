package net.thumbtack.school.hospital.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PatientEndpoint.class)
public class TestPatientEndpoint extends BaseTestEndpoint {

    public static Stream<Arguments> parametersRegPatientOk() {
        return Stream.of(
                Arguments.arguments("Иван", "Иванов", "Иванович",
                        "ivanИв123", "ivan3454646",
                        "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567"),
                Arguments.arguments("Магомед-оглы", "Петров", "Иванович",
                        "Петров12", "35*?роk35jl",
                        "magomed-petrov@gmail.com", "ул. Центральная, д. 4а, корп. 1, кв. 4", "8-904-207-45-67"),
                Arguments.arguments("Мария Кристина", "Петрова-Сергеева", null,
                        "petrovaСерг", "ьдвь@%_7**%*&^(",
                        "maria.kristina_petr@yandex.ru", "пр. Мира, д. 4а, корп. 1, кв. 4", "+7-923-2074567")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersRegPatientOk")
    public void testRegisterPatientOk(String firstName, String lastName, String patronymic,
                                      String login, String password,
                                      String email, String address, String phone) throws Exception {
        RegPatientDtoRequest regPatientReg = new RegPatientDtoRequest();
        regPatientReg.setFirstName(firstName);
        regPatientReg.setLastName(lastName);
        regPatientReg.setPatronymic(patronymic);
        regPatientReg.setLogin(login);
        regPatientReg.setPassword(password);
        regPatientReg.setEmail(email);
        regPatientReg.setAddress(address);
        regPatientReg.setPhone(phone);

        PatientDtoResponse responseService = new PatientDtoResponse();
        responseService.setId(1);
        responseService.setFirstName(firstName);
        responseService.setLastName(lastName);
        responseService.setPatronymic(patronymic);
        responseService.setEmail(email);
        responseService.setAddress(address);
        responseService.setPhone(phone);
        responseService.setJavaSessionId("123");
        Mockito.when(patientService.register(regPatientReg)).thenReturn(responseService);

        MvcResult result = mvc.perform(post("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regPatientReg)))
                .andReturn();

        PatientDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                PatientDtoResponse.class);
        PatientDtoResponse responseExp = new PatientDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName(firstName);
        responseExp.setLastName(lastName);
        responseExp.setPatronymic(patronymic);
        responseExp.setEmail(email);
        responseExp.setAddress(address);
        responseExp.setPhone(phone);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseExp, responseAct),
                () -> assertEquals(responseService.getJavaSessionId(),
                        result.getResponse().getCookie(SessionService.getCookieName()).getValue())
        );
    }

    public static Stream<Arguments> parametersInvalidNamesLoginPass() {
        return Stream.of(
                Arguments.arguments(null, null, null, null, null),
                Arguments.arguments("", "", "", "", ""),
                Arguments.arguments("   ", "   ", "  ", "  ", "   "),
                Arguments.arguments("Ivan", "Iванов", "Иванович&", "ghj545**5", ""),
                Arguments.arguments("ИванИванИванИванИванИванИванИванИванИванИванИванИва",
                        "ИвановИвановИвановИвановИвановИвановИвановИвановИва",
                        "ИвановичИвановичИвановичИвановичИвановичИвановичИва",
                        "ghj545клолот3т5ощотлтот46оти6д4гщо3ио5т3ит5ди5ио3ио",
                        "kjho35jo35***5lio%$$%&*$%^%*&&*^T*klgluhuiлвикhuhoi")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersInvalidNamesLoginPass")
    public void testRegisterPatientInvalidNamesLoginPass(String firstName, String lastName, String patronymic,
                                                         String login, String password) throws Exception {
        RegPatientDtoRequest regPatient = new RegPatientDtoRequest();
        regPatient.setFirstName(firstName);
        regPatient.setLastName(lastName);
        regPatient.setPatronymic(patronymic);
        regPatient.setLogin(login);
        regPatient.setPassword(password);
        regPatient.setEmail("ivan@mail.ru");
        regPatient.setAddress("ул. Строителей, д. 3, кв. 5");
        regPatient.setPhone("+79041234567");

        MvcResult result = mvc.perform(post("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regPatient)))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "firstName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "lastName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.LOGIN_INVALID.name(), "login", ValidationErrorCode.LOGIN_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "password", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        if (patronymic != null) {
            errorsExp.add(ValidationErrorCode.PATRONYMIC_INVALID.name(), "patronymic", ValidationErrorCode.PATRONYMIC_INVALID.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> parametersInvalidEmailAddressPhone() {
        return Stream.of(
                Arguments.arguments(null, null, null),
                Arguments.arguments("", "", "", ""),
                Arguments.arguments("  ", "  ", "   ", "  "),
                Arguments.arguments("*///mail.ru", "", "+7-904-123-45-6"),
                Arguments.arguments("ivan@", "", "+7-495-123-45-61"),
                Arguments.arguments("ivan.mail.ru", "", "+1-904-123-45-64"),
                Arguments.arguments("ivan@mail_ru", "", "8-3812-23-45-62"),
                Arguments.arguments("ivan@mailcom.", "", "8 923 223-45-62")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersInvalidEmailAddressPhone")
    public void testRegisterPatientInvalidEmailAddressPhone(
            String email, String address, String phone) throws Exception {
        RegPatientDtoRequest regPatient = new RegPatientDtoRequest();
        regPatient.setFirstName("Иван");
        regPatient.setLastName("Иванов");
        regPatient.setPatronymic("Иванович");
        regPatient.setLogin("ivan");
        regPatient.setPassword("ivan123563");
        regPatient.setEmail(email);
        regPatient.setAddress(address);
        regPatient.setPhone(phone);

        MvcResult result = mvc.perform(post("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regPatient)))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        if (email == null)
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "email", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        else
            errorsExp.add(ValidationErrorCode.EMAIL_INVALID.name(), "email", ValidationErrorCode.EMAIL_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "address", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        errorsExp.add(ValidationErrorCode.PHONE_INVALID.name(), "phone", ValidationErrorCode.PHONE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterPatientDataBaseExc() throws Exception {
        RegPatientDtoRequest regPatient = new RegPatientDtoRequest();
        regPatient.setFirstName("Иван");
        regPatient.setLastName("Иванов");
        regPatient.setPatronymic("Иванович");
        regPatient.setLogin("ivan");
        regPatient.setPassword("ivan123563");
        regPatient.setEmail("ivan@mail.ru");
        regPatient.setAddress("ул. Строителей, д. 3, кв. 5");
        regPatient.setPhone("+79041234567");

        Mockito.when(patientService.register(regPatient))
                .thenThrow(new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS));

        MvcResult result = mvc.perform(post("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regPatient)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.LOGIN_EXISTS.name(),
                DataBaseErrorCode.LOGIN_EXISTS.getField(),
                DataBaseErrorCode.LOGIN_EXISTS.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramsDateTimeInvalid() {
        return Stream.of(
                Arguments.arguments(null, null, ValidationErrorCode.EMPTY_NULL, ValidationErrorCode.EMPTY_NULL),
                Arguments.arguments("", "", ValidationErrorCode.EMPTY_NULL, ValidationErrorCode.EMPTY_NULL),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                        "15-30", ValidationErrorCode.DATE_INVALID, ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                        "15.30", ValidationErrorCode.DATE_INVALID, ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                        "25:30", ValidationErrorCode.DATE_INVALID, ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd MM yyyy")),
                        "15:80", ValidationErrorCode.DATE_INVALID, ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments(LocalDate.now().plusMonths(3).format(formatterDate),
                        "5:20", ValidationErrorCode.DATE_INVALID, ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments("01-03-2020", "1530", ValidationErrorCode.DATE_INVALID, ValidationErrorCode.TIME_FORMAT_INVALID)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsDateTimeInvalid")
    public void testInsertAppointmentInvalidDateTime(String date, String time,
                                                     ValidationErrorCode errorDate, ValidationErrorCode errorTime) throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate(date);
        appointReq.setTime(time);

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(errorDate.name(), "date", errorDate.getErrorCode());
        errorsExp.add(errorTime.name(), "time", errorTime.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramsDoctorInvalid() {
        return Stream.of(
                Arguments.arguments(null, null, ValidationErrorCode.APPOINTMENT_DOCTOR_ABSENT),
                Arguments.arguments(null, "", ValidationErrorCode.APPOINTMENT_DOCTOR_ABSENT),
                Arguments.arguments(1, "терапевт", ValidationErrorCode.APPOINTMENT_DOUBLE_DOCTOR)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsDoctorInvalid")
    public void testInsertAppointmentInvalidDoctor(Integer doctorId, String speciality,
                                                   ValidationErrorCode error) throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(doctorId);
        appointReq.setSpeciality(speciality);
        appointReq.setDate(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        appointReq.setTime("10:00");

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(error.name(), "doctor", error.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramErrorCodesDataBaseExc() {
        return Stream.of(
                Arguments.arguments(DataBaseErrorCode.SPECIALITY_INVALID),
                Arguments.arguments(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST),
                Arguments.arguments(DataBaseErrorCode.PATIENT_TIME_BUSY),
                Arguments.arguments(DataBaseErrorCode.PATIENT_APPOINT_EXIST),
                Arguments.arguments(DataBaseErrorCode.SLOT_TIME_FOR_DOCTOR_NOT_EXIST),
                Arguments.arguments(DataBaseErrorCode.AVAILABLE_DOCTORS_NOT_EXIST),
                Arguments.arguments(DataBaseErrorCode.SLOT_TIME_BUSY)
        );
    }

    @ParameterizedTest
    @MethodSource("paramErrorCodesDataBaseExc")
    public void testInsertAppointmentDataBaseException(DataBaseErrorCode errorCode) throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setTime("10:00");
        appointReq.setDate(LocalDate.now().format(formatterDate));

        Mockito.when(scheduleService.insertAppointment(appointReq, "123"))
                .thenThrow(new DataBaseException(errorCode));

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(errorCode.name(), errorCode.getField(), errorCode.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testInsertAppointmentCookieNotExist() throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setTime("10:00");
        appointReq.setDate(LocalDate.now().format(formatterDate));

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testInsertAppointmentAccessDenied() throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setTime("10:00");
        appointReq.setDate(LocalDate.now().format(formatterDate));

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkPatientAccess("123");

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testInsertAppointmentCookieMissing() throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setTime("10:00");
        appointReq.setDate(LocalDate.now().format(formatterDate));

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testInsertAppointmentOkDoctorId() throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setDoctorId(1);
        appointReq.setDate(LocalDate.now().plusMonths(2).minusDays(1).format(formatterDate));
        appointReq.setTime("10:00");

        AppointDtoResponse appointRespExp = new AppointDtoResponse();
        appointRespExp.setTicket("456");
        appointRespExp.setDoctorId(1);
        appointRespExp.setFirstName("Иван");
        appointRespExp.setLastName("Иванов");
        appointRespExp.setPatronymic("Иванович");
        appointRespExp.setSpeciality("терапевт");
        appointRespExp.setRoom("110");
        appointRespExp.setDate("22-01-2020");
        appointRespExp.setTime("10:00");
        Mockito.when(scheduleService.insertAppointment(appointReq, "123"))
                .thenReturn(appointRespExp);

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();
        AppointDtoResponse respAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                AppointDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(appointRespExp, respAct)
        );
    }

    @Test
    public void testInsertAppointmentOkSpeciality() throws Exception {
        AppointDtoRequest appointReq = new AppointDtoRequest();
        appointReq.setSpeciality("терапевт");
        appointReq.setDate(LocalDate.now().format(formatterDate));
        appointReq.setTime("10:00");

        AppointDtoResponse appointRespExp = new AppointDtoResponse();
        appointRespExp.setTicket("456");
        appointRespExp.setDoctorId(1);
        appointRespExp.setFirstName("Иван");
        appointRespExp.setLastName("Иванов");
        appointRespExp.setPatronymic("Иванович");
        appointRespExp.setSpeciality("терапевт");
        appointRespExp.setRoom("110");
        appointRespExp.setDate("22-01-2020");
        appointRespExp.setTime("10:00");
        Mockito.when(scheduleService.insertAppointment(appointReq, "123"))
                .thenReturn(appointRespExp);

        MvcResult result = mvc.perform(post("/api/tickets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(appointReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();
        AppointDtoResponse respAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                AppointDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(appointRespExp, respAct)
        );
    }

    public static Stream<Arguments> parametersUpdPatientOk() {
        return Stream.of(
                Arguments.arguments("Иван", "Иванов", "Иванович",
                        "ivan3454646", "ivan3454646",
                        "ivan@mail.ru", "ул. Строителей, д. 3, кв. 5", "+79041234567"),
                Arguments.arguments("Магомед-оглы", "Петров", "Иванович",
                        "35*?роk35jl", "35*?роk35jl",
                        "magomed-petrov@gmail.com", "ул. Центральная, д. 4а, корп. 1, кв. 4", "8-904-207-45-67"),
                Arguments.arguments("Мария Кристина", "Петрова-Сергеева", null,
                        "ьдвь@%_7**%*&^(", "knЛООТ?(*(",
                        "maria.kristina_petr@yandex.ru", "пр. Мира, д. 4а, корп. 1, кв. 4", "+7-923-2074567")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersUpdPatientOk")
    public void testUpdatePatientOk(String firstName, String lastName, String patronymic,
                                    String oldPassword, String newPassword, String email, String address, String phone) throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName(firstName);
        updateReq.setLastName(lastName);
        updateReq.setPatronymic(patronymic);
        updateReq.setOldPassword(oldPassword);
        updateReq.setNewPassword(newPassword);
        updateReq.setEmail(email);
        updateReq.setAddress(address);
        updateReq.setPhone(phone);

        PatientDtoResponse responseExp = new PatientDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName(firstName);
        responseExp.setLastName(lastName);
        responseExp.setPatronymic(patronymic);
        responseExp.setEmail(email);
        responseExp.setAddress(address);
        responseExp.setPhone(phone);
        Mockito.when(patientService.update(updateReq, "abc-123")).thenReturn(responseExp);

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        PatientDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                PatientDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseExp, responseAct)
        );
    }

    public static Stream<Arguments> parametersInvalidNamesPass() {
        return Stream.of(
                Arguments.arguments(null, null, null, null, null),
                Arguments.arguments("", "", "", "", ""),
                Arguments.arguments("   ", "   ", "  ", "  ", "   "),
                Arguments.arguments("Ivan", "Iванов", "Иванович&", "ghj545**5", "ghj545**5"),
                Arguments.arguments("ИванИванИванИванИванИванИванИванИванИванИванИванИва",
                        "ИвановИвановИвановИвановИвановИвановИвановИвановИва",
                        "ИвановичИвановичИвановичИвановичИвановичИвановичИва",
                        "ghj545клолот3т5ощотлтот46оти6д4гщо3ио5т3ит5ди5ио3ио",
                        "kjho35jo35***5lio%$$%&*$%^%*&&*^T*klgluhuiлвикhuhoi")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersInvalidNamesPass")
    public void testUpdatePatientInvalidParameters(String firstName, String lastName, String patronymic,
                                                   String oldPassword, String newPassword) throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName(firstName);
        updateReq.setLastName(lastName);
        updateReq.setPatronymic(patronymic);
        updateReq.setOldPassword(oldPassword);
        updateReq.setNewPassword(newPassword);
        updateReq.setEmail("ivan@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 5");
        updateReq.setPhone("8-901-123-45-67");

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "firstName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "lastName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "oldPassword", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "newPassword", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        if (patronymic != null) {
            errorsExp.add(ValidationErrorCode.PATRONYMIC_INVALID.name(), "patronymic", ValidationErrorCode.PATRONYMIC_INVALID.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("parametersInvalidEmailAddressPhone")
    public void testUpdatePatientInvalidParameters(String email, String address, String phone) throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setOldPassword("1234567890");
        updateReq.setNewPassword("1234567890");
        updateReq.setEmail(email);
        updateReq.setAddress(address);
        updateReq.setPhone(phone);

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        if (email == null)
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "email", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        else
            errorsExp.add(ValidationErrorCode.EMAIL_INVALID.name(), "email", ValidationErrorCode.EMAIL_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "address", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        errorsExp.add(ValidationErrorCode.PHONE_INVALID.name(), "phone", ValidationErrorCode.PHONE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdatePatientCookieNotExist() throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setOldPassword("1234567890");
        updateReq.setNewPassword("1234567890");
        updateReq.setEmail("2498@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 1");
        updateReq.setPhone("8-901-123-12-12");

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("abc-123");

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdatePatientAccessDenied() throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setOldPassword("1234567890");
        updateReq.setNewPassword("1234567890");
        updateReq.setEmail("2498@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 1");
        updateReq.setPhone("8-901-123-12-12");

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkPatientAccess("abc-123");

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdatePatientCookieMissing() throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setOldPassword("1234567890");
        updateReq.setNewPassword("1234567890");
        updateReq.setEmail("2498@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 1");
        updateReq.setPhone("8-901-123-12-12");

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdatePatientDataBaseExc() throws Exception {
        UpdatePatientDtoRequest updateReq = new UpdatePatientDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setOldPassword("1234567890");
        updateReq.setNewPassword("1234567890");
        updateReq.setEmail("2498@mail.ru");
        updateReq.setAddress("ул. Центральная, д. 1");
        updateReq.setPhone("8-901-123-12-12");

        Mockito.when(patientService.update(updateReq, "abc-123"))
                .thenThrow(new DataBaseException(DataBaseErrorCode.PASSWORD_WRONG));

        MvcResult result = mvc.perform(put("/api/patients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.PASSWORD_WRONG.name(), DataBaseErrorCode.PASSWORD_WRONG.getField(),
                DataBaseErrorCode.PASSWORD_WRONG.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testGetPatientOk() throws Exception {
        PatientDtoResponse responseService = new PatientDtoResponse();
        responseService.setId(1);
        responseService.setFirstName("Иван");
        responseService.setLastName("Иванов");
        responseService.setPatronymic("Иванович");
        responseService.setEmail("ivan@mail.ru");
        responseService.setAddress("ул. Центральная, д. 25");
        responseService.setPhone("+79011234567");
        Mockito.when(patientService.getPatient(1)).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/patients/1")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        PatientDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                PatientDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramPathVariableIdInvalid")
    public void testGetPatientInvalidId(String patientId) throws Exception {
        MvcResult result = mvc.perform(get("/api/patients/" + patientId)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        if (patientId.equals("")) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "patientId", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "patientId",
                    ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testGetPatientAccessDenied() throws Exception {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkDoctorAdminAccess("abc-123");
        MvcResult result = mvc.perform(get("/api/patients/1")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testGetTickets() throws Exception {
        List<TicketDtoResponse> responseService = new ArrayList<>();
        TicketDtoResponse ticketResp1 = new TicketDtoResponse();
        ticketResp1.setTicket("D2503");
        ticketResp1.setRoom("110");
        ticketResp1.setDate("25-03-2019");
        ticketResp1.setTime("09:30");
        ticketResp1.setDoctorId(1);
        ticketResp1.setFirstName("Иван");
        ticketResp1.setLastName("Иванов");
        ticketResp1.setPatronymic("Иванович");
        ticketResp1.setSpeciality("терапевт");
        responseService.add(ticketResp1);
        TicketDtoResponse ticketResp2 = new TicketDtoResponse();
        ticketResp2.setTicket("D1004");
        ticketResp2.setRoom("111");
        ticketResp2.setDate("10-04-2019");
        ticketResp2.setTime("12:00");
        ticketResp2.setDoctorId(2);
        ticketResp2.setFirstName("Петр");
        ticketResp2.setLastName("Петров");
        ticketResp2.setPatronymic("Иванович");
        ticketResp2.setSpeciality("отоларинголог");
        responseService.add(ticketResp2);
        TicketDtoResponse ticketResp3 = new TicketDtoResponse();
        ticketResp3.setTicket("CD2603");
        ticketResp3.setRoom("110");
        ticketResp3.setDate("26-03-2019");
        ticketResp3.setTime("09:35");
        ticketResp3.setDoctorsCommission(Arrays.asList(
                new TicketDtoResponse.DoctorDto(1, "Иван", "Иванов",
                        "Иванович", "терапевт"),
                new TicketDtoResponse.DoctorDto(2, "Петр", "Петров",
                        "Иванович", "отоларинголог")));
        responseService.add(ticketResp3);
        TicketDtoResponse ticketResp4 = new TicketDtoResponse();
        ticketResp4.setTicket("CD0104");
        ticketResp4.setRoom("112");
        ticketResp4.setDate("01-04-2019");
        ticketResp4.setTime("10:00");
        ticketResp4.setDoctorsCommission(Arrays.asList(
                new TicketDtoResponse.DoctorDto(1, "Иван", "Иванов",
                        "Иванович", "терапевт"),
                new TicketDtoResponse.DoctorDto(3, "Семен", "Сергеев",
                        "Иванович", "отоларинголог")));
        responseService.add(ticketResp4);
        Mockito.when(ticketService.getTickets("abc-123"))
                .thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/tickets")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        List<TicketDtoResponse> ticketsRespAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8), new TypeReference<List<TicketDtoResponse>>(){});
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, ticketsRespAct)
        );
    }

    @Test
    public void testGetTicketsAccessDenied() throws Exception {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkPatientAccess("abc-123");
        MvcResult result = mvc.perform(get("/api/tickets")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testDeleteCommission() throws Exception {
        MvcResult result = mvc.perform(delete("/api/commissions/D123456")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals("{}", result.getResponse().getContentAsString(charsetUTF8))
        );
        Mockito.verify(ticketService).deleteTicketCommission("D123456","abc-123");
    }

    @Test
    public void testDeleteCommissionNullNumber() throws Exception {
        MvcResult result = mvc.perform(delete("/api/commissions/")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "ticketNumber",
                ValidationErrorCode.EMPTY_NULL.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
        Mockito.verify(ticketService, Mockito.never())
                .deleteTicketCommission(Mockito.anyString(),Mockito.anyString());
    }

    @Test
    public void testDeleteCommissionDataBaseExc() throws Exception {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET))
                .when(ticketService).deleteTicketCommission("D123456", "abc-123");
        MvcResult result = mvc.perform(delete("/api/commissions/D123456")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.name(), DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.getField(),
                DataBaseErrorCode.PATIENT_HAS_NOT_TICKET.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

}
