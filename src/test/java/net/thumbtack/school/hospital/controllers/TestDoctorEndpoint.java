package net.thumbtack.school.hospital.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import net.thumbtack.school.hospital.dto.CommissionDtoRequest;
import net.thumbtack.school.hospital.dto.CommissionDtoResponse;
import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = DoctorEndpoint.class)
public class TestDoctorEndpoint extends BaseTestEndpoint {

    public static Stream<Arguments> parameterDate() {
        return Stream.of(
                Arguments.arguments(LocalDate.now().format(formatterDate)),
                Arguments.arguments(LocalDate.now().plusMonths(2).minusDays(1).format(formatterDate))
        );
    }

    @ParameterizedTest
    @MethodSource("parameterDate")
    public void testInsertCommissionOk(String date) throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(2, 2, 5));
        comReq.setDate(date);
        comReq.setTime("12:00");
        comReq.setDuration(20);

        CommissionDtoResponse serviceResp = new CommissionDtoResponse();
        serviceResp.setTicket("123");
        serviceResp.setPatientId(10);
        serviceResp.setDoctorIds(Arrays.asList(1, 2, 5));
        serviceResp.setDate("20-03-2020");
        serviceResp.setTime("12:00");
        serviceResp.setRoom("110");
        serviceResp.setDuration(20);
        Mockito.when(commissionService.insert(comReq, "123"))
                .thenReturn(serviceResp);

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        CommissionDtoResponse respAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                CommissionDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(serviceResp, respAct)
        );
    }

    public static Stream<Arguments> parameterInsComInvalid1() {
        return Stream.of(
                Arguments.arguments(null, null, null, null),
                Arguments.arguments(0, Collections.emptyList(), "", 0),
                Arguments.arguments(-4, Arrays.asList(-2, 1, 0), "   ", -5)
        );
    }

    @ParameterizedTest
    @MethodSource("parameterInsComInvalid1")
    public void testInsertCommissionInvalid(Integer patientId, List<Integer> doctorIds, String room, Integer duration) throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(patientId);
        comReq.setDoctorIds(doctorIds);
        comReq.setRoom(room);
        comReq.setDate(LocalDate.now().format(formatterDate));
        comReq.setTime("12:00");
        comReq.setDuration(duration);

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        if (patientId == null) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "patientId", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "patientId", ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        }

        if (doctorIds == null || doctorIds.isEmpty()) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "doctorIds", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "doctorIds[0]", ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "doctorIds[2]", ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        }

        if (duration == null) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "duration", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "duration", ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        }
        errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "room", ValidationErrorCode.EMPTY_NULL.getErrorCode());

        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> parameterDateTimeInvalid() {
        return Stream.of(
                Arguments.arguments(null, null),
                Arguments.arguments("20-03-2020", "15.30"),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), "12-00"),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), "1200"),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd MM yyyy")), "34:00"),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), "12:60"),
                Arguments.arguments(LocalDate.now().plusMonths(2).plusDays(1).format(formatterDate), "3:15")
        );
    }

    @ParameterizedTest
    @MethodSource("parameterDateTimeInvalid")
    public void testInsertCommissionDateTimeInvalid(String date, String time) throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(1);
        comReq.setDoctorIds(Arrays.asList(1));
        comReq.setRoom("110");
        comReq.setDate(date);
        comReq.setTime(time);
        comReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        if (date == null) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "date", ValidationErrorCode.EMPTY_NULL.getErrorCode());
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "time", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "date", ValidationErrorCode.DATE_INVALID.getErrorCode());
            errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "time", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        }

        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }


    @Test
    public void testInsertCommissionCookieNotExist() throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1));
        comReq.setDate(LocalDate.now().format(formatterDate));
        comReq.setTime("12:00");
        comReq.setDuration(20);

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testInsertCommissionAccessDenied() throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1));
        comReq.setDate(LocalDate.now().format(formatterDate));
        comReq.setTime("12:00");
        comReq.setDuration(20);

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkDoctorAccess("123");

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testInsertCommissionCookieMissing() throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1));
        comReq.setDate(LocalDate.now().format(formatterDate));
        comReq.setTime("12:00");
        comReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }


    public static Stream<Arguments> paramErrorCodesDataBaseExc() {
        return Stream.of(
                Arguments.arguments(DataBaseErrorCode.PATIENT_ID_NOT_EXIST),
                Arguments.arguments(DataBaseErrorCode.ROOM_INVALID),
                Arguments.arguments(DataBaseErrorCode.DOCTOR_IDS_NOT_EXIST),
                Arguments.arguments(DataBaseErrorCode.SLOT_TIME_BUSY)
        );
    }

    @ParameterizedTest
    @MethodSource("paramErrorCodesDataBaseExc")
    public void testInsertCommissionDataBaseException(DataBaseErrorCode errorCode) throws Exception {
        CommissionDtoRequest comReq = new CommissionDtoRequest();
        comReq.setPatientId(10);
        comReq.setRoom("110");
        comReq.setDoctorIds(Arrays.asList(1));
        comReq.setDate(LocalDate.now().format(formatterDate));
        comReq.setTime("12:00");
        comReq.setDuration(20);

        Mockito.when(commissionService.insert(comReq, "123"))
                .thenThrow(new DataBaseException(errorCode));

        MvcResult result = mvc.perform(post("/api/commissions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(comReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(errorCode.name(), errorCode.getField(), errorCode.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("invalidQueryParams")
    public void testGetDoctorParamInvalid(String doctorId, String schedule, String startDate, String endDate) throws Exception {
        MvcResult result = mvc.perform(get("/api/doctors/"+doctorId)
                .param("schedule", schedule)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "id",
                ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "schedule",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "startDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "endDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramsGetDoctorOk() {
        return Stream.of(
                Arguments.arguments("yes", LocalDate.now().plusDays(10).format(formatterDate),
                        LocalDate.now().plusMonths(2).format(formatterDate), 1),
                Arguments.arguments("no", LocalDate.now().format(formatterDate),
                        LocalDate.now().plusDays(1).format(formatterDate), 3)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsGetDoctorOk")
    public void testGetDoctorOk(String schedule, String startDate, String endDate, int doctorId) throws Exception {
        DoctorDtoResponse doctorResponseExp = new DoctorDtoResponse();
        doctorResponseExp.setId(doctorId);
        doctorResponseExp.setFirstName("Иван");
        doctorResponseExp.setLastName("Иванов");
        doctorResponseExp.setPatronymic("Иванович");
        doctorResponseExp.setSpeciality("терапевт");
        doctorResponseExp.setRoom("110");
        List<SlotScheduleDto> daySchedulesDto = Arrays.asList(
                new SlotScheduleDto("09:00"));
        List<ScheduleDto> schedulesDto = Arrays.asList(
                new ScheduleDto("15-01-2020", daySchedulesDto));
        doctorResponseExp.setSchedule(schedulesDto);

        Mockito.when(doctorService.getDoctor(doctorId, "abc-123", schedule.equals("yes"), startDate, endDate))
                .thenReturn(doctorResponseExp);

        MvcResult result = mvc.perform(get("/api/doctors/"+doctorId)
                .param("schedule", schedule)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        DoctorDtoResponse doctorResp = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                DoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(doctorResponseExp, doctorResp)
        );
    }

    @Test
    public void testGetDoctorOkNoParams() throws Exception {
        DoctorDtoResponse doctorResponseExp = new DoctorDtoResponse();
        doctorResponseExp.setId(1);
        doctorResponseExp.setFirstName("Иван");
        doctorResponseExp.setLastName("Иванов");
        doctorResponseExp.setPatronymic("Иванович");
        doctorResponseExp.setSpeciality("терапевт");
        doctorResponseExp.setRoom("110");
        Mockito.when(doctorService.getDoctor(1, "abc-123", false, null, null))
                .thenReturn(doctorResponseExp);

        MvcResult result = mvc.perform(get("/api/doctors/1")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        DoctorDtoResponse doctorResp = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                DoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(doctorResponseExp, doctorResp)
        );
    }

    @ParameterizedTest
    @MethodSource("invalidQueryParams")
    public void testGetDoctorsParamInvalid(String schedule, String startDate, String endDate) throws Exception {
        MvcResult result = mvc.perform(get("/api/doctors/")
                .param("speciality", "ааа")
                .param("schedule", schedule)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "schedule",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "startDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "endDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsGetDoctorOk")
    public void testGetDoctorsOk(String schedule, String startDate, String endDate) throws Exception {
        DoctorDtoResponse doctorResponseExp1 = new DoctorDtoResponse();
        doctorResponseExp1.setId(1);
        doctorResponseExp1.setFirstName("Иван");
        doctorResponseExp1.setLastName("Иванов");
        doctorResponseExp1.setPatronymic("Иванович");
        doctorResponseExp1.setSpeciality("терапевт");
        doctorResponseExp1.setRoom("110");
        List<ScheduleDto> schedulesDto1 = Arrays.asList(
                new ScheduleDto("15-01-2020", Arrays.asList(
                        new SlotScheduleDto("09:00"))));
        doctorResponseExp1.setSchedule(schedulesDto1);

        DoctorDtoResponse doctorResponseExp2 = new DoctorDtoResponse();
        doctorResponseExp2.setId(2);
        doctorResponseExp2.setFirstName("Петр");
        doctorResponseExp2.setLastName("Петров");
        doctorResponseExp2.setPatronymic("Иванович");
        doctorResponseExp2.setSpeciality("терапевт");
        doctorResponseExp2.setRoom("111");
        List<ScheduleDto> schedulesDto2 = Arrays.asList(
                new ScheduleDto("16-01-2020", Arrays.asList(
                        new SlotScheduleDto("12:00"))));
        doctorResponseExp2.setSchedule(schedulesDto2);
        List<DoctorDtoResponse> responseService = new ArrayList<>();
        responseService.add(doctorResponseExp1);
        responseService.add(doctorResponseExp2);

        Mockito.when(doctorService.getDoctors("ааа", "abc-123", schedule.equals("yes"), startDate, endDate))
                .thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/doctors/")
                .param("speciality", "ааа")
                .param("schedule", schedule)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        List<DoctorDtoResponse> doctorResp = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8), new TypeReference<List<DoctorDtoResponse>>(){});

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, doctorResp)
        );
    }

    @Test
    public void testGetDoctorsNoParams() throws Exception {
        DoctorDtoResponse doctorResponseExp1 = new DoctorDtoResponse();
        doctorResponseExp1.setId(1);
        doctorResponseExp1.setFirstName("Иван");
        doctorResponseExp1.setLastName("Иванов");
        doctorResponseExp1.setPatronymic("Иванович");
        doctorResponseExp1.setSpeciality("терапевт");
        doctorResponseExp1.setRoom("110");

        DoctorDtoResponse doctorResponseExp2 = new DoctorDtoResponse();
        doctorResponseExp2.setId(2);
        doctorResponseExp2.setFirstName("Петр");
        doctorResponseExp2.setLastName("Петров");
        doctorResponseExp2.setPatronymic("Иванович");
        doctorResponseExp2.setSpeciality("терапевт");
        doctorResponseExp2.setRoom("111");
        List<DoctorDtoResponse> responseService = new ArrayList<>();
        responseService.add(doctorResponseExp1);
        responseService.add(doctorResponseExp2);

        Mockito.when(doctorService.getDoctors(null, "abc-123", false, null, null))
                .thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/doctors")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        List<DoctorDtoResponse> doctorResp = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8), new TypeReference<List<DoctorDtoResponse>>(){});

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, doctorResp)
        );
    }

}
