package net.thumbtack.school.hospital.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.thumbtack.school.hospital.service.*;
import net.thumbtack.school.hospital.utils.PropertiesUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.stream.Stream;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;

public class BaseTestEndpoint {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper mapper;

    @MockBean
    protected AdminService adminService;

    @MockBean
    protected DoctorService doctorService;

    @MockBean
    protected PatientService patientService;

    @MockBean
    protected ScheduleService scheduleService;

    @MockBean
    protected CommissionService commissionService;

    @MockBean
    protected TicketService ticketService;

    @MockBean
    protected SessionService sessionService;

    @MockBean
    protected SettingsService settingsService;

    @MockBean
    protected StatisticsService statisticsService;

    protected final Charset charsetUTF8 = Charset.forName("UTF-8");

    @BeforeAll()
    public static void setUpProperties() {
        boolean initProperties = PropertiesUtils.initProperties();
        if (!initProperties) {
            throw new RuntimeException("Can't read configuration file, stop");
        }
    }

    public static Stream<Arguments> paramPathVariableIdInvalid() {
        return Stream.of(
                Arguments.arguments(""),
                Arguments.arguments("0"),
                Arguments.arguments("-1")
        );
    }

    public static Stream<Arguments> invalidQueryParams() {
        return Stream.of(
                Arguments.arguments("0", "yess", "15-01-2020", "23-01-2020"),
                Arguments.arguments("-1", "not", LocalDate.now().plusMonths(2).plusDays(1).format(formatterDate),
                        LocalDate.now().plusMonths(3).format(formatterDate))
        );
    }
}
