package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.SettingsDtoResponse;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = SettingsEndpoint.class)
public class TestSettingsEndpoint extends BaseTestEndpoint {

    @Test
    public void testGetSettingsDefault() throws Exception {
        SettingsDtoResponse responseService = new SettingsDtoResponse(50, 10);
        Mockito.when(settingsService.getSettings(null))
                .thenReturn(responseService);
        MvcResult result = mvc.perform(get("/api/settings")).andReturn();
        SettingsDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                SettingsDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @Test
    public void testGetSettingsSomeUser() throws Exception {
        SettingsDtoResponse responseService = new SettingsDtoResponse(50, 10);
        Mockito.when(settingsService.getSettings("abc-123"))
                .thenReturn(responseService);
        MvcResult result = mvc.perform(get("/api/settings")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        SettingsDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                SettingsDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }
    
    @Test
    public void testGetSettingsCookieNotExist() throws Exception {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("abc-123");
        MvcResult result = mvc.perform(get("/api/settings")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(), DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

}
