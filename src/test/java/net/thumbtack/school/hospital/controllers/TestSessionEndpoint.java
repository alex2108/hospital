package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.*;
import net.thumbtack.school.hospital.dto.doctor.SlotScheduleDto;
import net.thumbtack.school.hospital.dto.doctor.ScheduleDto;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = SessionEndpoint.class)
public class TestSessionEndpoint extends BaseTestEndpoint {

    public static Stream<Arguments> paramLoginPassInvalid() {
        return Stream.of(
                Arguments.arguments(null, null),
                Arguments.arguments("", ""),
                Arguments.arguments("  ", "  "),
                Arguments.arguments("uo^(^(#", "thrt5764g"),
                Arguments.arguments("dfvrgerget35gvjhbibhjbysориыкор4525246пкупкувмвкпук",
                        "uo^(^(#dfvrgerget35gv$$#%@$@@@@@@@jhbibhjbysориыкор")
        );
    }

    @ParameterizedTest
    @MethodSource("paramLoginPassInvalid")
    public void testLoginInvalidValidation(String login, String password) throws Exception {
        LoginDtoRequest loginReq = new LoginDtoRequest(login, password);

        MvcResult result = mvc.perform(post("/api/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginReq)))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.LOGIN_INVALID.name(), "login",
                ValidationErrorCode.LOGIN_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "password",
                ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testLoginAdmin() throws Exception {
        LoginDtoRequest loginReq = new LoginDtoRequest("ivan", "ivan12356356");
        LoginDtoResponse loginRespService = new LoginDtoResponse();
        loginRespService.setId(1);
        loginRespService.setFirstName("Иван");
        loginRespService.setLastName("Иванов");
        loginRespService.setPatronymic("Иванович");
        loginRespService.setPosition("Главный администратор");
        loginRespService.setJavaSessionId("123");
        Mockito.when(sessionService.insert(loginReq)).thenReturn(loginRespService);

        MvcResult result = mvc.perform(post("/api/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginReq)))
                .andReturn();

        LoginDtoResponse loginRespAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                LoginDtoResponse.class);
        String javaSessionId = result.getResponse().getCookie(SessionService.getCookieName()).getValue();
        LoginDtoResponse loginRespExp = new LoginDtoResponse();
        loginRespExp.setId(1);
        loginRespExp.setFirstName("Иван");
        loginRespExp.setLastName("Иванов");
        loginRespExp.setPatronymic("Иванович");
        loginRespExp.setPosition("Главный администратор");
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(loginRespExp, loginRespAct),
                () -> assertEquals(loginRespService.getJavaSessionId(), javaSessionId)
        );
    }

    @Test
    public void testLoginDoctor() throws Exception {
        LoginDtoRequest loginReq = new LoginDtoRequest("ivan", "ivan12356356");
        LoginDtoResponse loginRespService = new LoginDtoResponse();
        loginRespService.setFirstName("Иван");
        loginRespService.setLastName("Иванов");
        loginRespService.setPatronymic("Иванович");
        loginRespService.setSpeciality("терапевт");
        loginRespService.setRoom("110");
        loginRespService.setSchedule(Arrays.asList(
                new ScheduleDto("15-01-2020", Arrays.asList(new SlotScheduleDto("09:00")))));
        loginRespService.setJavaSessionId("123");
        Mockito.when(sessionService.insert(loginReq)).thenReturn(loginRespService);

        MvcResult result = mvc.perform(post("/api/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginReq)))
                .andReturn();

        LoginDtoResponse loginRespAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                LoginDtoResponse.class);
        String javaSessionId = result.getResponse().getCookie(SessionService.getCookieName()).getValue();
        LoginDtoResponse loginRespExp = new LoginDtoResponse();
        loginRespExp.setFirstName("Иван");
        loginRespExp.setLastName("Иванов");
        loginRespExp.setPatronymic("Иванович");
        loginRespExp.setSpeciality("терапевт");
        loginRespExp.setRoom("110");
        loginRespExp.setSchedule(Arrays.asList(
                new ScheduleDto("15-01-2020", Arrays.asList(new SlotScheduleDto("09:00")))));

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(loginRespExp, loginRespAct),
                () -> assertEquals(loginRespService.getJavaSessionId(), javaSessionId)
        );
    }

    @Test
    public void testLoginPatient() throws Exception {
        LoginDtoRequest loginReq = new LoginDtoRequest("ivan", "ivan12356356");
        LoginDtoResponse loginRespService = new LoginDtoResponse();
        loginRespService.setId(1);
        loginRespService.setFirstName("Иван");
        loginRespService.setLastName("Иванов");
        loginRespService.setPatronymic("Иванович");
        loginRespService.setEmail("ivan@mail.ru");
        loginRespService.setAddress("ул. Строителей, д. 3, кв. 5");
        loginRespService.setPhone("+79041234567");
        loginRespService.setJavaSessionId("123");
        Mockito.when(sessionService.insert(loginReq)).thenReturn(loginRespService);

        MvcResult result = mvc.perform(post("/api/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginReq)))
                .andReturn();

        LoginDtoResponse loginRespAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                LoginDtoResponse.class);
        String javaSessionId = result.getResponse().getCookie(SessionService.getCookieName()).getValue();
        LoginDtoResponse loginRespExp = new LoginDtoResponse();
        loginRespExp.setId(1);
        loginRespExp.setFirstName("Иван");
        loginRespExp.setLastName("Иванов");
        loginRespExp.setPatronymic("Иванович");
        loginRespExp.setEmail("ivan@mail.ru");
        loginRespExp.setAddress("ул. Строителей, д. 3, кв. 5");
        loginRespExp.setPhone("+79041234567");
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(loginRespExp, loginRespAct),
                () -> assertEquals(loginRespService.getJavaSessionId(), javaSessionId)
        );
    }

    @Test
    public void testLoginPasswordNotExist() throws Exception {
        LoginDtoRequest loginReq = new LoginDtoRequest("ivan", "ivan12356356");
        Mockito.when(sessionService.insert(loginReq))
                .thenThrow(new DataBaseException(DataBaseErrorCode.LOGIN_PASSWORD_NOT_EXIST));

        MvcResult result = mvc.perform(post("/api/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginReq)))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.LOGIN_PASSWORD_NOT_EXIST.name(), DataBaseErrorCode.LOGIN_PASSWORD_NOT_EXIST.getField(),
                DataBaseErrorCode.LOGIN_PASSWORD_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct),
                () -> assertNull(result.getResponse().getCookie(SessionService.getCookieName()))
        );
    }

    @Test
    public void testGetUserOk() throws Exception {
        LoginDtoResponse responseService = new LoginDtoResponse();
        responseService.setId(1);
        responseService.setFirstName("Иван");
        responseService.setLastName("Иванов");
        responseService.setPatronymic("Иванович");
        responseService.setEmail("ivan@mail.ru");
        responseService.setAddress("ул. Строителей, д. 3, кв. 5");
        responseService.setPhone("+79041234567");

        Mockito.when(sessionService.getUser("abc-123")).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/account")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        LoginDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                LoginDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @Test
    public void testGetUserException() throws Exception {
        Mockito.when(sessionService.getUser("abc-123"))
                .thenThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST));

        MvcResult result = mvc.perform(get("/api/account")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(), DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());

        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testLogoutOk() throws Exception {
        MvcResult result = mvc.perform(delete("/api/sessions")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals("{}", result.getResponse().getContentAsString(charsetUTF8))
        );
        Mockito.verify(sessionService).delete("abc-123");
    }

    @Test
    public void testLogoutCookieNotExist() throws Exception {
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).delete("abc-123");
        MvcResult result = mvc.perform(delete("/api/sessions")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(), DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }
}
