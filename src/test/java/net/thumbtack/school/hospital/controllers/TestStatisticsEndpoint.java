package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.StatisticsDoctorDtoResponse;
import net.thumbtack.school.hospital.dto.StatisticsPatientDtoResponse;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = StatisticsEndpoint.class)
public class TestStatisticsEndpoint extends BaseTestEndpoint {

    @ParameterizedTest
    @MethodSource("invalidQueryParams")
    public void testPatientStatisticsInvalidParam(String patientId, String yesNo,
                                      String startDate, String endDate) throws Exception {
        MvcResult result = mvc.perform(get("/api/statistics/patients/"+patientId)
                .param("details", yesNo)
                .param("byDays", yesNo)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "patientId",
                ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "details",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "byDays",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "startDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "endDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramsStatisticsOk() {
        return Stream.of(
                Arguments.arguments("yes", LocalDate.now().plusDays(10).format(formatterDate),
                        LocalDate.now().plusMonths(2).format(formatterDate)),
                Arguments.arguments("no", LocalDate.now().format(formatterDate),
                        LocalDate.now().plusDays(1).format(formatterDate))
        );
    }

    @ParameterizedTest
    @MethodSource("paramsStatisticsOk")
    public void testPatientStatisticsOkWithParams(String yesNo, String startDate, String endDate) throws Exception {
        StatisticsPatientDtoResponse responseService = new StatisticsPatientDtoResponse();
        responseService.setPatientId(1);
        responseService.setFirstName("Иван");
        responseService.setLastName("Иванов");
        responseService.setPatronymic("Иванович");
        responseService.setAddress("ул. Строителей, д. 3, кв. 5");
        responseService.setEmail("ivan@mail.ru");
        responseService.setPhone("+79041234567");
        responseService.setStartDate("25-03-2019");
        responseService.setEndDate("20-04-2019");
        responseService.addPropStatistics("appointment", new StatisticsPatientDtoResponse.Statistics(5, "01:55"));
        responseService.addPropStatistics("commission", new StatisticsPatientDtoResponse.Statistics(2, "01:00"));
        Mockito.when(statisticsService.getPatientStatistics(1, startDate, endDate,
                yesNo.equals("yes"), yesNo.equals("yes"))).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/statistics/patients/1")
                .param("details", yesNo)
                .param("byDays", yesNo)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        StatisticsPatientDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                StatisticsPatientDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @Test
    public void testPatientStatisticsOkNoParams() throws Exception {
        StatisticsPatientDtoResponse responseService = new StatisticsPatientDtoResponse();
        responseService.setPatientId(1);
        responseService.setFirstName("Иван");
        responseService.setLastName("Иванов");
        responseService.setPatronymic("Иванович");
        responseService.setAddress("ул. Строителей, д. 3, кв. 5");
        responseService.setEmail("ivan@mail.ru");
        responseService.setPhone("+79041234567");
        responseService.setStartDate("25-03-2019");
        responseService.setEndDate("20-04-2019");
        responseService.addPropStatistics("total", new StatisticsPatientDtoResponse.Statistics(5, "01:55"));
        Mockito.when(statisticsService.getPatientStatistics(1, null, null,
                false, false)).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/statistics/patients/1")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        StatisticsPatientDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                StatisticsPatientDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @ParameterizedTest
    @MethodSource("invalidQueryParams")
    public void testDoctorStatisticsInvalidParam(String doctorId, String yesNo,
                                                 String startDate, String endDate) throws Exception {
        MvcResult result = mvc.perform(get("/api/statistics/doctors/"+doctorId)
                .param("details", yesNo)
                .param("byDays", yesNo)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "id",
                ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "details",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "byDays",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "startDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "endDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsStatisticsOk")
    public void testDoctorStatisticsOkWithParams(String yesNo, String startDate, String endDate) throws Exception {
        StatisticsDoctorDtoResponse responseService = new StatisticsDoctorDtoResponse();
        responseService.setDoctorId(1);
        responseService.setFirstName("Иван");
        responseService.setLastName("Иванов");
        responseService.setPatronymic("Иванович");
        responseService.setSpeciality("терапевт");
        responseService.setRoom("110");
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail15 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail15.setDate("15-01-2020");
        statDetail15.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(2, "01:30", "01:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail17 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail17.setDate("17-01-2020");
        statDetail17.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(1, "02:00", "00:30"));
        statDetail17.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1, "00:20"));
        Mockito.when(statisticsService.getDoctorStatistics(1, startDate, endDate,
                yesNo.equals("yes"), yesNo.equals("yes"))).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/statistics/doctors/1")
                .param("details", yesNo)
                .param("byDays", yesNo)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        StatisticsDoctorDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                StatisticsDoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @Test
    public void testDoctorStatisticsOkNoParams() throws Exception {
        StatisticsDoctorDtoResponse responseService = new StatisticsDoctorDtoResponse();
        responseService.setDoctorId(1);
        responseService.setFirstName("Иван");
        responseService.setLastName("Иванов");
        responseService.setPatronymic("Иванович");
        responseService.setSpeciality("терапевт");
        responseService.setRoom("110");
        responseService.setStartDate("15-01-2020");
        responseService.setEndDate("30-01-2020");
        responseService.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(5, "05:00", "02:30"));
        Mockito.when(statisticsService.getDoctorStatistics(1, null, null,
                false, false)).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/statistics/doctors/1")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        StatisticsDoctorDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                StatisticsDoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @ParameterizedTest
    @MethodSource("invalidQueryParams")
    public void testAllDoctorsStatisticsInvalidParam(String yesNo, String startDate, String endDate) throws Exception {
        MvcResult result = mvc.perform(get("/api/statistics/doctors")
                .param("details", yesNo)
                .param("byDays", yesNo)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "details",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.FORMAT_INVALID.name(), "byDays",
                ValidationErrorCode.FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "startDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "endDate",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramsStatisticsOk")
    public void testAllDoctorsStatisticsOkWithParams(String yesNo, String startDate, String endDate) throws Exception {
        StatisticsDoctorDtoResponse responseService = new StatisticsDoctorDtoResponse();
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail15 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail15.setDate("15-01-2020");
        statDetail15.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(2, "01:30", "01:00"));
        StatisticsDoctorDtoResponse.StatisticsOnDay statDetail17 = new StatisticsDoctorDtoResponse.StatisticsOnDay();
        statDetail17.setDate("17-01-2020");
        statDetail17.addPropStatistics("appointment", new StatisticsDoctorDtoResponse.Statistics(1, "02:00", "00:30"));
        statDetail17.addPropStatistics("commission", new StatisticsDoctorDtoResponse.Statistics(1, "00:20"));
        responseService.setByDays(Arrays.asList(statDetail15, statDetail17));
        Mockito.when(statisticsService.getAllDoctorsStatistics(startDate, endDate,
                yesNo.equals("yes"), yesNo.equals("yes"))).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/statistics/doctors")
                .param("details", yesNo)
                .param("byDays", yesNo)
                .param("startDate", startDate)
                .param("endDate", endDate)
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        StatisticsDoctorDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                StatisticsDoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }

    @Test
    public void testAllDoctorsStatisticsOkNoParams() throws Exception {
        StatisticsDoctorDtoResponse responseService = new StatisticsDoctorDtoResponse();
        responseService.setStartDate("15-01-2020");
        responseService.setEndDate("30-01-2020");
        responseService.addPropStatistics("total", new StatisticsDoctorDtoResponse.Statistics(5, "05:00", "02:30"));
        Mockito.when(statisticsService.getAllDoctorsStatistics( null, null,
                false, false)).thenReturn(responseService);

        MvcResult result = mvc.perform(get("/api/statistics/doctors")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        StatisticsDoctorDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                StatisticsDoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseService, responseAct)
        );
    }
}
