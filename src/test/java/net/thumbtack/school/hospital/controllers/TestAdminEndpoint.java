package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.dto.AdminDtoResponse;
import net.thumbtack.school.hospital.dto.RegAdminDtoRequest;
import net.thumbtack.school.hospital.dto.UpdateAdminDtoRequest;
import net.thumbtack.school.hospital.dto.doctor.*;
import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import static net.thumbtack.school.hospital.service.CommonService.formatterDate;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.servlet.http.Cookie;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AdminEndpoint.class)
public class TestAdminEndpoint extends BaseTestEndpoint {

    public static Stream<Arguments> parametersRegAdminOk() {
        return Stream.of(
                Arguments.arguments("Иван", "Иванов", "Иванович", "ivanИв123", "ivan3454646", "Главный администратор"),
                Arguments.arguments("Магомед-оглы", "Петров", "Иванович", "Петров12", "35*?роk35jl", "Admin"),
                Arguments.arguments("Мария Кристина", "Петрова-Сергеева", null, "petrovaСерг", "43546_7**^(&^()()&", "Admin123")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersRegAdminOk")
    public void testRegisterAdminOk(String firstName, String lastName, String patronymic,
                                    String login, String password, String position) throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName(firstName);
        regAdmin.setLastName(lastName);
        regAdmin.setPatronymic(patronymic);
        regAdmin.setLogin(login);
        regAdmin.setPassword(password);
        regAdmin.setPosition(position);

        AdminDtoResponse regAdminRespExp = new AdminDtoResponse();
        regAdminRespExp.setId(1);
        regAdminRespExp.setFirstName(firstName);
        regAdminRespExp.setLastName(lastName);
        regAdminRespExp.setPatronymic(patronymic);
        regAdminRespExp.setPosition(position);
        Mockito.when(adminService.register(regAdmin)).thenReturn(regAdminRespExp);

        MvcResult result = mvc.perform(post("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regAdmin))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        AdminDtoResponse regAdminResp = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                AdminDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(regAdminRespExp, regAdminResp)
        );
    }

    public static Stream<Arguments> parametersRegAdminInvalid() {
        return Stream.of(
                Arguments.arguments(null, null, null, null, null, null),
                Arguments.arguments("", "", "", "", "", ""),
                Arguments.arguments("    ", "    ", "    ", "  ", "   ", "  "),
                Arguments.arguments("Ivan", "Iванов", "Иванович&", "ghj545***###", "fghtgh4*3", ""),
                Arguments.arguments("ИванИванИванИванИванИванИванИванИванИванИванИванИва",
                        "ИвановИвановИвановИвановИвановИвановИвановИвановИва",
                        "ИвановичИвановичИвановичИвановичИвановичИвановичИва",
                        "ghj545клолот3т5ощотлтот46оти6д4гщо3ио5т3ит5ди5ио3ио",
                        "kjho35jo35***5lio%$$%&*$%^%*&&*^T*klgluhuiлвикhuhoi",
                        "")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersRegAdminInvalid")
    public void testRegisterAdminInvalidParameters(String firstName, String lastName, String patronymic,
                                                   String login, String password, String position) throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName(firstName);
        regAdmin.setLastName(lastName);
        regAdmin.setPatronymic(patronymic);
        regAdmin.setLogin(login);
        regAdmin.setPassword(password);
        regAdmin.setPosition(position);

        MvcResult result = mvc.perform(post("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regAdmin))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "firstName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "lastName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.LOGIN_INVALID.name(), "login", ValidationErrorCode.LOGIN_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "password", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "position", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        if (patronymic != null) {
            errorsExp.add(ValidationErrorCode.PATRONYMIC_INVALID.name(), "patronymic", ValidationErrorCode.PATRONYMIC_INVALID.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterAdminDataBaseExc() throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("Иванов");
        regAdmin.setPatronymic("Иванович");
        regAdmin.setLogin("ivan");
        regAdmin.setPassword("ivan34524525");
        regAdmin.setPosition("Главный администратор");

        Mockito.when(adminService.register(regAdmin))
                .thenThrow(new DataBaseException(DataBaseErrorCode.LOGIN_EXISTS));

        MvcResult result = mvc.perform(post("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regAdmin))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.LOGIN_EXISTS.name(), DataBaseErrorCode.LOGIN_EXISTS.getField(), DataBaseErrorCode.LOGIN_EXISTS.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterAdminCookieNotExist() throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("Иванов");
        regAdmin.setPatronymic("Иванович");
        regAdmin.setLogin("ivan");
        regAdmin.setPassword("ivan34524525");
        regAdmin.setPosition("Главный администратор");

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(post("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regAdmin))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterAdminAccessDenied() throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("Иванов");
        regAdmin.setPatronymic("Иванович");
        regAdmin.setLogin("ivan");
        regAdmin.setPassword("ivan34524525");
        regAdmin.setPosition("Главный администратор");

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkAdminAccess("123");

        MvcResult result = mvc.perform(post("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regAdmin))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterAdminCookieMissing() throws Exception {
        RegAdminDtoRequest regAdmin = new RegAdminDtoRequest();
        regAdmin.setFirstName("Иван");
        regAdmin.setLastName("Иванов");
        regAdmin.setPatronymic("Иванович");
        regAdmin.setLogin("ivan");
        regAdmin.setPassword("ivan34524525");
        regAdmin.setPosition("Главный администратор");

        MvcResult result = mvc.perform(post("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regAdmin)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }


    public static Stream<Arguments> parametersRegDoctorPersonalInfoOk() {
        return Stream.of(
                Arguments.arguments("Иван", "Иванов", "Иванович", "ivanИв123", "ivan3454646"),
                Arguments.arguments("Магомед-оглы", "Петров", "Иванович", "Петров12", "35*?роk35jl"),
                Arguments.arguments("Мария Кристина", "Петрова-Сергеева", null, "petrovaСерг", "43546_7**^(&^()()&")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersRegDoctorPersonalInfoOk")
    public void testRegisterDoctorOk(String firstName, String lastName, String patronymic,
                                     String login, String password) throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName(firstName);
        regDoctorReq.setLastName(lastName);
        regDoctorReq.setPatronymic(patronymic);
        regDoctorReq.setLogin(login);
        regDoctorReq.setPassword(password);
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        DoctorDtoResponse doctorResponseExp = new DoctorDtoResponse();
        doctorResponseExp.setId(1);
        doctorResponseExp.setFirstName(firstName);
        doctorResponseExp.setLastName(lastName);
        doctorResponseExp.setPatronymic(patronymic);
        doctorResponseExp.setSpeciality("терапевт");
        doctorResponseExp.setRoom("110");
        List<SlotScheduleDto> daySchedulesDto = Arrays.asList(
                new SlotScheduleDto("09:00"));
        List<ScheduleDto> schedulesDto = Arrays.asList(
                new ScheduleDto("15-01-2020", daySchedulesDto));
        doctorResponseExp.setSchedule(schedulesDto);
        Mockito.when(doctorService.register(regDoctorReq))
                .thenReturn(doctorResponseExp);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();
        DoctorDtoResponse doctorResp = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                DoctorDtoResponse.class);

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(doctorResponseExp, doctorResp)
        );

    }

    public static Stream<Arguments> paramRegDoctorInvalidPersonalInfo() {
        return Stream.of(
                Arguments.arguments(null, null, null, null, null),
                Arguments.arguments("", "", "", "", ""),
                Arguments.arguments("   ", "   ", "    ", "    ", "   "),
                Arguments.arguments("Ivan", "Iванов", "Иванович&", "ghj545***###", "njln56&(("),
                Arguments.arguments("ИванИванИванИванИванИванИванИванИванИванИванИванИва",
                        "ИвановИвановИвановИвановИвановИвановИвановИвановИва",
                        "ИвановичИвановичИвановичИвановичИвановичИвановичИва",
                        "ghj545клолот3т5ощотлтот46оти6д4гщо3ио5т3ит5ди5ио3ио",
                        "kjho35jo35***5lio%$$%&*$%^%*&&*^T*klgluhuiлвикhuhoi")
        );
    }

    @ParameterizedTest
    @MethodSource("paramRegDoctorInvalidPersonalInfo")
    public void testRegisterDoctorInvalidPersonalInfo(String firstName, String lastName, String patronymic,
                                                      String login, String password) throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName(firstName);
        regDoctorReq.setLastName(lastName);
        regDoctorReq.setPatronymic(patronymic);
        regDoctorReq.setLogin(login);
        regDoctorReq.setPassword(password);
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "firstName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "lastName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.LOGIN_INVALID.name(), "login", ValidationErrorCode.LOGIN_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "password", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        if (patronymic != null) {
            errorsExp.add(ValidationErrorCode.PATRONYMIC_INVALID.name(), "patronymic", ValidationErrorCode.PATRONYMIC_INVALID.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }


    public static Stream<Arguments> paramErrorCodesDataBaseExc() {
        return Stream.of(
                Arguments.arguments(DataBaseErrorCode.ROOM_INVALID),
                Arguments.arguments(DataBaseErrorCode.ROOM_BUSY),
                Arguments.arguments(DataBaseErrorCode.SPECIALITY_INVALID)
        );
    }

    @ParameterizedTest
    @MethodSource("paramErrorCodesDataBaseExc")
    public void testRegisterDoctorDataBaseException(DataBaseErrorCode errorCode) throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1235232");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110000");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        Mockito.when(doctorService.register(regDoctorReq))
                .thenThrow(new DataBaseException(errorCode));

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(errorCode.name(), errorCode.getField(), errorCode.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramInvalidDatesDuration() {
        return Stream.of(
                Arguments.arguments(null, null, null, ValidationErrorCode.EMPTY_NULL, ValidationErrorCode.EMPTY_NULL),
                Arguments.arguments("", "", 0, ValidationErrorCode.EMPTY_NULL, ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED),
                Arguments.arguments("34nlk", "43nlnk", 0, ValidationErrorCode.DATE_INVALID, ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED),
                Arguments.arguments(
                        LocalDate.now().plusDays(5).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                        LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                        -20,
                        ValidationErrorCode.DATE_INVALID, ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED),
                Arguments.arguments("15-01-2020", "20-01-2020", -20, ValidationErrorCode.DATE_INVALID, ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED),
                Arguments.arguments(
                        LocalDate.now().plusMonths(2).plusDays(1).format(formatterDate),
                        LocalDate.now().plusMonths(3).format(formatterDate),
                        -20,
                        ValidationErrorCode.DATE_INVALID, ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED)
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidDatesDuration")
    public void testRegisterDoctorInvalidDatesDuration(String dateStart, String dateEnd, Integer duration,
                                                       ValidationErrorCode dateError, ValidationErrorCode durationError) throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1231241");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(dateStart);
        regDoctorReq.setDateEnd(dateEnd);
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("15:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(duration);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(dateError.name(), "dateStart", dateError.getErrorCode());
        errorsExp.add(dateError.name(), "dateEnd", dateError.getErrorCode());
        errorsExp.add(durationError.name(), "duration", durationError.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramInvalidTimes() {
        return Stream.of(
                Arguments.arguments(null, null, ValidationErrorCode.EMPTY_NULL),
                Arguments.arguments("", "", ValidationErrorCode.EMPTY_NULL),
                Arguments.arguments("10.00", "15-30", ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments("36:00", "15:78", ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments("12:1", "3:20", ValidationErrorCode.TIME_FORMAT_INVALID),
                Arguments.arguments("sfln4", "gh:12", ValidationErrorCode.TIME_FORMAT_INVALID)
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidTimes")
    public void testRegisterDoctorInvalidTimes(String timeStart, String timeEnd, ValidationErrorCode timeError) throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1231241");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart(timeStart);
        weekSchedule.setTimeEnd(timeEnd);
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(timeError.name(), "weekSchedule.timeStart", timeError.getErrorCode());
        errorsExp.add(timeError.name(), "weekSchedule.timeEnd", timeError.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> paramInvalidWeekOfDay() {
        return Stream.of(
                Arguments.arguments(Arrays.asList("Monday", "Tue")),
                Arguments.arguments(Arrays.asList("MON", "Fri")),
                Arguments.arguments(Arrays.asList("Wed", "Sat"))
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidWeekOfDay")
    public void testRegisterDoctorInvalidWeekDays(List<String> weekDays) throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1231241");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("12:00");
        weekSchedule.setWeekDays(weekDays);
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.name(), "weekSchedule.weekDays", ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterDoctorInvalidWeekDaysSchedule() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1231313");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Tuesday", "15:00", "45:30"));
        weekDaysSchedule.add(new DaySchedule("Sat", "10:00", "12:00"));
        weekDaysSchedule.add(new DaySchedule("Mon", "10:456", "12.00"));
        regDoctorReq.setWeekDaysSchedule(weekDaysSchedule);
        regDoctorReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.name(), "weekDaysSchedule[0].weekDay", ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.name(), "weekDaysSchedule[1].weekDay", ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "weekDaysSchedule[0].timeEnd", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "weekDaysSchedule[2].timeStart", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "weekDaysSchedule[2].timeEnd", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }


    @Test
    public void testRegisterDoctorNullSchedule() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan12313231");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        regDoctorReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.SCHEDULE_ABSENT.name(), "schedule", ValidationErrorCode.SCHEDULE_ABSENT.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterDoctorDoubleSchedule() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1231241");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("12:00");
        regDoctorReq.setWeekSchedule(weekSchedule);
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Mon", "10:00", "12:00"));
        regDoctorReq.setWeekDaysSchedule(weekDaysSchedule);
        regDoctorReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.DOUBLE_SCHEDULE.name(), "schedule", ValidationErrorCode.DOUBLE_SCHEDULE.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterDoctorInvalidChronology() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("ivan");
        regDoctorReq.setPassword("ivan1231241");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().plusDays(20).format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(19).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("12:00");
        weekSchedule.setTimeEnd("10:00");
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(20);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.DATE_CHRONOLOGY_INVALID.name(), "schedule", ValidationErrorCode.DATE_CHRONOLOGY_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_CHRONOLOGY_INVALID.name(), "schedule", ValidationErrorCode.TIME_CHRONOLOGY_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterDoctorCookieNotExist() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("иванннннннннн");
        regDoctorReq.setPassword("длтлтлтлтзлт");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterDoctorAccessDenied() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("иванннннннннн");
        regDoctorReq.setPassword("длтлтлтлтзлт");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);


        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkAdminAccess("123");

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testRegisterDoctorCookieMissing() throws Exception {
        RegDoctorDtoRequest regDoctorReq = new RegDoctorDtoRequest();
        regDoctorReq.setFirstName("Иван");
        regDoctorReq.setLastName("Иванов");
        regDoctorReq.setPatronymic("Иванович");
        regDoctorReq.setLogin("иван");
        regDoctorReq.setPassword("длтлтлтлтзлт");
        regDoctorReq.setSpeciality("терапевт");
        regDoctorReq.setRoom("110");
        regDoctorReq.setDateStart(LocalDate.now().format(formatterDate));
        regDoctorReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        regDoctorReq.setWeekSchedule(weekSchedule);
        regDoctorReq.setDuration(30);

        MvcResult result = mvc.perform(post("/api/doctors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(regDoctorReq)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    public static Stream<Arguments> parametersUpdateAdminOk() {
        return Stream.of(
                Arguments.arguments("Иван", "Иванов", "Иванович", "Главный администратор", "ivan3454646", "Nll676)*^("),
                Arguments.arguments("Магомед-оглы", "Петров", "Иванович", "Admin", "35*?роk35jl", "лзщ:(808NUN"),
                Arguments.arguments("Мария Кристина", "Петрова-Сергеева", null, "Admin123", "43546_7**^(&^()()&", "kjnkОТТЩ78")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersUpdateAdminOk")
    public void testUpdateAdminOk(String firstName, String lastName, String patronymic, String position,
                                  String oldPassword, String newPassword) throws Exception {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName(firstName);
        updateReq.setLastName(lastName);
        updateReq.setPatronymic(patronymic);
        updateReq.setPosition(position);
        updateReq.setOldPassword(oldPassword);
        updateReq.setNewPassword(newPassword);

        AdminDtoResponse responseExp = new AdminDtoResponse();
        responseExp.setId(1);
        responseExp.setFirstName(firstName);
        responseExp.setLastName(lastName);
        responseExp.setPatronymic(patronymic);
        responseExp.setPosition(position);
        Mockito.when(adminService.update(updateReq, "123")).thenReturn(responseExp);

        MvcResult result = mvc.perform(put("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        AdminDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                AdminDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(responseExp, responseAct)
        );
    }

    public static Stream<Arguments> parametersUpdateAdminInvalid() {
        return Stream.of(
                Arguments.arguments(null, null, null, null, null, null),
                Arguments.arguments("", "", "", "", "", ""),
                Arguments.arguments("    ", "    ", "    ", "  ", "   ", "  "),
                Arguments.arguments("Ivan", "Iванов", "Иванович&", "", "g545***##", "fghtgh4*3"),
                Arguments.arguments("ИванИванИванИванИванИванИванИванИванИванИванИванИва",
                        "ИвановИвановИвановИвановИвановИвановИвановИвановИва",
                        "ИвановичИвановичИвановичИвановичИвановичИвановичИва",
                        "",
                        "ghj545клолот3т5о*&*Y)*Nl8u46оти6д4г*Y&(Y*09_+о3ио$^",
                        "kjho35jo35***5lio%$$%&*$%^%*&&*^T*klgluhuiлвикhuhoi")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersUpdateAdminInvalid")
    public void testUpdateAdminInvalidParameters(String firstName, String lastName, String patronymic, String position,
                                                 String oldPassword, String newPassword) throws Exception {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName(firstName);
        updateReq.setLastName(lastName);
        updateReq.setPatronymic(patronymic);
        updateReq.setPosition(position);
        updateReq.setOldPassword(oldPassword);
        updateReq.setNewPassword(newPassword);

        MvcResult result = mvc.perform(put("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);

        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "firstName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.NAME_INVALID.name(), "lastName", ValidationErrorCode.NAME_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "oldPassword", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.PASSWORD_INVALID.name(), "newPassword", ValidationErrorCode.PASSWORD_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "position", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        if (patronymic != null) {
            errorsExp.add(ValidationErrorCode.PATRONYMIC_INVALID.name(), "patronymic", ValidationErrorCode.PATRONYMIC_INVALID.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateAdminCookieNotExist() throws Exception {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPosition("Главный администратор");
        updateReq.setOldPassword("ivan34524525");
        updateReq.setNewPassword("ivan34524525");

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(put("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateAdminAccessDenied() throws Exception {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPosition("Главный администратор");
        updateReq.setOldPassword("ivan34524525");
        updateReq.setNewPassword("ivan34524525");

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkAdminAccess("123");

        MvcResult result = mvc.perform(put("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateAdminCookieMissing() throws Exception {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPosition("Главный администратор");
        updateReq.setOldPassword("ivan34524525");
        updateReq.setNewPassword("ivan34524525");

        MvcResult result = mvc.perform(put("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateAdminDataBaseExc() throws Exception {
        UpdateAdminDtoRequest updateReq = new UpdateAdminDtoRequest();
        updateReq.setFirstName("Иван");
        updateReq.setLastName("Иванов");
        updateReq.setPosition("Главный администратор");
        updateReq.setOldPassword("ivan34524525");
        updateReq.setNewPassword("ivan34524525");

        Mockito.when(adminService.update(updateReq, "123"))
                .thenThrow(new DataBaseException(DataBaseErrorCode.PASSWORD_WRONG));

        MvcResult result = mvc.perform(put("/api/admins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.PASSWORD_WRONG.name(), DataBaseErrorCode.PASSWORD_WRONG.getField(),
                DataBaseErrorCode.PASSWORD_WRONG.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorOk1() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusMonths(2).minusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        DoctorDtoResponse serviceResponse = new DoctorDtoResponse();
        serviceResponse.setId(1);
        serviceResponse.setFirstName("Иван");
        serviceResponse.setLastName("Иванов");
        serviceResponse.setPatronymic("Иванович");
        serviceResponse.setSpeciality("терапевт");
        serviceResponse.setRoom("110");
        List<SlotScheduleDto> daySchedulesDto = Arrays.asList(
                new SlotScheduleDto("09:00"));
        List<ScheduleDto> schedulesDto = Arrays.asList(
                new ScheduleDto("15-01-2020", daySchedulesDto));
        serviceResponse.setSchedule(schedulesDto);
        Mockito.when(doctorService.update(updateReq, 1))
                .thenReturn(serviceResponse);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        DoctorDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                DoctorDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(serviceResponse, responseAct)
        );
    }

    @Test
    public void testUpdateDoctorOk2() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusMonths(2).minusDays(1).format(formatterDate));
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Mon", "10:00", "12:00"));
        updateReq.setWeekDaysSchedule(weekDaysSchedule);
        updateReq.setDuration(20);

        DoctorDtoResponse serviceResponse = new DoctorDtoResponse();
        serviceResponse.setId(1);
        serviceResponse.setFirstName("Иван");
        serviceResponse.setLastName("Иванов");
        serviceResponse.setPatronymic("Иванович");
        serviceResponse.setSpeciality("терапевт");
        serviceResponse.setRoom("110");
        List<SlotScheduleDto> daySchedulesDto = Arrays.asList(
                new SlotScheduleDto("09:00"));
        List<ScheduleDto> schedulesDto = Arrays.asList(
                new ScheduleDto("15-01-2020", daySchedulesDto));
        serviceResponse.setSchedule(schedulesDto);
        Mockito.when(doctorService.update(updateReq, 1))
                .thenReturn(serviceResponse);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        DoctorDtoResponse responseAct = mapper.readValue(
                result.getResponse().getContentAsString(charsetUTF8),
                DoctorDtoResponse.class);
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals(serviceResponse, responseAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidDatesDuration")
    public void testUpdateDoctorInvalidDatesDuration(String dateStart, String dateEnd, Integer duration,
                                                     ValidationErrorCode dateError, ValidationErrorCode durationError) throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(dateStart);
        updateReq.setDateEnd(dateEnd);
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("15:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(duration);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(dateError.name(), "dateStart", dateError.getErrorCode());
        errorsExp.add(dateError.name(), "dateEnd", dateError.getErrorCode());
        errorsExp.add(durationError.name(), "duration", durationError.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidTimes")
    public void testUpdateDoctorInvalidTimes(String timeStart, String timeEnd, ValidationErrorCode timeError) throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart(timeStart);
        weekSchedule.setTimeEnd(timeEnd);
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(timeError.name(), "weekSchedule.timeStart", timeError.getErrorCode());
        errorsExp.add(timeError.name(), "weekSchedule.timeEnd", timeError.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidWeekOfDay")
    public void testUpdateDoctorInvalidWeekDays(List<String> weekDays) throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("12:00");
        weekSchedule.setWeekDays(weekDays);
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.name(), "weekSchedule.weekDays", ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorInvalidWeekDaysSchedule() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Tuesday", "15:00", "45:30"));
        weekDaysSchedule.add(new DaySchedule("Sat", "10:00", "12:00"));
        weekDaysSchedule.add(new DaySchedule("Mon", "10:456", "12.00"));
        updateReq.setWeekDaysSchedule(weekDaysSchedule);
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.name(), "weekDaysSchedule[0].weekDay", ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.name(), "weekDaysSchedule[1].weekDay", ValidationErrorCode.WEEK_DAY_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "weekDaysSchedule[0].timeEnd", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "weekDaysSchedule[2].timeStart", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_FORMAT_INVALID.name(), "weekDaysSchedule[2].timeEnd", ValidationErrorCode.TIME_FORMAT_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorNullSchedule() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.SCHEDULE_ABSENT.name(), "schedule", ValidationErrorCode.SCHEDULE_ABSENT.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorDoubleSchedule() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("12:00");
        updateReq.setWeekSchedule(weekSchedule);
        List<DaySchedule> weekDaysSchedule = new ArrayList<>();
        weekDaysSchedule.add(new DaySchedule("Mon", "10:00", "12:00"));
        updateReq.setWeekDaysSchedule(weekDaysSchedule);
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.DOUBLE_SCHEDULE.name(), "schedule", ValidationErrorCode.DOUBLE_SCHEDULE.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorInvalidChronology() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().plusDays(20).format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusDays(19).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("12:00");
        weekSchedule.setTimeEnd("10:00");
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.DATE_CHRONOLOGY_INVALID.name(), "schedule", ValidationErrorCode.DATE_CHRONOLOGY_INVALID.getErrorCode());
        errorsExp.add(ValidationErrorCode.TIME_CHRONOLOGY_INVALID.name(), "schedule", ValidationErrorCode.TIME_CHRONOLOGY_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @ParameterizedTest
    @MethodSource("paramPathVariableIdInvalid")
    public void testUpdateDoctorInvalidDoctorId(String doctorId) throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("10:00");
        weekSchedule.setTimeEnd("12:00");
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(20);

        MvcResult result = mvc.perform(put("/api/doctors/"+doctorId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        if (doctorId.equals("")) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(),
                    "doctorId", ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(),
                    "doctorId", ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorCookieNotExist() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorAccessDenied() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkAdminAccess("123");

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorCookieMissing() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().plusDays(1).format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        MvcResult result = mvc.perform(put("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq)))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(),
                SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testUpdateDoctorDataBaseException() throws Exception {
        UpdateDoctorDtoRequest updateReq = new UpdateDoctorDtoRequest();
        updateReq.setDateStart(LocalDate.now().format(formatterDate));
        updateReq.setDateEnd(LocalDate.now().format(formatterDate));
        WeekSchedule weekSchedule = new WeekSchedule();
        weekSchedule.setTimeStart("09:00");
        weekSchedule.setTimeEnd("11:00");
        weekSchedule.setWeekDays(Arrays.asList("Mon", "Wed", "Fri"));
        updateReq.setWeekSchedule(weekSchedule);
        updateReq.setDuration(30);

        Mockito.when(doctorService.update(updateReq, 2))
                .thenThrow(new DataBaseException(DataBaseErrorCode.SLOT_TIME_BUSY));

        MvcResult result = mvc.perform(put("/api/doctors/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.SLOT_TIME_BUSY.name(), DataBaseErrorCode.SLOT_TIME_BUSY.getField(),
                DataBaseErrorCode.SLOT_TIME_BUSY.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testDeleteDoctorOk() throws Exception {
        DeleteDoctorDtoRequest deleteReq =
                new DeleteDoctorDtoRequest(LocalDate.now().plusDays(15).format(formatterDate));
        MvcResult result = mvc.perform(delete("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals("{}", result.getResponse().getContentAsString(charsetUTF8))
        );
        Mockito.verify(doctorService).delete(deleteReq, 1);
    }

    public static Stream<Arguments> paramInvalidFiredDate() {
        return Stream.of(
                Arguments.arguments("15-04-2019"),
                Arguments.arguments(LocalDate.now().plusMonths(2).plusDays(1).format(formatterDate)),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))),
                Arguments.arguments(LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
        );
    }

    @ParameterizedTest
    @MethodSource("paramInvalidFiredDate")
    public void testDeleteDoctorInvalidDate(String firedDate) throws Exception {
        DeleteDoctorDtoRequest deleteReq = new DeleteDoctorDtoRequest(firedDate);
        MvcResult result = mvc.perform(delete("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.DATE_INVALID.name(), "date",
                ValidationErrorCode.DATE_INVALID.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
        Mockito.verify(doctorService, Mockito.never()).delete(deleteReq, 1);
    }

    @ParameterizedTest
    @MethodSource("paramPathVariableIdInvalid")
    public void testDeleteDoctorInvalidId(String doctorId) throws Exception {
        DeleteDoctorDtoRequest deleteReq = new DeleteDoctorDtoRequest(LocalDate.now().format(formatterDate));
        MvcResult result = mvc.perform(delete("/api/doctors/"+doctorId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        if (doctorId.equals("")) {
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "doctorId",
                    ValidationErrorCode.EMPTY_NULL.getErrorCode());
        } else {
            errorsExp.add(ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.name(), "doctorId",
                    ValidationErrorCode.NON_POSITIVE_NOT_ALLOWED.getErrorCode());
        }
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
        Mockito.verify(doctorService, Mockito.never()).delete(deleteReq, 1);
    }

    @Test
    public void testDeleteDoctorDataBaseExc() throws Exception {
        DeleteDoctorDtoRequest deleteReq =
                new DeleteDoctorDtoRequest(LocalDate.now().format(formatterDate));
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST))
                .when(doctorService).delete(deleteReq, 1);
        MvcResult result = mvc.perform(delete("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.DOCTOR_ID_NOT_EXIST.name(), DataBaseErrorCode.DOCTOR_ID_NOT_EXIST.getField(),
                DataBaseErrorCode.DOCTOR_ID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }


    @Test
    public void testDeleteDoctorCookieNotExist() throws Exception {
        DeleteDoctorDtoRequest deleteReq =
                new DeleteDoctorDtoRequest(LocalDate.now().format(formatterDate));
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST))
                .when(sessionService).checkAuthorization("123");
        MvcResult result = mvc.perform(delete("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.name(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getField(),
                DataBaseErrorCode.JAVASESSIONID_NOT_EXIST.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testDeleteDoctorAccessDenied() throws Exception {
        DeleteDoctorDtoRequest deleteReq =
                new DeleteDoctorDtoRequest(LocalDate.now().format(formatterDate));
        Mockito.doThrow(new DataBaseException(DataBaseErrorCode.OPERATION_NOT_ALLOWED))
                .when(sessionService).checkAdminAccess("123");
        MvcResult result = mvc.perform(delete("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq))
                .cookie(new Cookie(SessionService.getCookieName(), "123")))
                .andReturn();

        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(DataBaseErrorCode.OPERATION_NOT_ALLOWED.name(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getField(),
                DataBaseErrorCode.OPERATION_NOT_ALLOWED.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }

    @Test
    public void testDeleteDoctorCookieMissing() throws Exception {
        DeleteDoctorDtoRequest deleteReq =
                new DeleteDoctorDtoRequest(LocalDate.now().format(formatterDate));
        MvcResult result = mvc.perform(delete("/api/doctors/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(deleteReq)))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(ValidationErrorCode.COOKIE_MISSING.name(), SessionService.getCookieName(),
                ValidationErrorCode.COOKIE_MISSING.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }
}
