package net.thumbtack.school.hospital.controllers;

import net.thumbtack.school.hospital.errorhandler.ResponseError;
import net.thumbtack.school.hospital.exceptions.DataBaseErrorCode;
import net.thumbtack.school.hospital.exceptions.DataBaseException;
import net.thumbtack.school.hospital.service.SessionService;
import net.thumbtack.school.hospital.validator.ValidationErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TicketEndpoint.class)
public class TestTicketEndpoint extends BaseTestEndpoint {

    @Test
    public void testDeleteAppointment() throws Exception {
        MvcResult result = mvc.perform(delete("/api/tickets/D123456")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        assertAll(
                () -> assertEquals(200, result.getResponse().getStatus()),
                () -> assertEquals("{}", result.getResponse().getContentAsString(charsetUTF8))
        );
        Mockito.verify(ticketService).deleteTicketAppointment("D123456","abc-123");
    }

    @Test
    public void testDeleteAppointmentNullNumber() throws Exception {
        MvcResult result = mvc.perform(delete("/api/tickets/")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
            errorsExp.add(ValidationErrorCode.EMPTY_NULL.name(), "ticketNumber",
                    ValidationErrorCode.EMPTY_NULL.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
        Mockito.verify(ticketService, Mockito.never())
                .deleteTicketAppointment(Mockito.anyString(),Mockito.anyString());
    }

    public static Stream<Arguments> paramDataBaseErrors() {
        return Stream.of(
                Arguments.arguments(DataBaseErrorCode.TICKET_APPOINT_NUMBER_NOT_EXIST),
                Arguments.arguments(DataBaseErrorCode.DOCTOR_HAS_NOT_TICKET),
                Arguments.arguments(DataBaseErrorCode.PATIENT_HAS_NOT_TICKET)
        );
    }

    @ParameterizedTest
    @MethodSource("paramDataBaseErrors")
    public void testDeleteAppointmentDataBaseExc(DataBaseErrorCode errorCode) throws Exception {
        Mockito.doThrow(new DataBaseException(errorCode))
                .when(ticketService).deleteTicketAppointment("D123456", "abc-123");
        MvcResult result = mvc.perform(delete("/api/tickets/D123456")
                .cookie(new Cookie(SessionService.getCookieName(), "abc-123")))
                .andReturn();
        ResponseError errorsAct =
                mapper.readValue(result.getResponse().getContentAsString(charsetUTF8), ResponseError.class);
        ResponseError errorsExp = new ResponseError();
        errorsExp.add(errorCode.name(), errorCode.getField(), errorCode.getErrorCode());
        assertAll(
                () -> assertEquals(400, result.getResponse().getStatus()),
                () -> assertEquals(errorsExp, errorsAct)
        );
    }
}
