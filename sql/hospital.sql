DROP DATABASE IF EXISTS hospital;
CREATE DATABASE hospital;
USE hospital;

CREATE TABLE user (
id INT(11) NOT NULL AUTO_INCREMENT,
firstname varchar(50) NOT NULL,
lastname varchar(50) NOT NULL,
patronymic varchar(50),
login varchar(50) NOT NULL UNIQUE,
password varchar(50) NOT NULL,
primary key (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE admin (
user_id INT(11) NOT NULL,
position varchar(50) NOT NULL,
primary key (user_id),
foreign key (user_id) REFERENCES user(id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE speciality (
id INT(11) NOT NULL AUTO_INCREMENT,
name varchar(50) NOT NULL,
primary key (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE room (
id INT(11) NOT NULL AUTO_INCREMENT,
number varchar(50) NOT NULL,
primary key (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE doctor (
user_id INT(11) NOT NULL,
speciality_id INT(11) NOT NULL,
room_id INT(11) NOT NULL UNIQUE,
fireddate DATE,
primary key (user_id),
foreign key (user_id) REFERENCES user(id) ON DELETE CASCADE,
foreign key (speciality_id) REFERENCES speciality(id),
foreign key (room_id) REFERENCES room(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE patient (
user_id INT(11) NOT NULL,
email varchar(50) NOT NULL,
address varchar(50) NOT NULL,
phone varchar(50) NOT NULL,
primary key (user_id),
foreign key (user_id) REFERENCES user(id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE ticket (
id INT(11) NOT NULL AUTO_INCREMENT,
patient_id INT(11) NOT NULL,
number varchar(50) NOT NULL,
primary key (id),
foreign key (patient_id) REFERENCES patient(user_id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE schedule (
id INT(11) NOT NULL AUTO_INCREMENT,
doctor_id INT(11) NOT NULL,
date DATE NOT NULL,
primary key (id),
foreign key (doctor_id) REFERENCES doctor(user_id) ON DELETE CASCADE,
UNIQUE KEY doctor_schedule_key (doctor_id, date)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE slot_schedule (
id INT(11) NOT NULL AUTO_INCREMENT,
schedule_id INT(11) NOT NULL,
ticket_id INT(11),
time TIME NOT NULL,
duration INT(11) NOT NULL,
state ENUM ("VACANT", "BUSY") NOT NULL,
primary key (id),
foreign key (schedule_id) REFERENCES schedule(id) ON DELETE CASCADE,
foreign key (ticket_id) REFERENCES ticket(id) ON DELETE SET NULL,
UNIQUE KEY ticket_key (schedule_id, ticket_id, time)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE commission (
id INT(11) NOT NULL AUTO_INCREMENT,
room_id INT(11) NOT NULL,
ticket_id INT(11) NOT NULL,
date DATE NOT NULL,
time time NOT NULL,
duration INT(11) NOT NULL,
primary key (id),
foreign key (room_id) REFERENCES room(id),
foreign key (ticket_id) REFERENCES ticket(id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE doctor_commission (
id INT(11) NOT NULL AUTO_INCREMENT,
doctor_id INT(11) NOT NULL,
commission_id INT(11) NOT NULL,
primary key (id),
foreign key (doctor_id) REFERENCES doctor(user_id) ON DELETE CASCADE,
foreign key (commission_id) REFERENCES commission(id) ON DELETE CASCADE,
UNIQUE KEY doctor_comission_key (doctor_id, commission_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE session (
id INT(11) NOT NULL AUTO_INCREMENT,
user_id INT(11) NOT NULL UNIQUE,
javasessionid varchar(50) NOT NULL UNIQUE,
primary key (id),
foreign key (user_id) REFERENCES user(id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO user VALUES(NULL, "admin", "admin", NULL, "administrator", "administrator");
INSERT INTO admin VALUES(1, "admin");

INSERT INTO speciality VALUES(NULL, "терапевт"),
(NULL, "хирург"),
(NULL, "отоларинголог"),
(NULL, "стоматолог"),
(NULL, "эндокринолог"),
(NULL, "кардиолог"),
(NULL, "уролог"),
(NULL, "окулист");

INSERT INTO room VALUES(NULL, "110"),
(NULL, "111"),
(NULL, "111а"),
(NULL, "112"),
(NULL, "201"),
(NULL, "202"),
(NULL, "203"),
(NULL, "203а"),
(NULL, "203б"),
(NULL, "204"),
(NULL, "301"),
(NULL, "301а"),
(NULL, "303"),
(NULL, "302"),
(NULL, "302а"),
(NULL, "302б");